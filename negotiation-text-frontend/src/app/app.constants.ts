import {InjectionToken, Provider} from '@angular/core';

export interface IConstants {
  apiBaseUrl: string;
}

export function NegConstantsFactory(): IConstants {
  return {
    apiBaseUrl: '/api',
  };
}


export const NEG_CONSTANTS = new InjectionToken<IConstants>('NEG_CONSTANTS');

export const ConstantsProvider: Provider = {provide: NEG_CONSTANTS, useValue: NegConstantsFactory()};
