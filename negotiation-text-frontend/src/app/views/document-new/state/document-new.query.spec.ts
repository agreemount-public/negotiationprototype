import { DocumentNewQuery } from './document-new.query';
import { DocumentNewStore } from './document-new.store';

describe('DocumentNewQuery', () => {
  let query: DocumentNewQuery;

  beforeEach(() => {
    query = new DocumentNewQuery(new DocumentNewStore());
  });

  it('should create an instance', () => {
    expect(query).toBeTruthy();
  });

});
