import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { DocumentNewStore, DocumentNewState } from './document-new.store';

@Injectable({ providedIn: 'root' })
export class DocumentNewQuery extends Query<DocumentNewState> {

  constructor(protected store: DocumentNewStore) {
    super(store);
  }

}
