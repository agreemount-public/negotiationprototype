import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DocumentNewService } from './document-new.service';
import { DocumentNewStore } from './document-new.store';
import {RouterTestingModule} from '@angular/router/testing';
import {TestingConstantsProvider} from '../../../app.constants.spec';

describe('DocumentNewService', () => {
  let documentNewService: DocumentNewService;
  let documentNewStore: DocumentNewStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentNewService, DocumentNewStore, TestingConstantsProvider],
      imports: [ HttpClientTestingModule, RouterTestingModule ]
    });

    documentNewService = TestBed.get(DocumentNewService);
    documentNewStore = TestBed.get(DocumentNewStore);
  });

  it('should be created', () => {
    expect(documentNewService).toBeDefined();
  });

});
