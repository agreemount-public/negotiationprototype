import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

export interface DocumentNewState {
   key: string;
}

export function createInitialState(): DocumentNewState {
  return {
    key: ''
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'DocumentNew' })
export class DocumentNewStore extends Store<DocumentNewState> {

  constructor() {
    super(createInitialState());
  }

}

