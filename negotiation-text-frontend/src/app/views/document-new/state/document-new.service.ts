import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DocumentNewStore} from './document-new.store';
import {finalize} from 'rxjs/operators';
import {DocumentStore} from '../../document/state/document/document.store';
import {Router} from '@angular/router';
import {NEG_CONSTANTS, IConstants} from '../../../app.constants';
import {DocumentID} from '../../../state/common.model';

@Injectable({providedIn: 'root'})
export class DocumentNewService {

  constructor(private documentNewStore: DocumentNewStore,
              private http: HttpClient,
              private documentStore: DocumentStore,
              @Inject(NEG_CONSTANTS) private CONSTANTS: IConstants,
              private router: Router) {
  }

  create() {
    this.documentNewStore.setLoading();
    this.http.post<DocumentID>(`${this.CONSTANTS.apiBaseUrl}/document/empty/textual`, {})
      .pipe(
        finalize(() => this.documentNewStore.setLoading(false))
      ).subscribe(newDocument => {
      this.router.navigate(['documents', newDocument], {replaceUrl: true});
    });
  }
}
