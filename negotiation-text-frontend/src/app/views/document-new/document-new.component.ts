import { Component, OnInit } from '@angular/core';
import {DocumentNewService} from './state/document-new.service';
import {DocumentNewQuery} from './state/document-new.query';
import {Observable} from 'rxjs';

@Component({
  selector: 'agm-document-new',
  templateUrl: './document-new.component.html',
  styleUrls: ['./document-new.component.scss']
})
export class DocumentNewComponent implements OnInit {
  public loading$: Observable<boolean>;

  constructor(private documentNewService: DocumentNewService,
              private documentNewQuery: DocumentNewQuery) { }

  ngOnInit() {
    this.loading$ = this.documentNewQuery.selectLoading();
    this.createBlank();
  }

  createBlank() {
    this.documentNewService.create();
  }

  createFromTemplate() {

  }

  createFromFile() {

  }
}
