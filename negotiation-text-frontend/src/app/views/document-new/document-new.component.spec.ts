import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentNewComponent } from './document-new.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {DocumentNewService} from './state/document-new.service';
import {DocumentNewQuery} from './state/document-new.query';
import {TestingConstantsProvider} from '../../app.constants.spec';
import {SharedModule} from '../../shared/shared.module';

describe('DocumentNewComponent', () => {
  let component: DocumentNewComponent;
  let fixture: ComponentFixture<DocumentNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule],
      declarations: [ DocumentNewComponent ],
      providers: [DocumentNewService, DocumentNewQuery, TestingConstantsProvider]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
