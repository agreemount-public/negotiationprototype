import {Component, OnDestroy, OnInit} from '@angular/core';
import {DocumentQuery} from './state/document/document.query';
import {BehaviorSubject, Observable} from 'rxjs';
import {DocumentState} from './state/document/document.store';
import {FormControl} from '@angular/forms';
import {DocumentService} from './state/document/document.service';
import Delta from 'quill-delta';
import {filter, map, takeUntil, tap} from 'rxjs/operators';
import {ActivatedRoute, NavigationStart, Router} from '@angular/router';
import {RoutableComponent} from '../../services/utils/miscellaneous';
import Quill from 'quill';
import {ChangeSelectBlot} from './quill-ext/change-select-blot';
import {AgmAction, AgmDocument, DocumentID} from '../../state/common.model';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {BranchActions} from './state/action/action.model';
import {ActionQuery} from './state/action/action.query';
import {ActionService} from './state/action/action.service';
import {HashMap} from '@datorama/akita';

@Component({
  selector: 'agm-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
})
export class DocumentComponent extends RoutableComponent implements OnInit, OnDestroy {
  public document$: Observable<DocumentState>;

  public loading$: Observable<boolean>;
  public sidebarLoading$ = new BehaviorSubject<boolean>(false);
  public sidebarLoaded$ = new BehaviorSubject<boolean>(false);
  editorControl: FormControl;
  editor: Quill;
  public errorNotFound$: Observable<boolean>;
  public selectedChanges$: Observable<{ active: AgmDocument | null; branches: AgmDocument[] }>;
  public selectedChangesActions$: Observable<HashMap<AgmAction[]>>;
  public actionsLoading$: Observable<boolean>;


  constructor(private documentQuery: DocumentQuery,
              private documentService: DocumentService,
              private actionQuery: ActionQuery,
              private actionService: ActionService,
              route: ActivatedRoute, private router: Router) {
    super(route);
  }

  ngOnInit() {
    Quill.register(ChangeSelectBlot);

    this.router.events.pipe(untilDestroyed(this)).subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.documentService.clear();
      }
    });

    this.extractParams('id').subscribe(id => this.documentService.load(id));

    this.errorNotFound$ = this.documentQuery.errorNotFound$;

    this.selectedChanges$ = this.documentQuery.selectedChanges$;

    this.selectedChangesActions$ = this.documentQuery.selectedChangesActions();
    this.actionsLoading$ = this.actionQuery.selectLoading();

    this.document$ = this.documentQuery.select();
    this.loading$ = this.documentQuery.selectLoading().pipe(tap(loading => setTimeout(() => {
      this.sidebarLoaded$.next(true);
      this.sidebarLoading$.next(false);
    })));

    this.editorControl = new FormControl(new Delta());

    this.document$.pipe(map(state => state.ui.deltaOps),
      filter(deltaOps => this.editorControl == null ||
        (JSON.stringify(deltaOps) !== JSON.stringify((this.editorControl.value as Delta).ops))
      ))
      .subscribe(deltaOps => {
        if (this.editor) {
          this.editor.update('user');
        }
        this.editorControl.setValue(new Delta(deltaOps), {emitEvent: false, emitViewToModelChange: false});
      });

    this.editorControl.valueChanges.pipe(filter(delta => delta != null)).subscribe((delta: Delta) => {
      this.documentService.updateDelta(delta);
    });
  }

  ngOnDestroy() {}

  publish() {
    this.documentService.publish();
  }

  onEditorCreated($event: Quill) {
    this.editor = $event;

  }

  invokeAction($event: { actionId: string; documentId: string }) {
    this.actionService.call($event.actionId);
  }

  selectBranch(docId: DocumentID) {
    this.documentService.updateSelect(docId);
  }

  /**
   * :TODO - this is a very simple apprach, but for now it is good enough - it will need to change when editable selects are introduced
   */
  selectionChanged($event: { editor: any; oldRange: Range | null; range: Range | null; source: string }) {
    if ($event.range !== null) {
      this.documentService.clearActiveDiscussion();
    }
  }
}
