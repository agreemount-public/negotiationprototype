import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DocumentComponent } from './document.component';
import {SharedModule} from '../../shared/shared.module';
import {SidebarComponent} from './sidebar/sidebar.component';
import {ReplayComponent} from './sidebar/replay/replay.component';
import {TimeAgoPipe} from 'time-ago-pipe';
import {ModalComponent} from '../../components/modal/modal.component';
import {ShareModalComponent} from './share-modal/share-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {QuillModule} from 'ngx-quill';
import {RouterTestingModule} from '@angular/router/testing';
import {DocumentNotFoundModule} from '../document-not-found/document-not-found.module';
import {TestingConstantsProvider} from '../../app.constants.spec';
import {TooltipModule} from 'ngx-bootstrap';
import {AvatarModule} from 'ngx-avatar';

describe('DocumentComponent', () => {
  let component: DocumentComponent;
  let fixture: ComponentFixture<DocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        ReactiveFormsModule,
        QuillModule.forRoot(),
        RouterTestingModule,
        DocumentNotFoundModule
      ],
      providers: [
        TestingConstantsProvider
      ],
      declarations: [
        DocumentComponent,
        SidebarComponent,
        ReplayComponent,
        TimeAgoPipe,
        ModalComponent,
        ShareModalComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
