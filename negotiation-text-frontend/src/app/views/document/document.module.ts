import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {DocumentComponent} from './document.component';
import {DocumentQuery} from './state/document/document.query';
import {DocumentService} from './state/document/document.service';
import {DocumentStore} from './state/document/document.store';
import {SidebarComponent} from './sidebar/sidebar.component';
import {ReplayComponent} from './sidebar/replay/replay.component';
import {TimeAgoPipe} from 'time-ago-pipe';
import {QuillModule} from 'ngx-quill';
import {ReactiveFormsModule} from '@angular/forms';
import { ShareModalComponent } from './share-modal/share-modal.component';
import {ModalComponent} from '../../components/modal/modal.component';
import {SharedModule} from '../../shared/shared.module';
import {InjectorModule} from '../../shared/injector.module';
import {DocumentNotFoundModule} from '../document-not-found/document-not-found.module';

@NgModule({
  declarations: [
    DocumentComponent,
    SidebarComponent,
    ReplayComponent,
    TimeAgoPipe,
    ModalComponent,
    ShareModalComponent
  ],
  imports: [
    InjectorModule,
    CommonModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
    SharedModule,
    DocumentNotFoundModule
  ],
  exports: [
    DocumentComponent,
    ShareModalComponent
  ]
})
export class DocumentModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DocumentModule,
      providers: [
        DocumentQuery,
        DocumentService,
        DocumentStore
      ]
    };
  }
}
