import {Inject, Injectable} from '@angular/core';
import {action, ID} from '@datorama/akita';
import {HttpClient, HttpRequest} from '@angular/common/http';
import {ActionStore} from './action.store';
import {BranchActions} from './action.model';
import {ActionID, AgmAction, AgmDocument, DocumentID} from '../../../../state/common.model';
import {delay, filter, finalize, map, tap, withLatestFrom} from 'rxjs/operators';
import {combineLatest, Observable, Subscription} from 'rxjs';
import {IConstants, NEG_CONSTANTS} from '../../../../app.constants';
import {ActionQuery} from './action.query';

@Injectable({providedIn: 'root'})
export class ActionService {
  constructor(private actionStore: ActionStore,
              private actionQuery: ActionQuery,
              private http: HttpClient,
              @Inject(NEG_CONSTANTS) private CONSTANTS: IConstants) {
  }

  @action('getForBranches')
  getForBranches(branchIds: DocumentID[]) {
    // load only ones which has not been loaded before
    branchIds = branchIds.filter(id => !this.actionQuery.hasEntity(id));

    if (branchIds.length === 0) { return; }

    this.actionStore.setLoading(true);
    const sub = combineLatest(...branchIds.map(branchId => this.get(branchId)
      .pipe(map(actions => ({actions, branchId} as BranchActions)))))
      .pipe(
        filter(() => this.actionQuery.getValue().documentId !== null),
        finalize(() => this.actionStore.setLoading(false))
      ).subscribe(branchesActions => this.actionStore.add(branchesActions));
  }

  @action('call')
  call(actionId: ActionID) {
    const documentId = this.actionQuery.getValue().documentId;
    if (documentId === null) { throw Error('trying to call action on the null document, it might be a store issue'); }

    this.actionStore.setLoading(true);
    this.http.post<AgmDocument>(`${this.CONSTANTS.apiBaseUrl}/action/invoke?actionId=${actionId}&documentId=${documentId}`, {})
      .pipe(
        delay(1000),
        finalize(() => this.actionStore.setLoading(false))
      )
      .subscribe(doc => {
        console.log('action invoked, response:', doc);
        // :TODO - reload document, etc
        alert(':TODO action invoked');
      });
  }

  private get(branchId: DocumentID): Observable<AgmAction[]> {
    return this.http.get<AgmAction[]>(`${this.CONSTANTS.apiBaseUrl}/action/available?documentId=${branchId}`);
  }

  setDocumentId(documentId: DocumentID) {
    this.actionStore.setDocumentId(documentId);
  }

  clear() {
    this.actionStore.setDocumentId(null);
    this.actionStore.setLoading(false);
    this.actionStore.set([]);
  }
}
