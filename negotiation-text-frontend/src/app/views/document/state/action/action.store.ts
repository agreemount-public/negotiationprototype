import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import {BranchActions} from './action.model';
import {DocumentID} from '../../../../state/common.model';

export interface ActionState extends EntityState<BranchActions> {
  documentId: DocumentID|null;
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'Action', idKey: 'branchId' })
export class ActionStore extends EntityStore<ActionState, BranchActions> {
  constructor() {
    super({
      documentId: null
    });
  }

  setDocumentId(documentId: DocumentID|null) {
    this.update({documentId});
  }
}

