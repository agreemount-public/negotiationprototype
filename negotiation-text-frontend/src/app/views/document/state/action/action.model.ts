import {ActionID, AgmAction, DocumentID} from '../../../../state/common.model';

export interface BranchActions {
  branchId: DocumentID;
  actions: AgmAction[];
}
