import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ActionStore, ActionState } from './action.store';
import {BranchActions} from './action.model';

@Injectable({
  providedIn: 'root'
})
export class ActionQuery extends QueryEntity<ActionState, BranchActions> {
  constructor(protected store: ActionStore) {
    super(store);
  }
}
