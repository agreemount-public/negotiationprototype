import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActionService } from './action.service';
import { ActionStore } from './action.store';
import {TestingConstantsProvider} from '../../../../app.constants.spec';

describe('ActionService', () => {
  let actionService: ActionService;
  let actionStore: ActionStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActionService, ActionStore, TestingConstantsProvider],
      imports: [ HttpClientTestingModule ]
    });

    actionService = TestBed.get(ActionService);
    actionStore = TestBed.get(ActionStore);
  });

  it('should be created', () => {
    expect(actionService).toBeDefined();
  });

});
