import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {createInitialState, DocumentStore} from './document.store';
import {finalize} from 'rxjs/operators';
import {Router} from '@angular/router';
import {action, HashMap} from '@datorama/akita';
import Delta from 'quill-delta';
import {NEG_CONSTANTS, IConstants} from '../../../../app.constants';
import {AgmAction, AgmDocument, DocumentID, IError} from '../../../../state/common.model';
import {isSelect, Merger} from '../../document-processor/merger';
import {compose} from '../../document-processor/composer';
import {DocumentQuery} from './document.query';
import {ActionService} from '../action/action.service';


@Injectable({providedIn: 'root'})
export class DocumentService {
  constructor(private documentStore: DocumentStore,
              private http: HttpClient,
              private router: Router,
              private documentQuery: DocumentQuery,
              private actionService: ActionService,
              @Inject(NEG_CONSTANTS) private CONSTANTS: IConstants) {

  }

  save() {
    const master = this.documentQuery.getValue().master;

    if (master == null) {
      return;
    }

    this.http.post(`${this.CONSTANTS.apiBaseUrl}/action/invoke?actionId=addBranchInDummyWay&documentId=${master.id}`, {
      metrics: {
        changeJSON: JSON.stringify(compose(new Delta(this.documentQuery.getValue().ui.deltaOps)).ops)
      }
    } as Partial<AgmDocument>);
  }

  @action('load')
  load(id: DocumentID) {
    if (this.documentQuery.getValue().id === id) {
      return;
    }

    this.documentStore.setLoading(true);
    this.documentStore.setError(null);

    this.http.get<AgmDocument[]>(`${this.CONSTANTS.apiBaseUrl}/query/documents`, {params: {documentId: id, queryId: 'myBranches'}})
      .pipe(
        finalize(() => this.documentStore.setLoading(false))
      ).subscribe(data => {
        const mainBranchIndex = data.findIndex(doc => doc.states.branch === 'main');
        const mainBranch = data[mainBranchIndex];

        const branches = data.reduce((map: HashMap<AgmDocument>, doc: AgmDocument) => {
          map[doc.id] = doc;
          return map;
        }, {});

        const merger = new Merger();
        merger.setState(branches);

        this.actionService.setDocumentId(id);
        this.documentStore.update(state => ({
          ...state,
          id,
          master: mainBranch,
          branches,
          ui: {
            deltaOps: merger.getComposed().ops,
            activeBranchesIDs: [],
            selectedActiveBranchID: null
          }
        }));
      }, (error: IError) => {
        // :todo THIS IS HACK - engine does not properly handle error codes, so for now assuem all errors are 404
        this.documentStore.setError(error);
      }
    );
  }

  @action('share')
  share(emails: string[]) {
    this.router.navigate([{outlets: {modal: null}}]);
  }

  @action('updateDelta')
  updateDelta(delta: Delta) {
    this.documentStore.update(state => ({...state, ui: {...state.ui, deltaOps: delta.ops}}));
  }

  @action('publish')
  publish() {
    alert(JSON.stringify(compose(new Delta(this.documentQuery.getValue().ui.deltaOps)).ops));
  }

  @action('updateSelect')
  updateSelect(branch: DocumentID) {
    this.documentStore.update(store => {
      return {
        ...store,
        ui: {
          ...store.ui,
          selectedActiveBranchID: branch,
          deltaOps: store.ui.deltaOps.map(op => {
            if (isSelect(op.insert)) {
              return {...op, insert: {select: {options: op.insert.select.options, value: branch}}};
            }
            return op;
          })
        }
      };
    });
  }

  @action('setActiveDiscussion')
  setActiveDiscussion(active: DocumentID, options: DocumentID[]) {
    const master: AgmDocument|null = this.documentQuery.getValue().master;
    if (master === null) { throw Error('no master branch set for document'); }

    this.actionService.getForBranches(options.filter(branchId => branchId !== master.id));

    this.documentStore.update(state => ({
      ...state,
      ui: {
        ...state.ui,
        activeBranchesIDs: options, selectedActiveBranchID: active
      }
    }));
  }

  @action('clearActiveDiscussion')
  clearActiveDiscussion() {
    this.documentStore.update(state => ({
      ...state,
      ui: {
        ...state.ui,
        activeBranchesIDs: [],
        selectedActiveBranchID: null
      }
    }));
  }

  @action('clear')
  clear() {
    this.documentStore.update(state => createInitialState());
    this.actionService.clear();
  }
}
