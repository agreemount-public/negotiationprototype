import {Injectable} from '@angular/core';
import {HashMap, Store, StoreConfig} from '@datorama/akita';
import {DeltaOperation} from 'quill';
import {AgmAction, AgmDocument, DocumentID} from '../../../../state/common.model';

export interface DocumentState {
  ui: {
    // State of the quill editor - mostly write only
    deltaOps: DeltaOperation[];
    activeBranchesIDs: DocumentID[];
    selectedActiveBranchID: DocumentID|null;
  };
  master: AgmDocument|null;
  branches: HashMap<AgmDocument>;
  id: string | null;
  sharedWith: string[];
}

export function createInitialState(): DocumentState {
  return {
    id: null,
    sharedWith: [],
    ui: {
      deltaOps: [],
      activeBranchesIDs: [],
      selectedActiveBranchID: null
    },
    master: null,
    branches: {},
  };
}

@Injectable({providedIn: 'root'})
@StoreConfig({name: 'Document'})
export class DocumentStore extends Store<DocumentState> {

  constructor() {
    super(createInitialState());
  }

}

