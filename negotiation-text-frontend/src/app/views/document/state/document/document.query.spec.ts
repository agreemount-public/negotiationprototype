import {DocumentQuery} from './document.query';
import {DocumentStore} from './document.store';
import {TestBed} from '@angular/core/testing';
import {TestingConstantsProvider} from '../../../../app.constants.spec';
import {ActionStore} from '../action/action.store';
import {ActionQuery} from '../action/action.query';
import {take, toArray} from 'rxjs/operators';


describe('DocumentQuery', () => {
  let query: DocumentQuery;
  let documentStore: DocumentStore;
  let actionStore: ActionStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentStore, DocumentQuery, ActionStore, ActionQuery, TestingConstantsProvider],
      imports: []
    });

    actionStore = TestBed.get(ActionStore);
    documentStore = TestBed.get(DocumentStore);
    query = TestBed.get(DocumentQuery);
  });

  it('should create an instance', () => {
    expect(query).toBeTruthy();
  });

  describe('selectedChangesActions', () => {
    it('should provide initial value', done => {
      query.selectedChangesActions().pipe(take(1)).subscribe(actionsMap => {
        expect(actionsMap).toEqual({});
        done();
      });
    });

    it('should change values when actions load and when selectedBranches change', done => {
      const branch1Actions = [{id: 'action_1', label: 'Action1'}];

      query.selectedChangesActions().pipe(take(3), toArray()).subscribe(actionsMapHashes => {
        expect(actionsMapHashes).toEqual([{},
          {
            1: branch1Actions,
            2: [],
            3: []
          }, {}]);
        done();
      });

      actionStore.setLoading(true);
      documentStore.update(state => ({
        ...state, ui: {
          ...state.ui,
          activeBranchesIDs: ['1', '2', '3'], selectedActiveBranchID: '1'
        },
        id: 'DOC_ID'
      }));

      actionStore.setDocumentId('DOC_ID');
      actionStore.set([
        {branchId: '1', actions: branch1Actions},
        {branchId: '2', actions: []},
        {branchId: '3', actions: []}
      ]);

      actionStore.setLoading(false);

      documentStore.update(state => ({
        ...state,
        ui: {
          ...state.ui,
          activeBranchesIDs: [],
          selectedActiveBranchID: null
        }
      }));
    });
  });
});
