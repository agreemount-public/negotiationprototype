import {Injectable} from '@angular/core';
import {HashMap, Query} from '@datorama/akita';
import {DocumentStore, DocumentState} from './document.store';
import {distinctUntilChanged, filter, map, mergeMap, startWith} from 'rxjs/operators';
import {AgmAction, AgmDocument, DocumentID, ErrorType} from '../../../../state/common.model';
import {combineLatest, concat, Observable, of} from 'rxjs';
import {ActionQuery} from '../action/action.query';
import {sortByCreatedAt} from '../../../../services/utils/miscellaneous';

@Injectable({providedIn: 'root'})
export class DocumentQuery extends Query<DocumentState> {
  readonly errorNotFound$: Observable<boolean> = this.selectError().pipe(map(error => error != null && error.type === ErrorType.NotFound));
  readonly selectedChanges$: Observable<{ active: AgmDocument | null, branches: AgmDocument[] }> = this.select(state =>
    ({
      active: state.ui.selectedActiveBranchID === null ? null : state.branches[state.ui.selectedActiveBranchID],
      branches: state.ui.activeBranchesIDs.map(id => state.branches[id]).sort(sortByCreatedAt)
    })
  );

  selectedChangesActions(): Observable<HashMap<AgmAction[]>> {
    return concat(of({}),
      (combineLatest(this.select(state => state.ui.activeBranchesIDs), this.actionQuery.selectLoading())
        .pipe(
          distinctUntilChanged(),
          filter(combined => combined != null && combined[1] === false),
          map(combined => combined[0]),
          map(branchesIds => branchesIds.reduce((acc, id) => {
            const entity = this.actionQuery.getEntity(id);
            acc[id] = entity ? entity.actions : [];
            return acc;
          }, {})))));
  }

  constructor(protected store: DocumentStore, private actionQuery: ActionQuery) {
    super(store);
  }

}
