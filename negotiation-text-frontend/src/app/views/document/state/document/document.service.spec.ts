import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DocumentService } from './document.service';
import { DocumentStore } from './document.store';
import {RouterTestingModule} from '@angular/router/testing';
import {TestingConstantsProvider} from '../../../../app.constants.spec';

describe('DocumentService', () => {
  let documentService: DocumentService;
  let documentStore: DocumentStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentService, DocumentStore, TestingConstantsProvider],
      imports: [ HttpClientTestingModule, RouterTestingModule ]
    });

    documentService = TestBed.get(DocumentService);
    documentStore = TestBed.get(DocumentStore);
  });

  it('should be created', () => {
    expect(documentService).toBeDefined();
  });

});
