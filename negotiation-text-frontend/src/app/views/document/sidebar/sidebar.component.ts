import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {AgmAction, AgmDocument, DocumentID} from '../../../state/common.model';
import {BranchActions} from '../state/action/action.model';
import {HashMap} from '@datorama/akita';


@Component({
  selector: 'agm-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  @Input() branches: AgmDocument[] = [];
  @Input() selectedBranch: AgmDocument;
  @Input() loading = true;
  @Input() actions: HashMap<AgmAction[]> = {};
  @Input() actionsLoading = true;

  @Output() selectBranch = new EventEmitter<DocumentID>();
  @Output() invokeAction = new EventEmitter<{ actionId: string, documentId: string }>();
}
