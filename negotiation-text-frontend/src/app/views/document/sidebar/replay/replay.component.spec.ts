import {DebugElement} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ReplayComponent} from './replay.component';
import {TimeAgoPipe} from 'time-ago-pipe';
import {By} from '@angular/platform-browser';
import {SharedModule} from '../../../../shared/shared.module';
import {agmDocumentFactory} from '../../../../app.factories.spec';

describe('ReplayComponent', () => {
  let fixture: ComponentFixture<ReplayComponent>;
  let component: ReplayComponent;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ReplayComponent, TimeAgoPipe],
        providers: [],
        imports: [SharedModule]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ReplayComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;

        component.selected = false;
        component.replay = agmDocumentFactory.build();
      });
  }));

  it('should have .card__scope--active class if selected', () => {
    component.selected = true;
    fixture.detectChanges();
    expect(debugElement.queryAll(By.css('.card__scope.card__scope--active')).length).toBe(1);
  });
});
