import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {ActionID, AgmAction, AgmDocument, DocumentID} from '../../../../state/common.model';
import {DeltaOperation} from 'quill';

// export interface IMessage {
//   id: string;
//   type: string;
//   side: string;
//   content: string;
//   actions: string[];
//   timestamp?: Date;
//   uuid: string;
// }

@Component({
  selector: 'agm-replay',
  templateUrl: './replay.component.html',
  styleUrls: ['./replay.component.scss']
})
export class ReplayComponent {
  public badgeTitle = '';
  public createdAt: Date;
  public type: 'draft' | 'offer' | 'original-text';
  public renderedText = '';

  private _replay: AgmDocument;

  @Input() selected = false;
  @Input() actions: AgmAction[] = [];
  @Input() actionsLoading = true;

  @Output() action = new EventEmitter<ActionID>();
  @Output() select = new EventEmitter<DocumentID>();

  @Input() set replay(value: AgmDocument) {
    this._replay = value;
    if (this._replay == null) {
      return;
    }

    this.createdAt = new Date(this._replay.createdAt);

    this.type = 'draft';

    if (this._replay.states.changeState === 'draft') {
      this.type = 'draft';
    }

    if (this._replay.states.branch === 'main') {
      this.type = 'original-text';
    }

    this.renderedText = this.getText(JSON.parse(this._replay.metrics.changeJSON));

    this.badgeTitle = this.type === 'draft' ? 'D' : '';
    this.badgeTitle += this._replay.id.charAt(0);
    // if (this._replay.id != null) {
    //     this.badgeTitle += this._replay.id;
    // }
  }

  get replay(): AgmDocument {
    return this._replay;
  }

  onSelect() {
    if (!this.selected) {
      this.select.emit(this.replay.id);
    }
  }

  private getText(ops: DeltaOperation[]) {
    return ops
      .filter(op => op.delete === undefined)
      .map(op => op.retain !== undefined ? {insert: ' [...] '} : op)
      .filter(op => typeof op.insert === 'string')
      .map(op => op.insert)
      .join('');
  }
}
