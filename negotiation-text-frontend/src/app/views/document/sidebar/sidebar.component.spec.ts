import {DebugElement} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {SidebarComponent} from './sidebar.component';
import {ReplayComponent} from './replay/replay.component';
import {TimeAgoPipe} from 'time-ago-pipe';
import {By} from '@angular/platform-browser';
import {SharedModule} from '../../../shared/shared.module';
import {agmDocumentFactory} from '../../../app.factories.spec';
import {TooltipModule} from 'ngx-bootstrap';
import {AvatarModule} from 'ngx-avatar';

describe('SidebarComponent', () => {
    let fixture: ComponentFixture<SidebarComponent>;
    let component: SidebarComponent;
    let debugElement: DebugElement;

    beforeEach(async(() => {
        TestBed
            .configureTestingModule({
                declarations: [SidebarComponent, ReplayComponent, TimeAgoPipe],
                providers: [],
                imports: [SharedModule]
            })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(SidebarComponent);
                component = fixture.componentInstance;
                debugElement = fixture.debugElement;

                const master = agmDocumentFactory.build();

                component.branches = [master];
                component.selectedBranch = master;
            });
    }));

    it('should display loading if loaded=false & loading=true', fakeAsync(() => {
        component.loading = true;
        fixture.detectChanges();
        expect(debugElement.queryAll(By.css('.loading.replay')).length).toBe(3);
    }));

    // :TODO - for now loading has been disabled, but it might return soon
    // it('should not display loading if loaded=true', fakeAsync(() => {
    //     component.loading = true;
    //     component.branches = [];
    //     expect(debugElement.queryAll(By.css('.loading.replay')).length).toBe(0);
    // }));
});
