// noinspection TypeScriptCheckImport
import {mapValues, cloneDeep, forEach} from 'lodash';
import * as _Quill from 'quill';
import Delta from 'quill-delta';

const Quill: any = _Quill;
// const Delta = Quill.import('delta');

export class Commit {
    id: string;
    delta: Delta;
    author = '';
    comment = '';
    motivation = '';

    get hash() {
        return this.id;
    }

    toJSON(): ISerializedCommit {
        return this;
    }

    constructor(deltaOrCommit: Delta) {
        this.delta = new Delta(deltaOrCommit.ops as any);
    }
}

export interface ISerializedCommit {
    id: string;
    delta: any;
    author: string;
    comment: string;
    motivation: string;
}

export interface IPush {
    comment: string;
    motivation: string;
    lastRepositoryCommitId: string;
    fromMaster: boolean;
    documentDelta: Delta;
}

export interface IPull {
    branchId: string;
    lastCommitId: string;
    deltas: Delta[];
}
