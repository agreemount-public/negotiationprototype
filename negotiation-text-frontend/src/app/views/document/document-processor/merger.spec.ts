import {async} from '@angular/core/testing';
import DeltaStatic from 'quill-delta';
import {ISelect, Merger, ReducedBranch} from './merger';
import Delta = require('quill-delta/dist/Delta');
import {DeltaOperation} from 'quill';
import {AgmDocument} from '../../../state/common.model';
import {guid, HashMap} from '@datorama/akita';
import {agmDocumentFactory} from '../../../app.factories.spec';


describe('Merger', () => {
  // const Delta = Quill.import('delta');
  let merger: Merger;

  beforeEach(async(() => {
    merger = new Merger();
  }));

  function makeMaster(content: string): AgmDocument {
    return agmDocumentFactory.build({
      states: {
        documentType: 'change',
        changeState: 'changeStateAgreed',
        position: 'first',
        branch: 'main'
      },
      metrics: {
        changeJSON: `[{"insert":"${content}"}]`
      }
    });
  }

  function makeBranches(...branchOps: DeltaOperation[][]): HashMap<AgmDocument> {
    return branchOps.map(ops => agmDocumentFactory.build({
      metrics: {
        changeJSON: JSON.stringify(ops)
      }
    })).reduce((acc: HashMap<AgmDocument>, val) => {
      acc[val.id] = val;
      return acc;
    }, {});
  }

  function makeInsert(content: string): DeltaStatic {
    return new Delta([{insert: content}]);
  }

  function makeSelect(...args: ReducedBranch[]): ISelect {
    return {select: {options: args, value: args[0].id}};
  }

  function ensureSelectCreation(masterContent: string, branchesContents: string[],
                                expectedOutput: (string | { branchIndex: number, content: string }[])[]) {
    const master = makeMaster(masterContent);
    const masterDelta: DeltaStatic = new Delta(JSON.parse(master.metrics.changeJSON));
    const branches = makeBranches(
      ...branchesContents.map(content => masterDelta.diff(new Delta([{insert: content}])).ops)
    );

    merger.setState({...branches, [master.id]: master});

    const branchesKeys = Object.keys(branches);
    branchesKeys.splice(0, 0, master.id);

    const expectedOps = expectedOutput.map(out => {
      if (typeof out === 'string') {
        return {insert: out};
      } else {
        return {insert: makeSelect(...out.map(option => ({id: branchesKeys[option.branchIndex], ops: [{insert: option.content}]})))};
      }
    });

    expect(merger.getComposed()).toEqual(new Delta(expectedOps));
  }

  it('should merge basic', () => {
    const branches = makeBranches([{insert: 'Ge'}, {delete: 2}]);
    const master = makeMaster('Put your text here [...]');
    merger.setState({...branches, [master.id]: master});

    expect(merger.getComposed()).toEqual(new Delta([{
      insert: makeSelect(
        {id: master.id, ops: [{insert: 'Pu'}]},
        {id: Object.keys(branches)[0], ops: [{insert: 'Ge'}]}
      )
    }, {insert: 't your text here [...]'}]));
  });

  it('should merge basic with 2 branches', () => {
    ensureSelectCreation('Put your text here [...]', ['Get your text here [...]', 'Set your text here [...]'],
      [[{branchIndex: 0, content: 'Pu'}, {branchIndex: 1, content: 'Ge'}, {branchIndex: 2, content: 'Se'}], 't your text here [...]']);
  });

  it('should work with empty leaf list', () => {
    const master = makeMaster('123');
    merger.setState({[master.id]: master});

    expect(merger.getComposed().ops).toEqual(JSON.parse(master.metrics.changeJSON));
  });

  // :TODO - fix for shorter master -> see issue #27
  it('should merge at the end', () => {
    ensureSelectCreation('123 ', ['1234', '123?'], [
      '123', [{branchIndex: 0, content: ' '}, {branchIndex: 1, content: '4'}, {branchIndex: 2, content: '?'}]
    ]);
  });

  // :TODO - there should be one other option in select - ('') for master
  it('should merge change in the middle', () => {
    ensureSelectCreation('123456', ['123?456', '123x456'], [
      '123', [{branchIndex: 1, content: '?'}, {branchIndex: 2, content: 'x'}], '456'
    ]);
  });

  it('should merge with delete', () => {
    ensureSelectCreation('123456', ['12?456', '12x456'],
      ['12', [{branchIndex: 0, content: '3'}, {branchIndex: 1, content: '?'}, {branchIndex: 2, content: 'x'}], '456']
    );
  });

  it('should merge multi char strings', () => {
    ensureSelectCreation('aaabbb', ['aaaccccbbb', 'aaaddddbbb'],
      ['aaa', [{branchIndex: 1, content: 'cccc'}, {branchIndex: 2, content: 'dddd'}], 'bbb']
    );
  });

  it('should merge multi char strings and deletes', () => {
    ensureSelectCreation('aaabbb', ['aaCCCCbb', 'aaaDDDDbbb'],
      ['aa', [{branchIndex: 0, content: 'ab'}, {branchIndex: 1, content: 'CCCC'}, {branchIndex: 2, content: 'aDDDDb'}], 'bb']);
  });

  it('should generate multiple selects from two changes', () => {
    ensureSelectCreation('aaabbb', ['Aaabbb', 'aaabbB'],
      [[{branchIndex: 0, content: 'a'}, {branchIndex: 1, content: 'A'}],
        'aabb',
        [{branchIndex: 0, content: 'b'}, {branchIndex: 2, content: 'B'}]]);
  });

  it('should generate multi select from two changes (2)', () => {
    ensureSelectCreation('aaabbb', ['Aaabbb', 'CaabbB'],
      [[{branchIndex: 0, content: 'a'}, {branchIndex: 1, content: 'A'}, {branchIndex: 2, content: 'C'}],
      'aabb',
      [{branchIndex: 0, content: 'b'}, {branchIndex: 2, content: 'B'}]]);
  });

  it('should generate multi select from two changes with multiple characters changed', () => {
    ensureSelectCreation('aaabbb', ['AAaabbb', 'aaabbBBB'], [
      [{branchIndex: 0, content: 'a'}, {branchIndex: 1, content: 'AA'}],
      'aabb',
      [{branchIndex: 0, content: 'b'}, {branchIndex: 2, content: 'BBB'}],
    ]);
  });
});
