import DeltaStatic from 'quill-delta';
import {mapValues, size, forEach} from 'lodash';
import * as _Quill from 'quill';
import {Commit, IPull, IPush, ISerializedCommit} from './commit';

const Quill: any = _Quill;
const Delta = Quill.import('delta');

export interface ISerializedRepository {
    branch: string|null;
    commits: ISerializedCommit[];
}

export type BranchStatus = 'published' | 'rejected' | 'accepted' | 'draft';

export interface IBranch {
    id: string;
    isMaster: boolean;
    owner: string;
    status: BranchStatus;
    // :TODO delete later, used for prototyping
    commit: DeltaStatic;
    commitRaw: string;
}

export class Repository {
    private branches: { [key: string]: IBranch } = {};
    private commits: Commit[] = [];
    public master: Commit | null = null;
    private head: Commit | null = null;
    private headId: string | null = null;

    currentBranchId: string|null = null;
    headState: DeltaStatic;
    workingCopy: DeltaStatic;
    masterState: DeltaStatic = new Delta();

    constructor() {
        this.workingCopy = new Delta();
    }

    serialize(): ISerializedRepository {
        return {
            commits: this.commits.map((commit: Commit) => commit.toJSON()),
            branch: this.currentBranchId
        };
    }

    private addCommit(commit: Commit): Commit {
        return commit;
    }

    addDelta(delta: DeltaStatic, prevCommitHash: string | null = null): Commit {
        return this.addCommit(new Commit(delta));
    }

    init(initialMaster: IPull) {
        initialMaster.deltas.forEach(delta => this.masterState = this.masterState.compose(delta));
    }

    new(firstState: DeltaStatic) {
        this.master = this.head = this.addDelta(firstState);
        this.workingCopy = this.headState = firstState;
    }

    commit(newState: DeltaStatic): Commit|null {
        // quick and dirty deep comparison
        if (JSON.stringify(this.headState) === JSON.stringify(newState)) {
            return null;
        }
        const commit = this.addDelta(this.headState.diff(newState));
        this.workingCopy = newState;
        return commit;
    }

    push(commitHash: string | null = null, comment: string = '', motivation: string = ''): IPush {
        return {
            fromMaster: true,
            comment,
            motivation,
            lastRepositoryCommitId: this.headId as string,
            documentDelta: this.headState.diff(this.workingCopy)
        };
    }

    pull(pull: IPull) {
        this.workingCopy = new Delta(this.masterState);
        this.currentBranchId = pull.branchId;
        this.headId = pull.lastCommitId;
        pull.deltas.forEach(delta => this.workingCopy = this.workingCopy.compose(delta));
        this.headState = new Delta(this.workingCopy);
    }
}
