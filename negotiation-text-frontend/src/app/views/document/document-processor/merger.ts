import {DeltaOperation} from 'quill';
import {findKey, forEach} from 'lodash';
import DeltaStatic from 'quill-delta';
import Delta from 'quill-delta/dist/Delta';
import {AgmDocument, DocumentID} from '../../../state/common.model';
import {HashMap} from '@datorama/akita';
import {sortByCreatedAt} from '../../../services/utils/miscellaneous';


export interface ISelectData {
  options: ReducedBranch[]; // HashMap<DeltaOperation[]>;
  value: DocumentID;
}

export interface ISelect {
  select: ISelectData;
}

export interface IChange {
  leaf: number;
  op: DeltaOperation;
}

export interface ReducedBranch {
  id: DocumentID;
  ops: DeltaOperation[];
}

export function isSelect(obj: any): obj is ISelect {
  return obj != null && obj.select !== undefined;
}

export class Merger {
  // DeltaCls: any;

  masterId: DocumentID;
  master: DeltaStatic;
  leafs: ReducedBranch[] = [];

  private ranges: boolean[][] = [];
  private reducedRanges: boolean[] = [];

  constructor() {}

  setState(branches: HashMap<AgmDocument>) {
    const masterId: DocumentID|undefined = findKey(branches, doc => doc.states.branch === 'main');
    if (masterId === undefined) { throw Error('no main branch among branches'); }

    this.master = new Delta(JSON.parse(branches[masterId].metrics.changeJSON));
    this.masterId = masterId;

    this.leafs = Object.keys(branches)
      .sort((docIdA, docIdB) => -sortByCreatedAt(branches[docIdA], branches[docIdB]))
      .filter(docId => branches[docId].states.branch === 'other')
      .map(docId => ({id: docId, ops: JSON.parse(branches[docId].metrics.changeJSON)}));
  }

  getComposed(): DeltaStatic {
    if (this.leafs.length === 0) {
      return this.master;
    }

    this.generateRangesMap();
    const changes = this.preprocessBranches();
    this.findModifiedSpots();
    const out = this.getUnchangedSpots();
    const rangesIndexes = this.calculateShifts();
    const selects = this.createSelectEmbeds(changes, rangesIndexes);
    return this.composeFinal(selects, out);
  }

  /*******************************************************************************************
   * Parts of main algorithm
   *******************************************************************************************/

  private generateRangesMap() {
    for (const leaf of this.leafs) {
      const leafsRanges: boolean[] = [];
      for (let i = 0; i < this.master.length(); ++i) {
        leafsRanges.push(false);
      }
      this.ranges.push(leafsRanges);
    }
  }

  private preprocessBranches(): { [key: number]: IChange[] } {
    const changes: { [key: number]: IChange[] } = {};
    this.leafs.forEach((d: ReducedBranch, leafId) => {
      const range: boolean[] = this.ranges[leafId];
      let position = 0;
      d.ops.forEach((dd: DeltaOperation) => {
        if (dd.retain != null) {
          position += dd.retain;
        } else if (dd.delete != null) {
          for (let i = 0; i < dd.delete; ++i) {
            range[position + i] = true;
          }

        } else if (dd.insert != null) {
          if (changes[position] == null) {
            changes[position] = [];
          }
          changes[position].push({leaf: leafId, op: dd.insert});
        } else {
          throw new Error('unknown DeltaOperation (no insert|retain|delete');
        }
      });
    });
    return changes;
  }

  /**
   * create general boolean map of spots which have changed in master in branch,
   * these places will be removed and added to select boxes
   */
  private findModifiedSpots() {
    for (let i = 0; i < this.master.length(); ++i) {
      this.reducedRanges.push(this.ranges.reduce((p: boolean, c: boolean[]) => p || c[i], false));
    }
  }

  private getUnchangedSpots(): DeltaStatic {
    let out: DeltaStatic = new Delta(); // this.DeltaCls();
    this.reducedRanges.forEach((val, i) => {
      if (!val) {
        out = out.concat(this.master.slice(i, i + 1));
      }
    });

    return out;
  }

  private calculateShifts(): number[] {
    let lastRangeIdx = -1;
    // maps every character in master to rangeIdx which is used to calculate shifts
    return this.reducedRanges.map((val, i, arr) => {
      if (!arr[i - 1] && val && i < arr.length - 1) {
        return ++lastRangeIdx;
      } else {
        return Math.max(0, lastRangeIdx);
      }
    });
  }

  private createSelectEmbeds(changes: { [key: number]: IChange[] }, rangesIndexes: number[]): { [position: number]: ReducedBranch[] } {
    const selects: { [position: number]: ReducedBranch[] } = {};
    // for every branch create select embed element and save it for later processing
    forEach(changes, (val: IChange[], key: string) => {
      const keyN: number = Number(key);
      const changeIdx = rangesIndexes[keyN] || 0;
      val.forEach(v => {
        const rangeStart = this.getRangeStart(v.leaf, keyN);
        const rangeEnd = this.getRangeEnd(v.leaf, keyN);

        const globalRangeStart = this.getGlobalRangeStart(keyN);
        const globalRangeEnd = this.getGlobalRangeEnd(keyN);

        let tmp: DeltaStatic = new Delta(); // this.DeltaCls();

        if (rangeStart == null && globalRangeStart != null && keyN > globalRangeStart) {
          this.master.slice(globalRangeStart, keyN).forEach(op => tmp = tmp.insert(op.insert as string | object));
        }

        tmp = tmp.insert(v.op);

        if (rangeEnd == null && globalRangeEnd != null && keyN <= globalRangeEnd) {
          this.master.slice(keyN, globalRangeEnd + 1).forEach(op => tmp = tmp.insert(op.insert as string | object));
        }

        const insertPoint = (globalRangeStart != null ? globalRangeStart : keyN) + changeIdx;

        if (selects[insertPoint] == null) {
          selects[insertPoint] = [];
          if (globalRangeStart != null && globalRangeEnd != null) {
            selects[insertPoint].push({ops: this.master.slice(globalRangeStart, globalRangeEnd + 1).ops, id: this.masterId});
          }
        }
        selects[insertPoint].push({ops: tmp.ops, id: this.leafs[v.leaf].id});
      });
    });

    return selects;
  }

  /**
   * Compose unchanged master with changes written in the previous step
   */
  private makeSelect(branches: ReducedBranch[]): ISelect {
    return {select: {options: branches, value: branches[0].id}};
  }

  private composeFinal(selects: { [position: number]: ReducedBranch[] }, out: DeltaStatic): DeltaStatic {
    forEach(selects, (val, key) => {
      const keyN: number = Number(key);
      const ops: DeltaOperation[] = [];
      if (keyN !== 0) {
        ops.push({retain: keyN});
      }

      out = out.compose(new Delta([ // this.DeltaCls([
        ...ops,
        {insert: this.makeSelect(val)}
      ]));
    });
    return out;
  }

  /*******************************************************************************************
   * Utility methods
   *******************************************************************************************/

  private rangeLength(range: boolean[], i: number): number {
    let l = 0;
    let tmp = i;
    while (range[tmp]) {
      --tmp;
      ++l;
    }
    tmp = i + 1;
    while (range[tmp]) {
      ++tmp;
      ++l;
    }

    return l;
  }

  private getGlobalRangeStart(i: number): number | null {
    if (!this.reducedRanges[i]) {
      return null;
    }
    while (this.reducedRanges[i]) {
      --i;
    }
    return i + 1;
  }

  private getGlobalRangeEnd(i: number): number | null {
    if (!this.reducedRanges[i]) {
      return null;
    }
    while (this.reducedRanges[i]) {
      ++i;
    }
    return i - 1;
  }

  private getRangeStart(leaf: number, i: number): number | null {
    if (!this.ranges[leaf][i]) {
      return null;
    }
    while (this.ranges[leaf][i]) {
      --i;
    }
    return i + 1;
  }

  private getRangeEnd(leaf: number, i: number): number | null {
    if (!this.ranges[leaf][i]) {
      return null;
    }
    while (this.ranges[leaf][i]) {
      ++i;
    }
    return i - 1;
  }
}
