import {compose} from './composer';
import Delta from 'quill-delta';

describe('compose', () => {
  it('should compose from merger output', () => {
    expect(compose(new Delta())).toEqual(new Delta());
  });
});
