import DeltaStatic from 'quill-delta';
import Delta from 'quill-delta/dist/Delta';
import {isSelect} from './merger';


export function compose(delta: DeltaStatic) {
  const output = new Delta();

  delta.ops.map(op => {
    if (isSelect(op.insert)) {
      const selectedBranchId = op.insert.select.value;
      const branch = op.insert.select.options.find(rb => rb.id === selectedBranchId);
      if (branch === undefined) {
        throw Error('select blob value does not exist among options');
      }

      output.push(branch.ops[0]);
    } else {
      output.push(op);
    }
  });

  return output;
}
