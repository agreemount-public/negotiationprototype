import { Component, OnInit } from '@angular/core';
import {DocumentQuery} from '../state/document/document.query';
import {Observable} from 'rxjs';
import {DocumentService} from '../state/document/document.service';

@Component({
  selector: 'agm-share-modal',
  templateUrl: './share-modal.component.html',
  styleUrls: ['./share-modal.component.scss']
})
export class ShareModalComponent implements OnInit {
  public sharedEmails$: Observable<string[]>;

  constructor(private documentQuery: DocumentQuery, private documentService: DocumentService) { }

  ngOnInit() {
    this.sharedEmails$ = this.documentQuery.select(state => state.sharedWith);
  }

  share() {
    const emails = [];
    this.documentService.share(emails);
  }
}
