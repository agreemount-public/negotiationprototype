import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShareModalComponent } from './share-modal.component';
import {ModalComponent} from '../../../components/modal/modal.component';
import {DocumentQuery} from '../state/document/document.query';
import {DocumentService} from '../state/document/document.service';
import {RouterTestingModule} from '@angular/router/testing';

class DocumentServiceMock {

}

describe('ShareModalComponent', () => {
  let component: ShareModalComponent;
  let fixture: ComponentFixture<ShareModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ ShareModalComponent, ModalComponent],
      providers: [
        DocumentQuery,
        {provide: DocumentService, useClass: DocumentServiceMock}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
