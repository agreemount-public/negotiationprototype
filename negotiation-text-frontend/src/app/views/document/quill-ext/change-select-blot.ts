import Quill from 'quill';
import {DocumentID} from '../../../state/common.model';
import {DocumentService} from '../state/document/document.service';
import {InjectorModule} from '../../../shared/injector.module';
import {ISelectData, ReducedBranch} from '../document-processor/merger';
import {DeltaOperation} from 'quill';
import * as Parchment from 'parchment';

const QuillBlockEmbed = Quill.import('blots/embed') as typeof Parchment.default.Embed;

export interface INodeChangeSelectBlot {
  __blot: { blot: ChangeSelectBlot };
}

/**
 * Contains blueprint for select boxes for selecting changes
 * @author Michał Szostak
 */
export class ChangeSelectBlot extends QuillBlockEmbed {
  // noinspection JSUnusedGlobalSymbols
  static readonly tagName = 'custom-select';
  // noinspection JSUnusedGlobalSymbols
  static readonly blotName = 'select';

  documentService: DocumentService;
  options: ReducedBranch[] = [];
  node: HTMLElement;
  selectNode: HTMLElement & { value: string };
  currentValue: DocumentID | null = null;

  constructor(node) {
    super(node);
    this.documentService = InjectorModule.Injector.get(DocumentService);
    this.node = node;
    this.selectNode = this.node.children[0].children[0] as any;
    this.currentValue = this.selectNode.value;
    this.options = node.__selectOptions;
    this.selectNode.addEventListener('change', event => {
      this.currentValue = (event.target as any).value;
      this.documentService.updateSelect(this.currentValue as string);
    });

    this.selectNode.addEventListener('focusin', event => {
      if (this.currentValue !== null) {
        this.documentService.setActiveDiscussion(this.currentValue, this.options.map(option => option.id));
      }
    });
    //
    // this.selectNode.addEventListener('focusout', event => {
    //   this.documentService.clearActiveDiscussion();
    // });
  }

  static create(change: ISelectData) {
    const node = super.create(change) as HTMLElement & { __selectOptions: ReducedBranch[] };

    node.innerHTML = this.prepareInnerHTML(change);
    node.__selectOptions = change.options;
    node.contentEditable = 'false';

    return node;
  }

  static prepareInnerHTML(changes: ISelectData) {
    let html = '<select class="branchSelect">';
    for (const option of changes.options) {
      if (option.ops.length === 0) { continue; }
      html += `<option ${changes.value === option.id ? 'selected' : ''}`;
      html += ` value="${option.id}">${option.ops[0].insert}</option>`;
    }
    html += '</select>';
    return html;
  }

  static value(node: Node) {
    if (isChangeSelectBlot(node)) {
      return {
        options: node.__blot.blot.options,
        value: node.__blot.blot.currentValue
      };
    }
  }

  // update(mutations: MutationRecord[], context: { [key: string]: any }): void {}

  // optimize(context: { [key: string]: any }): void {}

  /**
   * Disable deletion of selects
   */
  deleteAt() {}
}

export function isChangeSelectBlot(node: any): node is INodeChangeSelectBlot {
  return node.__blot != null && node.__blot.blot != null && node.__blot.blot instanceof ChangeSelectBlot;
}
