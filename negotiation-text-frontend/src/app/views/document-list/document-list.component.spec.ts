import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DocumentListComponent} from './document-list.component';
import {SharedModule} from '../../shared/shared.module';
import {RouterTestingModule} from '@angular/router/testing';
import {TestingConstantsProvider} from '../../app.constants.spec';
import {DocumentListQuery} from './state/document-list.query';
import {DocumentListService} from './state/document-list.service';
import {AgmDocument, DocumentID} from '../../state/common.model';

class MockDocumentListQuery {
  selectLoading() {}
  selectAll() {}
}

class MockDocumentListService {
  get() {}
  add(documentList: AgmDocument) {}
  update(id, documentList: Partial<AgmDocument>) {}
  remove(id: DocumentID) {}
}

describe('DocumentListComponent', () => {
  let component: DocumentListComponent;
  let fixture: ComponentFixture<DocumentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule],
      providers: [{provide: DocumentListQuery, useClass: MockDocumentListQuery},
        {provide: DocumentListService, useClass: MockDocumentListService},
        TestingConstantsProvider],
      declarations: [DocumentListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
