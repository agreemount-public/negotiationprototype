import { Component, OnInit } from '@angular/core';
import {DocumentListQuery} from './state/document-list.query';
import {Observable} from 'rxjs';
import {AgmDocument} from '../../state/common.model';
import {DocumentListService} from './state/document-list.service';

@Component({
  selector: 'agm-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit {
  public loading$: Observable<boolean>;
  public documents$: Observable<AgmDocument[]>;

  constructor(private documentListQuery: DocumentListQuery, private documentListService: DocumentListService) { }

  ngOnInit() {
    this.documentListService.get();
    this.loading$ = this.documentListQuery.selectLoading();
    this.documents$ = this.documentListQuery.selectAll();
  }

}
