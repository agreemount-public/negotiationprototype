import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { DocumentListStore, DocumentListState } from './document-list.store';
import {AgmDocument} from '../../../state/common.model';

@Injectable({
  providedIn: 'root'
})
export class DocumentListQuery extends QueryEntity<DocumentListState, AgmDocument> {

  constructor(protected store: DocumentListStore) {
    super(store);
  }
}
