import { DocumentListQuery } from './document-list.query';
import { DocumentListStore } from './document-list.store';

describe('DocumentListQuery', () => {
  let query: DocumentListQuery;

  beforeEach(() => {
    query = new DocumentListQuery(new DocumentListStore());
  });

  it('should create an instance', () => {
    expect(query).toBeTruthy();
  });

});
