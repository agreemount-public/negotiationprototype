import {Inject, Injectable} from '@angular/core';
import {ID} from '@datorama/akita';
import {HttpClient} from '@angular/common/http';
import {DocumentListStore} from './document-list.store';
import {finalize} from 'rxjs/operators';
import {NEG_CONSTANTS, IConstants} from '../../../app.constants';
import {AgmDocument, AgmDocumentResponse, DocumentID} from '../../../state/common.model';

@Injectable({providedIn: 'root'})
export class DocumentListService {

  constructor(private documentListStore: DocumentListStore,
              @Inject(NEG_CONSTANTS) private CONSTANTS: IConstants,
              private http: HttpClient) {
  }

  get() {
    this.documentListStore.setLoading(true);
    this.http.get<AgmDocumentResponse[]>(`${this.CONSTANTS.apiBaseUrl}/negotiations`)
      .pipe(
        finalize(() => this.documentListStore.setLoading(false))
      ).subscribe((entities) => this.documentListStore.set(entities.map(o => o.negotiation)));
  }

  add(documentList: AgmDocument) {
    this.documentListStore.add(documentList);
  }

  update(id, documentList: Partial<AgmDocument>) {
    this.documentListStore.update(id, documentList);
  }

  remove(id: DocumentID) {
    this.documentListStore.remove(id);
  }
}
