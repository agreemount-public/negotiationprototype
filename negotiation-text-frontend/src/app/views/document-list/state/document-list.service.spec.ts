import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DocumentListService } from './document-list.service';
import { DocumentListStore } from './document-list.store';
import {TestingConstantsProvider} from '../../../app.constants.spec';

describe('DocumentListService', () => {
  let documentListService: DocumentListService;
  let documentListStore: DocumentListStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentListService, DocumentListStore, TestingConstantsProvider],
      imports: [ HttpClientTestingModule ]
    });

    documentListService = TestBed.get(DocumentListService);
    documentListStore = TestBed.get(DocumentListStore);
  });

  it('should be created', () => {
    expect(documentListService).toBeDefined();
  });

});
