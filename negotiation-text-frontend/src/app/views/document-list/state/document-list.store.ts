import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import {AgmDocument} from '../../../state/common.model';

export interface DocumentListState extends EntityState<AgmDocument> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'DocumentList' })
export class DocumentListStore extends EntityStore<DocumentListState, AgmDocument> {

  constructor() {
    super();
  }

}

