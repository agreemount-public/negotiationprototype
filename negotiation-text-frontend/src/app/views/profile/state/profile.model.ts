import { ID } from '@datorama/akita';

export interface Profile {
  id: ID;
}

/**
 * A factory function that creates Profile
 */
export function createProfile(params: Partial<Profile>) {
  return {

  } as Profile;
}
