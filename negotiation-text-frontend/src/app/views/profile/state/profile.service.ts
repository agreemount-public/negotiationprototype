import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { ProfileStore } from './profile.store';
import { Profile } from './profile.model';

@Injectable({ providedIn: 'root' })
export class ProfileService {

  constructor(private profileStore: ProfileStore,
              private http: HttpClient) {
  }

  get() {
    this.http.get('https://akita.com').subscribe((entities: any) => this.profileStore.set(entities));
  }

  add(profile: Profile) {
    this.profileStore.add(profile);
  }

  update(id, profile: Partial<Profile>) {
    this.profileStore.update(id, profile);
  }

  remove(id: ID) {
    this.profileStore.remove(id);
  }
}
