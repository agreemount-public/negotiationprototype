import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ProfileStore, ProfileState } from './profile.store';
import { Profile } from './profile.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileQuery extends QueryEntity<ProfileState, Profile> {

  constructor(protected store: ProfileStore) {
    super(store);
  }

}
