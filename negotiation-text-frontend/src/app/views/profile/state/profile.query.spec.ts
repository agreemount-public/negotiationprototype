import { ProfileQuery } from './profile.query';
import { ProfileStore } from './profile.store';

describe('ProfileQuery', () => {
  let query: ProfileQuery;

  beforeEach(() => {
    query = new ProfileQuery(new ProfileStore());
  });

  it('should create an instance', () => {
    expect(query).toBeTruthy();
  });

});
