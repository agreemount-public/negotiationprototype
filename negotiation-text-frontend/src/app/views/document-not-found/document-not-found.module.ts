import {DocumentNotFoundComponent} from './document-not-found.component';
import {NgModule} from '@angular/core';

@NgModule({
  declarations: [
    DocumentNotFoundComponent
  ],
  imports: [
  ],
  exports: [
    DocumentNotFoundComponent
  ]
})
export class DocumentNotFoundModule {}
