import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'agm-document-not-found',
  templateUrl: './document-not-found.component.html',
  styleUrls: ['./document-not-found.component.scss']
})
export class DocumentNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
