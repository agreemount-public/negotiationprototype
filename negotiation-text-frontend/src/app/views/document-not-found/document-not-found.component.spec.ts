import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentNotFoundComponent } from './document-not-found.component';

describe('DocumentNotFoundComponent', () => {
  let component: DocumentNotFoundComponent;
  let fixture: ComponentFixture<DocumentNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
