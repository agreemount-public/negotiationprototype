import {Component, OnDestroy, OnInit} from '@angular/core';
import {SessionService} from '../../state/session/session.service';
import {SessionQuery} from '../../state/session/session.query';
import {Observable} from 'rxjs';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {AkitaNgFormsManager} from '@datorama/akita-ng-forms-manager';
import {LoginFormState, LoginState} from './state/login.store';
import {FormState} from '../../state/form.model';
import { FormGroup, FormControl, FormArray } from '@ng-stack/forms';

@Component({
  selector: 'agm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', './login.component.screenres.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  form: FormGroup<LoginFormState>;
  loading$: Observable<boolean>;

  constructor(private sessionService: SessionService,
              private sessionQuery: SessionQuery,
              private akitaNgFormsManager: AkitaNgFormsManager<FormState>) {
  }

  ngOnInit() {
    this.loading$ = this.sessionQuery.selectLoading();

    this.form = new FormGroup<LoginFormState>({
      username: new FormControl(''),
      password: new FormControl(''),
      rememberMe: new FormControl(false)
    });
    this.akitaNgFormsManager.upsert('login', this.form);

    this.loading$.pipe(untilDestroyed(this))
      .subscribe(isLoading => isLoading ? this.form.disable({onlySelf: false}) : this.form.enable({onlySelf: false}));
  }

  submit() {
    this.sessionService.login(this.form.get('username').value, this.form.get('password').value);
  }

  ngOnDestroy() {
    this.akitaNgFormsManager.unsubscribe();
  }

}
