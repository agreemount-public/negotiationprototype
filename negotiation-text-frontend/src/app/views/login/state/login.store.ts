import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

export interface LoginFormState {
  username: string;
  password: string;
  rememberMe: boolean;
}

// tslint:disable-next-line
export interface LoginState {
}

export function createInitialState(): LoginState {
  return {
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'Login' })
export class LoginStore extends Store<LoginState> {

  constructor() {
    super(createInitialState());
  }

}

