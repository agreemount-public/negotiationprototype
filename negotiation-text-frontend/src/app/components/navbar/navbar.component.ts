import {Component, OnInit} from '@angular/core';
import {DocumentQuery} from '../../views/document/state/document/document.query';
import {combineLatest, Observable, of} from 'rxjs';
import {DocumentState} from '../../views/document/state/document/document.store';
import {catchError, delay, distinctUntilChanged, map, startWith, tap, withLatestFrom} from 'rxjs/operators';
import {ApplicationStateQuery} from '../../state/application-state/application-state.query';
import {SessionService} from '../../state/session/session.service';
import {Router} from '@angular/router';

@Component({
  selector: 'agm-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public showDocumentControls$: Observable<boolean>;
  public username$: Observable<string|null>;

  constructor(private documentQuery: DocumentQuery, private applicationStateQuery: ApplicationStateQuery,
              private sessionService: SessionService, private router: Router) {}

  ngOnInit() {
    this.username$ = this.applicationStateQuery.select(state => state.username);

    this.showDocumentControls$ = combineLatest(
      this.documentQuery.selectLoading().pipe(map(isLoading => !isLoading)),
      this.documentQuery.select(store => store.id).pipe(map(id => id != null)),
    ).pipe(
      // a litle hack, as the document id is changed in another components in ngOnInit()
      delay(0),
      map(([isNotLoading, documentExists]) => isNotLoading && documentExists)
    );
  }

  logout() {
    this.sessionService.logout();
  }

  shareDocument() {
    this.router.navigate([{outlets: {modal: 'share'}}]);
  }
}
