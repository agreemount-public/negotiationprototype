import {Component, OnInit} from '@angular/core';
import {SessionQuery} from '../../state/session/session.query';
import {Observable} from 'rxjs';
import {resetStores} from '@datorama/akita';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'agm-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit {
  public loggedIn$: Observable<boolean>;
  constructor(private sessionQuery: SessionQuery) {}

  PRODUCTION: boolean = environment.production;

  /**
   * THIS IS ONLY FOR NON PRODUCTION PURPOSES
   */
  devRefreshState() {
    resetStores();
    location.reload();
  }

  ngOnInit(): void {
    this.loggedIn$ = this.sessionQuery.isLoggedIn$();
  }
}
