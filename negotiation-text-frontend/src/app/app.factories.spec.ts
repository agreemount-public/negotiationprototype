import * as Factory from 'factory.ts';
import {AgmDocument} from './state/common.model';
import {guid} from '@datorama/akita';

const TIME_NOW = new Date().getTime();

export const agmDocumentFactory = Factory.Sync.makeFactory<AgmDocument>({
  id: Factory.each(() => guid()),
  parentId: null,
  states: {
    documentType: 'change',
    changeState: 'changeStateAgreed',
    position: 'first',
    branch: 'other'
  },
  metrics: {
    changeJSON: `[{"insert":"Sample Content"}]`
  },
  hasValidMetrics: true,
  name: '',
  isLeaf: true,
  rootUuid: 'ZFWupXcXQX',
  slaUuid: 'oENGKKRUuJ',
  team: '',
  author: 'email@domain.com',
  createdAt: Factory.each(i => new Date(TIME_NOW + i * 1000).toISOString()),
  attributes: {
    orderSIDE1: 1553036295100,
    orderSIDE2: 1553036295100
  },
  relatedDocumentsForQuery: null,
});
