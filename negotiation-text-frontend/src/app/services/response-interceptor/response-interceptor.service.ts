import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {ErrorType, IError} from '../../state/common.model';

export interface ApiResponse {
  message: string | null;
  status: 'SUCCESS' | 'FAILED';
  data: any | any[] | null;
}

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(map((event: HttpEvent<ApiResponse>) => {
        if (event instanceof HttpResponse && event.body !== null) {
          if (event.body.status === 'FAILED') {
            throw {content: {__general: event.body.message}, type: ErrorType.Server} as IError;
          }
          event = event.clone({body: event.body.data});
        }
        return event;
      }
    ), catchError((error: HttpErrorResponse) => {
        return throwError({content: {__general: 'Unknown Error'}, type: ErrorType.NotFound} as IError);
    }));
  }
}
