import { TestBed, inject } from '@angular/core/testing';
import { ResponseInterceptor } from './response-interceptor.service';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';
import {SessionQuery} from '../../state/session/session.query';
import {SessionService} from '../../state/session/session.service';
import {Observable, throwError} from 'rxjs';

class MockHandler implements HttpHandler {
  handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    return null;
  }
}

describe('ResponseInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      providers: [ResponseInterceptor]
    });
  });

  it('should be created', inject([ResponseInterceptor], (service: ResponseInterceptor) => {
    expect(service).toBeTruthy();
  }));
});
