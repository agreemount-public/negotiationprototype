import {tap} from 'rxjs/operators';
import {Injectable, Injector} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse, HttpResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {SessionQuery} from '../../state/session/session.query';
import {SessionService} from '../../state/session/session.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(protected sessionQuery: SessionQuery,
              protected sessionService: SessionService,
              private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.sessionQuery.isLoggedIn()) {
      request = request.clone({
        setHeaders: {
          // Authorization: `Token ${this.sessionQuery.getToken()}`
          'x-user-email': this.sessionQuery.getToken() as string
        }
      });

      return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do stuff with response if you want
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            console.log('401 - redirecting to login');
            // clear token, if 401 has been thrown it might mean that
            // token is outdated
            this.sessionService.setToken(null);
            this.router.navigate(['/login']);
          }
        }
      }));
    }
    return next.handle(request);
  }
}
