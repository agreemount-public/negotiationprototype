import { TestBed, inject } from '@angular/core/testing';
import { AuthInterceptor } from './auth-interceptor.service';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';
import {SessionQuery} from '../../state/session/session.query';
import {SessionService} from '../../state/session/session.service';
import {Observable, of, throwError} from 'rxjs';

class MockHandler implements HttpHandler {
  handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    return of(new HttpResponse());
  }
}

class SessionServiceMock {
  setToken(token: string) {}
}

describe('AuthInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      providers: [AuthInterceptor, SessionQuery, {provide: SessionService, useClass: SessionServiceMock}]
    });
  });

  it('should be created', inject([AuthInterceptor], (service: AuthInterceptor) => {
    expect(service).toBeTruthy();
  }));

  it('should clear token and navigate to /login if 401 is thrown', (done) => inject([AuthInterceptor], (service: AuthInterceptor) => {
    const request = new HttpRequest<any>('GET', '/rest/sample/url');
    const handler = new MockHandler();

    const rSpy = jest.spyOn(TestBed.get(Router) as Router, 'navigate').mockReturnValue(new Promise(() => true));

    jest.spyOn(handler, 'handle').mockImplementation((req: HttpRequest<any>) => {
      return throwError(new HttpErrorResponse({
        status: 401
      }));
    });

    const spy = jest.spyOn(TestBed.get(SessionService), 'setToken');
    jest.spyOn(TestBed.get(SessionQuery), 'isLoggedIn').mockReturnValueOnce(true);
    jest.spyOn(TestBed.get(SessionQuery), 'getToken').mockReturnValueOnce('abc');

    service.intercept(request, handler).subscribe({
      error: (error) => {
        expect(spy).toHaveBeenCalledWith(null);
        expect(rSpy).toHaveBeenCalledWith(['/login']);
        done();
      }
    });
  })());
});
