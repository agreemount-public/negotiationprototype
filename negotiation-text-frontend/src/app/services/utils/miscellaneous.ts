import {ActivatedRoute} from '@angular/router';
import {untilDestroyed} from 'ngx-take-until-destroy';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Route, UrlMatchResult, UrlSegment, UrlSegmentGroup} from '@angular/router';
import {AgmDocument} from '../../state/common.model';


export type UrlMatcherFunction = (segments: Array<UrlSegment>, segmentGroup: UrlSegmentGroup, route: Route) => UrlMatchResult;

export function makeComplexUrlMatcher(paramName: string, regex: RegExp): UrlMatcherFunction {
  return (segments: Array<UrlSegment>, segmentGroup: UrlSegmentGroup, route: Route): UrlMatchResult => {
    const parts = [regex];
    const posParams: { [key: string]: UrlSegment } = {};
    const consumed: UrlSegment[] = [];

    let currentIndex = 0;

    for (const part of parts) {
      if (currentIndex >= segments.length) {
        // workaround https://github.com/angular/angular/issues/18303
        return null as any;
      }

      const current = segments[currentIndex];

      if (!part.test(current.path)) {
        // workaround https://github.com/angular/angular/issues/18303
        return null as any;
      }

      posParams[paramName] = current;
      consumed.push(current);
      currentIndex++;
    }

    if (route.pathMatch === 'full' &&
      (segmentGroup.hasChildren() || currentIndex < segments.length)) {
      // workaround https://github.com/angular/angular/issues/18303
      return null as any;
    }

    return {consumed, posParams};
  };
}

export class RoutableComponent {
  constructor(protected route: ActivatedRoute) {
  }

  extractParams(paramName: string): Observable<string> {
    return this.route.params.pipe(untilDestroyed(this), map(params => params[paramName]));
  }
}

export function sortByCreatedAt(a: AgmDocument, b: AgmDocument): 0|1|-1 {
  if (a.createdAt === b.createdAt) { return 0; }
  if (a.createdAt < b.createdAt) { return 1; }
  return -1;
}
