import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import { RoutableComponent } from './miscellaneous';
import {RouterTestingModule} from '@angular/router/testing';
import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'agm-my-routable-component',
  template: '<div></div>',
  styleUrls: []
})
class MyRoutableComponent extends RoutableComponent {
  constructor(route: ActivatedRoute) {
    super(route);
  }
}

describe('RoutableComponent', () => {
  let component: RoutableComponent;
  let fixture: ComponentFixture<MyRoutableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [],
      declarations: [MyRoutableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRoutableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
