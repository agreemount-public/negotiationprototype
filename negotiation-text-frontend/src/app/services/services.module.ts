import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthInterceptor} from './auth-interceptor/auth-interceptor.service';
import {IsLoggedIn, IsNotLoggedIn} from './auth-guard/auth-guard.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
})
export class ServicesModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ServicesModule,
      providers: [
        AuthInterceptor,
        IsLoggedIn,
        IsNotLoggedIn
      ]
    };
  }
}
