import {Route, RouterModule, Routes, UrlSegment, UrlSegmentGroup} from '@angular/router';
import {LoginComponent} from './views/login/login.component';
import {IsLoggedIn, IsNotLoggedIn} from './services/auth-guard/auth-guard.service';
import {PageNotFoundComponent} from './views/page-not-found/page-not-found.component';
import {DocumentListComponent} from './views/document-list/document-list.component';
import {DocumentNewComponent} from './views/document-new/document-new.component';
import {DocumentComponent} from './views/document/document.component';
import {ProfileComponent} from './views/profile/profile.component';
import {ShareModalComponent} from './views/document/share-modal/share-modal.component';
import {makeComplexUrlMatcher} from './services/utils/miscellaneous';
import {DocumentNotFoundComponent} from './views/document-not-found/document-not-found.component';

export enum RouteIds {
  documentNotFound,
  login,
  profile,
  documentList,
  documentNew,
  document,
  pageNotFound
}

// Match standard document id which is 24 characters encoded number
const _documentIDMatcher = makeComplexUrlMatcher('id', new RegExp('^[a-f0-9]{24}$'));

export function documentIDMatcher(segments: Array<UrlSegment>, segmentGroup: UrlSegmentGroup, route: Route) {
  return _documentIDMatcher(segments, segmentGroup, route);
}

export const ROUTES: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [IsNotLoggedIn], data: {routeId: RouteIds.login}},
  {path: 'profile/', component: ProfileComponent, canActivate: [IsLoggedIn], data: {routeId: RouteIds.profile}},
  {
    path: 'documents', canActivate: [IsLoggedIn],
    children: [
      {path: '', component: DocumentListComponent, data: {routeId: RouteIds.documentList}},
      {path: 'new', component: DocumentNewComponent, data: {routeId: RouteIds.documentNew}},
      {matcher: documentIDMatcher, component: DocumentComponent, data: {routeId: RouteIds.document}, pathMatch: 'full'},
      {path: ':notFoundId', component: DocumentNotFoundComponent, data: {routeId: RouteIds.documentNotFound}},
    ]
  },
  {path: '', redirectTo: '/documents', pathMatch: 'full'},
  {
    path: 'share',
    component: ShareModalComponent,
    outlet: 'modal'
  },
  {path: '**', component: PageNotFoundComponent, data: {routeId: RouteIds.pageNotFound}}
];

export const routeConfig = RouterModule.forRoot(ROUTES);
