import {DeltaOperation} from 'quill';
import {HashMap} from '@datorama/akita';

export type DocumentID = string;
export type ActionID = string;

export interface AgmAction {
  id: ActionID;
  label: string;
  // type: null;
  // visible: boolean;
  // reloadDocumentAfter: boolean;
  // attributes: null;
  // documentAlias: 'BASE';
  // states: {};
}

export interface AgmDocument {
  id: DocumentID;
  parentId: DocumentID | null;
  states: {
    documentType?: string;
    mainState?: string;
    changeState: 'changeStateAgreed'|'draft'|'active'|'changeStateAgreedUnderCondition'|'agreed'|'rejected'|'inactive';
    position?: string;
    branch?: 'main'|'other';
  };
  metrics: {
    changeJSON: string;
  };
  hasValidMetrics: boolean;
  name: string;
  isLeaf: boolean;
  rootUuid: string;
  slaUuid: string;
  team: string;
  author: string; /*email*/
  createdAt: string; /*date encoded as string*/
  attributes: {
    orderSIDE1: number;
    orderSIDE2: number;
  };
  relatedDocumentsForQuery: any | null;
}

export interface AgmDocumentResponse {
  negotiation: AgmDocument;
  messages: string[];
  owner: string; /*email*/
  invited: any | null;
}

export enum ErrorType {
  NotFound,
  Server
}

export interface IError {
  type: ErrorType;
  content: {
    __general: string;
  };
}
