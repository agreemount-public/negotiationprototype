import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SessionQuery} from './session/session.query';
import {SessionService} from './session/session.service';
import {ApplicationStateQuery} from './application-state/application-state.query';
import {ApplicationStateService} from './application-state/application-state.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
})
export class StateModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StateModule,
      providers: [
        SessionQuery,
        SessionService,
        ApplicationStateQuery,
        ApplicationStateService
      ]
    };
  }
}
