import { ID } from '@datorama/akita';

export interface Session {
  initialized: boolean;
  token: string|null;
  roles: string[];
}

/**
 * A factory function that creates Session
 */
export function createSession(params: Partial<Session>) {
  return {
    initialized: false,
    token: params.token || null,
    roles: params.roles || [],
  } as Session;
}
