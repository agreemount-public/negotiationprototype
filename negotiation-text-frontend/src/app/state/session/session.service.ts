import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionStore } from './session.store';
import {LoginQuery} from '../../views/login/state/login.query';
import {AkitaNgFormsManager} from '@datorama/akita-ng-forms-manager';
import {delay, finalize, switchMap} from 'rxjs/operators';
import {FormState} from '../form.model';
import {from, Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import {action, resetStores} from '@datorama/akita';
import {ApplicationStateStore} from '../application-state/application-state.store';

@Injectable({ providedIn: 'root' })
export class SessionService {

  constructor(private sessionStore: SessionStore,
              private http: HttpClient,
              private loginQuery: LoginQuery,
              private router: Router,
              private applicationStateStore: ApplicationStateStore,
              private formManager: AkitaNgFormsManager<FormState>) {
  }
  init() {
    const token = localStorage.getItem('token');
    if (token != null) {
      this.sessionStore.update(store => ({...store, token}));
      this.applicationStateStore.update(store => ({...store, username: 'Jack'}));
    }
    this.sessionStore.update(store => ({...store, initialized: true}));
  }

  setToken(token: string | null) {
    this.sessionStore.update(state => ({...state, token}));
    if (token == null) {
      localStorage.removeItem('token');
    } else {
      localStorage.setItem('token', token);
    }
  }

  login(login: string, password: string) {
    this.sessionStore.setLoading(true);
    from([1]).pipe(delay(1000), finalize(() => this.sessionStore.setLoading(false))).subscribe(data => {
      this.setToken(login);
      this.applicationStateStore.update(store => ({...store, username: login}));
      this.router.navigate(['/']);
    });
    // this.http.post<any>(`/api/login`, this.loginQuery).pipe(finalize(() => this.sessionStore.setLoading(false))).subscribe(data => {
    // });
  }

  @action('logout')
  logout() {
    this.setToken(null);
    resetStores();
    this.router.navigate(['/login']);
  }
}
