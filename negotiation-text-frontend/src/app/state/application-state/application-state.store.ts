import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

export interface ApplicationStateState {
   username: string|null;
}

export function createInitialState(): ApplicationStateState {
  return {
    username: null
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'ApplicationState' })
export class ApplicationStateStore extends Store<ApplicationStateState> {

  constructor() {
    super(createInitialState());
  }

}

