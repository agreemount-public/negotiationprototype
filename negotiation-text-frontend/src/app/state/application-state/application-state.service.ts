import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationStateStore } from './application-state.store';

@Injectable({ providedIn: 'root' })
export class ApplicationStateService {

  constructor(private applicationStateStore: ApplicationStateStore,
              private http: HttpClient) {
  }

}
