import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApplicationStateService } from './application-state.service';
import { ApplicationStateStore } from './application-state.store';

describe('ApplicationStateService', () => {
  let applicationStateService: ApplicationStateService;
  let applicationStateStore: ApplicationStateStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationStateService, ApplicationStateStore],
      imports: [ HttpClientTestingModule ]
    });

    applicationStateService = TestBed.get(ApplicationStateService);
    applicationStateStore = TestBed.get(ApplicationStateStore);
  });

  it('should be created', () => {
    expect(applicationStateService).toBeDefined();
  });

});
