import { ApplicationStateQuery } from './application-state.query';
import { ApplicationStateStore } from './application-state.store';

describe('ApplicationStateQuery', () => {
  let query: ApplicationStateQuery;

  beforeEach(() => {
    query = new ApplicationStateQuery(new ApplicationStateStore());
  });

  it('should create an instance', () => {
    expect(query).toBeTruthy();
  });

});
