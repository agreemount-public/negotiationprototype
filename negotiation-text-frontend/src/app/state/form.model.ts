import {LoginFormState} from '../views/login/state/login.store';

export interface FormState {
  login: LoginFormState;
}
