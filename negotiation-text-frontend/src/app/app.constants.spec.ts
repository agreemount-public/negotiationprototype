import {Provider} from '@angular/core';
import {NEG_CONSTANTS, NegConstantsFactory} from './app.constants';

export const TestingConstantsProvider: Provider = {provide: NEG_CONSTANTS, useValue: NegConstantsFactory()};
