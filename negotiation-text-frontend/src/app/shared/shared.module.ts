import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { NgStackFormsModule } from '@ng-stack/forms';
import {TooltipModule} from 'ngx-bootstrap';
import {AvatarModule} from 'ngx-avatar';

library.add(fas);

@NgModule({
  declarations: [],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    FontAwesomeModule,
    NgStackFormsModule,
    TooltipModule.forRoot(),
    AvatarModule
  ],
  exports: [
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule,
    TooltipModule,
    AvatarModule
  ]
})
export class SharedModule { }
