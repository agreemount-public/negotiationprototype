import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {RootComponent} from './components/root/root.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {ProfileComponent} from './views/profile/profile.component';
import {LoginComponent} from './views/login/login.component';
import {DocumentNewComponent} from './views/document-new/document-new.component';
import {DocumentListComponent} from './views/document-list/document-list.component';
import {routeConfig} from './app.routes';
import {SharedModule} from './shared/shared.module';
import {StateModule} from './state/state.module';
import {PageNotFoundComponent} from './views/page-not-found/page-not-found.component';
import {environment} from '../environments/environment';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';
import {AkitaNgRouterStoreModule} from '@datorama/akita-ng-router-store';
import {SessionService} from './state/session/session.service';
import {DocumentModule} from './views/document/document.module';
import {BsDropdownModule} from 'ngx-bootstrap';
import {akitaConfig} from '@datorama/akita';
import {SessionQuery} from './state/session/session.query';
import {ConstantsProvider} from './app.constants';
import {ResponseInterceptor} from './services/response-interceptor/response-interceptor.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from './services/auth-interceptor/auth-interceptor.service';
import {ServicesModule} from './services/services.module';
import {DocumentNotFoundModule} from './views/document-not-found/document-not-found.module';

@NgModule({
  declarations: [
    RootComponent,
    NavbarComponent,
    ProfileComponent,
    LoginComponent,
    DocumentNewComponent,
    DocumentListComponent,
    PageNotFoundComponent,
  ],
  imports: [
    ...(environment.production ? [] : [AkitaNgDevtools.forRoot()]),
    AkitaNgRouterStoreModule.forRoot(),
    BrowserModule,
    SharedModule,
    ServicesModule.forRoot(),
    routeConfig,
    StateModule.forRoot(),
    DocumentModule.forRoot(),
    BsDropdownModule.forRoot(),
    DocumentNotFoundModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'pl'},
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true },
    ConstantsProvider,
  ],
  bootstrap: [RootComponent]
})
export class AppModule {
  constructor(private sessionService: SessionService, private sessionQuery: SessionQuery) {
    akitaConfig({
      resettable: true
    });

    if (!sessionQuery.isInitialized()) {
      this.sessionService.init();
    }
  }
}
