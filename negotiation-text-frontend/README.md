## Negotiation Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.5.

## Dependencies

To compile frontend you will need:

* node 10+, npm 5+

Backend

* maven
* JAVA

## System requirements

To run this project you will need to setup backend and it's dependencies:

* http://gitlab.docker/agreemount/negotiation/negotiation-text-server
  * The backend has only a single system dependency - mongo db, make sure to install it and run it
  * run `mvn spring-boot:run` in the `negotiation-text-server` directory to run backend

## Development server

To run basic development server run:

* `npm start` - this will run server with proxy and hmr configured

### Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. This will build a production build with `aot`, minification and optimization enabled.

### Running lint

This project has linting enabled, it is integrated into CI. If you want to check whether your changes pass linting run:

```
npm run lint
```

### Running unit tests

Unit tests are handled by **JEST**, to run tests run:

```
npm test
```

### Running end-to-end tests

E2E tests are handled by **Cypress** to run them run:

```
npm run e2e
```

