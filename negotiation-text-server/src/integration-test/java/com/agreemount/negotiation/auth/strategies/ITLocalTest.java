package com.agreemount.negotiation.auth.strategies;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.auth.IAuthStrategy;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.IUserDAO;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITLocalTest {

    private static final char[] TEMP_PASS = new char[]{'k', 'i', 't', 't', 'y', '1', '2', '3'};
    private static final String TEMP_EMAIL = "john@travolta.pl";
    private static final String TEMP_NAME = "Jogn Travolta";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    @Qualifier("local")
    private IAuthStrategy localStrategy;

    @Autowired
    @Qualifier("google")
    private IAuthStrategy googleStrategy;

    @Autowired
    private IUserDAO userDAO;

    @Test
    public void shouldSignUpUser() {
        com.agreemount.negotiation.bean.providers.Local signedUpUser = signUpUser(Boolean.FALSE);

        assertThat(signedUpUser.getPassword()).isNull();
        assertThat(signedUpUser.getHash()).isNotNull();
        assertThat(signedUpUser.getSalt()).isNotNull();
        assertThat(signedUpUser.getConfirmationHash()).isNotNull();
        assertThat(signedUpUser.isConfirmed()).isFalse();
    }

    @Test
    public void shouldThrowExceptionWhenPasswordIsNotGiven() {
        com.agreemount.negotiation.bean.providers.Local user = new com.agreemount.negotiation.bean.providers.Local();
        user.setPassword(null);
        user.setName("test");
        user.setEmail("john@travolta.pl");

        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Password is not set");

        localStrategy.signUp(user);
    }

    @Test
    public void shouldSignInUser() {
        signUpUser(Boolean.TRUE);

        User user = localStrategy.login(TEMP_EMAIL, TEMP_PASS);

        assertThat(TEMP_NAME).isEqualTo(user.getName());
        assertThat(((com.agreemount.negotiation.bean.providers.Local) user).getPassword()).isNull();
        assertThat(((com.agreemount.negotiation.bean.providers.Local) user).getHash()).isNull();
        assertThat(((com.agreemount.negotiation.bean.providers.Local) user).getSalt()).isNull();
    }

    @Test
    public void shouldThrowExceptionForIncorrectCredentials() {
        signUpUser(Boolean.TRUE);

        thrown.expect(AuthException.class);
        thrown.expectMessage("Authentication failed: incorrect username or password");

        localStrategy.login(TEMP_EMAIL, new char[]{'i', 'n', 'c', 'o', 'r', 'r', 'e', 'c', 't'});
    }

    @Test
    public void shouldThrowExceptionWhenUserHaveNotConfirmedEmail() {
        signUpUser(Boolean.FALSE);

        thrown.expect(AuthException.class);
        thrown.expectMessage("Account email address is not confirmed");

        localStrategy.login(TEMP_EMAIL, TEMP_PASS);
    }

    @Test
    public void shouldThrowExceptionIfUserTryToUseSocialAccountOnLocal() {
        signUpGoogleUser();

        thrown.expect(AuthException.class);
        thrown.expectMessage("Given address is assigned to social media account");

        localStrategy.login(TEMP_EMAIL, TEMP_PASS);
    }


    @Test
    public void shouldActivateAccount() {
        // given
        com.agreemount.negotiation.bean.providers.Local local = signUpUser(Boolean.FALSE);
        String confirmationHash = local.getConfirmationHash();

        // when
        Boolean isConfirmed = localStrategy.confirmAccountAddress(confirmationHash);
        com.agreemount.negotiation.bean.providers.Local user = (Local) userDAO.getByEmail(TEMP_EMAIL);

        // then
        assertThat(isConfirmed).isTrue();
        assertThat(user.isConfirmed()).isTrue();
    }

    @Test
    public void shouldNotActivateAccountWhishIsAlreadyActivatedAccount() {
        // given
        com.agreemount.negotiation.bean.providers.Local local = signUpUser(Boolean.TRUE);
        String confirmationHash = local.getConfirmationHash();

        thrown.expect(AuthException.class);
        thrown.expectMessage("Account is already activated");

        // when
        localStrategy.confirmAccountAddress(confirmationHash);
    }

    private com.agreemount.negotiation.bean.providers.Google signUpGoogleUser() {
        com.agreemount.negotiation.bean.providers.Google user = new com.agreemount.negotiation.bean.providers.Google();
        user.setName(TEMP_NAME);
        user.setEmail(TEMP_EMAIL);
        com.agreemount.negotiation.bean.providers.Google signedUpUser =
                (com.agreemount.negotiation.bean.providers.Google) googleStrategy.signUp(user);

        return signedUpUser;
    }


    private com.agreemount.negotiation.bean.providers.Local signUpUser(boolean isAccountConfirmed) {
        com.agreemount.negotiation.bean.providers.Local user = new com.agreemount.negotiation.bean.providers.Local();
        user.setPassword(TEMP_PASS);
        user.setName(TEMP_NAME);
        user.setEmail(TEMP_EMAIL);
        user.setConfirmed(isAccountConfirmed);
        com.agreemount.negotiation.bean.providers.Local signedUpUser =
                (com.agreemount.negotiation.bean.providers.Local) localStrategy.signUp(user);

        return signedUpUser;
    }

    @Before
    public void setUp() {
        userDAO.deleteAll(User.class);
    }
}
