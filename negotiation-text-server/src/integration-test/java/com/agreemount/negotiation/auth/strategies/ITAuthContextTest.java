package com.agreemount.negotiation.auth.strategies;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.auth.AuthContext;
import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.providers.Facebook;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.TestCase.assertNotNull;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITAuthContextTest {

    private static final String USER_FACEBOOK_TOKEN = "123123#$%123";
    private static final String USER_EMAIL = "test@test.pl";
    private static final char[] USER_PASSWORD = new char[]{'k', 'i', 't', 't', 'y', '1', '2', '3'};
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Autowired
    private IUserDAO userDAO;
    @Autowired
    private AuthContext context;

    @Test
    public void shouldReturnUserIfExists() {
        User user = persistUser(localUser());

        User actualUser = context.getUser(user.getId());

        assertNotNull(actualUser);
    }

    @Test
    public void shouldReturnNullIfUserDoesNotExist() {
        User actualUser = context.getUser("123");

        assertNull(actualUser);
    }

    @Test
    public void shouldPersistLocalUserWhenLocalUserGiven() {
        User user = localUser();

        User actualUser = context.signUp(user);

        assertNotNull(actualUser);
        assertNotNull(((Local) actualUser).getHash());
        assertNotNull(((Local) actualUser).getSalt());
    }

    @Test
    public void shouldResetPasswordAfterSignUp() {
        User user = localUser();

        User actualUser = context.signUp(user);

        assertNotNull(actualUser);
        assertNull(((Local) actualUser).getPassword());
    }

    @Test
    public void shouldPersistFacebookUserWhenFacebookUserGiven() {
        User user = facebookUser();

        User actualUser = context.signUp(user);

        assertNotNull(actualUser);
        assertNotNull(((Facebook) actualUser).getToken());
    }

    @Test
    public void shouldReturnLocalUserWhenLoginToSystemWithCorrectCredentials() {
        context.signUp(localUser());

        User actualUser = context.login(USER_EMAIL, USER_PASSWORD);

        assertNotNull(actualUser);
        assertThat(actualUser.get_class(), containsString("Local"));
        assertEquals(USER_EMAIL, actualUser.getEmail());
    }

    @Test
    public void shouldResetHashAndSaltWhenLoginToSystemWithCorrectCredentials() {
        context.signUp(localUser());

        User actualUser = context.login(USER_EMAIL, USER_PASSWORD);

        assertNotNull(actualUser);
        assertNull(((Local) actualUser).getPassword());
        assertNull(((Local) actualUser).getHash());
        assertNull(((Local) actualUser).getSalt());
    }

    @Test
    public void shouldThrowExceptionWhenLoginToSystemWithIncorrectCredentials() {
        context.signUp(localUser());

        thrown.expectMessage("Authentication failed: incorrect username or password");
        thrown.expect(AuthException.class);

        context.login(USER_EMAIL, new char[]{'i', 'n', 'c', 'o', 'r', 'r', 'e', 'c', 't'});
    }

    @Test
    public void shouldPersistSocialUserWithInitialPaymentUnits() {
        User user = facebookUser();

        User actualUser = context.signUp(user);

        assertEquals(UserDAO.INITIAL_PAYMENT_UNITS, actualUser.getPaymentUnits());
    }

    @Test
    public void shouldPersistLocalUserWithInitialPaymentUnits() {
        User user = localUser();

        User actualUser = context.signUp(user);

        assertEquals(UserDAO.INITIAL_PAYMENT_UNITS, actualUser.getPaymentUnits());
    }

    private User localUser() {
        Local user = new Local();
        user.setEmail(USER_EMAIL);
        user.setConfirmed(true);
        user.setPassword(USER_PASSWORD);
        return user;
    }

    private User facebookUser() {
        Facebook user = new Facebook();
        user.setToken(USER_FACEBOOK_TOKEN);
        user.setEmail(USER_EMAIL);
        return user;
    }

    private User persistUser(User user) {
        userDAO.save(user);
        return user;
    }

    @After
    @Before
    public void afterEachTest() {
        userDAO.deleteAll(User.class);
    }

}
