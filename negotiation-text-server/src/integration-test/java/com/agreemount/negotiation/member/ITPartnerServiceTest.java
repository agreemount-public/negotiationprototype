package com.agreemount.negotiation.member;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.dao.ITeamDAO;
import org.assertj.core.api.AbstractListAssert;
import org.assertj.core.api.ObjectAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITPartnerServiceTest {

    public static final String TEAM_NAME = "myTeam";
    public static final String DOCUMENT_ID = "123789";
    public static final String SIDE_OWNER = "john@builder.com";
    public static final String SIDE_SPOUSE = "ann@builder.com";
    public static final String SIDE_SPOUSE_PARTNER = "ann-partner@builder.com";
    public static final String SIDE_OWNER_PARTNER = "john-partner@builder.com";
    public static final boolean REGISTERED = true;
    public static final boolean NOT_REGISTERED = false;
    public static final boolean PARTNER = true;
    public static final boolean NOT_PARTNER = false;

    @Autowired
    private ITeamDAO teamDAO;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @After
    @Before
    public void setUp() {
        cleanData();
    }

    @Test
    public void shouldInvitePartner() {
        givenDocument(DOCUMENT_ID, TEAM_NAME);
        givenTeam(TEAM_NAME,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                side(SIDE_SPOUSE, UserRole.SIDE2, REGISTERED),
                partner(SIDE_SPOUSE_PARTNER, UserRole.SIDE2, REGISTERED)
        );

        whenInvitePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);

        thenSides().containsOnly(
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, NOT_REGISTERED),
                side(SIDE_SPOUSE, UserRole.SIDE2, REGISTERED),
                partner(SIDE_SPOUSE_PARTNER, UserRole.SIDE2, REGISTERED)
        );
    }

    @Test
    public void shouldDeletePartner() {
        givenDocument(DOCUMENT_ID, TEAM_NAME);
        givenTeam(TEAM_NAME,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_SPOUSE_PARTNER, UserRole.SIDE1, REGISTERED)
        );

        whenDeletePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);

        thenSides().containsOnly(
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_SPOUSE_PARTNER, UserRole.SIDE1, REGISTERED)
        );
    }

    private void givenDocument(String documentId, String teamName) {
        mongoTemplate.save(getDocument(documentId, teamName));
    }

    private Document getDocument(String documentId, String teamName) {
        Document document = new Document();
        document.setTeam(teamName);
        document.setId(documentId);
        return document;
    }

    private void givenTeam(String teamName, User2Team... sides) {
        mongoTemplate.save(team(teamName, sides));
    }

    private Team team(String teamName, User2Team... user2Teams) {
        Team team = new Team();
        team.setName(teamName);
        team.setUsers2Team(Arrays.asList(user2Teams));
        return team;
    }

    private User2Team member(String login, UserRole role, boolean isUserRegistered, boolean isPartner) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(role);
        user2Team.setUserRegistered(isUserRegistered);
        user2Team.setPartner(isPartner);
        return user2Team;
    }

    private User2Team side(String login, UserRole role, boolean isUserRegistered) {
        return member(login, role, isUserRegistered, NOT_PARTNER);
    }

    private User2Team partner(String login, UserRole role, boolean isUserRegistered) {
        return member(login, role, isUserRegistered, PARTNER);
    }

    private void cleanData() {
        mongoTemplate.remove(new Query(), Document.class);
        mongoTemplate.remove(new Query(), Team.class);
        mongoTemplate.remove(new Query(), User.class);
    }

    private void whenInvitePartner(String requestor, String partner, String teamId) {
        partnerService.invitePartner(requestor, partner, teamId);
    }

    private void whenDeletePartner(String requestor, String partner, String teamId) {
        partnerService.deletePartner(requestor, partner, teamId);
    }

    private AbstractListAssert<?, List<? extends User2Team>, User2Team, ObjectAssert<User2Team>> thenSides() {
        return assertThat(teamDAO.getTeamByName(TEAM_NAME).getUsers2Team());
    }

}
