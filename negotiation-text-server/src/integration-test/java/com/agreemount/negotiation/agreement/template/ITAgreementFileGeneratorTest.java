package com.agreemount.negotiation.agreement.template;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.document.Relation;
import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.slaneg.db.DocumentOperations;
import com.agreemount.slaneg.db.RelationOperations;
import com.google.common.collect.Maps;
import org.apache.commons.collections.map.HashedMap;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.assertj.core.api.PathAssert;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.StringAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Collectors;

import static com.agreemount.negotiation.bean.enums.Metrics.*;
import static com.agreemount.negotiation.bean.enums.States.*;
import static com.google.common.collect.Sets.newHashSet;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITAgreementFileGeneratorTest {
    private static final String NEGOTIATION_AND_ITEMS_SLA_UUID = UUID.randomUUID().toString();
    private static final String TEAM_NAME = "myTeamName";
    private static final String SIDE1_USER_LOGIN = "side1@user.com";
    private static final String SIDE1_USER_NAME = "side1UserName";
    private static final String SIDE2_USER_LOGIN = "side2@user.com";
    private static final String SIDE2_USER_NAME = "side2UserName";
    private static final String AUTHOR = "author@mailinator.com";
    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    @Autowired
    private AgreementFileGenerator agreementFileGenerator;
    @Autowired
    private ITeamDAO teamDAO;
    @Autowired
    private IUserDAO userDAO;
    @Autowired
    private IdentityProvider identityProvider;
    @Autowired
    private DocumentOperations documentOperations;
    @Autowired
    private RelationOperations relationOperations;

    private Document negotiationDocument;
    private SoftAssertions softly;

    @Before
    public void setUp() throws Exception {
        cleanData();
        softly = new SoftAssertions();
        negotiationDocument = givenNegotiationDocument();
        agreementFileGenerator.enrichedAgreementFolder = testFolder.getRoot().getAbsolutePath();
        Identity identitySide1 = new Identity();
        identitySide1.setLogin(SIDE1_USER_LOGIN);
        identitySide1.setRoles(Arrays.asList("customer"));
        when(identityProvider.getIdentity()).thenReturn(identitySide1);
        createSide1Side2UserAndAddThemBothToTheSameTeam();
    }

    @After
    public void tearDown() throws Exception {
        cleanData();
    }

    @Test
    public void generate() throws Exception {
        givenNegotiationItems(
                item(states("asset", "SIDE1"), metrics("Cash", "Money to split", 1000, 100, 0)),
                item(states("asset", "SIDE2"), metrics("Automobiles", "Ford Focus Kombi", 20000, 0, 80)),
                item(states("asset", "SIDE1"), metrics("Automobiles", "Ford Focus Kombi", 20000, 20, 0)),
                item(states("asset", "SIDE1"), metrics("Jewelery", "All Jewelery", 30000, 100, 0)),
                item(states("asset", "SIDE2"), metrics("Boats", "Two yachts", 200000, 0, 100)),
                item(states("asset", "SIDE1"), metrics("Collectibles", "Stamps", 40000, 100, 0)),
                item(states("debt", "SIDE1"), metrics("Mortgages on real estate", "Family house mortgage", 400000, 100, 0)),
                item(states("debt", "SIDE2"), metrics("Charge/credit cards accounts", "Vacation", 5000, 0, 100)),
                item(states("WrongItemType", "SIDE2"), metrics("noCategory", "noDescription", 10, 0, 100))
        );

        Path enrichedFilePath = agreementFileGenerator.generate(negotiationDocument.getId(), Region.NY);

        assertFile(enrichedFilePath).exists().isReadable();
        assertFileContentDoesNotContain(enrichedFilePath, allNYDocumentPlaceholders());
        assertFileContent(enrichedFilePath)
                .contains(newHashSet(
                        SIDE2_USER_NAME, SIDE1_USER_NAME,
                        "Money to split 100% of 1000",/*Cash*/
                        "Ford Focus Kombi 20% of 20000",/*Automobiles*/
                        "Ford Focus Kombi 80% of 20000",/*Automobiles*/
                        "All Jewelery 100% of 30000", /*Jewelery*/
                        "Two yachts 100% of 200000",/*Boats*/
                        "Stamps 100% of 40000",/*Collectibles*/
                        "Family house mortgage 100% of 400000",/*debts - Mortgage*/
                        "Vacation 100% of 5000"/*debts - Vacation*/))
                .doesNotContain(
                        "noDescription 100% of 10");
        softly.assertAll();
    }

    @Test
    public void generateMyDivorcePaper() throws Exception {
        givenNegotiationItems(
                item(states("asset", "SIDE1"), metrics("Cash", "Money to split", 1000, 100, 0)),
                item(states("asset", "SIDE1"), metrics("Boats", "Luxury yacht", 2500000, 2000000, 500000)),
                item(states("asset", "SIDE2"), metrics("Automobiles", "Ford Focus Kombi", 20000, 20, 80, "will never buy ford")),
                item(states("debt", "SIDE2"), metrics("Charge/credit cards accounts", "Vacation", 5000, 0, 100, 42)),
                item(states("WrongItemType", "SIDE2"), metrics("noCategory", "noDescription", 10, 0, 100))
        );

        Path enrichedFilePath = agreementFileGenerator.generate(negotiationDocument.getId(), Region.MY_DIVORCE);
        InputStream is = Files.newInputStream(enrichedFilePath);

        File targetFile = new File("mdp-export.docx");

        java.nio.file.Files.copy(
                is,
                targetFile.toPath(),
                StandardCopyOption.REPLACE_EXISTING);


        assertFile(enrichedFilePath).exists().isReadable();
        assertFileContentDoesNotContain(enrichedFilePath, allMyDivorceDocumentPlaceholders());
        assertFileContent(enrichedFilePath).contains(newHashSet(SIDE2_USER_NAME, SIDE1_USER_NAME, SIDE1_USER_LOGIN, SIDE2_USER_LOGIN,
                "Cash: Money to split of value $1,000.00",
                "Cash: Money to split of value $1,000.00 is awarded in total",
                "Automobiles: Ford Focus Kombi of value $20,000.00",
                "Automobiles: Ford Focus Kombi of value $20,000.00 is awarded in part 80% which is $16,000.00; spouses agreed also that will never buy ford",
                "(no such items)",
                "Charge/credit cards accounts: Vacation of value $5,000.00",
                "Charge/credit cards accounts: Vacation of value $5,000.00 with monthly payment $42.00 is awarded in total"
        ));
        softly.assertAll();
    }

    private StringAssert assertFileContent(Path enrichedFilePath) throws IOException {
        return softly.assertThat(getContentFromTempFile(enrichedFilePath));
    }

    private PathAssert assertFile(Path enrichedFilePath) {
        return softly.assertThat(enrichedFilePath);
    }

    private void givenNegotiationItems(Document... items) {
        newHashSet(items).forEach(document -> {
            documentOperations.saveDocument(document);

            Relation relation = new Relation();
            relation.setLeftId(document.getId());
            relation.setRightId(negotiationDocument.getId());
            relation.setName("is_item_in");
            relationOperations.storeRelation(relation);
        });
    }

    private Set<String> allMyDivorceDocumentPlaceholders() {
        Set<String> allPlacehodlersInDocument = new HashSet<>();
        allPlacehodlersInDocument.add("${dateTime.of.agreement}");
        allPlacehodlersInDocument.add("${personalData.name.wife}");
        allPlacehodlersInDocument.add("${personalData.name.husband}");
        allPlacehodlersInDocument.add("${personalData.email.wife}");
        allPlacehodlersInDocument.add("${personalData.email.husband}");
        allPlacehodlersInDocument.add("${COMMUNITY.ASSET.[]}");
        allPlacehodlersInDocument.add("${COMMUNITY.DEBT.[]}");
        allPlacehodlersInDocument.add("${SPOUSE_1.ASSET.[]}");
        allPlacehodlersInDocument.add("${SPOUSE_1.DEBT.[]}");
        allPlacehodlersInDocument.add("${SPOUSE_2.ASSET.[]}");
        allPlacehodlersInDocument.add("${SPOUSE_2.DEBT.[]}");
        return allPlacehodlersInDocument;
    }


    private void assertFileContentDoesNotContain(Path path, Set<String> allPlaceholdersInDocument) throws IOException {
        String enrichedContent = getContentFromTempFile(path);
        for (String placeholder : allPlaceholdersInDocument) {
            softly.assertThat(enrichedContent).doesNotContain(placeholder);
        }
    }

    private String getContentFromTempFile(Path enrichedFile) throws IOException {
        XWPFDocument template = new XWPFDocument(Files.newInputStream(enrichedFile));
        return template.getParagraphs().stream().map(p -> p.getText()).collect(Collectors.joining());
    }

    private Set<String> allNYDocumentPlaceholders() {
        Set<String> allPlacehodlersInDocument = new HashSet<>();
        allPlacehodlersInDocument.add("${month.and.day.of.agreement}");
        allPlacehodlersInDocument.add("${year.of.agreement}");
        allPlacehodlersInDocument.add("${personalData.name.wife}");
        allPlacehodlersInDocument.add("${personalData.name.husband}");

        allPlacehodlersInDocument.add("${asset.Collectibles.husband}");
        allPlacehodlersInDocument.add("${asset.Collectibles.wife}");

        allPlacehodlersInDocument.add("${asset.Automobiles.husband}");
        allPlacehodlersInDocument.add("${asset.Automobiles.wife}");

        allPlacehodlersInDocument.add("${asset.Jewelery.husband}");
        allPlacehodlersInDocument.add("${asset.Jewelery.wife}");

        allPlacehodlersInDocument.add("${asset.Cash.husband}");
        allPlacehodlersInDocument.add("${asset.Cash.wife}");


        allPlacehodlersInDocument.add("${debt.Mortgages on real estate.husband}");
        allPlacehodlersInDocument.add("${debt.Mortgages on real estate.wife}");

        allPlacehodlersInDocument.add("${debt.Charge/credit cards accounts.husband}");
        allPlacehodlersInDocument.add("${debt.Charge/credit cards accounts.wife}");


        //default fill in
        allPlacehodlersInDocument.add("${asset.Life insurance.husband}");
        allPlacehodlersInDocument.add("${asset.Life insurance.wife}");

        allPlacehodlersInDocument.add("${asset.Retirement plans.husband}");
        allPlacehodlersInDocument.add("${asset.Retirement plans.wife}");

        allPlacehodlersInDocument.add("${asset.Real estate.husband}");
        allPlacehodlersInDocument.add("${asset.Real estate.wife}");

        allPlacehodlersInDocument.add("${asset.Furniture @ furnishings.husband}");
        allPlacehodlersInDocument.add("${asset.Furniture @ furnishings.wife}");

        allPlacehodlersInDocument.add("${asset.Sporting and entertainment quipment.husband}");
        allPlacehodlersInDocument.add("${asset.Sporting and entertainment quipment.wife}");

        allPlacehodlersInDocument.add("${asset.Stocks/Bonds.husband}");
        allPlacehodlersInDocument.add("${asset.Stocks/Bonds.wife}");

        allPlacehodlersInDocument.add("${asset.Notes/Money owed to you.husband}");
        allPlacehodlersInDocument.add("${asset.Notes/Money owed to you.wife}");

        allPlacehodlersInDocument.add("${asset.Business interests.husband}");
        allPlacehodlersInDocument.add("${asset.Business interests.wife}");

        allPlacehodlersInDocument.add("${asset.Other.husband}");
        allPlacehodlersInDocument.add("${asset.Other.wife}");


        allPlacehodlersInDocument.add("${debt.Auto loan.husband}");
        allPlacehodlersInDocument.add("${debt.Auto loan.wife}");

        allPlacehodlersInDocument.add("${debt.Bank/credit union loans.husband}");
        allPlacehodlersInDocument.add("${debt.Bank/credit union loans.wife}");

        allPlacehodlersInDocument.add("${debt.Money you owe.husband}");
        allPlacehodlersInDocument.add("${debt.Money you owe.wife}");

        allPlacehodlersInDocument.add("${debt.Judgments.husband}");
        allPlacehodlersInDocument.add("${debt.Judgments.wife}");

        allPlacehodlersInDocument.add("${debt.Other.husband}");
        allPlacehodlersInDocument.add("${debt.Other.wife}");

        return allPlacehodlersInDocument;
    }

    private Document givenNegotiationDocument() {
        Map<String, String> states = new HashedMap();
        states.put(DOCUMENT_TYPE.getValue(), "split");
        states.put(MAIN_STATE.getValue(), "open");

        Document document = prepareDocument(states, Maps.newHashMap());
        document.setName("NegotiationDoc");
        document.setIsLeaf(true);

        documentOperations.saveDocument(document);
        return document;
    }

    private Map<String, Object> metrics(String category, String description, Integer value, Integer splitSide1, Integer splitSide2) {
        Map<String, Object> metrics = new HashedMap();
        metrics.put(ITEM_CATEGORY.getValue(), category);
        metrics.put(ITEM_DESCRIPTION.getValue(), description);
        metrics.put(ITEM_VALUE.getValue(), value);
        metrics.put(SPLIT_PERCENT_SIDE1.getValue(), splitSide1);
        metrics.put(SPLIT_PERCENT_SIDE2.getValue(), splitSide2);
        return metrics;
    }

    private Map<String, Object> metrics(String category, String description, Integer value, Integer splitSide1, Integer splitSide2, String splitComment) {
        Map<String, Object> metrics = metrics(category, description, value, splitSide1, splitSide2);
        metrics.put(SPLIT_DESCRIPTION.getValue(), splitComment);
        return metrics;
    }

    private Map<String, Object> metrics(String category, String description, Integer value, Integer splitSide1, Integer splitSide2, Integer monthlyPayment) {
        Map<String, Object> metrics = metrics(category, description, value, splitSide1, splitSide2);
        metrics.put(ITEM_MONTHLY_PAYMENT.getValue(), monthlyPayment);
        return metrics;
    }


    private Map<String, String> states(String itemType, String itemOwner) {
        Map<String, String> states = new HashedMap();
        states.put(ITEM_TYPE.getValue(), itemType);
        states.put(ITEM_OWNER.getValue(), itemOwner);
        states.put(ITEM_DESC_HAS_CHANGES.getValue(), "true");
        states.put(ITEM_VALUE_HAS_CHANGES.getValue(), "true");
        states.put(DOCUMENT_TYPE.getValue(), "item");
        states.put(ITEM_STATE.getValue(), "visible");
        states.put(AGREED_CONTENT_SIDE1.getValue(), "true");
        states.put(AGREED_CONTENT_SIDE2.getValue(), "true");
        states.put(AGREED_OWNER_SIDE1.getValue(), "true");
        states.put(AGREED_OWNER_SIDE2.getValue(), "true");

        return states;
    }

    private Document item(Map<String, String> states, Map<String, Object> metrics) {
        Document document = prepareDocument(states, metrics);
        document.setIsLeaf(true);

        return document;
    }

    private Document prepareDocument(Map<String, String> states, Map<String, Object> metrics) {
        Document document = new Document();
        document.setStates(states);
        document.setMetrics(metrics);
        document.setHasValidMetrics(true);
        document.setRootUuid(UUID.randomUUID().toString());
        document.setSlaUuid(NEGOTIATION_AND_ITEMS_SLA_UUID);
        document.setTeam(TEAM_NAME);
        document.setAuthor(AUTHOR);

        return document;
    }

    private void createSide1Side2UserAndAddThemBothToTheSameTeam() {
        createUser(SIDE1_USER_LOGIN, SIDE1_USER_NAME);
        createUser(SIDE2_USER_LOGIN, SIDE2_USER_NAME);

        User2Team user2TeamSide1 = getUser2Team(SIDE1_USER_LOGIN, UserRole.SIDE1);
        User2Team user2TeamSide2 = getUser2Team(SIDE2_USER_LOGIN, UserRole.SIDE2);

        createTeamWithMembers(TEAM_NAME, Arrays.asList(user2TeamSide1, user2TeamSide2));
    }

    private void createTeamWithMembers(String teamName, List<User2Team> users2Team) {
        Team team = new Team();
        team.setName(teamName);
        team.setUsers2Team(users2Team);
        teamDAO.save(team);
    }

    private User2Team getUser2Team(String side1UserLogin, UserRole side1) {
        User2Team userSide1 = new User2Team();
        userSide1.setLogin(side1UserLogin);
        userSide1.setRole(side1);
        return userSide1;
    }

    private void createUser(String side1UserLogin, String side1UserName) {
        User side1User = new Local();
        side1User.setEmail(side1UserLogin);
        side1User.setName(side1UserName);
        userDAO.save(side1User);
    }

    private void cleanData() {
        documentOperations.removeAll();
        teamDAO.deleteAll(Team.class);
        userDAO.deleteAll(User.class);
    }

}