package com.agreemount.negotiation.agreement.template.docx;

import com.agreemount.negotiation.ApplicationTest;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileSystemException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.String.format;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITDocumentHelperTest {
    private static final String DIR_IT_RESOURCES = "classpath:/";
    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private DocumentHelper documentHelper;

    @Test
    public void shouldCreateXWPFDocumentFromGivenDocxFile() {
        //given
        Path templatePath = Paths.get("template/template.docx");

        //when
        XWPFDocument actualXWPF = documentHelper.readTemplate(templatePath);

        //then
        assertThat(actualXWPF).isNotNull();
        assertThat(actualXWPF.getParagraphs()).isNotNull();
        assertFalse(actualXWPF.getParagraphs().isEmpty());
    }

    @Test
    public void shouldThrowExceptionWhenReadingFileFromIncorrectGivenPath() {
        //given
        String templatePath = "template/fileDoesNotExist.docx";

        //then
        thrown.expect(RuntimeException.class);
        thrown.expectCause(IsInstanceOf.instanceOf(FileNotFoundException.class));
        thrown.expectMessage(format("Can not read template from" +
                " given path %s and create POI Document", templatePath));

        //when
        documentHelper.readTemplate(Paths.get(templatePath));
    }

    @Test
    public void shouldThrowExceptionWhenReadingFileWithIncorrectGivenFormat() {
        //given
        Path templatePath = Paths.get("template/template.txt");

        //then
        thrown.expect(RuntimeException.class);
        thrown.expectCause(IsInstanceOf.instanceOf(InvalidFormatException.class));

        //when
        documentHelper.readTemplate(templatePath);
    }

    @Test
    public void write() throws Exception {
        //given
        File tempFolder = testFolder.newFolder();
        Path documentPath = Paths.get(tempFolder.getPath() + File.separator + "xwpfdocument.docx");
        XWPFDocument document = new XWPFDocument();

        //when
        documentHelper.write(document, documentPath);

        //then
        assertTrue(new File(documentPath.toString()).exists());
    }

    @Test
    public void shouldThrowExceptionWhenWritingToIncorrectPath() throws Exception {
        //given
        File tempFolder = testFolder.newFolder();
        Path documentPath = Paths.get(tempFolder.getPath());
        XWPFDocument document = new XWPFDocument();

        //then
        thrown.expect(RuntimeException.class);
        thrown.expectCause(IsInstanceOf.instanceOf(FileSystemException.class));

        //when
        documentHelper.write(document, documentPath);
    }

}