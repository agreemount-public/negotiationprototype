package com.agreemount.negotiation.agreement.template.docx.sandbox;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.agreement.template.docx.DocumentHelper;
import com.agreemount.negotiation.agreement.template.replacer.PlaceholderReplacer;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITDocxServiceTest {

    private static final Path PATH = Paths.get("emptyDocx.docx");
    private static final Path TEMPLATE_PATH = Paths.get("template/template.docx");
    private static final Path TEMPLATE_WITH_APPENDED_TEXT_PATH = Paths.get("templateWithAppendedText.docx");
    private static final Path TEMPLATE_WITH_FILLED_IN_PLACEHOLDERS_PATH = Paths.get("templateWithFilledInPlaceholders.docx");

    @Autowired
    private DocxService docxService;

    @Autowired
    private DocumentHelper documentHelper;

    @Autowired
    private Appender appender;

    @Autowired
    private PlaceholderReplacer replacePlaceholder;


    @AfterClass
    public static void cleanUp() throws IOException {
        Files.deleteIfExists(PATH);
    }

    @BeforeClass
    public static void setUp() throws IOException {
        Files.deleteIfExists(TEMPLATE_WITH_APPENDED_TEXT_PATH);
        Files.deleteIfExists(TEMPLATE_WITH_FILLED_IN_PLACEHOLDERS_PATH);
    }


    @Test
    public void shouldCreateAndSaveEmptyDocument() {

        XWPFDocument document = docxService.createEmpty();
        documentHelper.write(document, PATH);

        boolean isSaved = Files.exists(PATH);
        assertTrue(isSaved);
    }

    @Test
    public void shouldAppendTextToTemplateAndSaveAsNewDocxFile() {

        XWPFDocument templateDocument = documentHelper.readTemplate(TEMPLATE_PATH);
        XWPFDocument document = appender.appendText("This is new text that I want to append. " +
                "It should be bold with font size 12, placed in Heading1 styled paragraph", templateDocument);
        documentHelper.write(document, TEMPLATE_WITH_APPENDED_TEXT_PATH);

        assertTrue(Files.exists(TEMPLATE_WITH_APPENDED_TEXT_PATH));
    }

    @Test
    public void shouldReplacePlaceholdersInTemplate() {
        Map<String, Object> placeholderValueMap = new HashMap<>();
        placeholderValueMap.put("$_SIDE_2_ADDRESS", "Agreemount Sp. Z O.O., Kraków ul. Zielona 12");
        placeholderValueMap.put("$_DATE", ZonedDateTime.now().toString());
        placeholderValueMap.put("$_SIDE_1_NAME", "Adam Side1");
        placeholderValueMap.put("$_SIDE_2_NAME", "Ewa Side2");

        XWPFDocument document = documentHelper.readTemplate(TEMPLATE_PATH);
        XWPFDocument filledInDocument = replacePlaceholder.replace(placeholderValueMap, document);
        documentHelper.write(filledInDocument, TEMPLATE_WITH_FILLED_IN_PLACEHOLDERS_PATH);

        assertTrue(Files.exists(TEMPLATE_WITH_FILLED_IN_PLACEHOLDERS_PATH));
    }

}
