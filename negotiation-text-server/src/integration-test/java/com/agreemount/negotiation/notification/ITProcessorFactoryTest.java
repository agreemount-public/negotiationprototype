package com.agreemount.negotiation.notification;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.notification.processors.AddSideProcessor;
import com.agreemount.negotiation.notification.processors.DefaultProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITProcessorFactoryTest {

    @Autowired
    private ProcessorFactory processorFactory;

    @Test
    public void givenNotificationWithAddMemberTypeThenReturnAddMemberProcessor() {
        Notification notification = new NotificationBuilder()
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), "bob@mailinator.com"))
                .addParameter(new Notification.Parameter("Another", "second arh"))
                .setNotificationStatus(NotificationStatus.FAILED)
                .setNotificationType(NotificationType.ADD_SIDE)
                .build();


        ProcessorTemplate processor = processorFactory.createProcessor(notification);
        assertThat(processor, instanceOf(AddSideProcessor.class));
    }

    @Test
    public void givenNotificationWithTestTypeThenReturnDefaultProcessor() {
        Notification notification = new NotificationBuilder()
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), "bob@mailinator.com"))
                .addParameter(new Notification.Parameter("Another", "second arh"))
                .setNotificationStatus(NotificationStatus.FAILED)
                .setNotificationType(NotificationType.TEST)
                .build();


        ProcessorTemplate processor = processorFactory.createProcessor(notification);
        assertThat(processor, instanceOf(DefaultProcessor.class));
    }
}
