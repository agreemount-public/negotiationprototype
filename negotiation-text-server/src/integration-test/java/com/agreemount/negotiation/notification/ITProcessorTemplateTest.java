package com.agreemount.negotiation.notification;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.notification.*;
import com.agreemount.negotiation.dao.IGeneratedNotificationDAO;
import com.agreemount.negotiation.dao.INotificationDAO;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.engine.extension.action.impl.ITSendEmailImplTest;
import com.agreemount.slaneg.db.DocumentOperations;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITProcessorTemplateTest {

    private static final String EMAIL_TITLE = "fancy topic";
    private static final String AUTHOR = "author@mailinator.com";
    private static final String PAGE_TITLE = "head-topic";
    private static final String EMAIL_INFO = "The most popular design pattern is big ball of mud";
    private static final String RECEIVER = "bob@builder.com";
    private static final String SIDE_USER = "SIDE1";
    private static final String NOTIFI_ID = "id123";
    private static final String DOCUMENT_NAME = "mydoc";
    private static final String DOCUMENT_ID = "123qwe456";
    private static final String MY_TEAM_NAME = "myTeamName";
    private static final String SIDE_USER_LOGIN = "side@user.com";
    private static final String NOT_EXISTING_SITE = "SIDE2";
    private static final String NEGOTIATION_ID = "NEGOTIATION_ID";
    private static final String SLA_UUID = "g5g45g5g5";

    @Autowired
    private ProcessorFactory processorFactory;

    @Autowired
    private IGeneratedNotificationDAO generatedNotificationDAO;

    @Autowired
    private INotificationDAO notificationDAO;

    @Autowired
    private ITeamDAO teamDAO;

    @Autowired
    private DocumentOperations documentOperations;


    @Before
    public void setUp() {
        cleanData();
        prepareDocument();
        prepareParentDocument();
        prepareTeam();
    }

    @After
    public void tearDown() {
        cleanData();
    }

    @Test
    public void givenNotificationWithoutCorrelatedDocumentThenThrowException() {
        Notification notification = getTempNotification(NotificationType.ADD_SIDE, true);
        notification.getParameters().add(new Notification.Parameter("documentId", "123incorrectId456"));
        processorFactory.createProcessor(notification).processNotification(notification);

        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();

        assertThat(emailToSend).isNull();

        notification = notificationDAO.getById(notification.getId());

        assertThat(notification.getNotificationStatus()).isEqualTo(NotificationStatus.FAILED);
        assertThat(notification.getMessage()).contains("document not found: ");
    }

    @Test
    public void givenNotificationWhenRequiredParamIsMissingThenThrowExceptionAndSaveMsgInDB() {
        Notification notification = getTempNotification(NotificationType.ADD_SIDE, true);
        processorFactory.createProcessor(notification).processNotification(notification);

        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();

        assertThat(emailToSend).isNull();

        notification = notificationDAO.getById(notification.getId());

        assertThat(notification.getNotificationStatus()).isEqualTo(NotificationStatus.FAILED);
        assertThat(notification.getMessage().contains("required parameter is missing [documentId]"));
    }

    @Test
    public void givenNotificationWithMissingViewWhenProcessingThenSaveError() {
        Notification notification = getTempNotification(NotificationType.TEST, false);
        processorFactory.createProcessor(notification).processNotification(notification);

        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();
        assertThat(emailToSend).isNull();

        Notification notificationToRetrieve = notificationDAO.getNext();
        assertThat(notificationToRetrieve).isNull();

        Notification notifi = notificationDAO.getById(NOTIFI_ID);
        assertThat(notifi).isNotNull();
        assertThat(notifi.getNotificationStatus()).isEqualTo(NotificationStatus.FAILED);
        assertThat(notifi.getMessage()).contains("Unable to find resource");
    }

    @Test
    public void givenNotificationWithoutTitleWhenProcessingThenSetTitleBasedOnContentWithoutTags() {
        Notification notification = getTempNotification(NotificationType.ADD_SIDE, false);
        notification.getParameters().add(new Notification.Parameter("documentId", DOCUMENT_ID));
        processorFactory.createProcessor(notification).processNotification(notification);

        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();

        String actualContent = emailToSend.getPreparedEmail().getContent();
        String actualTitle = emailToSend.getPreparedEmail().getTitle();
        String expectedEmail = "<html>\n" +
                "<head><title>head-topic</title></head>\n" +
                "<body>The most popular design pattern is big ball of mud</body>\n" +
                "<div>Document Name = mydoc</div>\n" +
                "<div class=\"linkToNegotiation\">linkToNegotiation: http://localhost:8090/#/negotiation/NEGOTIATION_ID</div>\n" +
                "</html>";

        assertThat(actualContent).isEqualTo(expectedEmail);
        assertThat(actualTitle).contains("head-topic The most popu");
    }

    @Test
    public void shouldThrowExceptionWhenBothReceiverAndReceiverSideAreAbsent() {
        Notification notification = getNotificationWithReceiver(null, null);

        processorFactory.createProcessor(notification).processNotification(notification);

        notification = notificationDAO.getById(notification.getId());
        assertThat(notification.getNotificationStatus()).isEqualTo(NotificationStatus.FAILED);
        assertThat(notification.getMessage()).contains("Either receiver or receiverSide is required");
    }

    @Test
    public void shouldThrowExceptionWhenBothReceiverAndReceiverSideArePresent() {
        Notification notification = getNotificationWithReceiver(RECEIVER, SIDE_USER);

        processorFactory.createProcessor(notification).processNotification(notification);

        notification = notificationDAO.getById(notification.getId());
        assertThat(notification.getNotificationStatus()).isEqualTo(NotificationStatus.FAILED);
        assertThat(notification.getMessage()).contains("Cannot set both: receiver and receiverSide");
    }

    @Test
    public void shouldConvertNotificationWithCustomParameterToProperHtml() {
        Notification notification = getTempNotification(NotificationType.NEW_DOCUMENT, true);
        notification.getParameters().add(new Notification.Parameter("documentId", DOCUMENT_ID));

        processorFactory.createProcessor(notification).processNotification(notification);

        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();
        assertThat(emailToSend.getPreparedEmail().getContent()).contains("FiRsT_sec0nd_thir8");
    }

    @Test
    public void givenCCAndBCCShouldBeSaveInGeneratedMail() {
        Notification notification = getTempNotificationWithBccAndCC();
        notification.getParameters().add(new Notification.Parameter("documentId", DOCUMENT_ID));

        processorFactory.createProcessor(notification).processNotification(notification);

        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();
        assertThat(Arrays.asList(ITSendEmailImplTest.BCC_1)).isEqualTo(emailToSend.getPreparedEmail().getBcc());
        assertThat(Arrays.asList(ITSendEmailImplTest.CC_1, ITSendEmailImplTest.CC_2)).isEqualTo(emailToSend.getPreparedEmail().getCc());
    }

    @Test
    public void shouldProcessMessageWithProperlyDefinedReceiverSide() {
        Notification notification = getNotificationWithReceiver(null, SIDE_USER);

        processorFactory.createProcessor(notification).processNotification(notification);

        notification = notificationDAO.getById(notification.getId());
        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();

        assertThat(notification.getNotificationStatus()).isEqualTo(NotificationStatus.PROCESSED);
        assertThat(notification.getParameters()).contains(new Notification.Parameter("receiverSide", SIDE_USER));
        assertThat(emailToSend.getPreparedEmail().getReceiver()).contains(SIDE_USER_LOGIN);
    }

    @Test
    public void shouldSetHostParamProperly() {
        Notification notification = getNotificationWithReceiver(null, SIDE_USER);
        processorFactory.createProcessor(notification).processNotification(notification);

        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();

        assertThat(emailToSend.getPreparedEmail().getContent()).contains("http://localhost:8090");
    }

    @Test
    public void shouldThrowExceptionWhenReceiverSideNotFound() {
        Notification notification = getNotificationWithReceiver(null, NOT_EXISTING_SITE);

        processorFactory.createProcessor(notification).processNotification(notification);

        notification = notificationDAO.getById(notification.getId());
        assertThat(notification.getNotificationStatus()).isEqualTo(NotificationStatus.FAILED);
        assertThat(notification.getMessage()).contains("User for side [" + NOT_EXISTING_SITE + "] not found");
    }

    @Test
    public void shouldThrowExceptionWhenReceiverSideIsDefinedIncorrectly() {
        Notification notification = getNotificationWithReceiver(null, "U_n_D_e_F_i_N_e_D");

        processorFactory.createProcessor(notification).processNotification(notification);

        notification = notificationDAO.getById(notification.getId());
        assertThat(notification.getNotificationStatus()).isEqualTo(NotificationStatus.FAILED);
        assertThat(notification.getMessage()).contains("No enum constant");
    }

    @Test
    public void shouldGenerateResetPasswordConfirmationNotification() {
        Notification confirmationNotification = new NotificationBuilder()
                .setNotificationType(NotificationType.RESET_PASSWORD_CONFIRMATION)
                .addParameter(new Notification.Parameter("title", "[Agreemount] Reset your password"))
                .addParameter(new Notification.Parameter("confirmationHash", DOCUMENT_ID))
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), RECEIVER))
                .build();

        processorFactory.createProcessor(confirmationNotification).processNotification(confirmationNotification);


        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();

        assertThat(emailToSend.getPreparedEmail().getContent()).contains("Reset your password");
        assertThat(emailToSend.getPreparedEmail().getTitle()).isEqualTo("[Agreemount] Reset your password");
    }


    @Test
    public void shouldGenerateResetPasswordNotification() {
        Notification confirmationNotification = new NotificationBuilder()
                .setNotificationType(NotificationType.RESET_PASSWORD)
                .addParameter(new Notification.Parameter("title", "[Agreemount] Your password has been changed"))
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), RECEIVER))
                .build();

        processorFactory.createProcessor(confirmationNotification).processNotification(confirmationNotification);


        GeneratedNotification emailToSend = generatedNotificationDAO.getNext();

        assertThat(emailToSend.getPreparedEmail().getContent()).contains("Your password has been changed");
        assertThat(emailToSend.getPreparedEmail().getTitle()).isEqualTo("[Agreemount] Your password has been changed");
    }

    private Notification getTempNotificationWithBccAndCC() {
        Notification notification = getTempNotification(NotificationType.NEW_DOCUMENT, true);
        notification.getParameters().add(new Notification.Parameter("cc",
                Arrays.asList(ITSendEmailImplTest.CC_1, ITSendEmailImplTest.CC_2)));
        notification.getParameters().add(new Notification.Parameter("bcc",
                Arrays.asList(ITSendEmailImplTest.BCC_1)));
        return notification;
    }

    private Notification getTempNotification(NotificationType notificationType, Boolean setTitle) {
        NotificationBuilder notificationBuilder = new NotificationBuilder()
                .setNotificationStatus(NotificationStatus.NEW)
                .setNotificationType(notificationType)
                .setId(NOTIFI_ID)
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), RECEIVER))
                .addParameter(new Notification.Parameter("info", EMAIL_INFO))
                .addParameter(new Notification.Parameter("pageTitle", PAGE_TITLE))
                .addParameter(new Notification.Parameter("customParameters", Arrays.asList("FiRsT", "sec0nd", "thir8")));

        if (setTitle) {
            notificationBuilder.addParameter(new Notification.Parameter(Notification.Parameter.Keys.TITLE.getValue(), EMAIL_TITLE));
        }

        Notification notification = notificationBuilder.build();

        notificationDAO.save(notification);
        return notification;
    }

    private Notification getNotificationWithReceiver(String receiver, String receiverSide) {
        return new NotificationBuilder()
                .setNotificationType(NotificationType.ADD_SIDE)
                .addParameter(new Notification.Parameter("info", EMAIL_INFO))
                .addParameter(new Notification.Parameter("pageTitle", PAGE_TITLE))
                .addParameter(new Notification.Parameter("documentId", DOCUMENT_ID))
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), receiver))
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER_SIDE.getValue(), receiverSide))
                .build();
    }

    private void prepareDocument() {
        Document document = new Document();
        document.setId(DOCUMENT_ID);
        document.setName(DOCUMENT_NAME);
        document.setAuthor(AUTHOR);
        document.setTeam(MY_TEAM_NAME);
        document.setSlaUuid(SLA_UUID);
        documentOperations.saveDocument(document);
    }

    private void prepareTeam() {
        List<User2Team> records = new ArrayList<>();
        User2Team user = new User2Team();
        user.setLogin(SIDE_USER_LOGIN);
        user.setRole(UserRole.SIDE1);
        records.add(user);
        Team team = new Team();
        team.setName(MY_TEAM_NAME);
        team.setUsers2Team(records);
        teamDAO.save(team);
    }

    public void cleanData() {
        documentOperations.removeAll();
        generatedNotificationDAO.deleteAll(GeneratedNotification.class);
        notificationDAO.deleteAll(Notification.class);
        teamDAO.deleteAll(Team.class);
    }

    private void prepareParentDocument() {
        Document document = new Document();
        document.setId(NEGOTIATION_ID);
        document.setIsLeaf(true);
        document.setSlaUuid("g5g45g5g5");
        document.setState("documentType", "split");

        documentOperations.saveDocument(document);
    }

}
