package com.agreemount.negotiation.engine.extension.action.impl.payment;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.logic.ActionLogic;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.UserPaymentUnitsEntry;
import com.agreemount.negotiation.bean.providers.Facebook;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.agreemount.negotiation.engine.extension.action.definition.payment.ChangeAccountBalanceDef;
import com.agreemount.slaneg.action.ActionContextFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITChangeAccountBalanceTest {

    public static final Long PAYMENT_UNITS_500 = 500L;
    public static final Long PAYMENT_UNITS_NEGATIVE_750 = -750L;
    public static final Long PAYMENT_UNITS_1000 = 1000L;
    public static final String USER_EMAIL_1 = "user1";
    public static final String DOCUMENT_ID = "123";
    public static final String DESC_1 = "desc1";
    public static final String DESC_2 = "desc2";

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private IdentityProvider identityProvider;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ActionLogic actionLogic;

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenIdentityProviderIsNotInitialized() {
        // given
        when(identityProvider.getIdentity()).thenReturn(null);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_NEGATIVE_750));
        ChangeAccountBalanceDef changeBalance = getChangeBalanceDef(PAYMENT_UNITS_500);

        // when
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), changeBalance);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenUserIsNotLogged() {
        // given
        setUser(null);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_NEGATIVE_750));
        ChangeAccountBalanceDef changeBalance = getChangeBalanceDef(PAYMENT_UNITS_500);

        // when
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), changeBalance);
    }


    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenPaymentUnitsIsNotGiven() {
        // given
        setUser(USER_EMAIL_1);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_NEGATIVE_750));
        ChangeAccountBalanceDef changeBalance = getChangeBalanceDef(null);

        // when
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), changeBalance);
    }

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhenUserNotFoundInDB() {
        // given
        setUser(USER_EMAIL_1);
        ChangeAccountBalanceDef changeBalance = getChangeBalanceDef(PAYMENT_UNITS_500);

        // when
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), changeBalance);
    }

    @Test
    public void shouldAddFundsToUserAccountBalance() {
        // given
        setUser(USER_EMAIL_1);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_1000));
        ChangeAccountBalanceDef changeBalance = getChangeBalanceDef(PAYMENT_UNITS_500);

        // when
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), changeBalance);

        // then
        User userFromDB = userDAO.getByEmail(USER_EMAIL_1);
        assertThat(userFromDB.getPaymentUnits()).isEqualTo(1500L);
    }

    @Test
    public void shouldSubtractFundsFromUserAccountBalance() {
        // given
        setUser(USER_EMAIL_1);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_500));
        ChangeAccountBalanceDef changeBalance = getChangeBalanceDef(PAYMENT_UNITS_NEGATIVE_750);

        // when
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), changeBalance);

        // then
        User userFromDB = userDAO.getByEmail(USER_EMAIL_1);
        assertThat(userFromDB.getPaymentUnits()).isEqualTo(-250L);
    }

    @Test
    public void shouldPerformProperOperationWhenUserHasNotDefinedFundsFromUserAccountBalance() {
        // given
        setUser(USER_EMAIL_1);
        userDAO.save(getUser(USER_EMAIL_1, null));
        ChangeAccountBalanceDef changeBalance = getChangeBalanceDef(PAYMENT_UNITS_NEGATIVE_750);

        // when
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), changeBalance);

        // then
        User userFromDB = userDAO.getByEmail(USER_EMAIL_1);
        assertThat(userFromDB.getPaymentUnits()).isEqualTo(-750L);
    }

    @Test
    public void shouldSavePaymentsUnitEntriesWhileChangingAccountBalance() {
        // given
        setUser(USER_EMAIL_1);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_1000));

        // when
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), getChangeBalanceDef(PAYMENT_UNITS_500, DESC_1));
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(DOCUMENT_ID)), getChangeBalanceDef(PAYMENT_UNITS_NEGATIVE_750));
        actionLogic.run(actionContextFactory.createInstance(prepareDocument(null)), getChangeBalanceDef(PAYMENT_UNITS_1000, DESC_2));

        // then
        // then
        User userFromDB = userDAO.getByEmail(USER_EMAIL_1);
        assertThat(userFromDB.getPaymentUnitEntries()).usingElementComparatorIgnoringFields("createdAt").containsExactly(
                getUserPaymentEntry(1000L, 500L, 1500L, DOCUMENT_ID, DESC_1),
                getUserPaymentEntry(1500L, -750L, 750L, DOCUMENT_ID, null),
                getUserPaymentEntry(750L, 1000L, 1750L, null, DESC_2)
        );
    }

    private UserPaymentUnitsEntry getUserPaymentEntry(Long before, Long requested, Long after, String documentId, String desc) {
        UserPaymentUnitsEntry firstEntry = new UserPaymentUnitsEntry();
        firstEntry.setCreatedAt(LocalDateTime.now());
        firstEntry.setDescription(desc);
        firstEntry.setPaymentUnitsBefore(before);
        firstEntry.setRequestedAmount(requested);
        firstEntry.setPaymentUnitsAfter(after);
        firstEntry.setDocumentId(documentId);
        return firstEntry;
    }

    private ChangeAccountBalanceDef getChangeBalanceDef(Long paymentUnits, String description) {
        ChangeAccountBalanceDef changeAccountBalanceDef = new ChangeAccountBalanceDef();
        changeAccountBalanceDef.setPaymentUnits(paymentUnits);
        changeAccountBalanceDef.setDescription(description);
        return changeAccountBalanceDef;
    }

    private ChangeAccountBalanceDef getChangeBalanceDef(Long paymentUnits) {
        return getChangeBalanceDef(paymentUnits, null);
    }

    private Document prepareDocument(String documentId) {
        Document document = new Document();
        document.setId(documentId);
        document.setName("myTestDoc");
        return document;
    }

    private User getUser(String mail, Long units) {
        User user = new Facebook();
        user.setEmail(mail);
        user.setPaymentUnits(units);
        return user;
    }

    private void setUser(String login) {
        Identity identity = new Identity();
        identity.setLogin(login);
        when(identityProvider.getIdentity()).thenReturn(identity);
    }

    @Before
    @After
    public void cleanData() {
        mongoTemplate.remove(new Query(), User.class);
    }

}