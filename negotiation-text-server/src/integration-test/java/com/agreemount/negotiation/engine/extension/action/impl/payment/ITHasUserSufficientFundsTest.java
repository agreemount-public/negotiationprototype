package com.agreemount.negotiation.engine.extension.action.impl.payment;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.logic.ConstraintLogic;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.providers.Facebook;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.agreemount.negotiation.engine.extension.action.definition.payment.HasUserSufficientFundsDef;
import com.agreemount.slaneg.action.ActionContextFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITHasUserSufficientFundsTest {

    public static final Long PAYMENT_UNITS_500 = 500L;
    public static final Long PAYMENT_UNITS_750 = 750L;
    public static final Long PAYMENT_UNITS_1000 = 1000L;
    public static final String USER_EMAIL_1 = "user1";

    @Autowired
    private ConstraintLogic constraintLogic;

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private IdentityProvider identityProvider;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenIdentityProviderIsNotInitialized() {
        // given
        when(identityProvider.getIdentity()).thenReturn(null);
        HasUserSufficientFundsDef constraint = getIsPaymentApplicableDef(PAYMENT_UNITS_750);

        // when
        constraintLogic.isAvailable(constraint, actionContextFactory.createInstance(prepareDocument()));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenUserIsNotLogged() {
        // given
        setUser(null);
        HasUserSufficientFundsDef constraint = getIsPaymentApplicableDef(PAYMENT_UNITS_750);

        // when
        constraintLogic.isAvailable(constraint, actionContextFactory.createInstance(prepareDocument()));
    }


    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenPaymentUnitsIsNotGiven() {
        // given
        HasUserSufficientFundsDef constraint = getIsPaymentApplicableDef(null);

        // when
        constraintLogic.isAvailable(constraint, actionContextFactory.createInstance(prepareDocument()));
    }

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhenUserNotFoundInDB() {
        // given
        setUser(USER_EMAIL_1);
        HasUserSufficientFundsDef constraint = getIsPaymentApplicableDef(PAYMENT_UNITS_750);

        // when
        constraintLogic.isAvailable(constraint, actionContextFactory.createInstance(prepareDocument()));
    }

    @Test
    public void shouldReturnTrueWhenUserFundsEqualToGivenUnits() {
        // given
        setUser(USER_EMAIL_1);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_750));
        HasUserSufficientFundsDef constraint = getIsPaymentApplicableDef(PAYMENT_UNITS_750);

        // when
        Boolean sufficientFunds = constraintLogic.isAvailable(constraint, actionContextFactory.createInstance(prepareDocument()));

        // then
        assertThat(sufficientFunds).isTrue();
    }

    @Test
    public void shouldReturnTrueWhenUserHaveSufficientPaymentsUnitsThanGiven() {
        // given
        setUser(USER_EMAIL_1);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_1000));
        HasUserSufficientFundsDef constraint = getIsPaymentApplicableDef(PAYMENT_UNITS_750);

        // when
        Boolean sufficientFunds = constraintLogic.isAvailable(constraint, actionContextFactory.createInstance(prepareDocument()));

        // then
        assertThat(sufficientFunds).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenUserHaveInsufficientPaymentsUnitsThanGiven() {
        // given
        setUser(USER_EMAIL_1);
        userDAO.save(getUser(USER_EMAIL_1, PAYMENT_UNITS_500));
        HasUserSufficientFundsDef constraint = getIsPaymentApplicableDef(PAYMENT_UNITS_750);

        // when
        Boolean sufficientFunds = constraintLogic.isAvailable(constraint, actionContextFactory.createInstance(prepareDocument()));

        // then
        assertThat(sufficientFunds).isFalse();
    }

    private HasUserSufficientFundsDef getIsPaymentApplicableDef(Long paymentUnits) {
        HasUserSufficientFundsDef constraint = new HasUserSufficientFundsDef();
        constraint.setPaymentUnits(paymentUnits);
        return constraint;
    }

    private Document prepareDocument() {
        Document document = new Document();
        document.setId("123");
        document.setName("myTestDoc");
        return document;
    }

    private User getUser(String mail, Long units) {
        User user = new Facebook();
        user.setEmail(mail);
        user.setPaymentUnits(units);
        return user;
    }

    private void setUser(String login) {
        Identity identity = new Identity();
        identity.setLogin(login);
        when(identityProvider.getIdentity()).thenReturn(identity);
    }

    @Before
    @After
    public void cleanData() {
        mongoTemplate.remove(new Query(), User.class);
    }

}