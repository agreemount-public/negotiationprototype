package com.agreemount.negotiation.engine.extension.action.impl;

import com.agreemount.bean.document.Document;
import com.agreemount.logic.ActionLogic;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.INotificationDAO;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.assertNotNull;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITSendEmailImplTest {

    public static final String ID_DOC = "123";
    public static final String BCC_1 = "frodo@mailinator.com";
    public static final String BCC_2 = "sauron@mailinator.com";
    public static final String CC_1 = "saruman@mailinator.com";
    public static final String CC_2 = "faramir@mailinator.com";
    public static final String CUSTOM_1 = "first custom param";
    public static final String CUSTOM_2 = "second custom param";
    public static final String CUSTOM_3 = "third custom param";
    public static final String RECEIVER = "bobbuilder@mailinator.com";
    public static final String RECEIVER_SIDE = "SIDE1";
    public static final String TITLE = "Bob Builder has started building";
    public static final String TEST_ALIAS = "testAlias";

    public static final String REQUEST_WITH_MIN_PARAMS = "testReq";
    public static final String REQUEST_WITH_ALL_PARAMS = "testReqAllParams";
    public static final String REQUEST_WITH_INVALID_ALIAS = "testReqInvalidAlias";
    public static final String REQUEST_WITH_WRONG_NOTIFI_TYPE = "testReqWrongType";

    @Autowired
    private INotificationDAO notificationDAO;

    @Autowired
    private ActionLogic actionLogic;

    @Test
    public void shouldCreateProperNotificationWhenYamlRuleTriggerSendAction() {
        // given
        Document document = prepareDocument();

        // when
        actionLogic.runAction(document, TEST_ALIAS, REQUEST_WITH_MIN_PARAMS);

        // then
        Notification savedNotification = notificationDAO.getNext();

        assertNotNull(savedNotification);
        assertThat(savedNotification.getParameters(), hasParams("documentId", ID_DOC));
        assertEquals(NotificationType.NEW_DOCUMENT, savedNotification.getNotificationType());
        assertEquals(NotificationStatus.NEW, savedNotification.getNotificationStatus());

        Set<Notification.Parameter> parameters = savedNotification.getParameters();
        assertFalse(hasParam(parameters, "cc"));
        assertFalse(hasParam(parameters, "bcc"));
        assertFalse(hasParam(parameters, "customParameters"));
        assertFalse(hasParam(parameters, "title"));
        assertFalse(hasParam(parameters, "receiver"));
    }

    @Test
    public void shouldCreateNotificationWithAdditionalDataWhenYamlGeneratorTriggerSendAction() {
        // given
        Document document = prepareDocument();

        //when
        actionLogic.runAction(document, TEST_ALIAS, REQUEST_WITH_ALL_PARAMS);

        // then
        Notification savedNotification = notificationDAO.getNext();

        assertNotNull(savedNotification);
        assertThat(savedNotification.getParameters(), hasParams("documentId", ID_DOC));
        assertEquals(NotificationType.NEW_DOCUMENT, savedNotification.getNotificationType());
        assertEquals(NotificationStatus.NEW, savedNotification.getNotificationStatus());

        assertThat(savedNotification.getParameters(), hasParams("cc", Arrays.asList(CC_1, CC_2)));
        assertThat(savedNotification.getParameters(), hasParams("bcc", Arrays.asList(BCC_1, BCC_2)));
        assertThat(savedNotification.getParameters(), hasParams("customParameters", Arrays.asList(CUSTOM_1, CUSTOM_2, CUSTOM_3)));
        assertThat(savedNotification.getParameters(), hasParams("title", TITLE));
        assertThat(savedNotification.getParameters(), hasParams("receiver", RECEIVER));
        assertThat(savedNotification.getParameters(), hasParams("receiverSide", RECEIVER_SIDE));
    }

    @Test
    public void shouldNotCreateNotificationWhenAliasIsIncorrect() {
        // given
        Document document = prepareDocument();

        // when
        actionLogic.runAction(document, TEST_ALIAS, REQUEST_WITH_INVALID_ALIAS);

        // then
        assertNull(notificationDAO.getNext());
    }

    @Test
    public void shouldNotCreateNotificationWhenNotificationTypeIsIncorrect() {
        // given
        Document document = prepareDocument();

        //when
        actionLogic.runAction(document, TEST_ALIAS, REQUEST_WITH_WRONG_NOTIFI_TYPE);

        // then
        assertNull(notificationDAO.getNext());
    }

    private Document prepareDocument() {
        Document document = new Document();
        document.setId(ID_DOC);
        document.setName("myTestDoc");
        return document;
    }

    private Matcher<Iterable<Notification.Parameter>> hasParams(String key, List<String> values) {
        return hasItems(new Notification.Parameter(key, values));
    }

    private Matcher<Iterable<Notification.Parameter>> hasParams(String key, String value) {
        return hasItems(new Notification.Parameter(key, value));

    }

    private boolean hasParam(Set<Notification.Parameter> parameters, String key) {
        return parameters.stream().anyMatch(parameter -> parameter.getKey().equals(key));
    }

    @Before
    public void beforeEachMethod() {
        notificationDAO.deleteAll(Notification.class);
    }
}
