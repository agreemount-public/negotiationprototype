package com.agreemount.negotiation.engine.extension.action.impl;

import com.agreemount.bean.document.Document;
import com.agreemount.logic.ActionLogic;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.db.DocumentOperations;
import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.util.Maps.newHashMap;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITBumpUpSequenceTest {

    private static final String ID_DOC = "123";
    private static final String MAIN_DOC_ALIAS = "testAlias";
    private static final String ACTION_ID = "testCreateChangeAndUpdateSequence";
    private static final String MAIN_DOC_METRIC_SEQUENCE_KEY = "maxSequenceValue";
    private static final String CHANGE_DOC_ALIAS = "doc1";
    private static final String CHANGE_DOC_METRIC_SEQUENCE_KEY = "sequenceValue";
    private Set<String> changeDocumentIds = new HashSet<>();

    @Autowired
    private ActionLogic actionLogic;

    @Autowired
    private DocumentOperations documentOperations;

    @Before
    public void setUp() throws Exception {
        documentOperations.removeAll();
    }

    @Test
    public void shouldInitMaxSequence() {
        String rootDocumentName = "myTestDoc";
        Document rootDocument = prepareDocument(rootDocumentName, new HashedMap());

        ActionContext actionContext = actionLogic.runAction(rootDocument, MAIN_DOC_ALIAS, ACTION_ID);

        Document mainDocument = fetchMainDocument(actionContext);
        assertSequenceValue(mainDocument, MAIN_DOC_METRIC_SEQUENCE_KEY, 1);


        Document changeDocument = fetchChangeDocument(actionContext);
        assertSequenceValue(changeDocument, CHANGE_DOC_METRIC_SEQUENCE_KEY, 0);
    }

    @Test
    public void shouldUseSequenceAndBumpUpMaxSequence() {
        int maxSequenceValue = 123;
        String rootDocumentName = "myTestDoc";
        Document rootDocument = prepareDocument(rootDocumentName,
                newHashMap(MAIN_DOC_METRIC_SEQUENCE_KEY, maxSequenceValue));

        ActionContext actionContext = actionLogic.runAction(rootDocument, MAIN_DOC_ALIAS, ACTION_ID);

        Document mainDocument = fetchMainDocument(actionContext);
        assertSequenceValue(mainDocument, MAIN_DOC_METRIC_SEQUENCE_KEY, maxSequenceValue + 1);


        Document changeDocument = fetchChangeDocument(actionContext);
        assertSequenceValue(changeDocument, CHANGE_DOC_METRIC_SEQUENCE_KEY, maxSequenceValue);
    }

    private void assertSequenceValue(Document document, String sequenceKey, int sequenceValue) {
        assertThat(document.getMetrics().keySet()).contains(sequenceKey);
        assertThat(document.getMetrics().get(sequenceKey)).isEqualTo(sequenceValue);
    }

    private Document fetchChangeDocument(ActionContext actionContext) {
        return documentOperations.getDocument(actionContext.getDocument(CHANGE_DOC_ALIAS).getId());
    }

    private Document fetchMainDocument(ActionContext actionContext) {
        return documentOperations.getDocumentByName(actionContext.getDocument(MAIN_DOC_ALIAS).getName());
    }

    private Document prepareDocument(String documentName, Map<String, Object> metrics) {
        Document document = new Document();
        document.setId(ID_DOC);
        document.setName(documentName);
        document.setMetrics(metrics);
        return document;
    }

}
