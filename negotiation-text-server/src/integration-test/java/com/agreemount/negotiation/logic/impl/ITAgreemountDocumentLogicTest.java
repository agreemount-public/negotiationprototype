package com.agreemount.negotiation.logic.impl;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.slaneg.db.DocumentOperations;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITAgreemountDocumentLogicTest {

    @Autowired
    private DocumentOperations documentOperations;

    @Autowired
    private AgreemountDocumentLogic agreemountDocumentLogic;

    @Autowired
    private IdentityProvider identityProvider;

    @Test
    public void shouldReturnNewDocument() {
        Identity identity = new Identity();
        identity.setLogin("plgbob");
        when(identityProvider.getIdentity()).thenReturn(identity);

        String newId = agreemountDocumentLogic.saveEmptyDocument("split");
        Document document = documentOperations.getDocument(newId);
        assertNotNull(document);
        assertNotNull(document.getId());
        assertNotNull(document.getName());
    }


}
