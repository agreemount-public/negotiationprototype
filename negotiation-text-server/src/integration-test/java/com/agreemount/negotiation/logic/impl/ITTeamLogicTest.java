package com.agreemount.negotiation.logic.impl;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;




@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITTeamLogicTest {

    public static final String OWNER_LOGIN = "ownerLogin";

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TeamLogic teamLogic;

    @Test
    public void shouldSetProperOwnerRoleWhileSaving() {
        // when
        teamLogic.saveTeamWithOwner(OWNER_LOGIN);

        // then
        assertThat(findTeam().getUsers2Team()).contains(getMember(OWNER_LOGIN, UserRole.SIDE1));
    }

    private User2Team getMember(String login, UserRole side) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(side);
        return user2Team;
    }

    private Team findTeam() {
        return mongoTemplate.findOne(new Query(), Team.class);
    }

    @Before
    @After
    public void cleanData() {
        mongoTemplate.remove(new Query(), Team.class);
    }

}