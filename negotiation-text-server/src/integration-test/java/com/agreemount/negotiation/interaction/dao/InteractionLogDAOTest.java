package com.agreemount.negotiation.interaction.dao;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.interaction.InteractionLog;
import org.assertj.core.groups.Tuple;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.assertj.core.groups.Tuple.tuple;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class InteractionLogDAOTest {


    @Autowired
    private InteractionLogDAO interactionLogDAO;

    @Test
    public void shouldReturnNotProcessedRecord() {
        interactionLogDAO.save(getProcessed());
        interactionLogDAO.save(getNotProcessed());

        InteractionLog ia = interactionLogDAO.getNext();
        assertThat(ia.getProcessedAt()).isNull();

        ia.setProcessedAt(LocalDateTime.now());
        interactionLogDAO.save(ia);

        ia = interactionLogDAO.getNext();
        assertThat(ia).isNull();
    }


    @Test
    public void shouldMarkSimilarInteractionLogsAsProcessed() {
        InteractionLog baseInteractionLog = getInteractionLog("slaUuid1", "user1@email.com", null);

        interactionLogDAO.save(baseInteractionLog);
        interactionLogDAO.save(getInteractionLog("slaUuid1", "user1@email.com", null));
        interactionLogDAO.save(getInteractionLog("slaUuid1", "user1@email.com", null));
        interactionLogDAO.save(getInteractionLog("slaUuid1", "user2@email.com", null));
        interactionLogDAO.save(getInteractionLog("slaUuid2", "user1@email.com", null));
        interactionLogDAO.save(getInteractionLog("slaUuid2", "user2@email.com", null));

        interactionLogDAO.markSimilarAsProcessed("slaUuid1", "user1@email.com");


        List<InteractionLog> notprocessed = findNotProcessedRecords();
        assertThat(notprocessed)
                .extracting(InteractionLog::getSlaUuid, InteractionLog::getAuthorEmail)
                .containsExactlyInAnyOrder(
                        tuple("slaUuid1", "user2@email.com"),
                        tuple("slaUuid2", "user1@email.com"),
                        tuple("slaUuid2", "user2@email.com")
                );
    }

    private List<InteractionLog> findNotProcessedRecords() {
        return interactionLogDAO.getMongoTemplate()
                .find(new Query().addCriteria(Criteria.where("processedAt").is(null)), InteractionLog.class);
    }




    private InteractionLog getInteractionLog(String documentId, String authorEmail, LocalDateTime processedAt) {
        InteractionLog interactionLog = new InteractionLog();
        interactionLog.setCreatedAt(LocalDateTime.now());
        interactionLog.setSlaUuid(documentId);
        interactionLog.setProcessedAt(processedAt);
        interactionLog.setAuthorEmail(authorEmail);

        return interactionLog;
    }

    private InteractionLog getNotProcessed() {
        InteractionLog interactionLog = new InteractionLog();
        interactionLog.setCreatedAt(LocalDateTime.now());
        interactionLog.setDocumentId("documentId123456");
        interactionLog.setProcessedAt(null);
        interactionLog.setAuthorEmail("user@email.com");

        return interactionLog;
    }

    private InteractionLog getProcessed() {
        InteractionLog interactionLog = new InteractionLog();
        interactionLog.setCreatedAt(LocalDateTime.now().minusDays(2l));
        interactionLog.setDocumentId("documentId123456");
        interactionLog.setProcessedAt(LocalDateTime.now().minusHours(2l));
        interactionLog.setAuthorEmail("user@email.com");

        return interactionLog;
    }

    @Before
    @After
    public void cleanData() {
        interactionLogDAO.deleteAll(InteractionLog.class);
    }

}