package com.agreemount.negotiation.interaction.notification.dao;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.bean.notification.NotificationType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class InteractionNotificationDAOTest {

    public static final String ONE_EMAIL_COM = "one@email.com";
    public static final String TWO_EMAIL_COM = "two@email.com";
    public static final String DOC_A = "docA";
    public static final String DOC_B = "docB";

    @Autowired
    private InteractionNotificationDAO interactionNotificationDAO;

    @Test
    public void shouldNotFindNewerThanDay() throws Exception {
        boolean result = interactionNotificationDAO.existNewerThanDay(DOC_A, ONE_EMAIL_COM);
        assertFalse(result);

    }

    @Test
    public void shouldFindNewerThanDay() throws Exception {
        boolean result = interactionNotificationDAO.existNewerThanDay(DOC_B, ONE_EMAIL_COM);
        assertTrue(result);
    }

    private void buildAndStoreNotification(String receiver, String documentId, NotificationType type, NotificationStatus status,
                                LocalDateTime createdAt) {
        NotificationBuilder notificationBuilder = new NotificationBuilder();
        notificationBuilder
                .setNotificationStatus(status)
                .setNotificationType(type)
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), receiver))
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.DOCUMENT_ID.getValue(), documentId))
        ;

        Notification notification = notificationBuilder.build();
        notification.setCreatedAt(createdAt);
        interactionNotificationDAO.save(notification);
    }


    @Before
    public void before() {
        cleanData();
        buildAndStoreNotification(ONE_EMAIL_COM, DOC_A, NotificationType.OTHER_SIDE_INTERACTION, NotificationStatus.PROCESSED, LocalDateTime.now().minusDays(2));
//        //processed but with failure
        buildAndStoreNotification(ONE_EMAIL_COM, DOC_A, NotificationType.OTHER_SIDE_INTERACTION, NotificationStatus.FAILED, LocalDateTime.now().minusHours(2));
//        //not processed
        buildAndStoreNotification(ONE_EMAIL_COM, DOC_A, NotificationType.OTHER_SIDE_INTERACTION, NotificationStatus.NEW, LocalDateTime.now().minusHours(50));
//        //for other receiver
        buildAndStoreNotification(TWO_EMAIL_COM, DOC_A, NotificationType.OTHER_SIDE_INTERACTION, NotificationStatus.PROCESSED, LocalDateTime.now().minusHours(2));
        //for other document
        buildAndStoreNotification(ONE_EMAIL_COM, DOC_B, NotificationType.OTHER_SIDE_INTERACTION, NotificationStatus.PROCESSED, LocalDateTime.now().minusHours(2));
    }

    @After
    public void cleanData() {
        interactionNotificationDAO.deleteAll(Notification.class);
    }


}