package com.agreemount.negotiation.sender;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.PreparedEmail;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

import static junit.framework.Assert.fail;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITMailerTest {

    @Autowired
    private ISender emailISender;

    @Rule
    public Timeout timeout = Timeout.seconds(5);

    @Test
    @Ignore("mariusz: this case tests email server which is not stable currently")
    public void givenEmailWhenTryingToSendEmailThenSend() {
        PreparedEmail preparedEmail = new PreparedEmail();
        preparedEmail.setContent("content");
        preparedEmail.setTitle("title");
        preparedEmail.setReceiver("generate1234@mailinator.com");
        preparedEmail.setCc(Arrays.asList("generate123_cc1@mailinator.com", "generate123_cc2@mailinator.com"));
        preparedEmail.setBcc(Arrays.asList("generate123_bc1@mailinator.com", "generate123_bc2@mailinator.com"));

        try {
            emailISender.send(preparedEmail);
        } catch (Exception e) {
            fail("PreparedEmail should be sent");
        }
    }

}
