package com.agreemount.negotiation;

import com.agreemount.bean.document.Document;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.action.DocumentFactory;
import com.agreemount.slaneg.fixtures.FileRulesProvidersConfiguration;
import com.agreemount.testing.TestIdentityProvider;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;

import static org.mockito.Mockito.mock;



@EnableAutoConfiguration
@PropertySources({
        @PropertySource("classpath:application.properties"),
        @PropertySource("classpath:engine.properties"),
        @PropertySource("classpath:mongo.properties"),
        @PropertySource("classpath:mailer.properties"),
        @PropertySource("classpath:cron.properties"),
        @PropertySource("classpath:tag-finder.properties"),
        @PropertySource("classpath:paypal/paypal-local.properties"),
        @PropertySource("classpath:log4j.properties"),
})
@ComponentScan(
        basePackages = {"com.agreemount"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                        value = Application.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                        value = com.agreemount.negotiation.configuration.CronConfiguration.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                        value = FileRulesProvidersConfiguration.class)
        }
)
public class ApplicationTest {

    @Bean
    public ActionContextFactory<ActionContext> getActionContextFactory() {
        return new ActionContextFactory<>(ActionContext.class);
    }

    @Bean
    public TestIdentityProvider identityProvider() {
        return mock(TestIdentityProvider.class);
    }

    @Bean
    public ActionContext actionContext() {
        return mock(ActionContext.class);
    }

    @Bean
    public DocumentFactory<Document> getDocumentFactory() {
        return new DocumentFactory<>(Document.class);
    }

}
