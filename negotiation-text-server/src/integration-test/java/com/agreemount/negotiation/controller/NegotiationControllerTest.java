package com.agreemount.negotiation.controller;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.NegotiationOnDashboard;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.logic.impl.AgreemountDocumentLogic;
import com.google.common.collect.Lists;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;



@RunWith(MockitoJUnitRunner.class)
public class NegotiationControllerTest {

    private static final String TEAM_1 = "TEAM_1";
    private static final String DOC_ID_1 = "DOC_ID_1";

    @Mock
    private AgreemountDocumentLogic agreemountDocumentLogic;

    @InjectMocks
    private NegotiationController controller;

    @Test
    public void shouldReturnSuccessResponseForGetDashboardData() {
        // given
        when(agreemountDocumentLogic.fetchDashboardData()).thenReturn(Lists.newArrayList(
                new NegotiationOnDashboard(document(TEAM_1, DOC_ID_1), Lists.newArrayList())
        ));

        // when
        Response response = controller.getDashboardData();

        // then
        assertThat(response.getStatus()).isEqualTo(ResponseType.SUCCESS);
        assertThat((List<User2Team>) response.getData()).hasSize(1);
    }

    @Test
    public void shouldReturnFailedResponseForGetDashboardData() {
        // given
        doThrow(Exception.class).when(agreemountDocumentLogic).fetchDashboardData();

        // when
        Response response = controller.getDashboardData();

        // then
        assertThat(response.getStatus()).isEqualTo(ResponseType.FAILED);
        AssertionsForClassTypes.assertThat(response.getMessage()).startsWith("Cannot fetch documents");
    }

    private Document document(String teamId, String teamName) {
        Document document = new Document();
        document.setTeam(teamName);
        document.setId(teamId);
        return document;
    }

}