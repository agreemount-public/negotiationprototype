package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.renderer.actions.IAngularDirectiveAction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITAngularDirectiveTest {

    @Autowired
    private IAngularDirectiveAction angularDirective;

    @Test
    public void givenTextWhenTagsThenAddDirectiveToThem() {
        String body = "Podział majatku pomiędzy Adam i Ewa: " +
                "- Ewa: Dom w zielonkach - Ewa: Samochod marki Porshe :) " +
                "- Adam: Działka <Scenario1Doc03>letniskowa</Scenario1Doc03> " +
                "- Ewa: udziały w firmie X S.A.<Scenario1Doc02></Scenario1Doc02>";

        String directive = "content-tag";

        Set<String> tags = new HashSet<>();
        tags.add("Scenario1Doc03");

        String expectedBody = "Podział majatku pomiędzy Adam i Ewa: " +
                "- Ewa: Dom w zielonkach - Ewa: Samochod marki Porshe :) " +
                "- Adam: Działka <Scenario1Doc03 content-tag>letniskowa</Scenario1Doc03> " +
                "- Ewa: udziały w firmie X S.A.<Scenario1Doc02></Scenario1Doc02>";

        String resultBody = angularDirective.doIt(body, tags, directive);

        assertEquals(expectedBody, resultBody);
    }
}