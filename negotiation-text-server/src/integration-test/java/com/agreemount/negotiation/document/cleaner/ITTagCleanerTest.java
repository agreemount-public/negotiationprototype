package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.model.ITagDecorator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITTagCleanerTest {

    @Autowired
    private ITagCleaner<String, String> tagCleaner;

    @Autowired
    private ITagDecorator tagDecorator;

    @Test
    public void passesWhenAllTagsAreRemovedFromOriginalText() {
        String inputText = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: Dom w zielonkach"
                + "- Ewa: Samochod marki Peugoth"
                + "- <D3>Adam</D3>: Działka letniskowa "
                + "- <D4>Ewa</D4>: udziały w firmie X S.A."
                + "- Tata <772ce8dc-d643-42cd-a18d-1e71606f87b0>Adama</772ce8dc-d643-42cd-a18d-1e71606f87b0> kolekcję znaczków"
                + "<D2></D2>";

        Set<String> removedTags = new HashSet<>();
        removedTags.add("D2");
        removedTags.add("D3");
        removedTags.add("D4");
        removedTags.add("772ce8dc-d643-42cd-a18d-1e71606f87b0");


        String expectedOutput = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: Dom w zielonkach"
                + "- Ewa: Samochod marki Peugoth"
                + "- Adam: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A."
                + "- Tata Adama kolekcję znaczków";


        String resultText = tagCleaner.clean(inputText, removedTags);
        assertEquals(expectedOutput, resultText);


        for (String tagName : removedTags) {
            assertFalse(resultText.contains(tagDecorator.openTag(tagName)));
            assertFalse(resultText.contains(tagDecorator.closeTag(tagName)));
        }

    }

    @Test
    public void expectedOuptutTextAsInputOneWhenOriginalTextDoesNotContainTag() {
        String inputText = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: Dom w zielonkach"
                + "- Ewa: Samochod marki Peugoth"
                + "- <D3>Adam</D3>: Działka letniskowa "
                + "- <D4>Ewa</D4>: udziały w firmie X S.A."
                + "<D2></D2>";

        Set<String> removedTags = new HashSet<>();
        removedTags.add("D14");


        String resultText = tagCleaner.clean(inputText, removedTags);
        assertEquals(inputText, resultText);


        for (String tagName : removedTags) {
            assertFalse(resultText.contains(tagDecorator.openTag(tagName)));
            assertFalse(resultText.contains(tagDecorator.closeTag(tagName)));
        }
    }

}
