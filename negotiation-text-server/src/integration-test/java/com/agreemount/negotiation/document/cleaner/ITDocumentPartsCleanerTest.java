package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITDocumentPartsCleanerTest {

    @Autowired
    private ITagCleaner<Map<String, String>, String> documentPartsCleaner;

    @Test
    public void givenTextAndTagNameListToCleanWhenTextContainsTagsThenReturnTextWithoutTags() {

        Map<String, String> parts = new HashMap<>();
        parts.put("D2", "Podział majątku pomiędzy Adam i Ewa <D3>po równo</D3>");
        parts.put("D3", "Adam:Ewa, 70%:30%");
        parts.put("D4", "<D3>-Ewa: pełny </D3>udział w spółce MBM Group<D5></D5>");
        parts.put("D5", " i akcje PKN Orlen");

        Set<String> removedTags = new HashSet<>();
        removedTags.add("D3");
        removedTags.add("D5");

        Map<String, String> expectedParts = new HashMap<>();
        expectedParts.put("D2", "Podział majątku pomiędzy Adam i Ewa po równo");
        expectedParts.put("D4", "-Ewa: pełny udział w spółce MBM Group");


        Map<String, String> actualParts = documentPartsCleaner.clean(parts, removedTags);

        assertNotNull(actualParts);
        assertEquals(expectedParts, actualParts);

    }

}
