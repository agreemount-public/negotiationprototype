package com.agreemount.negotiation.document.action;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.action.ActionFactory;
import com.agreemount.slaneg.action.definition.CreateNewLeaf;
import com.agreemount.slaneg.action.definition.CreateNewRoot;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class CreateNewLeafActionConfigurationTest {

    @Autowired
    private ActionFactory actionFactory;

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Test
    public void shouldCopyOrdersWhenCreatingNewLeaf() {
        CreateNewLeaf createNewLeaf = new CreateNewLeaf();
        createNewLeaf.setAlias("newLeaf");
        createNewLeaf.setParentAlias("parentAlias");


        long order = 23423432L;

        Document parent = new Document();
        parent.getAttributes().put(OrderSide.ORDER_SIDE_1.getValue(), order);
        parent.getAttributes().put(OrderSide.ORDER_SIDE_2.getValue(), order);

        ActionContext ac = actionContextFactory.createInstance();
        ac.addDocument("parentAlias", parent);
        actionFactory.create(createNewLeaf).runWithSubaction(ac);

        Document document = ac.getDocument("newLeaf");

        long orderSIDE1 = (long)document.getAttributes().get(OrderSide.ORDER_SIDE_1.getValue());
        long orderSIDE2 = (long)document.getAttributes().get(OrderSide.ORDER_SIDE_2.getValue());

        Assert.assertEquals(order, orderSIDE1);
        Assert.assertEquals(order, orderSIDE2);

    }
}
