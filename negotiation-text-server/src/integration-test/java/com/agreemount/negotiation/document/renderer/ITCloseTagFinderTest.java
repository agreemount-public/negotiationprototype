package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.finder.ITagFinder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITCloseTagFinderTest {

    @Autowired
    @Qualifier(value = "closeTagFinder")
    private ITagFinder<String, String> tagFinder;


    @Test
    public void giventTextWhenCloseTagThenFindIt() {

        String text = "Text z <D1> tagiem</D2> zamykającym </D3>";
        Set<String> closedTags = tagFinder.find(text);

        assertEquals(2, closedTags.size());

    }

    @Test
    public void giventTextWhenCloseTagUUIDLikeThenFindIt() {

        String text = "Text z tagiem</9b4520d3-5976-a905-1d8d-e5f5b002efa9> zamykającym </9b4520d3-5976-a905-1d8d-e5f5b002efa7>";
        Set<String> closedTags = tagFinder.find(text);

        assertEquals(2, closedTags.size());

    }

    @Test
    public void giventTextWhenCloseTagsAreBrokenThenSkipIt() {

        String text = "Text z <D1> tagiem</D 2> zamykającym </D3  >";
        Set<String> closedTags = tagFinder.find(text);

        assertEquals(0, closedTags.size());

    }

    @Test
    public void giventTextWhenCloseTagsHasNoNameThenSkipIt() {

        String text = "Text z <D1> tagiem</> zamykającym </ >";
        Set<String> closedTags = tagFinder.find(text);

        assertEquals(0, closedTags.size());

    }

    @Test
    public void giventTextWhenCloseTagNameIsOnlyNumericThenFindIt() {

        String text = "Text z <D1> tagiem</> zamykającym </12314>";
        Set<String> closedTags = tagFinder.find(text);

        assertEquals(1, closedTags.size());

    }

}
