package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.finder.ITagFinder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITOpenTagFinderTest {

    @Autowired
    @Qualifier(value = "openTagFinder")
    private ITagFinder<String, String> tagFinder;

    @Test
    public void givenTextWhenSingleOpenTagThenFindIt() {

        String text = "Text z <D1> tagiem otwierającym";
        Set<String> openTags = tagFinder.find(text);

        assertEquals(1, openTags.size());

    }

    @Test
    public void givenTextWhenOpenTagThenFindIt() {

        String text = "Text z <D1> tagiem<D2> otwierającym </D3>";
        Set<String> openTags = tagFinder.find(text);

        assertEquals(2, openTags.size());

    }

    @Test
    public void givenTextWhenOpenTagUUIDLikeFormatThenFindIt() {

        String text = "Text z <9b4520d3-5976-a905-1d8d-e5f5b002efa7> tagiem<7b4520d3-5976-a905-1d8d-A5f5b002efa4> otwierającym </D3>";
        Set<String> openTags = tagFinder.find(text);

        assertEquals(2, openTags.size());

    }

    @Test
    public void givenTextWhenOpenTagUUIDLikeFormatAndAdditianalAttributesThenFindIt() {

        String text = "Text z <9b4520d3-5976-a905-1d8d-e5f5b002efa7> tagiem<7b4520d3-5976-a905-1d8d-A5f5b002efa4 ng-directive> otwierającym </D3>";
        Set<String> openTags = tagFinder.find(text);

        assertEquals(2, openTags.size());

    }

    @Test
    public void givenTextWhenTwoOpenTagsAreBrokenThenFindIt() {

        String text = "Text z < D 1> tagiem< D 2> otwierajacym </D3  >";
        Set<String> closedTags = tagFinder.find(text);

        assertEquals(0, closedTags.size());

    }

    @Test
    public void givenTextWhenOneOpenTagsAreBrokenThenFindIt() {

        String text = "Text z <D1> tagiem< D 2> otwierajacym </D3>";
        Set<String> closedTags = tagFinder.find(text);

        assertEquals(1, closedTags.size());

    }

}
