package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITDocumentBodyCleanerTest {

    @Autowired
    private ITagCleaner<String, String> documentBodyCleaner;

    @Test
    public void givenTextAndTagNameListToCleanWhenTextContainsTagsThenReturnTextWithoutTags() {
        String body = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: <D6>Dom w zielonkach</D6>"
                + "- Ewa: Samochod marki Peugoth"
                + "- <D7>Adam</D7>: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A."
                + "<D8></D8>";

        Set<String> removedTags = new HashSet<>();
        removedTags.add("D6");
        removedTags.add("D7");

        String expectedBody = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: Dom w zielonkach"
                + "- Ewa: Samochod marki Peugoth"
                + "- Adam: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A."
                + "<D8></D8>";

        String actualBody = documentBodyCleaner.clean(body, removedTags);

        assertNotNull(actualBody);
        assertEquals(expectedBody, actualBody);
    }


    @Test
    public void givenTextAndTagNameListToCleanWhenTextContainsTagAsTheLastOneThenReturnTextWithoutTag() {
        String body = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: <D6>Dom w zielonkach</D6>"
                + "- Ewa: Samochod marki Peugoth"
                + "- <D7>Adam</D7>: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A."
                + "<D8></D8>";

        Set<String> removedTags = new HashSet<>();
        removedTags.add("D8");

        String expectedBody = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: <D6>Dom w zielonkach</D6>"
                + "- Ewa: Samochod marki Peugoth"
                + "- <D7>Adam</D7>: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A."
                + "";

        String actualBody = documentBodyCleaner.clean(body, removedTags);

        assertNotNull(actualBody);
        assertEquals(expectedBody, actualBody);
    }

    @Test
    public void givenTextAndNoTagsToCleanThenReturnTextIsTheSameAsTheInputOne() {
        String body = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: <D6>Dom w zielonkach</D6>"
                + "- Ewa: Samochod marki Peugoth"
                + "- <D7>Adam</D8>: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A."
                + "<D8></D8>";

        Set<String> removedTags = new HashSet<>();

        String actualBody = documentBodyCleaner.clean(body, removedTags);

        assertNotNull(actualBody);
        assertEquals(body, actualBody);

    }
}
