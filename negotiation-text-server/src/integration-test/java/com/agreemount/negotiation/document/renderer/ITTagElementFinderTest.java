package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.finder.ITagElementFinder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITTagElementFinderTest {

    @Autowired
    @Qualifier(value = "tagNameFinder")
    private ITagElementFinder<String> tagElementFinder;


    @Test
    public void giventOpenTagWhenNameIsD2ThenReturnD2Name() {

        String text = "<D2 name='cos'>";
        String tagName = tagElementFinder.find(text);

        assertEquals("D2", tagName);

    }

    @Test
    public void giventCloseTagWhenNameIsD2ThenReturnD2Name() {

        String text = "</D2>";
        String tagName = tagElementFinder.find(text);

        assertEquals("D2", tagName);

    }

    @Test(expected = IllegalStateException.class)
    public void giventTagWhenItIsBrokenThenExceptionExpected() {

        String text = "</ >";
        String tagName = tagElementFinder.find(text);

    }

}
