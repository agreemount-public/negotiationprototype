package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.model.DocumentView;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITDocumentConflictCleanerTest {

    @Autowired
    private IDocumentConflictCleaner documentConflictCleaner;

    private static Map<String, String> allParts = null;

    private final String documentTag = "D1";
    private final String documentBody = "Podział majątku i obowiązków pomiędzy Adam i Ewa:</D2>"
            + "- Ewa: </D3>Dom w zielonkach"
            + "- Ewa: Samochod marki</D4> Peugoth"
            + "<D5></D5>";

    private final String expectedBody = "Podział majątku i obowiązków pomiędzy Adam i Ewa:"
            + "- Ewa: Dom w zielonkach"
            + "- Ewa: Samochod marki Peugoth"
            + "";

    @BeforeClass
    public static void setUp() {
        allParts = new HashMap();
        allParts.put("D2", "");
        allParts.put("D3", "");
        allParts.put("D4", "");
        allParts.put("D5", "");
    }


    @Test
    public void givenTextAndAcceptedTagsWhenCorruptedTagsCoversAcceptedTagsThenReturnConflictedTags() {
        Set<String> acceptedTags = new HashSet<>();
        acceptedTags.add("D2");
        acceptedTags.add("D5");

        Set<String> remainsTags = new HashSet<>();
        remainsTags.add("D3");
        remainsTags.add("D4");

        DocumentView documentView = documentConflictCleaner.clean(documentBody, documentTag, allParts, acceptedTags, remainsTags);


        Set<String> expectedConflictedTags = new HashSet<>();
        expectedConflictedTags.add("D2");
        expectedConflictedTags.add("D5");

        Set<String> expectedUnresolvedTags = new HashSet<>();
        expectedUnresolvedTags.add("D3");
        expectedUnresolvedTags.add("D4");

        assertEquals(expectedBody, documentView.getBody());
        assertEquals(expectedConflictedTags, documentView.getConflictedTags());
        assertEquals(expectedUnresolvedTags, documentView.getUnresolvedTags());
    }

}
