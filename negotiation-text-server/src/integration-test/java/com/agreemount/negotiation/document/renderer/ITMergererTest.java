package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.merger.IMerger;
import com.agreemount.negotiation.document.model.DocumentResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITMergererTest {

    @Autowired
    private IMerger merger;

    private final String tag = "D1";
    private final String body = "<D2>Podział majątku i obowiązków pomiędzy Adam i Ewa:</D2>"
            + "- Ewa: <D6>Dom w zielonkach</D6>"
            + "- Ewa: Samochod marki Peugoth"
            + "<D4></D4>"
            + "- <D7>Adam</D7>: Działka letniskowa "
            + "- Ewa: udziały w firmie X S.A."
            + "<D8></D8>";

    private Map<String, String> documentParts = null;

    @Before
    public void setUp() {
        documentParts = new HashMap<>();
        documentParts.put("D2", "Podział majątku pomiędzy Adam i Ewa: <D3>po równo</D3>");
        documentParts.put("D3", " Adam:Ewa, 70%:30%");
        documentParts.put("D4", "- Ewa: pełny udział w spółce MBM Group<D5></D5>");
        documentParts.put("D5", " i akcje PKN Orlen");
        documentParts.put("D6", " połowa domu w Zielonkach");
        documentParts.put("D7", " Ewa");
        documentParts.put("D8", " Adam: Rybki piranie");
    }

    @Test
    public void givenDocumentResourceWhenAcceptedTagsHasNotGotCommonAreaThenReplaceTagContent() {


        Set<String> acceptedTags = new HashSet();
        acceptedTags.add("D4");
        Set<String> remainTags = new HashSet();
        remainTags.add("D2");
        remainTags.add("D3");
        remainTags.add("D5");
        remainTags.add("D6");
        remainTags.add("D7");
        remainTags.add("D8");

        DocumentResource resource = new DocumentResource(
                acceptedTags,
                remainTags,
                body,
                tag,
                documentParts
        );


        String expectedBody = "<D2>Podział majątku i obowiązków pomiędzy Adam i Ewa:</D2>"
                + "- Ewa: <D6>Dom w zielonkach</D6>"
                + "- Ewa: Samochod marki Peugoth"
                + "- Ewa: pełny udział w spółce MBM Group<D5></D5>"
                + "- <D7>Adam</D7>: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A."
                + "<D8></D8>";

        Map<String, String> expectedDocumentParts = new HashMap<>();
        expectedDocumentParts = documentParts.entrySet()
                .stream()
                .filter(p -> !acceptedTags.contains(p.getKey()))
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

        DocumentResource actualDocumentResource = (DocumentResource) merger.merge(resource);

        assertNotNull(actualDocumentResource);
        assertEquals(expectedBody, actualDocumentResource.getBody());
        assertEquals(expectedDocumentParts, actualDocumentResource.getDocumentParts());
        assertEquals(new HashSet<String>(), actualDocumentResource.getAcceptedTags());
        assertEquals(remainTags, actualDocumentResource.getRemainTags());

    }

    @Test
    public void givenDocumentResourceWhenAcceptedTagsArePlacedInDocumentPartsThenReplaceTagContent() {


        Set<String> acceptedTags = new HashSet();
        acceptedTags.add("D8");
        acceptedTags.add("D3");
        acceptedTags.add("D2");
        acceptedTags.add("D4");


        Set<String> remainTags = new HashSet();
        remainTags.add("D5");
        remainTags.add("D6");
        remainTags.add("D7");


        DocumentResource resource = new DocumentResource(
                acceptedTags,
                remainTags,
                body,
                tag,
                documentParts
        );


        String expectedBody = "Podział majątku pomiędzy Adam i Ewa:  Adam:Ewa, 70%:30%"
                + "- Ewa: <D6>Dom w zielonkach</D6>"
                + "- Ewa: Samochod marki Peugoth"
                + "- Ewa: pełny udział w spółce MBM Group<D5></D5>"
                + "- <D7>Adam</D7>: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A."
                + " Adam: Rybki piranie";

        Map<String, String> expectedDocumentParts = new HashMap<>();
        expectedDocumentParts = documentParts.entrySet()
                .stream()
                .filter(p -> !acceptedTags.contains(p.getKey()))
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));


        DocumentResource actualDocumentResource = (DocumentResource) merger.merge(resource);

        assertNotNull(actualDocumentResource);
        assertEquals(expectedBody, actualDocumentResource.getBody());
        assertEquals(expectedDocumentParts, actualDocumentResource.getDocumentParts());
        assertEquals(new HashSet<String>(), actualDocumentResource.getAcceptedTags());
        assertEquals(remainTags, actualDocumentResource.getRemainTags());

    }


    @Test
    public void givenDocumentWhenAcceptedTagsAreInsideAnotherOnesThenReturnListOfUnmergedTags() {


        Set<String> acceptedTags = new HashSet();
        acceptedTags.add("D2");
        acceptedTags.add("D3");
        Set<String> remainTags = new HashSet();

        String body = "<D2>Podział majątku <D3></D3>pomiędzy Adam i Ewa:</D2>";
        Map<String, String> documentParts = new HashMap<>();
        documentParts.put("D2", "Podział majątku pomiędzy Adam i Ewa po równo");
        documentParts.put("D3", "i obowiązków ");

        DocumentResource resource = new DocumentResource(
                acceptedTags,
                remainTags,
                body,
                tag,
                documentParts
        );


        String expectedBody = "Podział majątku pomiędzy Adam i Ewa po równo";
        Set<String> expectedAcceptedTags = new HashSet();
        expectedAcceptedTags.add("D3");

        Map<String, String> expectedDocumentParts = new HashMap<>();
        expectedDocumentParts.put("D3", "i obowiązków ");

        DocumentResource actualDocumentResource = (DocumentResource) merger.merge(resource);

        assertNotNull(actualDocumentResource);
        assertEquals(expectedBody, actualDocumentResource.getBody());
        assertEquals(expectedDocumentParts, actualDocumentResource.getDocumentParts());
        assertEquals(expectedAcceptedTags, actualDocumentResource.getAcceptedTags());
        assertEquals(remainTags, actualDocumentResource.getRemainTags());

    }

}
