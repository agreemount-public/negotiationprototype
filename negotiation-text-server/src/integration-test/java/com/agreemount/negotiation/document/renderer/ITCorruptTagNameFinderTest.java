package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.finder.ICorruptTagNameFinder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITCorruptTagNameFinderTest {

    @Autowired
    private ICorruptTagNameFinder corruptedTagsNameFinder;

    @Test
    public void givenTexWithNoOpenOrNoCloseTagWhenTagsInGoodFormatThenSetOfCorruptedTagNames() throws Exception {

        Set<String> openTagNames = new HashSet<>();
        Set<String> closeTagNames = new HashSet<>();

        String text = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: <D2>Dom w zielonkach"
                + "- Ewa: Samochod marki Peugoth"
                + "- Adam: Działka letniskowa </D3>"
                + "- Ewa</D4>: udziały w <D5>firmie X S.A."
                + "Zakończenie tekstu ze złym tagiem otwierajacym < D 6>koniec";

        Set<String> expectedOpenTagNames = new HashSet<>();
        expectedOpenTagNames.add("D2");
        expectedOpenTagNames.add("D5");

        Set<String> expectedCloseTagNames = new HashSet<>();
        expectedCloseTagNames.add("D3");
        expectedCloseTagNames.add("D4");

        openTagNames.addAll(corruptedTagsNameFinder.onlyOpenTags(text));
        closeTagNames.addAll(corruptedTagsNameFinder.onlyCloseTags(text));

        assertFalse(openTagNames.isEmpty());
        assertFalse(closeTagNames.isEmpty());

        assertEquals(expectedOpenTagNames, openTagNames);
        assertEquals(expectedCloseTagNames, closeTagNames);
    }

    @Test
    public void givenTextWhenOnlyCorrectXMLTagsThenEmptyCorruptedTagSet() {
        Set<String> openTagNames = new HashSet<>();
        Set<String> closeTagNames = new HashSet<>();

        String text = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: <D2>Dom w zielonkach</D2>"
                + "- Ewa: Samochod marki Peugoth"
                + "- Adam: Działka letniskowa <D3 attribute='as'></D3>";

        Set<String> expectedOpenTagNames = new HashSet<>();
        Set<String> expectedCloseTagNames = new HashSet<>();

        openTagNames.addAll(corruptedTagsNameFinder.onlyOpenTags(text));
        closeTagNames.addAll(corruptedTagsNameFinder.onlyCloseTags(text));

        assertTrue(openTagNames.isEmpty());
        assertTrue(closeTagNames.isEmpty());

        assertEquals(expectedOpenTagNames, openTagNames);
        assertEquals(expectedCloseTagNames, closeTagNames);
    }

    @Test
    public void givenTextWhenNoTagsThenEmptyCorruptedTagSet() {
        Set<String> openTagNames = new HashSet<>();
        Set<String> closeTagNames = new HashSet<>();

        String text = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: Dom w zielonkach"
                + "- Ewa: Samochod marki Peugeot < 4 dni dla Adama"
                + "- Adam: Część działki letniskowa > 20 a";

        openTagNames.addAll(corruptedTagsNameFinder.onlyOpenTags(text));
        closeTagNames.addAll(corruptedTagsNameFinder.onlyCloseTags(text));

        assertTrue(openTagNames.isEmpty());
        assertTrue(closeTagNames.isEmpty());
    }

}
