package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.model.Document;
import com.agreemount.negotiation.document.model.DocumentResource;
import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.negotiation.document.model.IDocumentResource;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITDocumentRenderer {
    private static final String NEW_LINE = System.lineSeparator();
    private static final String BR_TAG = "<br>";

    @Autowired
    @Qualifier(value="documentRenderer")
    private IRenderer renderer;

    @Test
    public void shouldReturnMergedDocumentWithTagsAndInformationAboutConflicts() throws Exception {

        // given
        Set<String> acceptedTags = new HashSet<>();
        acceptedTags.add("D2");
        acceptedTags.add("D3");
        acceptedTags.add("C1");
        acceptedTags.add("D10");
        acceptedTags.add("D4");


        Set<String> remainTags = new HashSet<>();
        remainTags.add("D5");
        remainTags.add("D6");
        remainTags.add("D7");
        remainTags.add("D9");

        String tag = "D1";
        String body = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: <D6> Dom jednorodzinny </D6> w Zielonkach  <D7></D7>"
                + "- Ewa: Samochod marki Peugoth"
                + "- <D5>Adam</D5>: Działka letniskowa "
                + "- Adam: udziały w firmie X S.A."
                + "<D2>-<C1>Ewa</C1>: jacht <D9></D2></D9>";

        String bodyD2 = "- Adam: obligacje skarbowe na kwotę <D3>20</D3>k EUR"
                + "<D4>jakas <D10>trest</D4>EUR</D10>"
                + "- Adam: iPAD";
        String bodyD3 = "10";
        String bodyD4 = "- Ewa: obligacje skarbowe na kwotę 10k ";
        String bodyD5 = "Ewa";
        String bodyD6 = "Pół domu w Zielonkach";
        String bodyD7 = " ul. Wrzosowa 5";
        String bodyD8 = "pod adresem ul. Wrzosowa 5, Zielonki";
        String bodyD9 = " i kuter";
        String bodyD10 = " USD";
        String bodyC1 = "Adam";

        Map<String, String> documentParts = new HashMap<>();
        documentParts.put("D2", bodyD2);
        documentParts.put("D3", bodyD3);
        documentParts.put("D4", bodyD4);
        documentParts.put("D5", bodyD5);
        documentParts.put("D6", bodyD6);
        documentParts.put("D7", bodyD7);
        documentParts.put("D8", bodyD8);
        documentParts.put("D9", bodyD9);
        documentParts.put("D10", bodyD10);
        documentParts.put("C1", bodyC1);

        IDocumentResource resource = new DocumentResource(acceptedTags, remainTags, body, tag, documentParts);

        // when
        DocumentView documentView = renderer.render(resource, null);

        // then
        String expectedBodyOutput = "Podział majątku pomiędzy Adam i Ewa:"
                + "- Ewa: <D6> Dom jednorodzinny </D6> w Zielonkach  <D7></D7>"
                + "- Ewa: Samochod marki Peugoth"
                + "- <D5>Adam</D5>: Działka letniskowa "
                + "- Adam: udziały w firmie X S.A."
                + "- Adam: obligacje skarbowe na kwotę 10k EUR"
                + "- Ewa: obligacje skarbowe na kwotę 10k EUR</D10>"
                + "- Adam: iPAD</D9>";


        Set<String> conflictedTags = new HashSet<>();
        Set<String> remainContentTags = new HashSet<>();
        for (String tagName : remainTags) {
            remainContentTags.add(tagName);
        }


        Set<String> expectedConflictedTags = new HashSet<>();
        expectedConflictedTags.add("D10");
        expectedConflictedTags.add("C1");

        Set<String> expectedUnresolvedTags = new HashSet<>();
        expectedUnresolvedTags.add("D9");

        Set<String> artefact = new HashSet<>();
        artefact.add("D8");

        Document expectedDocument = new Document(expectedBodyOutput, "D1", remainContentTags);

        assertDocumentView(documentView, expectedBodyOutput, expectedDocument);

        assertEquals(expectedConflictedTags, documentView.getConflictedTags());
        assertEquals(expectedUnresolvedTags, documentView.getUnresolvedTags());
    }

    @Test
    public void givenBodyWithNewLinesWhenRenderThenChangeNewLinesIntoHtmlBr() throws Exception {

        // given
        Set<String> acceptedTags = Sets.newHashSet("D2");
        Set<String> remainTags = Sets.newHashSet("D5");
        String tag = "D1";
        String body = new StringBuilder()
                .append("Podział majątku pomiędzy Adam i Ewa:").append(NEW_LINE)
                .append("- <D5>Adam</D5>: Działka letniskowa ").append(NEW_LINE)
                .append("- Adam: udziały w firmie X S.A.").append(NEW_LINE)
                .append("<D2>- Ewa jacht</D2>")
                .append(NEW_LINE)
                .toString();

        String bodyD2 = "- Adam: obligacje skarbowe na kwotę 20k EUR";
        String bodyD5 = "Ewa";

        Map<String, String> documentParts = new HashMap<>();
        documentParts.put("D2", bodyD2);
        documentParts.put("D5", bodyD5);

        IDocumentResource resource = new DocumentResource(acceptedTags, remainTags, body, tag, documentParts);

        // when
        DocumentView documentView = renderer.render(resource, null);

        // then
        String expectedBodyOutput = new StringBuilder()
                .append("Podział majątku pomiędzy Adam i Ewa:").append(BR_TAG)
                .append("- <D5>Adam</D5>: Działka letniskowa ").append(BR_TAG)
                .append("- Adam: udziały w firmie X S.A.").append(BR_TAG)
                .append("- Adam: obligacje skarbowe na kwotę 20k EUR")
                .toString();

        Set<String> remainContentTags = Sets.newHashSet();
        for (String tagName : remainTags) {
            remainContentTags.add(tagName);
        }

        Document expectedDocument = new Document(expectedBodyOutput, "D1", remainContentTags);

        assertDocumentView(documentView, expectedBodyOutput, expectedDocument);
    }

    @Test
    public void givenBodyWithEmptyLinesAtTheBeginingWhenRenderThenChangeAllNewLinesIntoHtmlBr() throws Exception {

        // given
        String tag = "D1";
        String body = bodyWithEmptyLinesAtTheBegining(NEW_LINE, false);
        IDocumentResource resource = new DocumentResource(Sets.newHashSet(), Sets.newHashSet(), body, tag, Maps.newHashMap());

        // when
        DocumentView documentView = renderer.render(resource, null);

        // then
        String expectedBodyOutput = bodyWithEmptyLinesAtTheBegining(BR_TAG, true);
        Document expectedDocument = new Document(expectedBodyOutput, "D1", Sets.newHashSet());

        assertDocumentView(documentView, expectedBodyOutput, expectedDocument);
    }

    @Test
    public void givenBodyWithEmptyLinesAtTheEndWhenRenderThenChangeAllBisideTheLastOneNewLineIntoHtmlBr() throws Exception {

        //given
        String tag = "D1";
        String body = bodyWithEmptyLinesAtTheEnd(NEW_LINE, false);
        IDocumentResource resource = new DocumentResource(Sets.newHashSet(), Sets.newHashSet(), body, tag, Maps.newHashMap());

        //when
        DocumentView documentView = renderer.render(resource, null);

        //then
        String expectedBodyOutput = bodyWithEmptyLinesAtTheEnd(BR_TAG, true);
        Document expectedDocument = new Document(expectedBodyOutput, "D1", Sets.newHashSet());

        assertDocumentView(documentView, expectedBodyOutput, expectedDocument);
    }

    private String bodyWithNewLines(String lineSeparator, boolean isHtmlMode) {
        StringBuilder bodyBuilder = new StringBuilder()
                .append("Podział majątku pomiędzy Adam i Ewa:").append(NEW_LINE)
                .append("- <D5>Adam</D5>: Działka letniskowa ").append(NEW_LINE)
                .append("- Adam: udziały w firmie X S.A.").append(NEW_LINE)
                .append("<D2>- Ewa jacht</D2>");

        if (!isHtmlMode) {
            bodyBuilder.append(lineSeparator);
        }

        return bodyBuilder.toString();
    }

    private String bodyWithEmptyLinesAtTheBegining(String lineSeparator, boolean isHtmlMode) {
        StringBuilder bodyBuilder = new StringBuilder()
                .append(lineSeparator)
                .append(lineSeparator)
                .append("Podział majątku pomiędzy Adam i Ewa:").append(lineSeparator)
                .append("- Adam: Działka letniskowa ").append(lineSeparator)
                .append("- Ewa jacht");

        if (!isHtmlMode) {
            bodyBuilder.append(lineSeparator);
        }

        return bodyBuilder.toString();
    }

    private String bodyWithEmptyLinesAtTheEnd(String lineSeparator, boolean isHtmlMode) {
        StringBuilder bodyBuilder = new StringBuilder()
                .append("Podział majątku pomiędzy Adam i Ewa:").append(lineSeparator)
                .append("- Adam: Działka letniskowa ").append(lineSeparator)
                .append("- Ewa jacht").append(lineSeparator)
                .append(lineSeparator);

        if (!isHtmlMode) {
            bodyBuilder.append(lineSeparator);
        }

        return bodyBuilder.toString();
    }

    private void assertDocumentView(DocumentView documentView, String expectedBodyOutput, Document expectedDocument) {
        assertEquals(expectedBodyOutput, documentView.getBody());
        assertEquals(expectedDocument.getBody(), documentView.getDocument().getBody());
        assertEquals(expectedDocument.getDocumentTag(), documentView.getDocument().getDocumentTag());
    }
}
