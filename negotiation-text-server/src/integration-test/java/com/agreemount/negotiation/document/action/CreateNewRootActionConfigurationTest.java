package com.agreemount.negotiation.document.action;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.action.ActionFactory;
import com.agreemount.slaneg.action.definition.CreateNewRoot;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class CreateNewRootActionConfigurationTest {

    @Autowired
    private ActionFactory actionFactory;

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Test
    public void shouldAddOrdersWhenCreatingNewRoot() {
        CreateNewRoot createNewRoot = new CreateNewRoot();
        createNewRoot.setAlias("newRoot");

        long time = System.currentTimeMillis();

        ActionContext ac = actionContextFactory.createInstance();
        actionFactory.create(createNewRoot).runWithSubaction(ac);

        Document document = ac.getDocument("newRoot");

        long orderSIDE1 = (long)document.getAttributes().get(OrderSide.ORDER_SIDE_1.getValue());
        long orderSIDE2 = (long)document.getAttributes().get(OrderSide.ORDER_SIDE_2.getValue());


        Assert.assertNotNull(orderSIDE1);
        Assert.assertNotNull(orderSIDE2);
        Assert.assertTrue(orderSIDE1 >= time);
        Assert.assertTrue(orderSIDE2 >= time);

    }
}
