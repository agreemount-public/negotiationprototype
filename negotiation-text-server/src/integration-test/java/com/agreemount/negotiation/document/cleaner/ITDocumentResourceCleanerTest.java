package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.document.model.DocumentResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ITDocumentResourceCleanerTest {

    @Autowired
    private DocumentResourceCleaner documentResourceCleaner;

    @Test
    public void givenDocumentResourceWhenDocumentBodyContainsTagsThatAreNotAcceptedOrRemainOnesThenReturnTextWithoutTheseTags() {
        String body = "<D2>Podział majątku i obowiązków pomiędzy Adam i Ewa:</D2>"
                + "- Ewa: <D6>Dom w zielonkach</D6>"
                + "- Ewa: Samochod marki Peugoth"
                + "<D4></D4>"
                + "- <D7>Adam</D7>: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A. <D3></D3>"
                + "<D8></D8>";
        String tag = "D1";

        Map<String, String> documentParts = new HashMap<>();
        documentParts.put("D2", "Podział majątku pomiędzy Adam i Ewa <D3>po równo</D3>");
        documentParts.put("D3", "Adam:Ewa, 70%:30%");
        documentParts.put("D4", "<D3>-Ewa: pełny </D3>udział w spółce MBM Group<D5></D5>");
        documentParts.put("D5", " i akcje PKN Orlen");
        documentParts.put("D6", " połowa domu w Zielonkach");
        documentParts.put("D7", " Ewa");
        documentParts.put("D8", " Adam: Rybki piranie");

        Set<String> acceptedTags = new HashSet();
        acceptedTags.add("D2");
        acceptedTags.add("D4");
        Set<String> remainTags = new HashSet();
        remainTags.add("D6");
        remainTags.add("D8");

        DocumentResource resource = new DocumentResource(
                acceptedTags,
                remainTags,
                body,
                tag,
                documentParts
        );


        String expectedBody = "<D2>Podział majątku i obowiązków pomiędzy Adam i Ewa:</D2>"
                + "- Ewa: <D6>Dom w zielonkach</D6>"
                + "- Ewa: Samochod marki Peugoth"
                + "<D4></D4>"
                + "- Adam: Działka letniskowa "
                + "- Ewa: udziały w firmie X S.A. "
                + "<D8></D8>";

        Map<String, String> expectedParts = new HashMap<>();
        expectedParts.put("D2", "Podział majątku pomiędzy Adam i Ewa po równo");
        expectedParts.put("D4", "-Ewa: pełny udział w spółce MBM Group");
        expectedParts.put("D6", " połowa domu w Zielonkach");
        expectedParts.put("D8", " Adam: Rybki piranie");


        DocumentResource actualDocumentResource = (DocumentResource) documentResourceCleaner.clean(resource);

        assertNotNull(actualDocumentResource);
        assertEquals(expectedBody, actualDocumentResource.getBody());
        assertEquals(expectedParts, actualDocumentResource.getDocumentParts());

        assertEquals(acceptedTags, actualDocumentResource.getAcceptedTags());
        assertEquals(remainTags, actualDocumentResource.getRemainTags());

    }
}
