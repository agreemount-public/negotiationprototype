package com.agreemount.negotiation;

import com.agreemount.negotiation.engine.yamlparser.YamlProvidersConfiguration;
import com.agreemount.slaneg.fixtures.CachingRuleProvider;
import com.agreemount.slaneg.fixtures.DirectoryYamlProvider;
import com.agreemount.slaneg.fixtures.RulesProvider;
import com.agreemount.testing.TestingScenario;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;



@Configuration
@Import(YamlProvidersConfiguration.class)
public class ITYamlProvidersConfiguration {
    @Bean
    public RulesProvider<TestingScenario> testingScenarios() {
        return new DirectoryYamlProvider<>("testingScenarios");
    }

    @Bean(name = "testingScenariosYamlProvider")
    public RulesProvider<TestingScenario> testingScenariosYamlProvider(
            RulesProvider<TestingScenario> testingScenarios) {
        return new CachingRuleProvider<>(testingScenarios);
    }
}
