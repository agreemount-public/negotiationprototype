package com.agreemount.negotiation.rules;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.db.DocumentOperations;
import com.agreemount.slaneg.db.RelationOperations;
import com.agreemount.slaneg.fixtures.RulesProvider;
import com.agreemount.testing.TestIdentityManager;
import com.agreemount.testing.TestingScenario;
import com.agreemount.testing.action.step.TestStep;
import com.agreemount.testing.action.step.impl.TestStepImpl;
import com.agreemount.testing.action.step.impl.TestStepImplementationFactory;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.StringUtils;

import java.io.IOException;




@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationTest.class})
@WebAppConfiguration
@Log4j
public class RulesTest {

    @Autowired
    @Qualifier("testingScenariosYamlProvider")
    private RulesProvider<TestingScenario> testingScenariosYamlProvider;

    @Autowired
    private TestStepImplementationFactory testStepImplementationFactory;

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Autowired
    private TestIdentityManager identityStorage;

    @Autowired
    private DocumentOperations documentOperations;

    @Autowired
    private RelationOperations relationOperations;

    @Test
    public void testScenarios() throws IOException {

        clearDb();

        for (TestingScenario testingScenario : testingScenariosYamlProvider.getItems()) {
            testScenario(testingScenario);
        }

        //testDb();
    }

    private void clearDb() {
        documentOperations.removeAll();
        relationOperations.removeAll();
    }


    private void testScenario(TestingScenario testingScenario) {
        log.info("=============================================================");
        log.info("running test scenario: " + testingScenario.getDescription());
        log.info("=============================================================");

        identityStorage.setIdentities(testingScenario.getIdentities());

        ActionContext scenarioContext = actionContextFactory.createInstance();

        for(TestStep step : testingScenario.getSteps()) {

            if(!StringUtils.isEmpty(step.getAsUser())) {
                identityStorage.pretendToBe(step.getAsUser());
            }

            //runImplementation step
            TestStepImpl impl = testStepImplementationFactory.getImpl(step);
            impl.setDefinition(step);
            impl.run(scenarioContext);

            identityStorage.resetIdentity();
        }

        identityStorage.reset();

        log.info("=============================================================" );
        log.info("end of test scenario: " + testingScenario.getDescription());
        log.info("=============================================================");

    }

}
