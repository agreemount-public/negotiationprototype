package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.payment.PayPalResponse;
import com.agreemount.negotiation.dao.IPayPalResponseDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITPayPalResponseDAOTest {

    private static final String TXT_ID = "87T97255UA8044301";

    @Autowired
    private IPayPalResponseDAO payPalResponseDAO;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void shouldSaveAllDefinedFields() {
        // given
        Map<String, String> payload = getPayload();

        // when
        payPalResponseDAO.insert(payload);

        // then
        PayPalResponse actualResponse = findRecordByTxnId(TXT_ID);

        PayPalResponse expectedResponse = new PayPalResponse();
        expectedResponse.setPayerEmail("paypal_buyer@agreemount.com");
        expectedResponse.setReceiverEmail("paypal_merchant@agreemount.com");
        expectedResponse.setPayerStatus("verified");
        expectedResponse.setReceiverId("7G3PQUPZ3TZ7J");
        expectedResponse.setTxnId(TXT_ID);
        expectedResponse.setOptionSelection1("100 operations");
        expectedResponse.setPaymentGross("20.00");
        expectedResponse.setVerifySign("A3HZzQjxzoBA2-AmrrMtKbeXSlyhAEPvtWdj7gj6csPgXu9E2idm8Pu-");
        expectedResponse.setFirstName("John");
        expectedResponse.setPaymentDate("09: 03: 40 Apr 08 2017 PDT");
        expectedResponse.setBusiness("paypal_merchant@agreemount.com");
        expectedResponse.setPaymentStatus("Completed");
        expectedResponse.setAgreemountUserId("109631119191086058339");
        expectedResponse.setLastName("Travolta");
        expectedResponse.setPayerId("PK446GZY66C7A");
        expectedResponse.setIpnTrackId("b7b3a3abcb078");
        expectedResponse.setPayload(getPayload());

        assertThat(actualResponse).isEqualToIgnoringGivenFields(expectedResponse, "id", "createdAt");
        assertThat(actualResponse.getCreatedAt()).isNotNull();
        assertThat(actualResponse.getId()).isNotNull();
    }

    private Map<String, String> getPayload() {
        Map<String, String> payload = new HashMap<>();
        payload.put("charset", "windows-1252");
        payload.put("payer_email", "paypal_buyer@agreemount.com");
        payload.put("receiver_email", "paypal_merchant@agreemount.com");
        payload.put("payer_status", "verified");
        payload.put("receiver_id", "7G3PQUPZ3TZ7J");
        payload.put("item_number", "");
        payload.put("transaction_subject", "");
        payload.put("residence_country", "US");
        payload.put("test_ipn", "1");
        payload.put("txn_id", TXT_ID);
        payload.put("option_selection1", "100 operations");
        payload.put("payment_gross", "20.00");
        payload.put("protection_eligibility", "Eligible");
        payload.put("verify_sign", "A3HZzQjxzoBA2-AmrrMtKbeXSlyhAEPvtWdj7gj6csPgXu9E2idm8Pu-");
        payload.put("first_name", "John");
        payload.put("payment_date", "09: 03: 40 Apr 08 2017 PDT");
        payload.put("quantity", "1");
        payload.put("business", "paypal_merchant@agreemount.com");
        payload.put("payment_status", "Completed");
        payload.put("custom", "109631119191086058339");
        payload.put("last_name", "Travolta");
        payload.put("option_name1", "Options");
        payload.put("item_name", "Agreemount Button");
        payload.put("notify_version", "3.8");
        payload.put("mc_currency", "USD");
        payload.put("payment_type", "instant");
        payload.put("txn_type", "web_accept");
        payload.put("payment_fee", "0.88");
        payload.put("payer_id", "PK446GZY66C7A");
        payload.put("mc_fee", "0.88");
        payload.put("mc_gross", "20.00");
        payload.put("ipn_track_id", "b7b3a3abcb078");
        return payload;
    }

    private PayPalResponse findRecordByTxnId(String txnId) {
        return mongoTemplate.findOne(new Query(Criteria.where("txnId").is(txnId)), PayPalResponse.class);
    }

    @Before
    @After
    public void cleanData() {
        payPalResponseDAO.deleteAll(PayPalResponse.class);
    }
}
