package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.INotificationDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITNotificationDAOTest {

    @Autowired
    private INotificationDAO notificationDAO;

    @Test
    public void whenGettingNextNotificationInEmptyDBThenReturnNull() {
        assertNull(notificationDAO.getNext());
    }

    @Test
    public void whenGettingNextNotificationInDBWithOnlyProcessedNotificationsTenReturnNull() {
        Notification notification = new Notification();
        notification.setNotificationStatus(NotificationStatus.PROCESSED);
        notificationDAO.save(notification);

        assertNull(notificationDAO.getNext());
    }

    @Test
    public void whenGettingNextNotificationInDBWithNotificationInProperStateThenReturnIt() {
        Notification notification = new Notification();
        notification.setNotificationStatus(NotificationStatus.NEW);
        notification.setNotificationType(NotificationType.ADD_SIDE);
        notificationDAO.save(notification);

        Notification actual = notificationDAO.getNext();

        assertNotNull(actual);
        assertEquals(NotificationType.ADD_SIDE, actual.getNotificationType());
    }

    @Before
    @After
    public void cleanData() {
        notificationDAO.deleteAll(Notification.class);
    }

}
