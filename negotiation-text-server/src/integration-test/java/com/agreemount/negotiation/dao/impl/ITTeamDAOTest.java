package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITTeamDAOTest {

    public static final String TEAM_1 = "firstTeam";
    public static final String TEAM_2 = "secondTeam";
    public static final String TEAM_3 = "thirdTeam";
    public static final String USER_1 = "owner";
    public static final String USER_2 = "invited";

    @Autowired
    private ITeamDAO teamDAO;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void shouldSaveTeam() {
        Team team = getTeam(TEAM_1);

        teamDAO.save(team);

        Team myTeam = findTeam(TEAM_1);
        assertThat(myTeam).hasFieldOrPropertyWithValue("name", TEAM_1);
        assertThat(myTeam).hasFieldOrProperty("id").isNotNull();
        assertThat(myTeam).hasFieldOrProperty("users2Team").isNotNull();
    }


    @Test
    public void shouldSaveTeamWithGeneratedTeamNameWhenNameIsNotSetDirectly() {
        Team team = new Team();

        teamDAO.save(team);

        Team myTeam = mongoTemplate.findOne(new Query(), Team.class);
        assertThat(myTeam).hasFieldOrProperty("name").isNotNull();
        assertThat(myTeam).hasFieldOrProperty("id").isNotNull();
        assertThat(myTeam).hasFieldOrProperty("users2Team").isNotNull();
    }

    @Test
    public void shouldReturnTeamByName() {
        teamDAO.save(getTeam(TEAM_1));

        Team team = teamDAO.getTeamByName(TEAM_1);

        assertThat(team).hasFieldOrPropertyWithValue("name", TEAM_1);
    }


    @Test
    public void shouldReturnUserTeams() {
        teamDAO.save(getTeamWithMembers(TEAM_1, Lists.newArrayList(getMember(USER_1, UserRole.SIDE1), getMember(USER_2, UserRole.SIDE2))));
        teamDAO.save(getTeamWithMembers(TEAM_2, Lists.newArrayList(getMember(USER_2, UserRole.SIDE2))));
        teamDAO.save(getTeamWithMembers(TEAM_3, Lists.newArrayList(getMember(USER_1, UserRole.SIDE2))));

        List<Team> userTeams = teamDAO.getUserTeams(USER_2);

        assertThat(userTeams).extracting("name").containsExactly(TEAM_1, TEAM_2);
    }

    @Test
    public void shouldRemoveMembersByRole() {
        teamDAO.save(getTeamWithMembers(TEAM_1, Lists.newArrayList(getMember(USER_1, UserRole.SIDE1))));
        teamDAO.save(getTeamWithMembers(TEAM_2, Lists.newArrayList(getMember(USER_1, UserRole.SIDE2))));
        teamDAO.save(getTeamWithMembers(TEAM_3, Lists.newArrayList(getMember(USER_1, UserRole.SIDE1), getMember(USER_2, UserRole.SIDE2))));

        teamDAO.deleteMembersFromTeamByUserRole(UserRole.SIDE2);

        assertThat(mongoTemplate.findAll(Team.class)).usingElementComparatorIgnoringFields("id").containsExactly(
                getTeamWithMembers(TEAM_1, Lists.newArrayList(getMember(USER_1, UserRole.SIDE1))),
                getTeamWithMembers(TEAM_2, Lists.newArrayList()),
                getTeamWithMembers(TEAM_3, Lists.newArrayList(getMember(USER_1, UserRole.SIDE1)))
        );
    }

    @Test
    public void shouldAddUserToTeamAndSaveIt() {
        teamDAO.save(getTeam(TEAM_1));

        teamDAO.addMemberToTeam(getMember(USER_2, UserRole.SIDE2), TEAM_1);

        assertThat(findTeam(TEAM_1)).hasFieldOrPropertyWithValue("name", TEAM_1);
    }

    @Test
    public void shouldDeleteMember() {
        teamDAO.save(getTeamWithMembers(TEAM_1, Lists.newArrayList(getMember(USER_1, UserRole.SIDE1), getMember(USER_2, UserRole.SIDE2))));

        teamDAO.deleteByLogin(USER_1);

        assertThat(mongoTemplate.findAll(Team.class)).usingElementComparatorIgnoringFields("id").containsExactly(
                getTeamWithMembers(TEAM_1, Lists.newArrayList(getMember(USER_2, UserRole.SIDE2)))
        );
    }

    private User2Team getMember(String login, UserRole side) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(side);
        return user2Team;
    }

    private Team getTeam(String name) {
        Team team = new Team();
        team.setName(name);
        return team;
    }

    private Team getTeamWithMembers(String name, List<User2Team> users) {
        Team team = getTeam(name);
        team.setUsers2Team(users);
        return team;
    }

    private Team findTeam(String teamName) {
        return mongoTemplate.findOne(new Query(Criteria.where("name").is(teamName)), Team.class);
    }


    @Before
    @After
    public void cleanData() {
        teamDAO.deleteAll(Team.class);
    }

}