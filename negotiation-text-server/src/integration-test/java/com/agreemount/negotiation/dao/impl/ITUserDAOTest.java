package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.providers.Facebook;
import com.agreemount.negotiation.dao.IUserDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITUserDAOTest {

    public static final String USER_ID = "123";
    public static final String USER_EMAIL = "bob@builder.com";

    @Autowired
    private IUserDAO userDAO;

    @Test
    public void shouldGetUserById() {
        userDAO.save(getUser());

        User userFromDB = userDAO.getById(USER_ID);

        assertThat(userFromDB).hasFieldOrPropertyWithValue("id", USER_ID);
    }

    @Test
    public void shouldReturnNullWhenFindingUserByIdNotFound() {
        User userFromDB = userDAO.getById(USER_ID);

        assertThat(userFromDB).isNull();
    }

    @Test
    public void shouldGetUserByEmail() {
        userDAO.save(getUser());

        User userFromDB = userDAO.getByEmail(USER_EMAIL);

        assertThat(userFromDB).hasFieldOrPropertyWithValue("email", USER_EMAIL);
    }

    @Test
    public void shouldReturnNullWhenFindingUserByEmailNotFound() {
        User userFromDB = userDAO.getByEmail(USER_EMAIL);

        assertThat(userFromDB).isNull();
    }

    @Test
    public void shouldCreateUserWithInitialPaymentUnits() {
        userDAO.insert(getUser());

        User userFromDB = userDAO.getByEmail(USER_EMAIL);

        assertThat(userFromDB.getPaymentUnits()).isEqualTo(UserDAO.INITIAL_PAYMENT_UNITS);
    }

    @Before
    @After
    public void cleanData() {
        userDAO.deleteAll(User.class);
    }

    private User getUser() {
        User user = new Facebook();
        user.setId(USER_ID);
        user.setEmail(USER_EMAIL);
        return user;
    }

}