package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.PreparedEmail;
import com.agreemount.negotiation.bean.notification.GeneratedNotification;
import com.agreemount.negotiation.bean.notification.GeneratedNotificationStatus;
import com.agreemount.negotiation.dao.IGeneratedNotificationDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITGeneratedNotificationDAOTest {

    private final String mailBob = "bob@mailinator.com";
    private final String mailJohn = "john@mailinator.com";
    private final String mailPaul = "paul@mailinator.com";
    private final String mailAnn = "ann@mailinator.com";
    @Autowired
    private IGeneratedNotificationDAO generatedNotificationDAO;

    @Test
    public void whenGettingNextNotificationThenReturnNotificationInProperState() {
        GeneratedNotification next;

        next = generatedNotificationDAO.getNext();
        assertEquals(mailAnn, next.getPreparedEmail().getReceiver());

        next = generatedNotificationDAO.getNext();
        assertEquals(mailAnn, next.getPreparedEmail().getReceiver());

        generatedNotificationDAO.delete(next);

        next = generatedNotificationDAO.getNext();
        assertEquals(mailJohn, next.getPreparedEmail().getReceiver());

        generatedNotificationDAO.delete(next);

        next = generatedNotificationDAO.getNext();
        assertNull(next);
    }

    @Before
    public void setUp() {
        generatedNotificationDAO.deleteAll(GeneratedNotification.class);
        prepareData();
    }

    @After
    public void tearDown() {
        generatedNotificationDAO.deleteAll(GeneratedNotification.class);
    }

    private void prepareData() {
        LocalDateTime today = LocalDateTime.now();

        GeneratedNotification notificationTemp1 = getGeneratedNotifi(
                "content of email 1",
                mailBob,
                GeneratedNotificationStatus.PROCESSED,
                today.minusHours(1)
        );

        GeneratedNotification notificationTemp2 = getGeneratedNotifi(
                "content of email 2",
                mailJohn,
                GeneratedNotificationStatus.WAITING,
                today.minusDays(2)
        );

        GeneratedNotification notificationTemp3 = getGeneratedNotifi(
                "content of email 3",
                mailPaul,
                GeneratedNotificationStatus.WAITING,
                today.plusYears(100)
        );

        GeneratedNotification notificationTemp4 = getGeneratedNotifi(
                "content of email 4",
                mailAnn,
                GeneratedNotificationStatus.WAITING,
                today.minusHours(1)
        );

        generatedNotificationDAO.save(notificationTemp1);
        generatedNotificationDAO.save(notificationTemp2);
        generatedNotificationDAO.save(notificationTemp3);
        generatedNotificationDAO.save(notificationTemp4);
    }

    private GeneratedNotification getGeneratedNotifi(String content, String receiver, GeneratedNotificationStatus status,
                                                     LocalDateTime nextProcessing) {
        PreparedEmail email = new PreparedEmail();
        email.setContent(content);
        email.setReceiver(receiver);

        GeneratedNotification generatedNotification = new GeneratedNotification();
        generatedNotification.setCreatedAt(LocalDateTime.now());
        generatedNotification.setPreparedEmail(email);
        generatedNotification.setNextProcessing(nextProcessing);

        generatedNotification.setStatus(status);

        return generatedNotification;
    }

}






