package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.ApplicationTest;
import com.agreemount.negotiation.bean.ResetPassword;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationTest.class)
public class ITResetPasswordDAOTest {

    public static final String EMAIL_JOHN = "john@aaa.com";
    public static final String HASH_JOHN_1 = "hash-123";
    public static final String HASH_JOHN_2 = "hash-987";
    public static final String HASH_JOHN_3 = "hash-999";
    public static final String EMAIL_BOB = "bob@aaa.com";
    public static final String HASH_BOB = "hash-567";

    @Autowired
    private ResetPasswordDAO resetPasswordDAO;

    @Before
    @After
    public void cleanData() {
        resetPasswordDAO.deleteAll(ResetPassword.class);
    }

    @Test
    public void shouldGetRecordByHash() {
        persistResetPasswords(
                resetPassword(EMAIL_JOHN, HASH_JOHN_1, LocalDateTime.of(2018, Month.AUGUST, 28, 10, 11)),
                resetPassword(EMAIL_JOHN, HASH_JOHN_2, LocalDateTime.of(2018, Month.AUGUST, 28, 10, 12)),
                resetPassword(EMAIL_JOHN, HASH_JOHN_3, LocalDateTime.of(2018, Month.AUGUST, 28, 10, 19)),
                resetPassword(EMAIL_BOB, HASH_BOB, LocalDateTime.of(2018, Month.AUGUST, 28, 10, 10)));


        ResetPassword resetPassword = resetPasswordDAO.getByHash(HASH_JOHN_2);

        assertThat(resetPassword.getEmail()).isEqualTo(EMAIL_JOHN);
    }

    @Test
    public void shouldReturnNullWhenRecordNotFound() {
        persistResetPasswords(
                resetPassword(EMAIL_BOB, HASH_BOB, LocalDateTime.of(2018, Month.AUGUST, 28, 10, 10)));


        ResetPassword resetPassword = resetPasswordDAO.getByHash(HASH_JOHN_2);

        assertThat(resetPassword).isNull();
    }

    private ResetPassword resetPassword(String email, String hash, LocalDateTime localDateTime) {
        ResetPassword resetPassword = new ResetPassword();
        resetPassword.setEmail(email);
        resetPassword.setHash(hash);
        resetPassword.setCreatedAt(localDateTime);
        return resetPassword;
    }

    private void persistResetPasswords(ResetPassword... resetPasswords) {
        for (ResetPassword resetPassword : resetPasswords) {
            resetPasswordDAO.save(resetPassword);
        }
    }

}
