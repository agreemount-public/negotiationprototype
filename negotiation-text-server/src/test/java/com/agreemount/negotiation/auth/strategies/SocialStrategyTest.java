package com.agreemount.negotiation.auth.strategies;

import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserLogDAO;
import org.apache.commons.lang.NotImplementedException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class SocialStrategyTest {

    private static final boolean HAS_FREE_ACCESS_UNDEFINED = false;
    private static final boolean HAS_FREE_ACCESS = true;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private IUserDAO userDAO;

    @Mock
    private AccessVerifier accessVerifier;

    @Mock
    private UserLogDAO userLogDAO;

    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;

    @InjectMocks
    private SocialStrategy social = new SocialStrategy();

    private String USER_EMAIL = "bob@mailinator.com";

    private String USER_ID = "123";

    @Test
    public void shouldThrowExceptionWhenTryingToLogin() {
        thrown.expect(NotImplementedException.class);

        social.login("email", new char[]{'p', 'a', 's', 's'});
    }

    @Test
    public void shouldReturnUserWhenUserExistAndUpdateLastLoginDate() {
        // given
        User requestUser = prepareUser(USER_ID, USER_EMAIL, HAS_FREE_ACCESS_UNDEFINED);
        User persistedUser = prepareUser(USER_ID, USER_EMAIL, HAS_FREE_ACCESS);
        when(userDAO.getById(USER_ID)).thenReturn(persistedUser);

        // when
        this.social.signUp(requestUser);

        // then
        verify(accessVerifier, times(0)).hasUserFreeAccess(anyString());
        verify(userDAO, times(0)).save(requestUser);
        verify(userLogDAO, times(0)).insertNewLog(anyString(), anyString());
        verify(userDAO, times(1)).updateLastLoginDate(userArgumentCaptor.capture());

        assertThat(userArgumentCaptor.getValue()).extracting(User::isHasFreeAccess).containsExactly(HAS_FREE_ACCESS);
        assertThat(userArgumentCaptor.getValue()).extracting(User::getId).containsExactly(USER_ID);
        assertThat(userArgumentCaptor.getValue()).extracting(User::getEmail).containsExactly(USER_EMAIL);
    }

    @Test
    public void shouldSaveUserWhenUserDoesNotExistAndCheckIfSupposeToHaveFullAccess() {
        // given
        User user = prepareUser(USER_ID, USER_EMAIL);
        when(userDAO.getById(USER_ID)).thenReturn(null);
        when(accessVerifier.hasUserFreeAccess(USER_EMAIL)).thenReturn(true);

        // when
        this.social.signUp(user);

        // then
        verify(accessVerifier).hasUserFreeAccess(anyString());
        verify(userDAO, times(1)).insert(user);
        verify(userLogDAO).insertNewLog(USER_EMAIL, "give user full access during sign up");
    }

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhenTryingToLoginBlockedUser() {
        // given
        User user = mock(User.class);
        when(user.getId()).thenReturn(USER_ID);
        when(user.isBlocked()).thenReturn(true);
        when(userDAO.getById(USER_ID)).thenReturn(user);

        // when
        this.social.signUp(user);
    }


    private User prepareUser(String id, String email) {
        return prepareUser(id, email, false);
    }

    private User prepareUser(String id, String email, boolean hasFreeAccess) {
        com.agreemount.negotiation.bean.providers.Local user = new com.agreemount.negotiation.bean.providers.Local();
        user.setId(id);
        user.setPassword(email.toCharArray());
        user.setEmail(email);
        user.setHasFreeAccess(hasFreeAccess);
        return user;
    }

}
