package com.agreemount.negotiation.auth.strategies;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.user.UserService;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccessVerifierTest {

    @Mock
    private ITeamDAO teamDAO;

    @Mock
    private UserService userService;

    @InjectMocks
    private AccessVerifier verifier = new AccessVerifier();

    @Test
    public void shouldReturnFalseIfUserHasNoInvitations() {
        final String newUserEmail = "new.user@agree.com";

        List<Team> userTeams = new ArrayList<>();

        when(teamDAO.getUserTeams(newUserEmail)).thenReturn(userTeams);

        boolean result = verifier.hasUserFreeAccess(newUserEmail);
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueIfOneOfUserHasFullAccess() {
        final String newUserEmail = "new.user@agree.com";


        Team team1 = new Team();
        User2Team team1ToCurrentUser = new User2Team();
        team1ToCurrentUser.setLogin(newUserEmail);
        team1.addUser2Team(team1ToCurrentUser);

        final String inviterEmail = "mr.bean@luckyluck.com";
        User2Team team2Inviter = new User2Team();
        team2Inviter.setLogin(inviterEmail);
        team1.addUser2Team(team2Inviter);

        List<Team> userTeams = Arrays.asList(team1);

        when(teamDAO.getUserTeams(newUserEmail)).thenReturn(userTeams);
        User inviter = mock(User.class);
        when(inviter.isHasFreeAccess()).thenReturn(true);
        when(userService.getUsersOfTeam(any(), any())).thenReturn(Lists.newArrayList(inviter));

        boolean result = verifier.hasUserFreeAccess(newUserEmail);
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseIfUserHasInvitationsFromUsersWithoutFullAccess() {
        final String newUserEmail = "new.user@agree.com";


        Team team1 = new Team();
        User2Team team1ToCurrentUser = new User2Team();
        team1ToCurrentUser.setLogin(newUserEmail);
        team1.addUser2Team(team1ToCurrentUser);

        final String inviterEmail = "mr.bean@luckyluck.com";
        User2Team team2Inviter = new User2Team();
        team2Inviter.setLogin(inviterEmail);
        team1.addUser2Team(team2Inviter);

        List<Team> userTeams = Arrays.asList(team1);

        when(teamDAO.getUserTeams(newUserEmail)).thenReturn(userTeams);
        User inviter = mock(User.class);
        when(inviter.isHasFreeAccess()).thenReturn(false);
        when(userService.getUsersOfTeam(any(), any())).thenReturn(Lists.newArrayList(inviter));

        boolean result = verifier.hasUserFreeAccess(newUserEmail);
        assertFalse(result);
    }

}