package com.agreemount.negotiation.auth;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class PasswordTest {

    @Test
    public void givenSaltHasCorrectLength() {
        byte[] salt = Password.getSalt();
        assertThat("Expected length is", salt.length, is(64));
    }

    @Test
    public void givenHashHasBeenGenerated() throws Exception {
        final char[] password = "kotek123".toCharArray();
        final byte[] hash = Password.hashPassword(password, Password.getSalt());
        assertThat("Array is not null", hash, Matchers.notNullValue());
    }

    @Test
    public void givenIncorrectPasswordWhenCheckingThenReturnFalse() throws Exception {
        final char[] password = "kotek123".toCharArray();
        final byte[] salt = Password.getSalt();
        final byte[] hash = Password.hashPassword(password, salt);

        final char[] givenPassword = "trolololo".toCharArray();
        final boolean isPasswordCorrect = Password.isExpectedPassword(givenPassword, salt, hash);

        assertThat("Password is not correct", isPasswordCorrect, is(false));
    }

    @Test
    public void givenCorrectPasswordWhenCheckingThenReturnTrue() throws Exception {
        final char[] password = "kotek123".toCharArray();
        final byte[] salt = Password.getSalt();
        final byte[] hash = Password.hashPassword(password, salt);

        final char[] givenPassword = "kotek123".toCharArray();
        final boolean isPasswordCorrect = Password.isExpectedPassword(givenPassword, salt, hash);

        assertThat("Password is correct", isPasswordCorrect, is(true));
    }

    @Test
    public void givenCorruptedHashWhenCheckingThenReturnFalse() throws Exception {
        final char[] password = "kotek123".toCharArray();
        final byte[] salt = Password.getSalt();
        final byte[] hash = {0, 2, 3};
        final char[] givenPassword = "kotek123".toCharArray();
        final boolean isPasswordCorrect = Password.isExpectedPassword(givenPassword, salt, hash);

        assertThat("Password is not correct", isPasswordCorrect, is(false));
    }


}