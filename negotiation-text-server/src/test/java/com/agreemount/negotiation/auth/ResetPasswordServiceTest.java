package com.agreemount.negotiation.auth;


import com.agreemount.negotiation.bean.ResetPassword;
import com.agreemount.negotiation.bean.ResetPasswordDto;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.impl.NotificationDAO;
import com.agreemount.negotiation.dao.impl.ResetPasswordDAO;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.agreemount.negotiation.user.UserService;
import org.assertj.core.api.AbstractComparableAssert;
import org.assertj.core.api.IterableAssert;
import org.assertj.core.api.Java6Assertions;
import org.assertj.core.api.ListAssert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;

import java.util.List;
import java.util.Set;

import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.*;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResetPasswordServiceTest {

    public static final String EMAIL = "reset@email.org";
    public static final String HASH = "my-hash-123";
    public static final String DIFFERENT_PASS = "s1cr3t@";
    public static final String PASS = "kitty123";
    public static final boolean HAS_BEEN_USED = true;
    public static final boolean HAS_NOT_BEEN_USED = false;

    @InjectMocks
    private ResetPasswordService service;

    @Mock
    private ResetPasswordDAO resetPasswordDAO;

    @Mock
    private UserDAO userDAO;

    @Mock
    private NotificationDAO notificationDAO;

    @Mock
    private UserService userService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private ArgumentCaptor<ResetPassword> resetPasswordCaptor = ArgumentCaptor.forClass(ResetPassword.class);
    private ArgumentCaptor<Notification> notificationCaptor = ArgumentCaptor.forClass(Notification.class);
    private ResetPasswordDto resetPasswordDto;
    boolean passwordHasBeenUpdated;

    @Test
    public void shouldPersistConfirmationRecord() {
        givenUser(localUser(EMAIL));

        whenResetPasswordConfTriggered(EMAIL);

        ResetPassword resetPassword = resetPassword();
        assertThat(resetPassword.getEmail()).isEqualTo(EMAIL);
        assertThat(resetPassword.getHash()).isNotNull();
        assertThat(resetPassword.getCreatedAt()).isNotNull();
    }

    @Test
    public void shouldSendNotification() {
        givenUser(localUser(EMAIL));

        whenResetPasswordConfTriggered(EMAIL);

        thenNotificationType().isEqualTo(NotificationType.RESET_PASSWORD_CONFIRMATION);
        thenNotificationParameters().contains(
                new Notification.Parameter(TITLE.getValue(), "[Agreemount] Reset your password"),
                new Notification.Parameter(RECEIVER.getValue(), EMAIL)
        );
        thenNotificationParameterIsNotNull(CONFIRMATION_HASH.getValue()).isNotNull();
    }

    @Test
    public void shouldThrowExceptionWhenPasswordNotSet() {
        givenResetPasswordForHash(resetPasswordRecord(HASH, EMAIL, HAS_NOT_BEEN_USED), HASH);
        givenPersistedUserForEmail(localUser(EMAIL), EMAIL);
        givenResetPasswordDto(HASH, null, null);

        thenExpectedException(NullPointerException.class, "Password not set");

        whenResetPassword();
    }

    @Test
    public void shouldThrowExceptionWhenUserNotFound() {
        givenUser(null);

        thrown.expect(NullPointerException.class);
        thrown.expectMessage("User not found: " + EMAIL);

        whenResetPasswordConfTriggered(EMAIL);
    }

    @Test
    public void shouldThrowExceptionWhenUserNotFoundDuringReset() {
        givenResetPasswordForHash(resetPasswordRecord(HASH, EMAIL, HAS_NOT_BEEN_USED), HASH);
        givenPersistedUserForEmail(null, EMAIL);
        givenResetPasswordDto(HASH, PASS, PASS);

        thenExpectedException(NullPointerException.class, "User not found: " + EMAIL);

        whenResetPassword();
    }

    @Test
    public void shouldThrowExceptionWhenPasswordDifferentThanPasswordConfirmation() {
        givenResetPasswordForHash(resetPasswordRecord(HASH, EMAIL, HAS_NOT_BEEN_USED), HASH);
        givenPersistedUserForEmail(localUser(EMAIL), EMAIL);
        givenResetPasswordDto(HASH, PASS, DIFFERENT_PASS);

        thenExpectedException(IllegalStateException.class, "Given passwords are not equal");

        whenResetPassword();
    }

    @Test
    public void shouldThrowExceptionWhenResetPasswordNotFound() {
        givenResetPasswordForHash(null, HASH);
        givenPersistedUserForEmail(localUser(EMAIL), EMAIL);
        givenResetPasswordDto(HASH, PASS, PASS);

        thenExpectedException(NullPointerException.class, "Hash not found: " + HASH);

        whenResetPassword();
    }

    @Test
    public void shouldThrowExceptionWhenHashHasBeenAlreadyUsed() {
        Local user = localUser(EMAIL);
        givenResetPasswordForHash(resetPasswordRecord(HASH, EMAIL, HAS_BEEN_USED), HASH);
        givenPersistedUserForEmail(user, EMAIL);
        givenResetPasswordDto(HASH, PASS, PASS);

        thenExpectedException(IllegalStateException.class, "Hash has been already used: " + HASH);

        whenResetPassword();
    }

    @Test
    public void shouldChangePassword() {
        Local user = localUser(EMAIL);
        givenResetPasswordForHash(resetPasswordRecord(HASH, EMAIL, HAS_NOT_BEEN_USED), HASH);
        givenPersistedUserForEmail(user, EMAIL);
        givenResetPasswordDto(HASH, PASS, PASS);

        whenResetPassword();

        thenGeneratePasswordAndSaveIt(user);
    }


    @Test
    public void shouldMarkAsUsedAfterSuccessfulReset() {
        Local user = localUser(EMAIL);
        givenResetPasswordForHash(resetPasswordRecord(HASH, EMAIL, HAS_NOT_BEEN_USED), HASH);
        givenPersistedUserForEmail(user, EMAIL);
        givenResetPasswordDto(HASH, PASS, PASS);

        whenResetPassword();

        thenResetPasswordMarkAsUsed();
    }

    @Test
    public void shouldSendNotificationWhenPasswordChanged() {
        givenResetPasswordForHash(resetPasswordRecord(HASH, EMAIL, HAS_NOT_BEEN_USED), HASH);
        givenPersistedUserForEmail(localUser(EMAIL), EMAIL);
        givenResetPasswordDto(HASH, PASS, PASS);

        whenResetPassword();

        thenNotificationType().isEqualTo(NotificationType.RESET_PASSWORD);
        thenNotificationParameters().contains(
                new Notification.Parameter(TITLE.getValue(), "[Agreemount] Your password has been changed"),
                new Notification.Parameter(RECEIVER.getValue(), EMAIL)
        );
    }

    private void thenResetPasswordMarkAsUsed() {
        ArgumentCaptor<ResetPassword> captor = ArgumentCaptor.forClass(ResetPassword.class);
        verify(resetPasswordDAO).save(captor.capture());
        ResetPassword value = captor.getValue();
        assertThat(value.isUsed()).isTrue();
    }

    private void thenGeneratePasswordAndSaveIt(Local user) {
        verify(userService).setPasswordForUser(user);
        verify(userDAO).save(any());
    }

    private ResetPassword resetPasswordRecord(String hash, String email, boolean hasBeenUsed) {
        ResetPassword resetPassword = new ResetPassword();
        resetPassword.setHash(hash);
        resetPassword.setEmail(email);
        resetPassword.setUsed(hasBeenUsed);
        return resetPassword;
    }

    private OngoingStubbing<ResetPassword> givenResetPasswordForHash(ResetPassword resetRecord, String hash) {
        return when(resetPasswordDAO.getByHash(hash)).thenReturn(resetRecord);
    }

    private void givenPersistedUserForEmail(Local result, String email) {
        when(userDAO.getByEmail(email)).thenReturn(result);
    }

    private ResetPasswordDto resetPasswordDto(String hash, String password, String passwordConf) {
        ResetPasswordDto resetPasswordDto = new ResetPasswordDto();
        resetPasswordDto.setHash(hash);
        if (password != null) {
            resetPasswordDto.setPassword(password.toCharArray());
        }
        if (passwordConf != null) {
            resetPasswordDto.setPasswordConfirmation(passwordConf.toCharArray());
        }
        return resetPasswordDto;
    }

    private ResetPassword resetPassword() {
        verify(resetPasswordDAO).save(resetPasswordCaptor.capture());
        return resetPasswordCaptor.getValue();
    }

    private void whenResetPasswordConfTriggered(String email) {
        service.resetPasswordConfirmation(email);
    }

    private void givenUser(Local user) {
        givenPersistedUserForEmail(user, EMAIL);
    }

    private Notification thenNotification() {
        ArgumentCaptor<Notification> captor = ArgumentCaptor.forClass(Notification.class);
        verify(notificationDAO).save(captor.capture());
        return captor.getValue();
    }

    private IterableAssert<Notification.Parameter> thenNotificationParameters() {
        return Java6Assertions.assertThat(thenNotification().getParameters());
    }

    private ListAssert<String> thenNotificationParameterIsNotNull(String key) {
        Set<Notification.Parameter> parameters = thenNotification().getParameters();

        for (Notification.Parameter parameter : parameters) {
            if (parameter.getKey().equals(key)) {
                return Java6Assertions.assertThat(parameter.getValue());
            }
        }

        return Java6Assertions.assertThat((List<String>) null);
    }

    private AbstractComparableAssert<?, NotificationType> thenNotificationType() {
        return Java6Assertions.assertThat(thenNotification().getNotificationType());
    }

    private Local localUser(String email) {
        Local localUser = new Local();
        localUser.setEmail(email);
        return localUser;
    }

    private void givenResetPasswordDto(String hash, String pass, String passConf) {
        resetPasswordDto = resetPasswordDto(hash, pass, passConf);
    }

    private void whenResetPassword() {
        service.resetPassword(resetPasswordDto);
    }

    private void thenExpectedException(Class<? extends Exception> exeption, String message) {
        thrown.expectMessage(message);
        thrown.expect(exeption);
    }
}