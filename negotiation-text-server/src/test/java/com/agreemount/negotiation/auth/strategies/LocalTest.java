package com.agreemount.negotiation.auth.strategies;

import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.INotificationDAO;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserLogDAO;
import com.agreemount.negotiation.user.UserService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class LocalTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private IUserDAO userDAO;

    @Mock
    private INotificationDAO notificationDAO;

    @Mock
    private AccessVerifier accessVerifier;

    @Mock
    private UserLogDAO userLogDAO;

    @Mock
    private UserService userService;

    @InjectMocks
    private Local local = new Local();

    private String USER_EMAIL = "bob@mailinator.com";

    @Test
    public void shouldThrowExceptionWhenPasswordNotGiven() {
        com.agreemount.negotiation.bean.providers.Local user = new com.agreemount.negotiation.bean.providers.Local();

        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Password is not set");

        this.local.signUp(user);
    }

    @Test
    public void shouldThrowExceptionWhenEmailIsTaken() {
        User user = prepareUser();
        when(userDAO.getByEmail(USER_EMAIL)).thenReturn(user);

        thrown.expect(AuthException.class);
        thrown.expectMessage("That email is already taken:" + user.getEmail());

        this.local.signUp(user);
    }

    @Test
    public void shouldSignUpUser() {
        com.agreemount.negotiation.bean.providers.Local user = new com.agreemount.negotiation.bean.providers.Local();
        user.setPassword("kotek123".toCharArray());
        user.setEmail(USER_EMAIL);

        when(accessVerifier.hasUserFreeAccess(anyString())).thenReturn(true);
        when(userService.setPasswordForUser(any())).thenReturn(enricherdUser(user));

        this.local.signUp(user);

        assertNotNull(user.getHash());
        assertNotNull(user.getSalt());
        assertNotNull(user.getConfirmationHash());
        assertFalse(user.isConfirmed());
        assertTrue(user.isHasFreeAccess());

        verify(userDAO, times(1)).getByEmail(anyString());
        verify(userDAO, times(1)).insert(user);

        ArgumentCaptor<Notification> captor = ArgumentCaptor.forClass(Notification.class);
        verify(notificationDAO, times(1)).save(captor.capture());
        assertEquals(captor.getValue().getNotificationType(), NotificationType.ACCOUNT_ACTIVATION);
    }

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhenTryingToLoginBlockedUser() {
        char[] password = {'k', 'o', 't', 'e', 'k', '1', '2', '3'};

        com.agreemount.negotiation.bean.providers.Local user = new com.agreemount.negotiation.bean.providers.Local();
        user.setPassword(password);
        user.setEmail(USER_EMAIL);

        when(accessVerifier.hasUserFreeAccess(anyString())).thenReturn(true);
        when(userService.setPasswordForUser(any())).thenReturn(enricherdUser(user));

        this.local.signUp(user);

        user.setConfirmed(true);
        user.setBlocked(true);

        when(userDAO.getByEmail(USER_EMAIL)).thenReturn(user);

        local.login(USER_EMAIL, password);
    }

    @Test
    public void shouldRewriteNullPointerExceptionMessage() {
        when(userDAO.getByEmail(USER_EMAIL)).thenReturn(prepareUser());

        thrown.expect(AuthException.class);
        thrown.expectMessage("Authentication failed: incorrect username or password");

        this.local.login(USER_EMAIL, new char[]{'k', 'o', 't', 'e', 'k', '1', '2', '3'});
    }

    private com.agreemount.negotiation.bean.providers.Local enricherdUser(com.agreemount.negotiation.bean.providers.Local user) {
        user.setHash("123".getBytes());
        user.setSalt("123".getBytes());
        user.setConfirmationHash("123");
        return user;
    }


    private User prepareUser() {
        com.agreemount.negotiation.bean.providers.Local user = new com.agreemount.negotiation.bean.providers.Local();
        user.setPassword(new char[]{'k', 'o', 't', 'e', 'k', '1', '2', '3'});
        user.setEmail(USER_EMAIL);
        return user;
    }


}
