package com.agreemount.negotiation.logic;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.logic.impl.TeamLogic;
import com.agreemount.negotiation.user.UserService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;


@RunWith(MockitoJUnitRunner.class)
public class TeamLogicTest {

    private static final String TEAM_NAME = "team1";
    private static final String USER_2 = "user2";
    private static final String NOT_EXISTING_TEAM = "team2";
    private static final String USER_1 = "user1";
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Mock
    private ITeamDAO teamDAO;
    @Mock
    private UserService userService;
    @InjectMocks
    private ITeamLogic teamLogic = new TeamLogic();

    @Test
    public void shouldSaveTeamWithOwner() {
        teamLogic.saveTeamWithOwner("user");
        verify(teamDAO, times(1)).save(any());
    }

    @Test
    public void shouldThrowExceptionWhenTeamNotGiven() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Cannot save team without a member");

        teamLogic.saveTeamWithOwner(null);
        verify(teamDAO, times(0)).save(any());
    }

    @Test
    public void shouldNotFindUserForNotExistingTeam() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(getTeam());

        Optional<User2Team> userWithRole = teamLogic.getUserWithRole(NOT_EXISTING_TEAM, UserRole.SIDE2);

        assertThat(userWithRole.isPresent()).isEqualTo(false);
    }

    @Test
    public void shouldNotFindUserWithNotDefinedUsersCollection() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(new Team());

        Optional<User2Team> userWithRole = teamLogic.getUserWithRole(TEAM_NAME, UserRole.SIDE2);

        assertThat(userWithRole.isPresent()).isEqualTo(false);
    }

    @Test
    public void shouldNotFindUserForNotExistingSide() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(getTeam());

        Optional<User2Team> userWithRole = teamLogic.getUserWithRole(TEAM_NAME, UserRole.SIDE1);

        assertThat(userWithRole.isPresent()).isEqualTo(false);
    }

    @Test
    public void shouldFindUserForSpecificRole() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(getTeam());

        User2Team userWithRole = teamLogic.getUserWithRole(TEAM_NAME, UserRole.SIDE2).get();

        assertThat(userWithRole).isEqualTo(expectedUser(USER_2, UserRole.SIDE2));
    }

    @Test
    public void shouldFindBothSidesUsers() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(getTeam(true));

        User2Team userWithRole1 = teamLogic.getUserWithRole(TEAM_NAME, UserRole.SIDE1).get();
        User2Team userWithRole2 = teamLogic.getUserWithRole(TEAM_NAME, UserRole.SIDE2).get();

        assertThat(userWithRole1).isEqualTo(expectedUser(USER_1, UserRole.SIDE1));
        assertThat(userWithRole2).isEqualTo(expectedUser(USER_2, UserRole.SIDE2));
    }

    @Test
    public void shouldFindSide2UserRoleInTeamForSpecificUser() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(getTeam());

        UserRole userRole = teamLogic.userRoleInTeam(TEAM_NAME, USER_2).get();

        assertThat(userRole).isEqualTo(UserRole.SIDE2);
    }

    @Test
    public void shouldNotFindSideUserRoleInTeamForNonExistingUser() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(getTeam(true));

        Optional<UserRole> userRole = teamLogic.userRoleInTeam(TEAM_NAME, "nonExistingUserLogin");

        assertThat(userRole).isEmpty();
    }

    @Test
    public void shouldNotFindSideUserRoleInTeamForNonExistingTeamName() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(getTeam(true));

        Optional<UserRole> userRole = teamLogic.userRoleInTeam("nonExistingTeamName", USER_2);

        assertThat(userRole).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEwhenTryToFindSideUserRoleInTeamForGivenNullUser() {
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(getTeam(true));

        teamLogic.userRoleInTeam(TEAM_NAME, null);

    }

    @Test
    public void shouldFetchUsersForUsersTeams() {
        final String email = "em@ail.com";
        final String sideEmail1 = "side1@mail.com";
        final String sideEmail2 = "side2@mail.com";

        User2Team user2Team1 = mock(User2Team.class);
        when(user2Team1.getLogin()).thenReturn(email);

        User2Team user2Team2 = mock(User2Team.class);
        when(user2Team2.getLogin()).thenReturn(sideEmail1);

        Team team1 = new Team();
        Team team2 = new Team();
        Team team3 = new Team();

        User sideUser1 = mock(User.class);
        when(sideUser1.getEmail()).thenReturn(sideEmail1);

        User sideUser2 = mock(User.class);
        when(sideUser2.getEmail()).thenReturn(sideEmail2);


        when(userService.getUsersOfTeam(team1, email)).thenReturn(Arrays.asList(sideUser1));
        when(userService.getUsersOfTeam(team2, email)).thenReturn(Arrays.asList(sideUser2));
        when(userService.getUsersOfTeam(team3, email)).thenReturn(Arrays.asList());

        when(teamDAO.getUserTeams(email)).thenReturn(Arrays.asList(team1, team2, team3));

        List<User> users = teamLogic.getNegotiationSidesOf(email);
        assertThat(users).containsExactly(sideUser1, sideUser2);
    }

    @Test
    public void shouldReturnEmpyListOfSideUsersIfUserHasNoTeams() {
        final String email = "em@ail.com";

        when(teamDAO.getUserTeams(email)).thenReturn(Arrays.asList());

        List<User> users = teamLogic.getNegotiationSidesOf(email);
        assertThat(users).isEmpty();
    }


    private User2Team expectedUser(String user, UserRole side) {
        User2Team expected = new User2Team();
        expected.setLogin(user);
        expected.setRole(side);
        return expected;
    }

    private Team getTeam() {
        return getTeam(false);
    }

    private Team getTeam(boolean withBothSides) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(USER_2);
        user2Team.setRole(UserRole.SIDE2);

        List<User2Team> users2Team = new ArrayList<>();
        users2Team.add(user2Team);

        if (withBothSides == true) {
            User2Team user2Team2 = new User2Team();
            user2Team2.setLogin(USER_1);
            user2Team2.setRole(UserRole.SIDE1);
            users2Team.add(user2Team2);
        }

        Team team = new Team();
        team.setName(TEAM_NAME);
        team.setUsers2Team(users2Team);

        return team;
    }

}
