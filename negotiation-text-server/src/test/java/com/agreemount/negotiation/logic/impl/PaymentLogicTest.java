package com.agreemount.negotiation.logic.impl;

import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.UserPaymentUnitsEntry;
import com.agreemount.negotiation.bean.providers.Facebook;
import com.agreemount.negotiation.dao.impl.PayPalResponseDAO;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.google.common.collect.Iterables;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class PaymentLogicTest {
    private static final String USER_ID = "10";
    private static final long PAYMENT_UNITS_TO_ADD = 4000L;
    private static final long INITIAL_PAYMENT_UNITS = 500L;
    private static final String REST_PAYMENT = "REST payment";

    @InjectMocks
    private PaymentLogic paymentLogic;

    @Mock
    private UserDAO userDAO;

    @Mock
    private PayPalResponseDAO payPalResponseDAO;

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhenUserNotFound() {
        // given
        when(userDAO.getById(USER_ID)).thenThrow(NullPointerException.class);

        // when
        paymentLogic.changeAccountBalanceManually(USER_ID, PAYMENT_UNITS_TO_ADD);
    }

    @Test
    public void shouldChangeUserAccountBalance() {
        // given
        when(userDAO.getById(USER_ID)).thenReturn(getUser(USER_ID, INITIAL_PAYMENT_UNITS));

        // when
        paymentLogic.changeAccountBalanceManually(USER_ID, PAYMENT_UNITS_TO_ADD);

        // then
        User expectedUser = getUserWithHistoryEntry(USER_ID, INITIAL_PAYMENT_UNITS, PAYMENT_UNITS_TO_ADD);
        verify(userDAO, times(1)).getById(USER_ID);
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userDAO, times(1)).save(userCaptor.capture());
        assertThat(userCaptor.getValue()).isEqualToIgnoringGivenFields(expectedUser, "paymentUnitEntries");
        assertThat(userCaptor.getValue().getPaymentUnitEntries())
                .usingElementComparatorIgnoringFields("createdAt").containsExactly(Iterables.getOnlyElement(expectedUser.getPaymentUnitEntries()));

    }

    @Test
    public void shouldInitializePaymentUnitsWhenNotSet() {
        // given
        when(userDAO.getById(USER_ID)).thenReturn(getUser(USER_ID, null));

        // when
        paymentLogic.changeAccountBalanceManually(USER_ID, PAYMENT_UNITS_TO_ADD);

        // then
        User expectedUser = getUserWithHistoryEntry(USER_ID, 0L, PAYMENT_UNITS_TO_ADD);
        verify(userDAO, times(1)).getById(USER_ID);
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userDAO, times(1)).save(userCaptor.capture());
        assertThat(userCaptor.getValue()).isEqualToIgnoringGivenFields(expectedUser, "paymentUnitEntries");
        assertThat(userCaptor.getValue().getPaymentUnitEntries())
                .usingElementComparatorIgnoringFields("createdAt").containsExactly(Iterables.getOnlyElement(expectedUser.getPaymentUnitEntries()));
    }

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhenIncorrectDataGiven() {
        // given
        when(userDAO.getById(USER_ID)).thenReturn(getUser(USER_ID, INITIAL_PAYMENT_UNITS));

        // when
        paymentLogic.changeAccountBalance(getPayload(USER_ID, " text 4000 units "));
    }

    @Test
    public void shouldConvertPayPalUnitsToProperNumericalValue() {
        // given
        when(userDAO.getById(USER_ID)).thenReturn(getUser(USER_ID, INITIAL_PAYMENT_UNITS));

        // when
        paymentLogic.changeAccountBalance(getPayload(USER_ID, " 4000 units "));

        // then
        User expectedUser = getUserWithHistoryEntry(USER_ID, INITIAL_PAYMENT_UNITS, PAYMENT_UNITS_TO_ADD);
        verify(userDAO, times(1)).getById(USER_ID);
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userDAO, times(1)).save(userCaptor.capture());
        assertThat(userCaptor.getValue()).isEqualToIgnoringGivenFields(expectedUser, "paymentUnitEntries");
        assertThat(userCaptor.getValue().getPaymentUnitEntries())
                .usingElementComparatorIgnoringFields("createdAt").containsExactly(Iterables.getOnlyElement(expectedUser.getPaymentUnitEntries()));
    }

    @Test
    public void shouldSavePayPalResponse(){
        // given
        when(userDAO.getById(USER_ID)).thenReturn(getUser(USER_ID, INITIAL_PAYMENT_UNITS));

        // when
        paymentLogic.changeAccountBalance(getPayload(USER_ID, " 4000 units "));

        // then
        verify(payPalResponseDAO, timeout(1)).insert(getPayload(USER_ID, " 4000 units "));
    }

    private User getUser(String userId, Long initialPaymentUnits) {
        User user = new Facebook();
        user.setId(userId);
        user.setPaymentUnits(initialPaymentUnits);
        return user;
    }

    private User getUserWithHistoryEntry(String userId, Long initialPaymentUnits, Long requestedPaymentUnits) {
        User user = getUser(userId, requestedPaymentUnits + initialPaymentUnits);

        UserPaymentUnitsEntry entry = new UserPaymentUnitsEntry();
        entry.setCreatedAt(LocalDateTime.now());
        entry.setRequestedAmount(requestedPaymentUnits);
        entry.setPaymentUnitsBefore(initialPaymentUnits);
        entry.setPaymentUnitsAfter(requestedPaymentUnits + initialPaymentUnits);
        entry.setDescription(REST_PAYMENT);

        user.addUserPaymentUnitsEntry(entry);
        return user;
    }

    private Map<String, String> getPayload(String userId, String units) {
        HashMap<String, String> payload = new HashMap<>();
        payload.put("custom", userId);
        payload.put("option_selection1", units);
        return payload;
    }

}