package com.agreemount.negotiation.logic;

import com.agreemount.negotiation.bean.PreparedEmail;
import com.agreemount.negotiation.bean.notification.GeneratedNotification;
import com.agreemount.negotiation.dao.IGeneratedNotificationDAO;
import com.agreemount.negotiation.logic.impl.SendLogic;
import com.agreemount.negotiation.sender.ISender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;



@RunWith(MockitoJUnitRunner.class)
public class SendLogicTest {

    @Mock
    private IGeneratedNotificationDAO generatedNotificationDAO;

    @Mock
    private ISender sender;

    @InjectMocks
    private ISendLogic sendLogic = new SendLogic();

    @Test
    public void whenSendGeneratedEmailsThenSandAllOfThem() {
        GeneratedNotification gn1 = getGeneratedNotifi("content of email 1", "title of email 1", "bob123@mailinator.com");
        GeneratedNotification gn2 = getGeneratedNotifi("content of email 2", "title of email 2", "john123@mailinator.com");

        when(generatedNotificationDAO.getNext())
                .thenReturn(gn1)
                .thenReturn(gn2)
                .thenReturn(null)
        ;

        sendLogic.send();

        verify(generatedNotificationDAO, times(3)).getNext();
        verify(generatedNotificationDAO, times(2)).save(any(GeneratedNotification.class));
        verify(sender, times(2)).send(any(PreparedEmail.class));

        assertNull(gn1.getNextProcessing());
        assertNull(gn2.getNextProcessing());
    }


    @Test
    public void whenOneNotificationThrowExceptionThenOthersShouldBeSendNormally() {
        GeneratedNotification gn1 = getGeneratedNotifi("content of email 1", "title of email 1", "bob123@mailinator.com");
        GeneratedNotification gn2 = getGeneratedNotifi("content of email 2", "title of email 2", "john123@mailinator.com");

        when(generatedNotificationDAO.getNext())
                .thenReturn(gn1)
                .thenReturn(gn2)
                .thenReturn(null)
        ;

        doThrow(Exception.class).when(sender).send(gn1.getPreparedEmail());

        sendLogic.send();

        verify(generatedNotificationDAO, times(3)).getNext();
        verify(generatedNotificationDAO, times(2)).save(any(GeneratedNotification.class));
        verify(sender, times(2)).send(any(PreparedEmail.class));


        assertNotNull(gn1.getNextProcessing());
        assertNull(gn2.getNextProcessing());
    }

    private GeneratedNotification getGeneratedNotifi(String content, String title, String receiver) {
        PreparedEmail email = new PreparedEmail();
        email.setContent(content);
        email.setTitle(title);
        email.setReceiver(receiver);

        GeneratedNotification generatedNotification = new GeneratedNotification();
        generatedNotification.setCreatedAt(LocalDateTime.now());
        generatedNotification.setPreparedEmail(email);
        return generatedNotification;
    }


}
