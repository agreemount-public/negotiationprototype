package com.agreemount.negotiation.logic.impl;

import com.agreemount.EngineFacade;
import com.agreemount.bean.document.Document;
import com.agreemount.engine.facade.QueryFacade;
import com.agreemount.negotiation.bean.NegotiationOnDashboard;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;



@RunWith(MockitoJUnitRunner.class)
public class AgreemountDocumentLogicTest {

    private static final String TEAM_1 = "teamName1";
    private static final String TEAM_2 = "teamName2";
    private static final String DOC_2 = "DOC_2";
    private static final String DOC_1 = "DOC_1";
    private static final String MSG_1 = "MSG_1";
    private static final String MSG_BODY_1 = "MSG_BODY_1";
    private static final String MSG_2 = "MSG_2";
    private static final String MSG_BODY_2 = "MSG_BODY_2";
    private static final String INVITED_1 = "INVITED_1";
    private static final String INVITED_2 = "INVITED_2";
    private static final String OWNER_1 = "OWNER_1";
    private static final String OWNER_2 = "OWNER_2";

    @Mock
    private ActionContextFactory actionContextFactory;

    @Mock
    private QueryFacade queryFacade;

    @Mock
    private EngineFacade engineFacade;

    @Mock
    private TeamLogic teamLogic;

    @InjectMocks
    private AgreemountDocumentLogic agreemountDocumentLogic;

    @Before
    public void setUp() {
        when(actionContextFactory.createInstance(any(Document.class))).thenReturn(new ActionContext());
    }

    @Test
    public void shouldPrepareDashboardData() {
        // given
        mockUsers();
        mockDocuments(document(DOC_1, TEAM_1), document(DOC_2, TEAM_2));
        mockMessages(message(MSG_1, MSG_BODY_1), message(MSG_2, MSG_BODY_2));

        // when
        List<NegotiationOnDashboard> negotiations = agreemountDocumentLogic.fetchDashboardData();

        // then
        assertThat(messagesForNegotiation(negotiations, DOC_1, NegotiationOnDashboard::getOwner)).contains(OWNER_1);
        assertThat(messagesForNegotiation(negotiations, DOC_1, NegotiationOnDashboard::getInvited)).contains(INVITED_1);
        assertThat(messagesForNegotiation(negotiations, DOC_2, NegotiationOnDashboard::getOwner)).contains(OWNER_2);
        assertThat(messagesForNegotiation(negotiations, DOC_2, NegotiationOnDashboard::getInvited)).contains(INVITED_2);

        assertThat(messagesForNegotiation(negotiations, DOC_1, NegotiationOnDashboard::getMessages)).containsOnly(message(MSG_1, MSG_BODY_1), message(MSG_2, MSG_BODY_2));
    }


    @Test
    public void shouldPrepareDashboardDataWithoutMessages() {
        // given
        mockUsers();
        mockDocuments(document(DOC_1, TEAM_1), document(DOC_2, TEAM_2));
        mockMessages();

        // when
        List<NegotiationOnDashboard> negotiations = agreemountDocumentLogic.fetchDashboardData();

        // then
        assertThat(messagesForNegotiation(negotiations, DOC_1, NegotiationOnDashboard::getOwner)).contains(OWNER_1);
        assertThat(messagesForNegotiation(negotiations, DOC_1, NegotiationOnDashboard::getInvited)).contains(INVITED_1);
        assertThat(messagesForNegotiation(negotiations, DOC_2, NegotiationOnDashboard::getOwner)).contains(OWNER_2);
        assertThat(messagesForNegotiation(negotiations, DOC_2, NegotiationOnDashboard::getInvited)).contains(INVITED_2);

        assertThat(messagesForNegotiation(negotiations, DOC_1, NegotiationOnDashboard::getMessages)).isEmpty();
        assertThat(messagesForNegotiation(negotiations, DOC_2, NegotiationOnDashboard::getMessages)).isEmpty();
    }

    @Test
    public void shouldReturnEmptyListWhenNoDocumentsFound() {
        // given
        mockDocuments();
        mockMessages(message(MSG_1, MSG_BODY_1), message(MSG_2, MSG_BODY_2));

        // when
        List<NegotiationOnDashboard> negotiations = agreemountDocumentLogic.fetchDashboardData();

        // then
        assertThat(negotiations).isEmpty();
    }

    private void mockUsers() {
        mockUser(Optional.of(getUser(OWNER_1)), TEAM_1, UserRole.SIDE1);
        mockUser(Optional.of(getUser(OWNER_2)), TEAM_2, UserRole.SIDE1);
        mockUser(Optional.of(getUser(INVITED_1)), TEAM_1, UserRole.SIDE2);
        mockUser(Optional.of(getUser(INVITED_2)), TEAM_2, UserRole.SIDE2);
    }

    private Document document(String teamId, String teamName) {
        Document document = new Document();
        document.setTeam(teamName);
        document.setId(teamId);
        return document;
    }

    private Message message(String id, String body) {
        Message message = new Message();
        message.setId(id);
        message.setBody(body);
        return message;
    }

    private void mockDocuments(Document... documents) {
        when(queryFacade.getDocumentsForQuery(eq("mySplits"), any(ActionContext.class)))
                .thenReturn(newArrayList(documents));
    }

    private void mockMessages(Message... messages) {
        when(engineFacade.getAvailableMessages(any(ActionContext.class)))
                .thenReturn(newArrayList(messages));
    }

    private <T> T messagesForNegotiation(List<NegotiationOnDashboard> negotiations, String docId, Function<NegotiationOnDashboard, T> mapper) {
        return negotiations
                .stream()
                .filter(n -> n.getNegotiation().getId().equals(docId))
                .map(mapper)
                .findFirst()
                .get();
    }

    private User2Team getUser(String user1) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(user1);
        return user2Team;
    }

    private void mockUser(Optional<User2Team> user2Team, String teamName, UserRole role) {
        when(teamLogic.getUserWithRole(teamName, role)).thenReturn(user2Team);
    }
}