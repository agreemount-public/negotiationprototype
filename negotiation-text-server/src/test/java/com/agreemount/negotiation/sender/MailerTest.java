package com.agreemount.negotiation.sender;

import com.agreemount.negotiation.bean.PreparedEmail;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;



@RunWith(MockitoJUnitRunner.class)
public class MailerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private HtmlEmail sender;

    @Mock
    private Environment environment;

    @InjectMocks
    private Mailer mailer = new Mailer();

    @Test
    public void shouldNotThrowExceptionWhenSenderCantSendMessage() throws EmailException {
        // given
        PreparedEmail preparedEmail = prepareEmail();
        when(sender.send()).thenThrow(EmailException.class);
        when(environment.getProperty(anyString())).thenReturn("123");

        // when
        mailer.send(preparedEmail);

        // then
        assertTrue(true);
    }

    @Test
    @Ignore
    public void givenBccAndCCReceipentsShouldBeAddedToEmail() throws EmailException {
        // given
        when(environment.getProperty(anyString())).thenReturn("123");
        PreparedEmail preparedEmail = prepareEmail(
                Arrays.asList("generate123_cc1@mailinator.com"),
                Arrays.asList("generate123_bc1@mailinator.com", "generate123_bc2@mailinator.com"));

        // when
        mailer.send(preparedEmail);

        // then
        verify(sender, times(2)).addBcc(anyString());
        verify(sender, times(1)).addCc(anyString());
    }

    private PreparedEmail prepareEmail() {
        return prepareEmail(null, null);
    }

    private PreparedEmail prepareEmail(List<String> cc, List<String> bcc) {
        PreparedEmail preparedEmail = new PreparedEmail();
        preparedEmail.setContent("content");
        preparedEmail.setTitle("title");
        preparedEmail.setReceiver("generate123@mailinator.com");

        if (cc!=null){
            preparedEmail.setCc(cc);
        }

        if (bcc!=null){
            preparedEmail.setBcc(bcc);
        }

        return preparedEmail;
    }


}
