package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.member.SideService;
import com.google.common.collect.Lists;
import org.assertj.core.api.AbstractCharSequenceAssert;
import org.assertj.core.api.AbstractComparableAssert;
import org.assertj.core.api.AbstractListAssert;
import org.assertj.core.api.ObjectAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SideControllerTest {

    public static final String SIDE_1 = "SIDE_OWNER";
    public static final String SIDE_2 = "SIDE_SPOUSE";

    @InjectMocks
    private SideController sideController;

    @Mock
    private SideService sideService;

    private Response response;

    @Test
    public void shouldReturnSuccessWhileInvitingSide() {
        givenMockForInviteSide();

        whenInviteSide();

        thenStatusResponse().isEqualTo(ResponseType.SUCCESS);
        thenMessage().startsWith("Invitation has been sent to side");
    }

    @Test
    public void shouldReturnFailedWhileInvitingSide() {
        givenExceptionForInviteSide();

        whenInviteSide();

        thenStatusResponse().isEqualTo(ResponseType.FAILED);
        thenMessage().startsWith("Cannot send invitation to side");
    }

    @Test
    public void shouldReturnSuccessWhileFetchingSides() {
        givenSides(side(SIDE_1, UserRole.SIDE1), side(SIDE_2, UserRole.SIDE2));

        whenFetchSides();

        thenStatusResponse().isEqualTo(ResponseType.SUCCESS);
        thenResponseData().containsExactly(side(SIDE_1, UserRole.SIDE1), side(SIDE_2, UserRole.SIDE2));
    }

    @Test
    public void shouldReturnFailedWhileFetchingSides() {
        givenExceptionWhileFetchingSides();

        whenFetchSides();

        thenStatusResponse().isEqualTo(ResponseType.FAILED);
        thenMessage().startsWith("Cannot execute fetchSides");
    }

    private User2Team side(String login, UserRole role) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(role);
        return user2Team;
    }

    private AbstractCharSequenceAssert<?, String> thenMessage() {
        return assertThat(response.getMessage());
    }

    private AbstractComparableAssert<?, ResponseType> thenStatusResponse() {
        return assertThat(response.getStatus());
    }

    private void whenInviteSide() {
        response = sideController.inviteSide("", "", "");
    }

    private void givenMockForInviteSide() {
        doNothing().when(sideService).inviteSide(anyString(), anyString(), anyString());
    }

    private void givenExceptionForInviteSide() {
        doThrow(Exception.class).when(sideService).inviteSide(anyString(), anyString(), anyString());
    }

    private void whenFetchSides() {
        response = sideController.fetchSides("");
    }

    private AbstractListAssert<?, List<? extends User2Team>, User2Team, ObjectAssert<User2Team>> thenResponseData() {
        return assertThat((List<User2Team>) response.getData());
    }

    private void givenSides(User2Team... sides) {
        when(sideService.fetchSides(anyString())).thenReturn(Lists.newArrayList(sides));
    }

    private List<User2Team> givenExceptionWhileFetchingSides() {
        return doThrow(Exception.class).when(sideService).fetchSides(anyString());
    }

}