package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.agreement.template.AgreementFileGenerator;
import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.agreement.template.converter.DocxToPdfConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AgreementControllerTest {

    private static final String DOCUMENT_ID = "123";
    private static final String DOCUMENT_CONTENT = "testPDF";

    @Mock
    private AgreementFileGenerator agreementFileGenerator;

    @Mock
    private DocxToPdfConverter converter;

    @InjectMocks
    private AgreementController agreementController;

    @Test
    public void shouldDownloadFileWithContent() throws IOException {
        givenDocumentWithContent(DOCUMENT_ID, DOCUMENT_CONTENT);

        ResponseEntity<byte[]> response = agreementController.download(DOCUMENT_ID);

        assertThat(response.getBody()).containsExactly(DOCUMENT_CONTENT.getBytes());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void shouldReturnErrorStatusWhenExceptionOccured() throws IOException {
        when(agreementFileGenerator.generate(DOCUMENT_ID, Region.MY_DIVORCE)).thenThrow(RuntimeException.class);

        ResponseEntity<byte[]> response = agreementController.download(DOCUMENT_ID);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void givenDocumentWithContent(String documentId, String documentContent) throws IOException {
        Path docxPath = pathToFileWithContent("test");
        when(agreementFileGenerator.generate(documentId, Region.MY_DIVORCE)).thenReturn(docxPath);
        when(converter.convert(docxPath)).thenReturn(pathToFileWithContent(documentContent));
    }

    private Path pathToFileWithContent(String content) throws IOException {
        Path path = Paths.get(content);
        Files.write(path, content.getBytes());
        return path;
    }

}