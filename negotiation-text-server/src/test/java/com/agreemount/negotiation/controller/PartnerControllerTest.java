package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.member.PartnerService;
import com.google.common.collect.Lists;
import org.assertj.core.api.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PartnerControllerTest {

    public static final String SIDE_1 = "SIDE_OWNER";

    @InjectMocks
    private PartnerController partnerController;

    @Mock
    private PartnerService partnerService;

    private Response response;

    @Test
    public void shouldReturnSuccessWhileInvitingPartner() {
        givenMockForInvitePartner();

        whenInvitePartner();

        thenStatusResponse().isEqualTo(ResponseType.SUCCESS);
        thenMessage().startsWith("Invitation has been sent to partner");
    }

    @Test
    public void shouldReturnFailedWhileInvitingPartner() {
        givenExceptionForInvitePartner();

        whenInvitePartner();

        thenStatusResponse().isEqualTo(ResponseType.FAILED);
        thenMessage().startsWith("Cannot send invitation to partner");
    }


    @Test
    public void shouldReturnSuccessWhileFetchingPartners() {
        givenPartners(partner(SIDE_1, UserRole.SIDE1), partner(SIDE_1, UserRole.SIDE2));

        whenFetchPartners();

        thenStatusResponse().isEqualTo(ResponseType.SUCCESS);
        thenResponseData().containsExactly(partner(SIDE_1, UserRole.SIDE1), partner(SIDE_1, UserRole.SIDE2));
    }

    @Test
    public void shouldReturnFailedWhileFetchingPartners() {
        givenExceptionWhileFetchingPartners();

        whenFetchPartners();

        thenStatusResponse().isEqualTo(ResponseType.FAILED);
        thenMessage().startsWith("Cannot execute fetchPartners");
    }


    @Test
    public void shouldReturnSuccessWhileDeletingPartner() {
        givenResponseForDeletingPartner();

        whenDeletePartner();

        thenStatusResponse().isEqualTo(ResponseType.SUCCESS);
    }

    @Test
    public void shouldReturnFailedWhenExceptionIsThrown() {
        givenExceptionWhileDeletingPartner();

        whenDeletePartner();

        thenStatusResponse().isEqualTo(ResponseType.FAILED);
        thenMessage().startsWith("Cannot delete partner");
    }

    private User2Team partner(String login, UserRole role) {
        User2Team side = side(login, role);
        side.setPartner(true);
        return side;
    }

    private User2Team side(String login, UserRole role) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(role);
        return user2Team;
    }

    private AbstractCharSequenceAssert<?, String> thenMessage() {
        return assertThat(response.getMessage());
    }

    private AbstractComparableAssert<?, ResponseType> thenStatusResponse() {
        return AssertionsForInterfaceTypes.assertThat(response.getStatus());
    }

    private void whenInvitePartner() {
        response = partnerController.invitePartner(anyString(), anyString(), anyString());
    }

    private void givenMockForInvitePartner() {
        doNothing().when(partnerService).invitePartner(anyString(), anyString(), anyString());
    }

    private void givenExceptionForInvitePartner() {
        doThrow(Exception.class).when(partnerService).invitePartner(anyString(), anyString(), anyString());
    }

    private void whenFetchPartners() {
        response = partnerController.fetchPartners(anyString(), anyString());
    }

    private AbstractListAssert<?, List<? extends User2Team>, User2Team, ObjectAssert<User2Team>> thenResponseData() {
        return AssertionsForInterfaceTypes.assertThat((List<User2Team>) response.getData());
    }

    private void givenPartners(User2Team... partners) {
        when(partnerService.fetchPartners(anyString(), anyString())).thenReturn(Lists.newArrayList(partners));
    }

    private List<User2Team> givenExceptionWhileFetchingPartners() {
        return doThrow(Exception.class).when(partnerService).fetchPartners(anyString(), anyString());
    }


    private void givenResponseForDeletingPartner() {
        doNothing().when(partnerService).deletePartner(anyString(), anyString(), anyString());
    }

    private void whenDeletePartner() {
        response = partnerController.deletePartner("reqeuesto@email.com", "partnerToDelete@email.com", "DOCID_123");
    }

    private void givenExceptionWhileDeletingPartner() {
        doThrow(Exception.class).when(partnerService).deletePartner(anyString(), anyString(), anyString());
    }

}