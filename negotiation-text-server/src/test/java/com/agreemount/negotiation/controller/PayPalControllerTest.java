package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.logic.impl.PaymentLogic;
import com.paypal.ipn.IPNMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;



@RunWith(MockitoJUnitRunner.class)
public class PayPalControllerTest {

    @InjectMocks
    private  PayPalController payPalController;

    @Mock
    private IPNMessage ipnlistener;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private PaymentLogic paymentLogic;

    @Before
    public void setUp() {
        when(ipnlistener.getIpnMap()).thenReturn(getResponseMap());
    }

    @Test
    public void shouldFetchDataWhenProperlyValidated(){
        when(ipnlistener.validate()).thenReturn(true);

        payPalController.listener(httpServletRequest);

        verify(ipnlistener, times(1)).getIpnMap();
        verify(paymentLogic, times(1)).changeAccountBalance(getResponseMap());
    }

    @Test
    public void shouldNotFetchDataWhenConnectionFailed(){
        when(ipnlistener.validate()).thenReturn(false);

        payPalController.listener(httpServletRequest);

        verify(ipnlistener, times(0)).getIpnMap();
        verify(paymentLogic, times(0)).changeAccountBalance(getResponseMap());
    }


    private Map<String, String> getResponseMap() {
        Map<String, String> result = new HashMap<>();
        result.put("custom", "receiver@email.com");
        result.put("option_selection1", "100 operations");
        return result;
    }
}