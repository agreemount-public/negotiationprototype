package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.logic.impl.AgreemountDocumentLogic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;



@RunWith(MockitoJUnitRunner.class)
public class EmptyDocumentControllerTest {

    public static final String DOC_ID = "123";
    @InjectMocks
    private EmptyDocumentController emptyDocumentController;

    @Mock
    private AgreemountDocumentLogic agreemountDocumentLogic;

    @Test
    public void shouldReturnSuccessResponseWhenDocumentHasBeenCreated() {
        // given
        when(agreemountDocumentLogic.saveEmptyDocument("property-division")).thenReturn(DOC_ID);

        // when
        Response response = emptyDocumentController.create("property-division");

        // then
        assertEquals(ResponseType.SUCCESS, response.getStatus());
        assertEquals(DOC_ID, response.getData());
        assertEquals("Document has been created successfully", response.getMessage());
    }

    @Test
    public void shouldReturnFailedResponseWhenDocumentHasNotBeenCreated() {
        // given
        when(agreemountDocumentLogic.saveEmptyDocument("property-division")).thenThrow(NullPointerException.class);

        // when
        Response response = emptyDocumentController.create("property-division");

        // then
        assertEquals(ResponseType.FAILED, response.getStatus());
        assertNull(response.getData());
        assertEquals("Cannot create document", response.getMessage());
    }

}
