package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.auth.AuthContext;
import com.agreemount.negotiation.auth.ResetPasswordService;
import com.agreemount.negotiation.bean.ResetPasswordDto;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.bean.providers.Local;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class AuthControllerTest {

    private static final String USER_ID = "123";
    private static final String USER_EMAIL = "bob@builder.com";
    private static final char[] USER_PASSWORD = new char[]{'k', 'i', 't', 't', 'y', '1', '2', '3'};
    private static final String EXCEPTION_MSG = "something went wrong";

    @InjectMocks
    private AuthController authController;

    @Mock
    private AuthContext authContext;

    @Mock
    private ResetPasswordService resetPasswordService;

    @Test
    public void shouldReturnSuccessResponseWhenTryingToSignUp() {
        User user = localUser(USER_ID);
        when(authContext.signUp(any(User.class))).thenReturn(user);

        Response response = authController.signUp(user);

        assertThat(response.getStatus()).isEqualTo(ResponseType.SUCCESS);
        assertThat(((User) response.getData()).getId()).isEqualTo(USER_ID);
        assertThat(response.getMessage()).isNull();
    }

    @Test
    public void shouldReturnFailedResponseWhenExceptionWasThrownWhileTryingToSignUp() {
        User user = localUser(USER_ID);
        when(authContext.signUp(any(User.class))).thenThrow(new NullPointerException(EXCEPTION_MSG));

        Response response = authController.signUp(user);

        assertThat(response.getStatus()).isEqualTo(ResponseType.FAILED);
        assertThat(response.getData()).isNull();
        assertThat(response.getMessage()).isEqualTo(EXCEPTION_MSG);
    }

    @Test
    public void shouldReturnSuccessResponseWhenTryingToLogin() {
        User user = localUser(USER_ID);
        when(authContext.login(anyString(), any())).thenReturn(user);

        Response response = authController.login(USER_EMAIL, USER_PASSWORD);

        assertThat(response.getStatus()).isEqualTo(ResponseType.SUCCESS);
        assertThat(((User) response.getData()).getId()).isEqualTo(USER_ID);
        assertThat(response.getMessage()).isNull();
    }


    @Test
    public void shouldReturnFailedResponseWhenExceptionWasThrownWhileTryingToLogin() {
        when(authContext.login(anyString(), any())).thenThrow(new NullPointerException(EXCEPTION_MSG));

        Response response = authController.login(USER_EMAIL, USER_PASSWORD);

        assertThat(response.getStatus()).isEqualTo(ResponseType.FAILED);
        assertThat(response.getData()).isNull();
        assertThat(response.getMessage()).isEqualTo(EXCEPTION_MSG);
    }

    @Test
    public void shouldReturnSuccessResponseIfActivationSucceeded() {
        when(authContext.activateAccount(anyString())).thenReturn(true);

        Response response = authController.activateAccount("0123");

        assertThat(response.getStatus()).isEqualTo(ResponseType.SUCCESS);
        assertThat(response.getData()).isEqualTo(true);
        assertThat(response.getMessage()).isNull();
    }


    @Test
    public void shouldReturnFailedResponseIfActivationFailed() {
        when(authContext.activateAccount(anyString())).thenThrow(new NullPointerException(EXCEPTION_MSG));

        Response response = authController.activateAccount("0123");

        assertThat(response.getStatus()).isEqualTo(ResponseType.FAILED);
        assertThat(response.getMessage()).isEqualTo(EXCEPTION_MSG);
    }

    @Test
    public void shouldReturnSuccessWhenConfirmRestPasswordTriggeredCorrectly() {
        Response response = authController.confirmResetPassword("reset@password.com");

        assertThat(response.getStatus()).isEqualTo(ResponseType.SUCCESS);
        assertThat(response.getMessage()).isEqualTo("Confirmation will be sent shortly");
    }

    @Test
    public void shouldReturnFailedWhenConfirmRestPasswordThrownException() {
        doThrow(new NullPointerException(EXCEPTION_MSG)).when(resetPasswordService)
                .resetPasswordConfirmation(anyString());

        Response response = authController.confirmResetPassword("reset@password.com");

        assertThat(response.getStatus()).isEqualTo(ResponseType.FAILED);
        assertThat(response.getMessage()).isEqualTo(EXCEPTION_MSG);
    }

    @Test
    public void shouldReturnSuccessWhenResetPasswordTriggeredCorrectly() {
        String hash = "132";

        Response response = authController.resetPassword(resetPasswordDto(hash));

        assertThat(response.getStatus()).isEqualTo(ResponseType.SUCCESS);
        assertThat(response.getMessage()).isEqualTo("Password has been successfully reset");
    }

    @Test
    public void shouldReturnFailedWhenResetPasswordThrownException() {
        String hash = "132";
        doThrow(new NullPointerException(EXCEPTION_MSG)).when(resetPasswordService).resetPassword(any());

        Response response = authController.resetPassword(resetPasswordDto(hash));

        assertThat(response.getStatus()).isEqualTo(ResponseType.FAILED);
        assertThat(response.getMessage()).isEqualTo(EXCEPTION_MSG);
    }

    private ResetPasswordDto resetPasswordDto(String hash) {
        ResetPasswordDto resetPasswordDto = new ResetPasswordDto();
        resetPasswordDto.setHash(hash);
        return resetPasswordDto;
    }

    private User localUser(String userId) {
        User user = new Local();
        user.setId(userId);
        return user;
    }

}
