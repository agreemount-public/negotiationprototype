package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.auth.AuthContext;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.bean.providers.Facebook;
import com.agreemount.negotiation.logic.impl.PaymentLogic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class PaymentControllerTest {

    private static final String USER_ID = "10";
    private static final long PAYMENT_UNITS = 1024L;
    private static final long PAYMENT_UNITS_TO_ADD = 1000L;

    @InjectMocks
    private PaymentController paymentController;

    @Mock
    private AuthContext authContext;

    @Mock
    private PaymentLogic paymentLogic;

    @Test
    public void shouldReturnUserAccountBalance() {
        // given
        when(authContext.getUser(USER_ID)).thenReturn(getUser());

        // when
        Response response = paymentController.getAccountBalance(USER_ID);

        // then
        assertThat(response.getStatus()).isEqualTo(ResponseType.SUCCESS);
        assertThat(response.getData()).isEqualTo(PAYMENT_UNITS);
    }

    @Test
    public void shouldReturnFailedResponseWhenErrorOccured() {
        // given
        when(authContext.getUser(USER_ID)).thenThrow(Exception.class);

        // when
        Response response = paymentController.getAccountBalance(USER_ID);

        // then
        assertThat(response.getStatus()).isEqualTo(ResponseType.FAILED);
        assertThat(response.getMessage()).contains("Cannot fetch user account balance");
        assertThat(response.getData()).isNull();
    }

    @Test
    public void shouldChangeUserAccountBalance() {
        // given
        doNothing().when(paymentLogic).changeAccountBalanceManually(USER_ID, PAYMENT_UNITS_TO_ADD);

        // when
        Response response = paymentController.changeAccountBalance(USER_ID, PAYMENT_UNITS_TO_ADD);

        // then
        assertThat(response.getStatus()).isEqualTo(ResponseType.SUCCESS);
        assertThat(response.getMessage()).contains("Successfully changed user account balance");
    }

    @Test
    public void shouldReturnFailedWhenCannotChangeUserAccountBalance() {
        // given
        doThrow(Exception.class).when(paymentLogic).changeAccountBalanceManually(USER_ID, PAYMENT_UNITS_TO_ADD);

        // when
        Response response = paymentController.changeAccountBalance(USER_ID, PAYMENT_UNITS_TO_ADD);

        // then
        assertThat(response.getStatus()).isEqualTo(ResponseType.FAILED);
        assertThat(response.getMessage()).contains("Cannot change user account balance");
        assertThat(response.getData()).isNull();
    }


    private User getUser() {
        User user = new Facebook();
        user.setId(USER_ID);
        user.setPaymentUnits(PAYMENT_UNITS);
        return user;
    }
}