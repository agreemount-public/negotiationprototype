package com.agreemount.negotiation.interaction.notification;

import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.agreemount.negotiation.interaction.InteractionLog;
import com.agreemount.negotiation.interaction.notification.dao.InteractionNotificationDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreateNotificationFromInteractionPredicateTest {

    @Mock
    private InteractionNotificationDAO interactionNotificationDAO;

    @Mock
    private UserDAO userDAO;

    @InjectMocks
    private CreateNotificationFromInteractionPredicate createNotificationFromInteractionPredicate = new CreateNotificationFromInteractionPredicate();

    private static final String SOME_EMAIL_COM = "some@email.com";
    private static final String DOCUMENT_ID = "docid123";
    private static final String TEAM_NAME = "teamName";
    private static final String SIDE2_A_EMAIL_COM = "side2A@email.com";
    private static final String SIDE2_B_EMAIL_COM = "side2B@email.com";

    @Test
    public void shouldReturnFalseIfUserNotFound() {
        when(userDAO.getByEmail(SOME_EMAIL_COM)).thenReturn(null);
        when(interactionNotificationDAO.existNewerThanDay(any(),any())).thenReturn(false);

        boolean result = createNotificationFromInteractionPredicate.test(getInteractionLog());
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueIfUserNotLoggedYet() {
        User user = mock(User.class);
        when(user.getLastLoginAt()).thenReturn(null);

        when(userDAO.getByEmail(SOME_EMAIL_COM)).thenReturn(user);
        when(interactionNotificationDAO.existNewerThanDay(any(),any())).thenReturn(false);

        boolean result = createNotificationFromInteractionPredicate.test(getInteractionLog());
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseIfLoggedLessThan15MinutesBeforeCreatingInteractionLog() {
        User user = mock(User.class);
        when(user.getLastLoginAt()).thenReturn(LocalDateTime.now().minusMinutes(10l));

        when(userDAO.getByEmail(SOME_EMAIL_COM)).thenReturn(user);
        when(interactionNotificationDAO.existNewerThanDay(any(),any())).thenReturn(false);

        boolean result = createNotificationFromInteractionPredicate.test(getInteractionLog());
        assertFalse(result);
    }

    @Test
    public void shouldReturnFalseIfUserHasBeenNotifiedRegadingSameDocumentLessThanDayAgo() {
        User user = mock(User.class);
        when(user.getLastLoginAt()).thenReturn(LocalDateTime.now().minusMinutes(20l));

        when(userDAO.getByEmail(SOME_EMAIL_COM)).thenReturn(user);
        when(interactionNotificationDAO.existNewerThanDay(any(),any())).thenReturn(true);

        boolean result = createNotificationFromInteractionPredicate.test(getInteractionLog());
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueIfMeetsConditions() {
        User user = mock(User.class);
        when(user.getLastLoginAt()).thenReturn(LocalDateTime.now().minusMinutes(20l));

        when(userDAO.getByEmail(SOME_EMAIL_COM)).thenReturn(user);
        when(interactionNotificationDAO.existNewerThanDay(any(),any())).thenReturn(false);

        boolean result = createNotificationFromInteractionPredicate.test(getInteractionLog());
        assertTrue(result);
    }

    InterActionLogWithReceiver getInteractionLog() {
        InteractionLog il = new InteractionLog();
        il.setAuthorEmail(SOME_EMAIL_COM);
        il.setCreatedAt(LocalDateTime.now());

        return new InterActionLogWithReceiver(DOCUMENT_ID, il, SOME_EMAIL_COM);
    }

}