package com.agreemount.negotiation.interaction.action;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.interaction.InteractionLog;
import com.agreemount.negotiation.interaction.dao.InteractionLogDAO;
import com.agreemount.slaneg.action.ActionContext;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CreateInteractionLogActionImplTest {

    @Mock
    private IdentityProvider identityProvider;

    @Mock
    private InteractionLogDAO interactionLogDAO;

    @InjectMocks
    private CreateInteractionLogActionImpl createInteractionLogAction = new CreateInteractionLogActionImpl();

    @Test
    public void shouldThrowExceptionIfNoDocumentAlias() {
        CreateInteractionLogDef createInteractionLogDef = new CreateInteractionLogDef();

        createInteractionLogAction.setDefinition(createInteractionLogDef);

        ActionContext actionContext = new ActionContext();

        assertThatThrownBy(() -> createInteractionLogAction.run(actionContext)).hasMessage("Alias is not set!");
    }

    @Test
    public void shouldThrowExceptionIfDocumentNoFoundInActionContext() {
        final String documentAlias = "BASE";
        CreateInteractionLogDef createInteractionLogDef = new CreateInteractionLogDef();
        createInteractionLogDef.setDocumentAlias(documentAlias);

        createInteractionLogAction.setDefinition(createInteractionLogDef);

        ActionContext actionContext = new ActionContext();

        assertThatThrownBy(() -> createInteractionLogAction.run(actionContext))
                .hasMessage("document with alias [" + documentAlias + "] was not found");
    }

    @Test
    public void shouldThrowExceptionIfNoIdentityFound() {

        when(identityProvider.getIdentity()).thenReturn(null);

        final String documentAlias = "BASE";
        CreateInteractionLogDef createInteractionLogDef = new CreateInteractionLogDef();
        createInteractionLogDef.setDocumentAlias(documentAlias);

        createInteractionLogAction.setDefinition(createInteractionLogDef);

        ActionContext actionContext = new ActionContext();
        actionContext.addDocument(documentAlias, mock(Document.class));

        assertThatThrownBy(() -> createInteractionLogAction.run(actionContext)).hasMessage("no identity");
    }


    @Test
    public void shouldCreateInteractionLogAndSaveIt() {
        final String documentAlias = "BASE";
        final String documentId = "102";
        final String userEmail = "happy.user@email.com";

        Identity identity = mock(Identity.class);
        when(identity.getLogin()).thenReturn(userEmail);
        when(identityProvider.getIdentity()).thenReturn(identity);

        CreateInteractionLogDef createInteractionLogDef = new CreateInteractionLogDef();
        createInteractionLogDef.setDocumentAlias(documentAlias);

        createInteractionLogAction.setDefinition(createInteractionLogDef);

        ActionContext actionContext = new ActionContext();

        Document document = mock(Document.class);
        when(document.getId()).thenReturn(documentId);
        actionContext.addDocument(documentAlias, document);

        createInteractionLogAction.run(actionContext);


        ArgumentCaptor<InteractionLog> captor = ArgumentCaptor.forClass(InteractionLog.class);
        verify(interactionLogDAO).save(captor.capture());

        InteractionLog persisted = captor.getValue();


        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(persisted.getAuthorEmail()).isEqualTo(userEmail);
        softAssertions.assertThat(persisted.getDocumentId()).isEqualTo(documentId);
        softAssertions.assertThat(persisted.getCreatedAt()).isEqualToIgnoringSeconds(LocalDateTime.now());
        softAssertions.assertThat(persisted.getProcessedAt()).isNull();
        softAssertions.assertAll();
    }


}