package com.agreemount.negotiation.interaction.processing;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.dao.impl.TeamDAO;
import com.agreemount.negotiation.interaction.InteractionLog;
import com.agreemount.negotiation.interaction.dao.InteractionLogDAO;
import com.agreemount.negotiation.interaction.notification.NotificationCreator;
import com.agreemount.negotiation.logic.impl.AgreemountDocumentLogic;
import com.agreemount.negotiation.member.SideService;
import com.agreemount.negotiation.notification.processors.DefaultProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InteractionLogsProcessorTest {

    public static final String SOME_EMAIL_COM = "some@email.com";
    public static final String DOCUMENT_ID = "docid123";
    public static final String NEGOTIATION_ID = "negotioation123";
    public static final String SLA_UUID = "uuid222";
    public static final String TEAM_NAME = "teamName";
    public static final String SIDE2_A_EMAIL_COM = "side2A@email.com";
    public static final String SIDE2_B_EMAIL_COM = "side2B@email.com";

    @Mock
    private SideService sideService;

    @Mock
    private TeamDAO teamDAO;

    @Mock
    private InteractionLogDAO interactionLogDAO;

    @Mock
    private NotificationCreator notificationCreator;

    @Mock
    private AgreemountDocumentLogic agreemountDocumentLogic;

    @InjectMocks
    private InteractionLogsProcessor interactionLogsProcessor = new InteractionLogsProcessor();


    @Test
    public void processAll() throws Exception {

    }

    @Test
    public void shouldMarkSimilarInteactionLogsAsProcessedEvenIfDocumentNotFound() throws Exception {
        InteractionLog il = getInteractionLog();
        Document document = getNegotiation();
        when(agreemountDocumentLogic.getMainNegotiation(DOCUMENT_ID)).thenReturn(document);

        interactionLogsProcessor.process(il);


        Mockito.verify(interactionLogDAO).markSimilarAsProcessed(SLA_UUID, SOME_EMAIL_COM);
    }

    @Test
    public void shouldCreateNotifications() throws Exception {
        InteractionLog il = getInteractionLog();
        Document document = getNegotiation();
        when(agreemountDocumentLogic.getMainNegotiation(DOCUMENT_ID)).thenReturn(document);

        Team team = mock(Team.class);
        when(teamDAO.getTeamByName(TEAM_NAME)).thenReturn(team);

        when(sideService.getOtherSideEmail(team, SOME_EMAIL_COM)).thenReturn(Arrays.asList(SIDE2_A_EMAIL_COM, SIDE2_B_EMAIL_COM));


        interactionLogsProcessor.process(il);

        verify(notificationCreator).create(NEGOTIATION_ID, il, SIDE2_A_EMAIL_COM);
        verify(notificationCreator).create(NEGOTIATION_ID, il, SIDE2_B_EMAIL_COM);

        Mockito.verify(interactionLogDAO).markSimilarAsProcessed(SLA_UUID, SOME_EMAIL_COM);
    }

    Document getNegotiation() {
        Document document = mock(Document.class);
        when(document.getTeam()).thenReturn(TEAM_NAME);
        when(document.getId()).thenReturn(NEGOTIATION_ID);
        when(document.getSlaUuid()).thenReturn(SLA_UUID);

        return document;
    }

    InteractionLog getInteractionLog() {
        InteractionLog il = new InteractionLog();
        il.setAuthorEmail(SOME_EMAIL_COM);
        il.setDocumentId(DOCUMENT_ID);
        il.setSlaUuid(SLA_UUID);

        return  il;
    }


}