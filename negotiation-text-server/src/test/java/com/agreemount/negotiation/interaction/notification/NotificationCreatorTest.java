package com.agreemount.negotiation.interaction.notification;

import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.interaction.InteractionLog;
import com.agreemount.negotiation.interaction.notification.dao.InteractionNotificationDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class NotificationCreatorTest {

    @Mock
    private CreateNotificationFromInteractionPredicate createNotificationFromInteractionPredicate;

    @Mock
    private InteractionNotificationDAO interactionNotificationDAO;

    @InjectMocks
    private NotificationCreator notificationCreator = new NotificationCreator();

    public static final String SOME_EMAIL_COM = "some@email.com";
    public static final String DOCUMENT_ID = "docid123";
    public static final String RECEIVER_EMAIL = "side2A@email.com";


    @Test
    public void shouldNotCreateNotificationIfVerifierDecidesToNot() {
        Mockito.when(createNotificationFromInteractionPredicate.test(any())).thenReturn(false);
        notificationCreator.create(DOCUMENT_ID, getInteractionLog(), RECEIVER_EMAIL);
        Mockito.verify(interactionNotificationDAO, Mockito.times(0)).save(any());
    }

    @Test
    public void shouldCreateNotification() {
        Mockito.when(createNotificationFromInteractionPredicate.test(any())).thenReturn(true);
        notificationCreator.create(DOCUMENT_ID, getInteractionLog(), RECEIVER_EMAIL);
        ArgumentCaptor<Notification> captor = ArgumentCaptor.forClass(Notification.class);

        Mockito.verify(interactionNotificationDAO).save(captor.capture());

        assertEquals(captor.getValue().getNotificationType(), NotificationType.OTHER_SIDE_INTERACTION);
        assertEquals(captor.getValue().getProcessedAt(), null);
        Set<Notification.Parameter> parameters = captor.getValue().getParameters();

        assertThat(parameters)
            .extracting(Notification.Parameter::getKey, Notification.Parameter::getValue)
            .containsExactlyInAnyOrder(
                tuple(Notification.Parameter.Keys.DOCUMENT_ID.getValue(), Arrays.asList(DOCUMENT_ID)),
                tuple(Notification.Parameter.Keys.RECEIVER.getValue(), Arrays.asList(RECEIVER_EMAIL)),
                tuple(Notification.Parameter.Keys.TITLE.getValue(), Arrays.asList("Your spouse have send you new offer or made some change"))
            );
    }

    InteractionLog getInteractionLog() {
        InteractionLog il = new InteractionLog();
        il.setAuthorEmail(SOME_EMAIL_COM);
        il.setDocumentId(DOCUMENT_ID);

        return  il;
    }

}