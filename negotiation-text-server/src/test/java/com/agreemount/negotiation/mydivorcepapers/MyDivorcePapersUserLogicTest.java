package com.agreemount.negotiation.mydivorcepapers;

import com.agreemount.EngineFacade;
import com.agreemount.negotiation.auth.Password;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserLogDAO;
import com.agreemount.negotiation.logic.impl.TeamLogic;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static com.agreemount.negotiation.mydivorcepapers.MyDivorcePapersUserLogic.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MyDivorcePapersUserLogicTest {

    @Mock
    private IUserDAO userDAO;

    @Mock
    private PasswordCoder passwordCoder;

    @Mock
    private UserLogDAO userLogDAO;

    @Mock
    private TeamLogic teamLogic;

    @Mock
    private ActionContextFactory actionContextFactory;

    @Mock
    private EngineFacade engineFacade;

    @InjectMocks
    private MyDivorcePapersUserLogic myDivorcePapersUserLogic;


    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenCreatingUserIfEmailIsEmpty() throws Exception {
        NewUserRequest newUserRequest = new NewUserRequest();
        newUserRequest.setName("john kovalsky");
        newUserRequest.setPasswordEncoded(new char[]{'p', 'a', 's', 's'});

        myDivorcePapersUserLogic.createAccount(newUserRequest);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenCreatingUserIfNameIsEmpty() throws Exception {
        NewUserRequest newUserRequest = new NewUserRequest();
        newUserRequest.setEmail("john@kovalsky.ua");
        newUserRequest.setPasswordEncoded(new char[]{'p', 'a', 's', 's'});

        myDivorcePapersUserLogic.createAccount(newUserRequest);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenCreatingUserIfPasswordIsEmpty() throws Exception {
        NewUserRequest newUserRequest = new NewUserRequest();
        newUserRequest.setName("john kovalsky");
        newUserRequest.setEmail("john@kovalsky.ua");

        myDivorcePapersUserLogic.createAccount(newUserRequest);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenCreatingUserIfPasswordWrong() throws Exception {
        NewUserRequest newUserRequest = new NewUserRequest();
        newUserRequest.setName("john kovalsky");
        newUserRequest.setEmail("john@kovalsky.ua");
        newUserRequest.setPasswordEncoded(new char[]{'p', 'a', 's', 's'});

        when(userDAO.getByEmail("john@kovalsky.ua")).thenReturn(null);
        when(passwordCoder.decrypt(any(), any())).thenThrow(new Exception());

        myDivorcePapersUserLogic.createAccount(newUserRequest);
    }

    @Test
    public void shouldModifyExistingUserAndCreateNegotiationIfUserDoesntHaveFullAccess() throws Exception {
        checkExistingUserScenario(false, 1);
    }

    @Test
    public void shouldModifyExistingUserAndDoNotCreateNegotiationIfUserAlreadyHaveFullAccess() throws Exception {
        checkExistingUserScenario(true, 0);
    }

    @Before
    public void before() {
        Team t = mock(Team.class);
        when(t.getName()).thenReturn("newTeam123456");
        when(teamLogic.saveTeamWithOwner("john@kovalsky.ua")).thenReturn(t);
    }

    public void checkExistingUserScenario(boolean hasFullAccess, int createNewNegotiationTimes) throws Exception {
        NewUserRequest newUserRequest = new NewUserRequest();
        newUserRequest.setName("john kovalsky");
        newUserRequest.setEmail("john@kovalsky.ua");
        newUserRequest.setPasswordEncoded(new char[]{'p', 'a', 's', 's'});

        User existingUser = mock(User.class);
        when(existingUser.getEmail()).thenReturn("john@kovalsky.ua");
        when(existingUser.isHasFreeAccess()).thenReturn(hasFullAccess);

        when(userDAO.getByEmail("john@kovalsky.ua")).thenReturn(existingUser);

        String result = myDivorcePapersUserLogic.createAccount(newUserRequest);
        assertThat(result).isEqualTo(EXISTING_USER_RECEIVED_PRIVILEGES);

        verify(existingUser).setHasFreeAccess(true);
        verify(userDAO).save(existingUser);

        verify(userLogDAO).insertNewLog("john@kovalsky.ua", "give user full access by my divorce papers api");
        verify(engineFacade, times(createNewNegotiationTimes)).runAction(any(ActionContext.class), eq(NEW_NEGOTIATION_ACTION_NAME));
    }


    @Test
    public void shouldCreateNewUserWithRequiredDataAndCreateNewNegotationForHim() throws Exception {
        NewUserRequest newUserRequest = new NewUserRequest();
        newUserRequest.setName("john kovalsky");
        newUserRequest.setEmail("john@kovalsky.ua");
        newUserRequest.setPasswordEncoded(new char[]{'s', 'e', 'c', 'r', 'e', 't', 'P', 'a', 's', 's', 'w', 'o', 'r', 'd', 'E', 'n', 'c', 'o', 'd', 'e', 'd'});

        when(userDAO.getByEmail("john@kovalsky.ua")).thenReturn(null);
        when(passwordCoder.decrypt(any(), any())).thenReturn(new char[]{'s', 'e', 'c', 'r', 'e', 't', 'P', 'a', 's', 's', 'w', 'o', 'r', 'd', 'E', 'n', 'c', 'o', 'd', 'e', 'd'});

        String result = myDivorcePapersUserLogic.createAccount(newUserRequest);
        assertThat(result).isEqualTo(USER_CREATED);

        ArgumentCaptor<MyDivorcePapersUser> userCaptor = ArgumentCaptor.forClass(MyDivorcePapersUser.class);

        verify(passwordCoder).decrypt(eq(new char[]{'s', 'e', 'c', 'r', 'e', 't', 'P', 'a', 's', 's', 'w', 'o', 'r', 'd', 'E', 'n', 'c', 'o', 'd', 'e', 'd'}), any());
        verify(userDAO).insert(userCaptor.capture());


        MyDivorcePapersUser user = userCaptor.getValue();

        assertThat(user.getConfirmationHash()).isNullOrEmpty();
        assertThat(user.getEmail()).isEqualTo("john@kovalsky.ua");
        assertThat(user.getName()).isEqualTo("john kovalsky");
        assertThat(user.isConfirmed()).isEqualTo(true);
        assertThat(user.isHasFreeAccess()).isEqualTo(true);

        //check password
        byte[] salt = user.getSalt();
        byte[] hash = Password.hashPassword("secretPasswordEncoded".toCharArray(), salt);

        assertThat(user.getHash()).isEqualTo(hash);

        verify(userLogDAO).insertNewLog("john@kovalsky.ua", "create new user with full access by my divorce papers api");

        verify(engineFacade).runAction(any(ActionContext.class), eq(NEW_NEGOTIATION_ACTION_NAME));
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenUserEmailToBlockIsNotSet() throws Exception {
        BlockUserRequest request = new BlockUserRequest();
        request.setEmail(null);
        request.setReason("some reason");

        myDivorcePapersUserLogic.blockUser(request);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenReasonIsNotSet() throws Exception {
        BlockUserRequest request = new BlockUserRequest();
        request.setEmail("some@email.com");

        myDivorcePapersUserLogic.blockUser(request);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenUserToBlockDoesNotExist() throws Exception {
        final String userEmail = "john@kovalsky.ua";
        BlockUserRequest request = new BlockUserRequest();
        request.setEmail(userEmail);

        when(userDAO.getByEmail(userEmail)).thenReturn(null);

        myDivorcePapersUserLogic.blockUser(request);
    }

    @Test
    public void shouldBlockUserAndRemoveFullAccessRightsAndInsertLogWhenUserHasNoSides() throws Exception {
        final String userEmail = "john@kovalsky.ua";
        final String reason = "this is some serious reason";
        BlockUserRequest request = new BlockUserRequest();
        request.setEmail(userEmail);
        request.setReason(reason);

        User user = new Local();

        when(userDAO.getByEmail(userEmail)).thenReturn(user);
        when(teamLogic.getNegotiationSidesOf(userEmail)).thenReturn(new ArrayList<>());

        myDivorcePapersUserLogic.blockUser(request);


        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);

        verify(userDAO).save(userCaptor.capture());

        assertThat(userCaptor.getValue().isHasFreeAccess()).isFalse();
        assertThat(userCaptor.getValue().isBlocked()).isTrue();

        verify(userLogDAO).insertNewLog(userEmail, "block user by my divorce papers api. Reason: " + reason);
    }

    @Test
    public void shouldBlockUserAndRemoveFullAccessRightsAndInsertLogsForBlockUserAndOneSideWhichHaveFullAccessRights() throws Exception {
        final String userEmail = "john@kovalsky.ua";
        final String reason = "this is some serious reason";
        BlockUserRequest request = new BlockUserRequest();
        request.setEmail(userEmail);
        request.setReason(reason);

        User user = new Local();

        when(userDAO.getByEmail(userEmail)).thenReturn(user);


        User sideWithFullRights = spy(User.class);
        final String sideEmail = "side@email.com";
        sideWithFullRights.setHasFreeAccess(true);
        sideWithFullRights.setEmail(sideEmail);

        User sideWithoutFullRights = spy(User.class);
        sideWithoutFullRights.setHasFreeAccess(false);

        when(teamLogic.getNegotiationSidesOf(userEmail)).thenReturn(Arrays.asList(sideWithFullRights, sideWithoutFullRights));

        myDivorcePapersUserLogic.blockUser(request);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);

        verify(userDAO, times(2)).save(userCaptor.capture());

        assertThat(userCaptor.getAllValues().get(0).isHasFreeAccess()).isFalse();
        assertThat(userCaptor.getAllValues().get(0).isBlocked()).isTrue();

        assertThat(userCaptor.getAllValues().get(1).isHasFreeAccess()).isFalse();
        assertThat(userCaptor.getAllValues().get(1).isBlocked()).isTrue();

        //user withouth full access should not be blocked
        assertThat(sideWithoutFullRights.isHasFreeAccess()).isFalse();
        assertThat(sideWithoutFullRights.isBlocked()).isFalse();


        verify(userLogDAO).insertNewLog(userEmail, "block user by my divorce papers api. Reason: " + reason);
        verify(userLogDAO).insertNewLog(sideEmail, "block user by my divorce papers api. Reason: api blocked user "
                + userEmail + " who is side in negotiation.");
    }

}