package com.agreemount.negotiation.mydivorcepapers;

import org.junit.Test;

import javax.crypto.BadPaddingException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PasswordCoderTest {

    private static final byte[] keyValue = new byte[]{'T', 'h', 'i', 's', 'I', 's', 'A', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};
    private static final byte[] keyValue2 = new byte[]{'X', 'h', 'X', 's', 'I', 'X', 'A', 'x', 'e', 'X', 'r', 'e', 'X', 'K', 'e', 'X'};
    private PasswordCoder passwordCoder = new PasswordCoder();

    @Test
    public void shouldEncrypt() throws Exception {
        char[] password = {'m', 'y', 'p', 'a', 's', 's', 'w', 'o', 'r', 'd'};

        char[] passwordEnc = passwordCoder.encrypt(password, keyValue);

        assertThat(passwordEnc).isEqualTo(new char[]{'s', 'B', 'h', 'C', 'a', 'p', '4', 'u', 'r', 'E', '5', '0', 'a', '/', 'd', 'G', 'u', 'h', 'N', 'g', 'r', 'w', '=', '='});
    }

    @Test
    public void shouldDecrypt() throws Exception {
        char[] encryptedValue = {'s', 'B', 'h', 'C', 'a', 'p', '4', 'u', 'r', 'E', '5', '0', 'a', '/', 'd', 'G', 'u', 'h', 'N', 'g', 'r', 'w', '=', '='};

        char[] decryptedValue = passwordCoder.decrypt(encryptedValue, keyValue);

        assertThat(decryptedValue).isEqualTo(new char[]{'m', 'y', 'p', 'a', 's', 's', 'w', 'o', 'r', 'd'});
    }

    @Test(expected = BadPaddingException.class)
    public void shouldNotDecryptWithOtherKey() throws Exception {
        passwordCoder.decrypt(new char[]{'s', 'B', 'h', 'C', 'a', 'p', '4', 'u', 'r', 'E', '5', '0', 'a', '/', 'd', 'G', 'u', 'h', 'N', 'g', 'r', 'w', '=', '='}, keyValue2);
    }

}