package com.agreemount.negotiation.notification.processors;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.logic.impl.AgreemountDocumentLogic;
import com.google.common.collect.Maps;
import org.assertj.core.api.AbstractObjectAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddPartnerProcessorTest {

    public static final String DOCUMENT_ID = "doc123";
    public static final String DOCUMENT_NAME = "docName123";
    public static final String AUTHOR = "author@gmail.com";
    public static final String REQUESTOR = "bob-requestor@gmail.com";
    public static final String PARENT_ID = "456uy";

    @InjectMocks
    private AddPartnerProcessor addPartnerProcessor;

    @Mock
    private AgreemountDocumentLogic agreemountDocumentLogic;

    private Document document;

    private Map<String, Object> additionalParams;

    private Map<String, Object> result;

    @Test
    public void shouldSetVariable() {
        givenParentDocument(PARENT_ID);
        givenDocument(document(AUTHOR, DOCUMENT_ID, DOCUMENT_NAME));
        givenAdditionalParams(DOCUMENT_ID, REQUESTOR);

        whenPrepareParameters();

        thenNegotiationId().isEqualTo(PARENT_ID);
        thenDocName().isEqualTo(DOCUMENT_NAME);
        thenRequestor().isEqualTo(REQUESTOR);
    }

    private void givenParentDocument(String parentId) {
        Document document = new Document();
        document.setId(parentId);

        when(agreemountDocumentLogic.getMainNegotiation(DOCUMENT_ID)).thenReturn(document);
    }

    private AbstractObjectAssert<?, Object> thenRequestor() {
        return assertThat(result.get("requestor"));
    }

    private AbstractObjectAssert<?, Object> thenSenderEmail() {
        return assertThat(result.get("senderEmail"));
    }

    private AbstractObjectAssert<?, Object> thenDocName() {
        return assertThat(result.get("docName"));
    }

    private AbstractObjectAssert<?, Object> thenNegotiationId() {
        return assertThat(result.get("negotiationId"));
    }

    private void whenPrepareParameters() {
        result = addPartnerProcessor.prepareParameters(additionalParams);
    }

    private void givenAdditionalParams(String documentId, String requestor) {
        Map<String, Object> standardParameters = Maps.newHashMap();
        standardParameters.put("documentId", documentId);
        standardParameters.put("requestor", requestor);
        additionalParams = standardParameters;
    }

    private void givenDocument(Document document) {
        when(agreemountDocumentLogic.getCurrentNegotiation(DOCUMENT_ID)).thenReturn(document);
        this.document = document;
    }

    private Document document(String author, String id, String docName) {
        Document document = new Document();
        document.setAuthor(author);
        document.setId(id);
        document.setName(docName);
        return document;
    }
}