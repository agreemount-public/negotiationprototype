package com.agreemount.negotiation.agreement.template.enricher.mydivorce.enricher;

import com.agreemount.negotiation.agreement.template.enricher.mydivorce.MyDivorceStaticDataEnricher;
import com.agreemount.negotiation.util.LocalDateTimeProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class MyDivorceStaticDataEnricherTest {
    @Mock
    private LocalDateTimeProvider localDateTimeProvider;

    @InjectMocks
    private MyDivorceStaticDataEnricher myDivorceStaticDataEnricher;

    private Map<String, Object> placeholderValueMap;

    @Before
    public void setUp() throws Exception {
        placeholderValueMap = new HashMap<>();
    }

    @Test
    public void dateOfAgreement() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.of(2017, 11, 30, 12, 11);
        when(localDateTimeProvider.now()).thenReturn(localDateTime);

        myDivorceStaticDataEnricher.dateOfAgreement(placeholderValueMap);

        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("${dateTime.of.agreement}", "11/30/2017 12:11");
        assertThat(placeholderValueMap).containsAllEntriesOf(expectedMap);
    }

}