package com.agreemount.negotiation.agreement.template.fixtures;

import com.agreemount.bean.document.Document;


public class DocumentFixtureUtil {

    public static Document stubNegotiation(String sla) {
        Document document = new Document();
        document.setSlaUuid(sla);
        return document;
    }
}
