package com.agreemount.negotiation.agreement.template.replacer;

import com.agreemount.negotiation.agreement.template.replacer.PlaceholderFinder;
import org.assertj.core.api.AssertionsForInterfaceTypes;
import org.junit.Test;

import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PlaceholderFinderTest {

    private PlaceholderFinder placeholderFinder = new PlaceholderFinder();

    @Test
    public void shouldFindPlaceholderUsingRegexp(){
        String givenText = "String with ${placeholder} and sth more";
        Set<String> placeholders = placeholderFinder.findIn(givenText);
        AssertionsForInterfaceTypes.assertThat(placeholders).isNotNull();
        assertThat(placeholders.toArray()).containsExactly("${placeholder}");
    }

    @Test
    public void shouldFindPlaceholdersInGivenText() {
        String givenText = "String with ${placeholder} and uppedcase ${PLACEHOLDER} and camelcase ${Placeholder} " +
                "and with array ${Placeholder[1]} and with item field ${placeholderUser.name} " +
                "and with item field that can be an array ${placeholderUser.names[1]} " +
                "or array without specified index ${placeholderUser.names.[]} " +
                "and with @ ${Place@holder} and with whitespace ${Place holder} " +
                "and with white space and @ ${Place @ holder} and with slash ${Place/holder}" +
                "and underscore with number ${SPOUSE_1.ASSET.[]} ${SPOUSE_1.ASSET_2.[]} and sth more";
        Set<String> placeholders = placeholderFinder.findIn(givenText);
        assertThat(placeholders).isNotNull();
        assertThat(placeholders.toArray()).containsOnly(
                "${placeholder}", "${PLACEHOLDER}", "${Placeholder}",
                "${Placeholder[1]}", "${placeholderUser.name}", "${placeholderUser.names[1]}", "${placeholderUser.names.[]}",
                "${Place@holder}", "${Place holder}", "${Place @ holder}", "${Place/holder}",
                "${SPOUSE_1.ASSET.[]}", "${SPOUSE_1.ASSET_2.[]}");
    }

    @Test
    public void shouldNotFindAnyPlaceholders() {
        String givenText = "String with broken placehodlers {placeholder} and $placeholder and ${placeholder " +
                "and $placeholder} and ${}  and ${[]} and ${[1]} ${placeholder[1].place[2]} sth more";
        Set<String> placeholders = placeholderFinder.findIn(givenText);
        assertThat(placeholders).isNotNull();
        assertThat(placeholders.toArray()).isEmpty();
    }

}