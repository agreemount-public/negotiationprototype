package com.agreemount.negotiation.agreement.template.enricher.mydivorce;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.formatter.ItemFormatter;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.DocumentProvider;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.*;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.ASSET;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.DEBT;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MyDivorceItemEnricherTest {

    private static final String SPOUSE_1_ASSET_FORMATTED_1 = "category1: description1 of value 1";
    private static final String SPOUSE_1_ASSET_FORMATTED_2 = "category2: description2 of value 2";
    private static final String SPOUSE_2_DEBT_FORMATTED_1 = "category12: description12 of value 12";
    private static final String SPOUSE_2_DEBT_FORMATTED_2 = "category22: description22 of value 22";
    private static final String COMMUNITY_ASSET_FORMATTED_1 = "category13: description13 of value 13";
    private static final String COMMUNITY_ASSET_FORMATTED_2 = "category23: description23 of value 23";
    private static final List<String> NO_SUCH_ITEMS_LIST = singletonList("(no such items)");
    @Mock
    private DocumentProvider documentProvider;

    @Mock
    private ItemFormatter itemFormatter;

    @InjectMocks
    private MyDivorceItemEnricher enricher;

    private Map<String, Object> placeholderValueMap = new HashMap();

    @Test
    public void shouldEnrichMapByGivenFormattedItems() throws Exception {
        Document negotiation = givenNegotiation();
        givenAgreedItems(negotiation, ASSET, SPOUSE_1, item("1"), item("2"));
        givenFormattedItem(item("1"), SPOUSE_1, SPOUSE_1_ASSET_FORMATTED_1);
        givenFormattedItem(item("2"), SPOUSE_1, SPOUSE_1_ASSET_FORMATTED_2);

        givenAgreedItems(negotiation, DEBT, SPOUSE_2, item("12"), item("22"));
        givenFormattedItem(item("12"), SPOUSE_2, SPOUSE_2_DEBT_FORMATTED_1);
        givenFormattedItem(item("22"), SPOUSE_2, SPOUSE_2_DEBT_FORMATTED_2);

        givenAgreedItems(negotiation, ASSET, COMMUNITY, item("13"), item("23"));
        givenFormattedItem(item("13"), COMMUNITY, COMMUNITY_ASSET_FORMATTED_1);
        givenFormattedItem(item("23"), COMMUNITY, COMMUNITY_ASSET_FORMATTED_2);

        enricher.enrich(placeholderValueMap, negotiation);

        assertThat(placeholderValueMap)
                .containsEntry("${SPOUSE_1.ASSET.[]}", asList(SPOUSE_1_ASSET_FORMATTED_1, SPOUSE_1_ASSET_FORMATTED_2))
                .containsEntry("${SPOUSE_1.DEBT.[]}", NO_SUCH_ITEMS_LIST)
                .containsEntry("${SPOUSE_2.ASSET.[]}", NO_SUCH_ITEMS_LIST)
                .containsEntry("${SPOUSE_2.DEBT.[]}", asList(SPOUSE_2_DEBT_FORMATTED_1, SPOUSE_2_DEBT_FORMATTED_2))
                .containsEntry("${COMMUNITY.ASSET.[]}", asList(COMMUNITY_ASSET_FORMATTED_1, COMMUNITY_ASSET_FORMATTED_2))
                .containsEntry("${COMMUNITY.DEBT.[]}", NO_SUCH_ITEMS_LIST);

    }

    @Test
    public void shouldBeSingleNoSuchValueLabelWhenNoAgreedItems() throws Exception {
        Document negotiation = givenNegotiation();
        givenAgreedItems(negotiation, ASSET, SPOUSE_1);

        enricher.enrich(placeholderValueMap, negotiation);

        verify(itemFormatter, never()).itemAsString(anyObject(), eq(SPOUSE_1));
        assertThat(placeholderValueMap).containsEntry("${SPOUSE_1.ASSET.[]}", NO_SUCH_ITEMS_LIST);
    }

    private void givenFormattedItem(Document item, AwardedTo awardedTo, String itemAsString) {
        when(itemFormatter.itemAsString(argThat(hasProperty("id", equalTo(item.getId()))), eq(awardedTo))).thenReturn(itemAsString);
    }

    private void givenAgreedItems(Document negotiation, ItemType itemType, AwardedTo awardedTo, Document... items) {
        when(documentProvider.getAgreedDocuments(negotiation, itemType, awardedTo)).thenReturn(asList(items));
    }

    private Document item(String id) {
        Document document = new Document();
        document.setId(id);
        return document;
    }

    private Document givenNegotiation() {
        return new Document();
    }


}