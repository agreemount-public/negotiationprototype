package com.agreemount.negotiation.agreement.template.enricher;

import com.agreemount.negotiation.agreement.template.enricher.ny.NYStaticDataEnricher;
import com.agreemount.negotiation.util.LocalDateTimeProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class NYStaticDataEnricherTest {
    @Mock
    private LocalDateTimeProvider localDateTimeProvider;

    @InjectMocks
    private NYStaticDataEnricher NYStaticDataEnricher;

    private Map<String, Object> placeholderValueMap;

    @Before
    public void setUp() throws Exception {
        placeholderValueMap = new HashMap<>();
    }

    @Test
    public void dateOfAgreement() throws Exception {
        LocalDateTime localDateTime = LocalDateTime.of(2017, 11, 30, 12, 11);
        when(localDateTimeProvider.now()).thenReturn(localDateTime);

        NYStaticDataEnricher.dateOfAgreement(placeholderValueMap);

        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("${month.and.day.of.agreement}", "11, 30");
        expectedMap.put("${year.of.agreement}", "17");
        assertThat(placeholderValueMap).containsAllEntriesOf(expectedMap);
    }

}