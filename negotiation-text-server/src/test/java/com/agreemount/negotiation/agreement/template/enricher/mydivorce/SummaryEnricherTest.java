package com.agreemount.negotiation.agreement.template.enricher.mydivorce;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.enricher.USDValueFormatter;
import com.agreemount.negotiation.summary.PartialSummary;
import com.agreemount.negotiation.summary.Summary;
import com.agreemount.negotiation.summary.SummaryCalculator;
import com.agreemount.negotiation.summary.SummaryDataService;
import com.google.common.collect.Maps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SummaryEnricherTest {
    @Mock
    private SummaryCalculator summaryCalculator;

    @Mock
    private SummaryDataService summaryDataService;

    @Spy
    private USDValueFormatter usdValueFormatter;

    @InjectMocks
    private SummaryEnricher summaryEnricher;

    private Document negotiation = new Document();

    @Test
    public void shouldPrepareFormattedSummaryData() throws Exception {
        Map<String, Object> placeholderValueMap = Maps.newHashMap();
        givenCalculatedSummary(
                new PartialSummaryBuilder().withAssets(40).withDebts(10).withPercentage(10).build(),
                new PartialSummaryBuilder().withAssets(100).withDebts(40).withPercentage(90).build(),
                new PartialSummaryBuilder().withAssets(140).withDebts(50).build());

        summaryEnricher.enrich(placeholderValueMap, negotiation);

        assertThat(placeholderValueMap)
                .containsEntry("${summary.spouse1.percentage}", "10%")
                .containsEntry("${summary.spouse1.total}", "$30.00")
                .containsEntry("${summary.spouse1.assets}", "$40.00")
                .containsEntry("${summary.spouse1.debts}", "$10.00")

                .containsEntry("${summary.spouse2.percentage}", "90%")
                .containsEntry("${summary.spouse2.total}", "$60.00")
                .containsEntry("${summary.spouse2.assets}", "$100.00")
                .containsEntry("${summary.spouse2.debts}", "$40.00")

                .containsEntry("${summary.totalToDivide.total}", "$90.00")
                .containsEntry("${summary.totalToDivide.assets}", "$140.00")
                .containsEntry("${summary.totalToDivide.debts}", "$50.00");
    }

    @Test
    public void shouldFormatNullPercentageValue() {
        Map<String, Object> placeholderValueMap = Maps.newHashMap();
        givenCalculatedSummary(
                new PartialSummaryBuilder().withPercentage(null).build(),
                new PartialSummaryBuilder().withPercentage(0).build(),
                new PartialSummaryBuilder().build());

        summaryEnricher.enrich(placeholderValueMap, negotiation);

        assertThat(placeholderValueMap)
                .containsEntry("${summary.spouse1.percentage}", "--%")
                .containsEntry("${summary.spouse2.percentage}", "0%");
    }


    private void givenCalculatedSummary(PartialSummary side1Summary, PartialSummary side2Summary, PartialSummary totalToDivideSummary) {
        Summary summary = mock(Summary.class);
        when(summary.getSide1()).thenReturn(side1Summary);
        when(summary.getSide2()).thenReturn(side2Summary);
        when(summary.getTotalToDivide()).thenReturn(totalToDivideSummary);


        List<Document> items = new ArrayList<>();
        when(summaryDataService.getAgreedItems(negotiation)).thenReturn(items);
        when(summaryCalculator.buildSummary(items)).thenReturn(summary);
    }

    static class PartialSummaryBuilder {

        private Integer assets = 0;
        private Integer debts = 0;
        private Integer percentage;

        PartialSummaryBuilder withAssets(Integer assets) {
            this.assets = assets;
            return this;
        }

        PartialSummaryBuilder withDebts(Integer debts) {
            this.debts = debts;
            return this;
        }

        PartialSummaryBuilder withPercentage(Integer percentage) {
            this.percentage = percentage;
            return this;
        }

        PartialSummary build() {
            PartialSummary partialSummary = new PartialSummary();
            partialSummary.addAsset(assets);
            partialSummary.addDebts(debts);
            Optional.ofNullable(percentage)
                    .map(BigDecimal::valueOf)
                    .ifPresent(percent -> partialSummary.setPercentage(percent));
            return partialSummary;
        }

    }

}