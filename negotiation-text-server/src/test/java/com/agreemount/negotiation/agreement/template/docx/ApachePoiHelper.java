package com.agreemount.negotiation.agreement.template.docx;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;


public class ApachePoiHelper {

    public static XWPFDocument sampleDocx(String text) {
        XWPFDocument document = new XWPFDocument();
        XWPFParagraph tmpParagraph = document.createParagraph();
        XWPFRun tmpRun = tmpParagraph.createRun();
        tmpRun.setText(text);
        return document;
    }

    public static XWPFDocument sampleDocx(String initText, String... placeholderSplitByRun) {
        XWPFDocument document = new XWPFDocument();
        XWPFParagraph tmpParagraph = document.createParagraph();
        XWPFRun tmpRun = tmpParagraph.createRun();
        tmpRun.setText(initText);
        for (String runText : placeholderSplitByRun) {
            tmpParagraph.createRun().setText(runText);
        }
        return document;
    }

    public static String extractTextFromParagraphs(XWPFDocument xwpfDocument) {
        return new XWPFWordExtractor(xwpfDocument).getText().trim();
    }

}
