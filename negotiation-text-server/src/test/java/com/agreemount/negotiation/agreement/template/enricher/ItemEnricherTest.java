package com.agreemount.negotiation.agreement.template.enricher;

import com.agreemount.bean.document.Document;
import com.agreemount.engine.facade.QueryFacade;
import com.agreemount.negotiation.agreement.template.enricher.ny.ItemEnricher;
import com.agreemount.negotiation.agreement.template.fixtures.DocumentFixtureUtil;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.util.SideServiceUtil;
import com.agreemount.slaneg.action.ActionContextFactory;
import org.assertj.core.util.Lists;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.agreemount.negotiation.bean.enums.Metrics.*;
import static com.agreemount.negotiation.bean.enums.States.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ItemEnricherTest {

    public static final String NORMAL_NEGOTIATION_DOCUMENT_TYPE = "split";
    private static final String SLA_UUID = "jvaBszPYVo";
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private SideServiceUtil sideServiceUtil;

    @Mock
    private QueryFacade queryFacade;

    @Mock
    private ActionContextFactory actionContextFactory;


    @InjectMocks
    private ItemEnricher itemEnricher;

    private Matcher<Document> negotiationWithSlaUuid = hasProperty("slaUuid", equalTo(SLA_UUID));
    private Map<String, Object> placeholderValueMap;
    private Document negotiation;

    @Before
    public void setUp() throws Exception {
        placeholderValueMap = new HashMap<>();
        negotiation = DocumentFixtureUtil.stubNegotiation(SLA_UUID);

        when(sideServiceUtil.getSideForLoggedInUser(any(Document.class))).thenReturn(UserRole.SIDE1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRethrowIllegalArgumentException() {
        when(sideServiceUtil.getSideForLoggedInUser(any())).thenThrow(IllegalArgumentException.class);

        itemEnricher.enrich(placeholderValueMap, negotiation);
    }

    @Test
    public void shouldThrowExceptionWhenDocumentDoesNotHaveStatesCategory() {
        //given
        List<Document> negotiationItems = new LinkedList<>();
        Map<String, Object> metrics = metrics("Cash", "Lottery", 2000, 20, 80);
        Map<String, String> states = new HashMap<>();
        states.put(DOCUMENT_TYPE.getValue(), "item");
        Document item = stubDocument("1", metrics, states, "cashItem1");
        negotiationItems.add(item);

        when(queryFacade.getDocumentsForQuery(eq("AgreedItemsInSplit"), any()))
                .thenReturn(negotiationItems);

        // then
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage(containsString("document.states.itemType should not be null"));

        // when
        itemEnricher.enrich(placeholderValueMap, negotiation);
    }

    @Test
    public void shouldThrowExceptionWhenDocumentDoesNotHaveMetricsItemCategory() {
        //given
        List<Document> negotiationItems = new LinkedList<>();
        Map<String, String> states = states("SPLIT", "asset");
        Map<String, Object> metrics = metrics("Cash", "Lottery", 2000, 20, 80);
        metrics.remove(ITEM_CATEGORY.getValue());
        Document item = stubDocument("1", metrics, states, "cashItem1");
        negotiationItems.add(item);

        when(queryFacade.getDocumentsForQuery(eq("AgreedItemsInSplit"), any()))
                .thenReturn(negotiationItems);

        // then
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage(containsString("document.metrics.itemCategory should not be null"));

        // when
        itemEnricher.enrich(placeholderValueMap, negotiation);
    }

    @Test
    public void shouldSkipNegotiationDocument() {
        //given
        Map<String, Object> metricsCashItem1 = metrics("Cash", "Lottery", 2000, 20, 80);
        Map<String, String> states = new HashMap<>();
        states.put(DOCUMENT_TYPE.getValue(), NORMAL_NEGOTIATION_DOCUMENT_TYPE);
        states.put(MAIN_STATE.getValue(), "open");
        Document negotiation = stubDocument("1", metricsCashItem1, states, "QZsqgPhecz");

        doReturn(Lists.newArrayList(negotiation))
                .when(queryFacade)
                .getDocumentsForQuery(eq("AgreedItemsInSplit"), any());


        //when
        itemEnricher.enrich(placeholderValueMap, negotiation);

        //then
        assertThat(placeholderValueMap).doesNotContainKeys("${asset.Cash.wife}", "${asset.Cash.husband}");

    }

    @Test
    public void shouldEnrichAssetsAndDebtsPerCategoriesThatAreDividedToHusbandAndWife() {
        //given
        placeholderValueMap.put("${defaultPlaceholder}", "defaultPlaceholderValue");

        doReturn(mockTwoCashAndOneAutomobilAssetsItemPlusOneDebt())
                .when(queryFacade)
                .getDocumentsForQuery(eq("AgreedItemsInSplit"), any());


        //when
        itemEnricher.enrich(placeholderValueMap, negotiation);

        //then
        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("${asset.Automobiles.wife}", "Ford 90% of 10000");
        expectedMap.put("${debt.Mortgage.wife}", "Cottage house 40% of 200000");
        expectedMap.put("${asset.Cash.wife}", "Lottery 80% of 2000, Savings 50% of 500000");
        expectedMap.put("${debt.Mortgage.husband}", "Cottage house 60% of 200000");
        expectedMap.put("${defaultPlaceholder}", "defaultPlaceholderValue");
        expectedMap.put("${asset.Automobiles.husband}", "Ford 10% of 10000");
        expectedMap.put("${asset.Cash.husband}", "Lottery 20% of 2000, Savings 50% of 500000");
        assertThat(placeholderValueMap).containsAllEntriesOf(expectedMap);

    }

    private List<Document> mockTwoCashAndOneAutomobilAssetsItemPlusOneDebt() {
        List<Document> negotiationItems = new LinkedList<>();

        Map<String, Object> metricsCashItem1 = metrics("Cash", "Lottery", 2000, 20, 80);
        Document cashItem1 = stubDocument("1", metricsCashItem1, states("SPLIT", "asset"), "cashItem1");

        Map<String, Object> metricsCashItem2 = metrics("Cash", "Savings", 500000, 50, 50);
        Document cashItem2 = stubDocument("2", metricsCashItem2, states("SPLIT", "asset"), "cashItem2");

        Map<String, Object> metricsAuto1 = metrics("Automobiles", "Ford", 10000, 10, 90);
        Document autoItem1 = stubDocument("3", metricsAuto1, states("SPLIT", "asset"), "autoItem1");

        Map<String, Object> debt = metrics("Mortgage", "Cottage house", 200000, 60, 40);
        Document debtItem = stubDocument("4", debt, states("SPLIT", "debt"), "debtItem");

        negotiationItems.add(cashItem1);
        negotiationItems.add(cashItem2);
        negotiationItems.add(autoItem1);
        negotiationItems.add(debtItem);

        return negotiationItems;
    }

    private Map<String, Object> metrics(String category, String description, int value, int side1Percent, int side2Percent) {
        Map<String, Object> metrics = new HashMap<>();
        metrics.put(ITEM_CATEGORY.getValue(), category);
        metrics.put(ITEM_DESCRIPTION.getValue(), description);
        metrics.put(ITEM_VALUE.getValue(), value);
        metrics.put(SPLIT_PERCENT_SIDE1.getValue(), side1Percent);
        metrics.put(SPLIT_PERCENT_SIDE2.getValue(), side2Percent);
        metrics.put(SPLIT_DESCRIPTION.getValue(), "");
        return metrics;
    }

    private Map<String, String> states(String itemOwner, final String itemType) {

        Map<String, String> states = new HashMap<>();
        states.put(ITEM_TYPE.getValue(), itemType);
        states.put(ITEM_DESC_HAS_CHANGES.getValue(), "true");
        states.put(ITEM_VALUE_HAS_CHANGES.getValue(), "true");
        states.put(DOCUMENT_TYPE.getValue(), "item");
        states.put(ITEM_STATE.getValue(), "visible");
        states.put(ITEM_OWNER.getValue(), itemOwner);
        states.put(AGREED_CONTENT_SIDE1.getValue(), "true");
        states.put(AGREED_CONTENT_SIDE2.getValue(), "true");
        states.put(AGREED_OWNER_SIDE1.getValue(), "true");
        states.put(AGREED_OWNER_SIDE2.getValue(), "true");

        return states;
    }


    private Document stubDocument(String id, final Map<String, Object> metrics, final Map<String, String> states, String rootUUID) {
        Document document = new Document();
        document.setId(id);
        document.setSlaUuid(SLA_UUID);
        document.setRootUuid(rootUUID);
        document.setStates(states);
        document.setMetrics(metrics);

        return document;
    }
}