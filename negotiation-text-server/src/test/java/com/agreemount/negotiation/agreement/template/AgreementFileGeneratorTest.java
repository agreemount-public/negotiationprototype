package com.agreemount.negotiation.agreement.template;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.docx.DocumentHelper;
import com.agreemount.negotiation.agreement.template.replacer.PlaceholderReplacer;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.MyDivorceStaticDataEnricher;
import com.agreemount.negotiation.agreement.template.enricher.ny.ItemEnricher;
import com.agreemount.negotiation.agreement.template.enricher.PersonalDataEnricher;
import com.agreemount.negotiation.agreement.template.enricher.ny.NYStaticDataEnricher;
import com.agreemount.negotiation.agreement.template.fixtures.DocumentFixtureUtil;
import com.agreemount.negotiation.agreement.template.locator.MyDivorceTemplate;
import com.agreemount.negotiation.agreement.template.locator.NYTemplate;
import com.agreemount.slaneg.db.DocumentOperations;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;



@RunWith(MockitoJUnitRunner.class)
public class AgreementFileGeneratorTest {

    public static final String GENERATED_AGREEMENT_FOLDER = "/generated";
    public static final String TEMPLATE_NY_AGREEMENT_DOCX = "template/ny/Agreement.docx";
    private static final String SLA = "testSla";
    public static final String DOCUMENT_ID = "123";
    private static final String TEMPLATE_MY_DIVORCE_AGREEMENT_DOCX = "template/my/divorce/Agreement.docx";
    @Mock
    private ItemEnricher itemEnricher;

    @Mock
    private PersonalDataEnricher personalDataEnricher;

    @Spy
    private List<AgreementModelEnricher> agreementModelEnricherMap = new ArrayList<>();

    @Mock
    private DocumentHelper docxHelper;

    @Spy
    private List<StaticDataEnricher> staticDataEnrichers = new ArrayList<>();

    @Mock
    private MyDivorceStaticDataEnricher myDivorceStaticDataEnricher;

    @Mock
    private NYStaticDataEnricher nyStaticDataEnricher;

    @Mock
    private PlaceholderReplacer replacer;

    @InjectMocks
    private AgreementFileGenerator agreementFileGenerator = new AgreementFileGenerator();

    @Captor
    private ArgumentCaptor<XWPFDocument> docxCaptor;

    @Captor
    private ArgumentCaptor<Path> toPathCaptor;

    @Mock
    private DocumentOperations documentOperations;

    @Spy
    private List<TemplateLocator> templateLocators = new ArrayList<>();

    @Mock
    private MyDivorceTemplate myDivorceTemplate;

    @Mock
    private NYTemplate nyTemplate;

    @Mock
    private ApplicationContext applicationContext;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        when(itemEnricher.getRegion()).thenReturn(Region.NY);
        when(personalDataEnricher.getRegion()).thenReturn(Region.NY);
        agreementModelEnricherMap.add(itemEnricher);
        agreementModelEnricherMap.add(personalDataEnricher);

        when(nyTemplate.getRegion()).thenReturn(Region.NY);
        when(nyTemplate.getAgreementTemplateURI()).thenReturn(TEMPLATE_NY_AGREEMENT_DOCX);
        when(myDivorceTemplate.getRegion()).thenReturn(Region.MY_DIVORCE);
        when(myDivorceTemplate.getAgreementTemplateURI()).thenReturn(TEMPLATE_MY_DIVORCE_AGREEMENT_DOCX);
        templateLocators.add(nyTemplate);
        templateLocators.add(myDivorceTemplate);

        when(nyStaticDataEnricher.getRegion()).thenReturn(Region.NY);
        when(myDivorceStaticDataEnricher.getRegion()).thenReturn(Region.MY_DIVORCE);
        staticDataEnrichers.add(nyStaticDataEnricher);
        staticDataEnrichers.add(myDivorceStaticDataEnricher);

        agreementFileGenerator.enrichedAgreementFolder = GENERATED_AGREEMENT_FOLDER;

        when(applicationContext.getBean(eq(PlaceholderReplacer.class))).thenReturn(replacer);
    }

    @Test
    public void shouldVerifyMainFlowOfGenerator() {
        //given
        XWPFDocument xwpfDocument = new XWPFDocument();
        appendParagraphWithTextToDocument(xwpfDocument, "First Paragraph ${PlaceholderAtTheEnd}");
        when(docxHelper.readTemplate(Paths.get(TEMPLATE_NY_AGREEMENT_DOCX))).thenReturn(xwpfDocument);

        XWPFDocument enrichedXwpfDocument = new XWPFDocument();
        appendParagraphWithTextToDocument(enrichedXwpfDocument, "First Paragraph of this test");

        when(replacer.replace(anyMap(), argThat(equalTo(xwpfDocument)))).thenReturn(enrichedXwpfDocument);
        Document negotiation = DocumentFixtureUtil.stubNegotiation(SLA);
        when(documentOperations.getDocument(DOCUMENT_ID)).thenReturn(negotiation);

        //when
        agreementFileGenerator.generate(DOCUMENT_ID, Region.NY);

        //then
        verify(nyStaticDataEnricher).dateOfAgreement(any());
        verify(itemEnricher).enrich(any(), argThat(equalTo(negotiation)));
        verify(personalDataEnricher).enrich(any(), argThat(equalTo(negotiation)));
        verify(docxHelper).write(docxCaptor.capture(), toPathCaptor.capture());
        assertThat(docxCaptor.getValue()).isEqualTo(enrichedXwpfDocument);
        assertThat(toPathCaptor.getValue()).isEqualByComparingTo(Paths.get(GENERATED_AGREEMENT_FOLDER + File.separator + SLA + ".docx"));
    }

    @Test
    public void shouldThrowExceptionWhenDocumentNotFound(){
        //given
        when(documentOperations.getDocument(DOCUMENT_ID)).thenReturn(null);

        //then
        thrown.expectMessage("Can not find negotiation case by documentId["+DOCUMENT_ID+"]");
        thrown.expect(NullPointerException.class);

        //when
        agreementFileGenerator.generate(DOCUMENT_ID, Region.NY);
    }

    private void appendParagraphWithTextToDocument(XWPFDocument document, String text) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText(text);
    }

}