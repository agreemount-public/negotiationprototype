package com.agreemount.negotiation.agreement.template.replacer;

import com.agreemount.negotiation.agreement.template.docx.ApachePoiHelper;
import com.agreemount.negotiation.agreement.template.replacer.PlaceholderFinder;
import com.agreemount.negotiation.agreement.template.replacer.PlaceholderReplacer;
import com.google.common.collect.ImmutableMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.util.Sets;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Sets.newLinkedHashSet;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.when;




@RunWith(JUnitParamsRunner.class)
public class PlaceholderReplacerTest {

    private static final String PLACEHOLDER_1_KEY = "${placeholder[1]}";
    private static final String PLACEHOLDER_1_VALUE = "value_1";
    private static final String PLACEHOLDER_2_KEY = "${placeholder[2]}";
    private static final String PLACEHOLDER_2_VALUE = "2";
    private static final String PLACEHOLDER_KEY_WITHOUT_VALUE = "${placeholder[3]}";
    private static final String PLACEHOLDER_DEFAULT_VALUE = "_________________";
    private static Map<String, Object> placeholderValueMap = new HashMap<>();
    private static final String SAMPLE_TEXT = "Sample text that has two placeholders. " +
            "The first one is %s. The second one is %s. " +
            "And that is all what I want to test here.";
    @Mock
    private PlaceholderFinder placeholderFinder;

    @InjectMocks
    private PlaceholderReplacer replacePlaceholder = new PlaceholderReplacer();

    public PlaceholderReplacerTest() throws XmlException {
    }

    @BeforeClass
    public static void staticSetUp() throws Exception {
        placeholderValueMap.put(PLACEHOLDER_1_KEY, PLACEHOLDER_1_VALUE);
        placeholderValueMap.put(PLACEHOLDER_2_KEY, PLACEHOLDER_2_VALUE);
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldCopyStyleFromRunWithFirstPartOfPlaceholderToNewRunWithValue() {
        //given
        String templateText = "init tekst " + PLACEHOLDER_1_KEY;
        when(placeholderFinder.findIn(contains(templateText))).thenReturn(
                newLinkedHashSet(PLACEHOLDER_1_KEY, PLACEHOLDER_2_KEY));

        XWPFDocument templateDocx = new XWPFDocument();
        XWPFParagraph paragraph = templateDocx.createParagraph();
        XWPFRun runInit = paragraph.createRun();
        runInit.setText("init tekst ");
        defaultStyle(runInit);

        XWPFRun runWithFirstPartOfPlaceholder = paragraph.createRun();
        runWithFirstPartOfPlaceholder.setText("${");
        beginOfPlaceholderStyle(runWithFirstPartOfPlaceholder);

        XWPFRun runWithLastPartOfPlaceholder = paragraph.createRun();
        runWithLastPartOfPlaceholder.setText("placeholder[1]}");
        defaultStyle(runWithLastPartOfPlaceholder);

        //when
        XWPFDocument actualDocx = replacePlaceholder.replace(placeholderValueMap, templateDocx);

        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(actualDocx).isNotNull();
        softly.assertThat(actualDocx.getParagraphs()).isNotEmpty();
        softly.assertThat(actualDocx.getParagraphs().get(0).getRuns()).hasSize(2);
        assertThatHasStyleEqualsToDefault(runWithInitText(actualDocx), softly);
        assertThatHasStyleEqualsToBeginOfPlaceholder(runWithPlaceholder(actualDocx), softly);
        softly.assertAll();
    }

    @Test
    @Parameters(method = "placeholderValueSource")
    public void shouldReplacePlaceholderByCorrespondingValueOrDots(String placeholderKey, String placeholderValue) {
        //given
        String templateText = String.format(SAMPLE_TEXT, placeholderKey, PLACEHOLDER_2_KEY);
        when(placeholderFinder.findIn(contains(templateText))).thenReturn(
                newLinkedHashSet(placeholderKey, PLACEHOLDER_2_KEY));
        XWPFDocument templateDocx = ApachePoiHelper.sampleDocx(templateText);

        //when
        XWPFDocument actualDocx = replacePlaceholder.replace(placeholderValueMap, templateDocx);

        //then
        assertThat(actualDocx).isNotNull();
        String expectedText = String.format(SAMPLE_TEXT, placeholderValue, PLACEHOLDER_2_VALUE);
        assertThat(ApachePoiHelper.extractTextFromParagraphs(actualDocx)).isEqualTo(expectedText);
    }

    @Test
    public void shouldReplacePlaceholderByListOfSpaceSeparatedValues() {
        List<String> values = newArrayList("asset1", "asset2");
        String placeholder = "${section.paragraph.[]}";
        Map<String, Object> placeholderValues = ImmutableMap.of(
                PLACEHOLDER_2_KEY, PLACEHOLDER_2_VALUE,
                placeholder, values);
        String text = "Document %s with items:" + System.lineSeparator() + "%s";
        String templateText = String.format(text, PLACEHOLDER_2_KEY, placeholder);
        when(placeholderFinder.findIn(contains(templateText))).thenReturn(newLinkedHashSet(PLACEHOLDER_2_KEY, placeholder));
        XWPFDocument templateDocx = ApachePoiHelper.sampleDocx(templateText);

        XWPFDocument actualDocx = replacePlaceholder.replace(placeholderValues, templateDocx);

        assertThat(actualDocx).isNotNull();
        String expectedText = "asset1\nasset2";
        assertThat(ApachePoiHelper.extractTextFromParagraphs(actualDocx)).isEqualTo(expectedText);
    }

    @Test
    public void shouldReplacePlaceholderSplitByFewRuns() {
        //given
        String placeholderKey = "${placeholder.nextRun.run}";

        String templateText = "init tekst " + placeholderKey + " then new run and then end of text";
        when(placeholderFinder.findIn(contains(templateText))).thenReturn(
                newLinkedHashSet(placeholderKey, PLACEHOLDER_2_KEY));
        XWPFDocument templateDocx = ApachePoiHelper.sampleDocx("init ", "tekst ${", "placeholder", ".nextRun", ".run", "} ", "then new run", " and then end of text");
        placeholderValueMap.put(placeholderKey, PLACEHOLDER_1_VALUE);

        //when
        XWPFDocument actualDocx = replacePlaceholder.replace(placeholderValueMap, templateDocx);

        //then
        assertThat(actualDocx).isNotNull();
        String expectedText = "init tekst " + PLACEHOLDER_1_VALUE + " then new run and then end of text";
        assertThat(ApachePoiHelper.extractTextFromParagraphs(actualDocx)).isEqualTo(expectedText);
    }

    private Object[][] placeholderValueSource() {
        return new Object[][]{
                {PLACEHOLDER_KEY_WITHOUT_VALUE, PLACEHOLDER_DEFAULT_VALUE},
                {PLACEHOLDER_1_KEY, PLACEHOLDER_1_VALUE}
        };
    }

    @Test
    public void shouldNotDoAnythingWhenNoPlaceholders() {
        //given
        String templateText = "Template text without placeholders";
        XWPFDocument templateDocx = ApachePoiHelper.sampleDocx(templateText);
        when(placeholderFinder.findIn(contains(templateText))).thenReturn(Sets.newHashSet());

        //when
        XWPFDocument actualDocx = replacePlaceholder.replace(placeholderValueMap, templateDocx);

        //then
        assertThat(actualDocx).isNotNull();
        assertThat(ApachePoiHelper.extractTextFromParagraphs(actualDocx)).isEqualTo(templateText);
    }

    private void defaultStyle(XWPFRun run) {
        run.setBold(false);
        run.setCapitalized(false);
        run.setColor("00FFCC");
        run.setDoubleStrikethrough(false);
        run.setEmbossed(true);
        run.setFontFamily("Tahoma");
        run.setFontSize(12);
        run.setImprinted(true);
        run.setItalic(false);
        run.setKerning(2);
        run.setShadow(false);
        run.setSmallCaps(true);
        run.setStrikeThrough(true);
        run.setSubscript(VerticalAlign.SUBSCRIPT);
        run.setUnderline(UnderlinePatterns.DASH_DOT_DOT_HEAVY);
        run.isShadowed();
    }

    private void assertThatHasStyleEqualsToDefault(XWPFRun run, SoftAssertions softly) {
        softly.assertThat(run.isBold()).isFalse();
        softly.assertThat(run.isCapitalized()).isFalse();
        softly.assertThat(run.getColor()).isEqualTo("00FFCC");
        softly.assertThat(run.isDoubleStrikeThrough()).isFalse();
        softly.assertThat(run.isEmbossed()).isTrue();
        softly.assertThat(run.getFontFamily()).isEqualTo("Tahoma");
        softly.assertThat(run.getFontSize()).isEqualTo(12);
        softly.assertThat(run.isImprinted()).isTrue();
        softly.assertThat(run.isItalic()).isFalse();
        softly.assertThat(run.getKerning()).isEqualTo(2);
        softly.assertThat(run.isShadowed()).isFalse();
        softly.assertThat(run.isSmallCaps()).isTrue();
        softly.assertThat(run.isStrikeThrough()).isTrue();
        softly.assertThat(run.getSubscript()).isEqualTo(VerticalAlign.SUBSCRIPT);
        softly.assertThat(run.getUnderline()).isEqualTo(UnderlinePatterns.DASH_DOT_DOT_HEAVY);
    }

    private XWPFRun runWithInitText(XWPFDocument actualDocx) {
        return actualDocx.getParagraphs().get(0).getRuns().get(0);
    }

    private void beginOfPlaceholderStyle(XWPFRun run) {
        run.setBold(true);
        run.setCapitalized(true);
        run.setColor("00FFAA");
        run.setDoubleStrikethrough(true);
        run.setEmbossed(false);
        run.setFontFamily("Arial");
        run.setFontSize(16);
        run.setImprinted(false);
        run.setItalic(true);
        run.setKerning(1);
        run.setShadow(true);
        run.setSmallCaps(false);
        run.setStrikeThrough(false);
        run.setSubscript(VerticalAlign.BASELINE);
        run.setUnderline(UnderlinePatterns.DASH);
    }

    private void assertThatHasStyleEqualsToBeginOfPlaceholder(XWPFRun run, SoftAssertions softly) {
        softly.assertThat(run.isBold()).isTrue();
        softly.assertThat(run.isCapitalized()).isTrue();
        softly.assertThat(run.getColor()).isEqualTo("00FFAA");
        softly.assertThat(run.isDoubleStrikeThrough()).isTrue();
        softly.assertThat(run.isEmbossed()).isFalse();
        softly.assertThat(run.getFontFamily()).isEqualTo("Arial");
        softly.assertThat(run.getFontSize()).isEqualTo(16);
        softly.assertThat(run.isImprinted()).isFalse();
        softly.assertThat(run.isItalic()).isTrue();
        softly.assertThat(run.getKerning()).isEqualTo(1);
        softly.assertThat(run.isShadowed()).isTrue();
        softly.assertThat(run.isSmallCaps()).isFalse();
        softly.assertThat(run.isStrikeThrough()).isFalse();
        softly.assertThat(run.getSubscript()).isEqualTo(VerticalAlign.BASELINE);
        softly.assertThat(run.getUnderline()).isEqualTo(UnderlinePatterns.DASH);
    }

    private XWPFRun runWithPlaceholder(XWPFDocument actualDocx) {
        return actualDocx.getParagraphs().get(0).getRuns().get(1);
    }
}