package com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider;

import com.agreemount.bean.document.Document;
import com.agreemount.engine.facade.QueryFacade;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.*;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.ASSET;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.DEBT;
import static com.agreemount.negotiation.bean.enums.States.DOCUMENT_TYPE;
import static com.agreemount.negotiation.bean.enums.States.ITEM_TYPE;
import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DocumentProviderTest {

    private static final String ID_ASSET = "1";
    private static final String ID_ASSET_LOWERCASE = "111";
    private static final String ID_DEBT = "2";
    private static final String ITEM_DOCUMENT = "item";
    private static final String NEGOTIATION_DOCUMENT = "split";
    @Mock
    private QueryFacade queryFacade;

    @Mock
    private ActionContextFactory actionContextFactory;

    @Mock
    private ActionContext actionContext;

    @InjectMocks
    private DocumentProvider documentProvider;

    private Document negotiation;
    private List<Document> queryResultDocuments;

    @Before
    public void setUp() throws Exception {
        when(actionContextFactory.createInstance(any(Document.class))).thenReturn(actionContext);
        givenNegotiationDocument();
        givenQueryResultDocuments(
                document(ID_ASSET, ASSET, ITEM_DOCUMENT),
                document(ID_ASSET_LOWERCASE, ASSET.name().toLowerCase(), ITEM_DOCUMENT),
                document(ID_DEBT, DEBT, ITEM_DOCUMENT),
                document("3", "", ITEM_DOCUMENT),
                document("4", ASSET, NEGOTIATION_DOCUMENT));
    }

    @Test
    public void shouldFetchAllAgreedAssets() {
        givenQueryResult("AgreedItemsInSplit", queryResultDocuments);
        List<Document> agreedDocuments = documentProvider.getAgreedDocuments(negotiation, ASSET, COMMUNITY);
        assertThat(agreedDocuments).extracting(Document::getId).containsExactlyInAnyOrder(ID_ASSET_LOWERCASE, ID_ASSET);
    }

    @Test
    public void shouldFetchAllAgreedDebts() {
        givenQueryResult("AgreedItemsInSplit", queryResultDocuments);
        List<Document> agreedDocuments = documentProvider.getAgreedDocuments(negotiation, DEBT, COMMUNITY);
        assertThat(agreedDocuments).extracting(Document::getId).containsOnly(ID_DEBT);
    }


    @Test
    public void shouldFetchSpouse1Assets() {
        givenQueryResult("AgreedItemsAwardedToSIDE1", queryResultDocuments);
        List<Document> agreedDocuments = documentProvider.getAgreedDocuments(negotiation, ASSET, SPOUSE_1);
        assertThat(agreedDocuments).extracting(Document::getId).containsExactlyInAnyOrder(ID_ASSET_LOWERCASE, ID_ASSET);
    }

    @Test
    public void shouldFetchSpouse1Debts() {
        givenQueryResult("AgreedItemsAwardedToSIDE1", queryResultDocuments);
        List<Document> agreedDocuments = documentProvider.getAgreedDocuments(negotiation, DEBT, SPOUSE_1);
        assertThat(agreedDocuments).extracting(Document::getId).containsOnly(ID_DEBT);
    }


    @Test
    public void shouldFetchSpouse2Assets() {
        givenQueryResult("AgreedItemsAwardedToSIDE2", queryResultDocuments);
        List<Document> agreedDocuments = documentProvider.getAgreedDocuments(negotiation, ASSET, SPOUSE_2);
        assertThat(agreedDocuments).extracting(Document::getId).containsExactlyInAnyOrder(ID_ASSET_LOWERCASE, ID_ASSET);
    }

    @Test
    public void shouldFetchSpouse2Debts() {
        givenQueryResult("AgreedItemsAwardedToSIDE2", queryResultDocuments);
        List<Document> agreedDocuments = documentProvider.getAgreedDocuments(negotiation, DEBT, SPOUSE_2);
        assertThat(agreedDocuments).extracting(Document::getId).containsOnly(ID_DEBT);
    }

    private void givenQueryResult(String queryName, List<Document> documents) {
        when(queryFacade.getDocumentsForQuery(queryName, actionContext)).thenReturn(documents);
    }

    private void givenQueryResultDocuments(Document... documents) {
        queryResultDocuments = newArrayList(documents);
    }

    private void givenNegotiationDocument() {
        negotiation = new Document();
        negotiation.setState(DOCUMENT_TYPE.getValue(), NEGOTIATION_DOCUMENT);
    }

    private Document document(String id, ItemType itemType, String documentType) {
        Document document = document(id, documentType);
        document.setState(ITEM_TYPE.getValue(), itemType.name());
        return document;
    }

    private Document document(String id, String itemType, String documentType) {
        Document document = document(id, documentType);
        document.setState(ITEM_TYPE.getValue(), itemType);
        return document;
    }

    private Document document(String id, String documentType) {
        Document document = new Document();
        document.setId(id);
        document.setState(DOCUMENT_TYPE.getValue(), documentType);
        return document;
    }

}