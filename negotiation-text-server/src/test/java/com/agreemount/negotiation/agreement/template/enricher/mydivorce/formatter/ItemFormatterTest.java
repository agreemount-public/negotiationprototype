package com.agreemount.negotiation.agreement.template.enricher.mydivorce.formatter;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.enricher.USDValueFormatter;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;
import java.util.stream.Stream;

import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.COMMUNITY;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.SPOUSE_1;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.ASSET;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.DEBT;
import static com.agreemount.negotiation.bean.enums.Metrics.*;
import static com.agreemount.negotiation.bean.enums.States.ITEM_TYPE;
import static java.lang.String.format;
import static java.util.stream.Collectors.toMap;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class ItemFormatterTest {

    private static final String ID = "1";
    private static final String DESCRIPTION = "description1";
    private static final int VALUE_100 = 100;
    private static final String VALUE_100_USD = "$100.00";
    private static final String CATEGORY = "category1";
    private static final int SIDE1_30_PERCENT = 30;
    private static final int MONTHLY_PAYMENT = 12;
    private static final String MONTHLY_PAYMENT_USD_FORMAT = "$12.00";
    private static final int SIDE1_100_PERCENT = 100;
    private static final int PART_VALUE_30 = 30;
    private static final String PART_VALUE_30_USD_FORMAT = "$30.00";
    private static final int SIDE1_0_PERCENT = 0;
    private static final String COMMENT = "Comment to agree if";

    @InjectMocks
    private ItemFormatter itemFormatter;

    @Spy
    private USDValueFormatter usdValueFormatter;

    @Test(expected = NullPointerException.class)
    public void shouldThrowAnException() throws Exception {
        Document agreedItem = null;
        itemFormatter.itemAsString(agreedItem, COMMUNITY);
    }

    @Test
    public void shouldFormatAssetInTotal() throws Exception {
        Document agreedItem = givenAgreedItem(ID, ASSET, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_30_PERCENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, COMMUNITY);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s", CATEGORY, DESCRIPTION, VALUE_100_USD));
    }

    @Test
    public void shouldFormatDebtInTotal() throws Exception {
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_30_PERCENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, COMMUNITY);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s", CATEGORY, DESCRIPTION, VALUE_100_USD));
    }

    @Test
    public void shouldFormatDebtWithMonthlyPaymentInTotal() throws Exception {
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_30_PERCENT, MONTHLY_PAYMENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, COMMUNITY);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s with monthly payment %s",
                CATEGORY, DESCRIPTION, VALUE_100_USD, MONTHLY_PAYMENT_USD_FORMAT));
    }

    @Test
    public void shouldFormatAssetAwardedToSpouse1InTotal() throws Exception {
        Document agreedItem = givenAgreedItem(ID, ASSET, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_100_PERCENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s is awarded in total", CATEGORY, DESCRIPTION, VALUE_100_USD));
    }

    @Test
    public void shouldFormatAssetPartiallyAwardedToSpouse1() throws Exception {
        Document agreedItem = givenAgreedItem(ID, ASSET, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_30_PERCENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s is awarded in part %d%% which is %s"
                , CATEGORY, DESCRIPTION, VALUE_100_USD, SIDE1_30_PERCENT, PART_VALUE_30_USD_FORMAT));
    }

    @Test
    public void shouldBeEmptyWhenAwardedToSpouseWithZeroPercent() throws Exception {
        Document agreedItem = givenAgreedItem(ID, ASSET, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_0_PERCENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEmpty();
    }

    @Test
    public void shouldFormatDebtWithMonthlyPaymentPartiallyAwardedToSpouse1() throws Exception {
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_30_PERCENT, MONTHLY_PAYMENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s with monthly payment %s is awarded in part %d%% which is %s"
                , CATEGORY, DESCRIPTION, "$"+VALUE_100+".00", MONTHLY_PAYMENT_USD_FORMAT, SIDE1_30_PERCENT, PART_VALUE_30_USD_FORMAT));
    }

    @Test
    public void shouldPartValueBelowHalfBeRoundedHalfUpAndReturnedAsInteger() throws Exception {
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, 14, CATEGORY, SIDE1_30_PERCENT, MONTHLY_PAYMENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s with monthly payment %s is awarded in part %s%% which is %s"
                , CATEGORY, DESCRIPTION, "$14.00", MONTHLY_PAYMENT_USD_FORMAT, SIDE1_30_PERCENT, "$4.00"));
    }

    @Test
    public void shouldPartValueExactlyHalfBeRoundedHalfUpAndReturnedAsInteger() throws Exception {
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, 15, CATEGORY, SIDE1_30_PERCENT, MONTHLY_PAYMENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s with monthly payment %s is awarded in part %d%% which is %s"
                , CATEGORY, DESCRIPTION, "$15.00", MONTHLY_PAYMENT_USD_FORMAT, SIDE1_30_PERCENT, "$5.00"));
    }

    @Test
    public void shouldPartValueOverHalfBeRoundedHalfUpAndReturnedAsInteger() throws Exception {
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, 16, CATEGORY, SIDE1_30_PERCENT, MONTHLY_PAYMENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s with monthly payment %s is awarded in part %d%% which is %s"
                , CATEGORY, DESCRIPTION, "$16.00", MONTHLY_PAYMENT_USD_FORMAT, SIDE1_30_PERCENT, "$5.00"));
    }


    @Test
    public void shouldFormatDebtWithSpouse2CommentPartiallyAwardedToSpouse1() throws Exception {
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_30_PERCENT, COMMENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s is awarded in part %d%% which is %s; spouses agreed also that %s"
                , CATEGORY, DESCRIPTION, VALUE_100_USD, SIDE1_30_PERCENT, PART_VALUE_30_USD_FORMAT, COMMENT));
    }

    @Test
    public void shouldNotDisplayCommentSectionWhenCommentIsBlank() throws Exception {
        String blankComment = "   ";
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_30_PERCENT, blankComment);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s is awarded in part %d%% which is %s"
                , CATEGORY, DESCRIPTION, VALUE_100_USD, SIDE1_30_PERCENT, PART_VALUE_30_USD_FORMAT));
    }

    @Test
    public void shouldNotDisplayCommentSectionWhenSplitPercentEqual100() throws Exception {
        Document agreedItem = givenAgreedItem(ID, DEBT, DESCRIPTION, VALUE_100, CATEGORY, SIDE1_100_PERCENT, COMMENT);
        String itemAsString = itemFormatter.itemAsString(agreedItem, SPOUSE_1);
        assertThat(itemAsString).isEqualToIgnoringCase(format("%s: %s of value %s is awarded in total"
                , CATEGORY, DESCRIPTION, VALUE_100_USD, SIDE1_100_PERCENT, VALUE_100));
    }

    private Document givenAgreedItem(String id, ItemType itemType, String description, int value, String category, int side1Percent) {
        Map<String, Object> metrics = Stream.of(new Object[][]{
                {ITEM_CATEGORY.getValue(), category},
                {ITEM_DESCRIPTION.getValue(), description},
                {ITEM_VALUE.getValue(), value},
                {SPLIT_PERCENT_SIDE1.getValue(), side1Percent},
                {SPLIT_PERCENT_SIDE2.getValue(), 100 - side1Percent}
        }).collect(toMap(data -> (String) data[0], data -> data[1]));

        Document document = new Document();
        document.setId(id);
        document.setMetrics(metrics);
        document.setState(ITEM_TYPE.getValue(), itemType.name().toLowerCase());
        return document;
    }

    private Document givenAgreedItem(String id, ItemType itemType, String description, int value, String category, int side1Percent, int monthlyPayment) {
        Document document = givenAgreedItem(id, itemType, description, value, category, side1Percent);
        document.getMetrics().put(ITEM_MONTHLY_PAYMENT.getValue(), monthlyPayment);
        return document;
    }

    private Document givenAgreedItem(String id, ItemType itemType, String description, int value, String category, int side1Percent, String comment) {
        Document document = givenAgreedItem(id, itemType, description, value, category, side1Percent);
        document.getMetrics().put(SPLIT_DESCRIPTION.getValue(), comment);
        return document;
    }
}