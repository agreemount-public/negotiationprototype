package com.agreemount.negotiation.agreement.template.enricher;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.fixtures.DocumentFixtureUtil;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.util.SideServiceUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class PersonalDataEnricherTest {
    private static final String SLA_UUID = "jvaBszPYVo";
    private static final String USER_EMAIL_SIDE_1 = "ondrej@domena.com";
    private static final String USER_NAME_SIDE_1 = "Ondrej Kovalkow";
    private static final String USER_EMAIL_SIDE_2 = "lena@domena.com";
    private static final String USER_NAME_SIDE_2 = "Lena Kovalkow";

    @Mock
    private SideServiceUtil sideService;

    @InjectMocks
    private PersonalDataEnricher personalDataEnricher;

    private Map<String, Object> placeholderValueMap;
    private Document negotiation;
    private Optional<User> userSide1;
    private Optional<User> userSide2;

    @Before
    public void setUp() throws Exception {
        placeholderValueMap = new HashMap<>();
        negotiation = DocumentFixtureUtil.stubNegotiation(SLA_UUID);
        userSide1 = stubUser(USER_EMAIL_SIDE_1, USER_NAME_SIDE_1);
        userSide2 = stubUser(USER_EMAIL_SIDE_2, USER_NAME_SIDE_2);

    }

    @Test
    public void shouldFillInUsersNameAndLoginWhenSide1IsMappedToHusband() {
        UserRole mySideAsHusband = UserRole.SIDE1;

        when(sideService.getSideForLoggedInUser(negotiation)).thenReturn(mySideAsHusband);
        when(sideService.getUserByRole(negotiation, UserRole.SIDE1)).thenReturn(userSide1);
        when(sideService.getUserByRole(negotiation, UserRole.SIDE2)).thenReturn(userSide2);

        personalDataEnricher.enrich(placeholderValueMap, negotiation);

        //then
        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("${personalData.email.wife}", "lena@domena.com");
        expectedMap.put("${personalData.name.wife}", "Lena Kovalkow");
        expectedMap.put("${personalData.email.husband}", "ondrej@domena.com");
        expectedMap.put("${personalData.name.husband}", "Ondrej Kovalkow");
        assertThat(placeholderValueMap).containsAllEntriesOf(expectedMap);
    }

    @Test
    public void shouldFillInUsersNameAndLoginWhenSide2IsMappedToHusband() {
        UserRole mySideAsHusband = UserRole.SIDE2;

        when(sideService.getSideForLoggedInUser(negotiation)).thenReturn(mySideAsHusband);
        when(sideService.getUserByRole(negotiation, UserRole.SIDE1))
                .thenReturn(userSide1);
        when(sideService.getUserByRole(negotiation, UserRole.SIDE2))
                .thenReturn(userSide2);

        personalDataEnricher.enrich(placeholderValueMap, negotiation);

        //then
        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("${personalData.email.husband}", "lena@domena.com");
        expectedMap.put("${personalData.name.husband}", "Lena Kovalkow");
        expectedMap.put("${personalData.email.wife}", "ondrej@domena.com");
        expectedMap.put("${personalData.name.wife}", "Ondrej Kovalkow");
        assertThat(placeholderValueMap).containsAllEntriesOf(expectedMap);
    }

    @Test
    public void shouldNotFillPlaceholdersWhenUserByRoleIsNotPresent() {
        UserRole mySideAsHusband = UserRole.SIDE1;
        when(sideService.getSideForLoggedInUser(negotiation)).thenReturn(mySideAsHusband);
        when(sideService.getUserByRole(negotiation, UserRole.SIDE1))
                .thenReturn(Optional.ofNullable(null));
        when(sideService.getUserByRole(negotiation, UserRole.SIDE2))
                .thenReturn(Optional.ofNullable(null));

        personalDataEnricher.enrich(placeholderValueMap, negotiation);

        assertThat(placeholderValueMap).isEmpty();
    }

    private Optional<User> stubUser(String email, String name) {
        User user = new Local();
        user.setName(name);
        user.setEmail(email);
        return Optional.of(user);
    }

}