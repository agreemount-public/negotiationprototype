package com.agreemount.negotiation.member;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.auth.strategies.AccessVerifier;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.NotificationDAO;
import com.agreemount.slaneg.db.DocumentOperations;
import org.assertj.core.api.AbstractComparableAssert;
import org.assertj.core.api.IterableAssert;
import org.assertj.core.api.ListAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.*;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PartnerServiceTest {

    public static final String TEAM = "myTeam";
    public static final String DOCUMENT_ID = "123789";
    public static final String SIDE_INVITING = "bob@builder.com";

    public static final String SIDE_OWNER = "john@builder.com";
    public static final String SIDE_SPOUSE = "ann@builder.com";

    public static final String SIDE_OWNER_PARTNER = "john-partner@builder.com";
    public static final String SIDE_OWNER_PARTNER_2 = "john-partner2@builder.com";
    public static final String SIDE_SPOUSE_PARTNER = "ann-partner@builder.com";

    public static final String SIDE_NOT_CONFIRMED = "thomas@builder.com";
    public static final String SIDE_INCORRECT_EMAIL = "SIDE_INCORRECT_EMAIL";
    public static final boolean REGISTERED = true;
    public static final boolean NOT_REGISTERED = false;
    public static final boolean PARTNER = true;
    public static final boolean NOT_PARTNER = false;
    public static final boolean FREE_ACCESS = true;
    public static final boolean FULL_ACCESS = false;

    @Mock
    private ITeamDAO teamDAO;

    @Mock
    private IUserDAO userDAO;

    @Mock
    private NotificationDAO notificationDAO;

    @Mock
    private DocumentOperations documentOperations;

    @Mock
    private AccessVerifier verifier;

    @InjectMocks
    private PartnerService partnerService;

    private List<User2Team> users;

    @Before
    public void setUp() {
        when(userDAO.getByEmail(SIDE_OWNER)).thenReturn(new Local());
        when(userDAO.getByEmail(SIDE_SPOUSE)).thenReturn(new Local());
        when(userDAO.getByEmail(SIDE_OWNER_PARTNER)).thenReturn(new Local());
        when(userDAO.getByEmail(SIDE_SPOUSE_PARTNER)).thenReturn(new Local());
        when(userDAO.getByEmail(SIDE_INVITING)).thenReturn(null);
        when(userDAO.getByEmail(SIDE_NOT_CONFIRMED)).thenReturn(null);
        givenAccess(FULL_ACCESS);
    }

    @Test
    public void shouldInvitePartner() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM, side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)));

        whenInvitePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);

        thenTeamDaoExecuted().addMemberToTeam(partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, NOT_REGISTERED), TEAM);
    }

    @Test
    public void shouldSendNotificationWhenPartnerHasBeenInvited() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM, side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)));

        whenInvitePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);

        thenNotificationType().isEqualTo(NotificationType.ADD_PARTNER);
        thenNotificationParameters().containsOnly(
                new Notification.Parameter(REQUESTOR.getValue(), SIDE_OWNER),
                new Notification.Parameter(RECEIVER.getValue(), SIDE_OWNER_PARTNER),
                new Notification.Parameter(TITLE.getValue(), "Invitation to e-negotiation service"),
                new Notification.Parameter("documentId", DOCUMENT_ID)
        );
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldThrowExceptionWhenRequestorNotFound() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM, side(SIDE_SPOUSE, UserRole.SIDE1, REGISTERED)));

        whenInvitePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);
    }

    @Test(expected = DuplicateInvitationException.class)
    public void shouldThrowExceptionWhileTryingToInviteRegisteredPartner() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                side(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED)
        ));

        whenInvitePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);
    }

    @Test(expected = EmailAddressException.class)
    public void shouldThrowExceptionWhenIncorrectPartnerEmailGiven() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM, side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)));

        whenInvitePartner(SIDE_OWNER, SIDE_INCORRECT_EMAIL, DOCUMENT_ID);
    }


    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenPartnerTriesToInvitePartner() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED)
        ));

        whenInvitePartner(SIDE_OWNER_PARTNER, SIDE_OWNER_PARTNER_2, DOCUMENT_ID);
    }

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhenInviterHasNoAccessToInvite() {
        givenAccess(FREE_ACCESS);
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM, side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)));

        whenInvitePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);
    }

    @Test
    public void shouldReturnListOfPartners() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                side(SIDE_SPOUSE, UserRole.SIDE2, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER_2, UserRole.SIDE1, REGISTERED),
                partner(SIDE_SPOUSE_PARTNER, UserRole.SIDE2, REGISTERED)
        ));

        whenFetchPartners(SIDE_OWNER, DOCUMENT_ID);

        thenSides().containsExactly(
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER_2, UserRole.SIDE1, REGISTERED)
        );
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenPartnerTiresToFetchListOfPartners() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                side(SIDE_SPOUSE, UserRole.SIDE2, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER_2, UserRole.SIDE1, REGISTERED),
                partner(SIDE_SPOUSE_PARTNER, UserRole.SIDE2, REGISTERED)
        ));

        whenFetchPartners(SIDE_OWNER_PARTNER, DOCUMENT_ID);
    }

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhileFetchingResultWhenNoAccess() {
        givenAccess(FREE_ACCESS);
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM, side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)));

        whenFetchPartners(SIDE_OWNER, DOCUMENT_ID);
    }

    @Test
    public void shouldDeletePartner() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED)
        ));

        whenDeletePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);

        thenExecuted().deleteByLogin(SIDE_OWNER_PARTNER);
    }

    @Test
    public void shouldSendNotificationWhenPartnerHasBeenDeleted() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED)
        ));

        whenDeletePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);

        thenNotificationType().isEqualTo(NotificationType.DELETE_PARTNER);
        thenNotificationParameters().containsOnly(
                new Notification.Parameter(REQUESTOR.getValue(), SIDE_OWNER),
                new Notification.Parameter(RECEIVER.getValue(), SIDE_OWNER_PARTNER),
                new Notification.Parameter(TITLE.getValue(), "Invitation to e-negotiation service"),
                new Notification.Parameter("documentId", DOCUMENT_ID)
        );
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenPartnerToDeleteNotFound() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)
        ));

        whenDeletePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);
    }

    @Test(expected = AuthException.class)
    public void shouldThrowExceptionWhileDeletingPartnerWhenNoAccess() {
        givenAccess(FREE_ACCESS);
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED)
        ));

        whenDeletePartner(SIDE_OWNER, SIDE_OWNER_PARTNER, DOCUMENT_ID);
    }

    private void givenAccess(boolean b) {
        when(verifier.hasUserFreeAccess(any())).thenReturn(b);
    }

    private void whenDeletePartner(String requestor, String partner, String documentId) {
        partnerService.deletePartner(requestor, partner, documentId);
    }

    private ListAssert<User2Team> thenSides() {
        return assertThat(users);
    }

    private void whenFetchPartners(String sideEmail, String documentId) {
        users = partnerService.fetchPartners(sideEmail, documentId);
    }

    private Document document(String documentId, String teamName) {
        Document document = new Document();
        document.setTeam(teamName);
        document.setId(documentId);
        return document;
    }

    private User2Team member(String login, UserRole role, boolean isUserRegistered, boolean isPartner) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(role);
        user2Team.setUserRegistered(isUserRegistered);
        user2Team.setPartner(isPartner);
        return user2Team;
    }

    private User2Team side(String login, UserRole role, boolean isUserRegistered) {
        return member(login, role, isUserRegistered, NOT_PARTNER);
    }

    private User2Team partner(String login, UserRole role, boolean isUserRegistered) {
        return member(login, role, isUserRegistered, PARTNER);
    }

    private Team team(String name) {
        Team team = new Team();
        team.setName(name);
        return team;
    }

    private void whenInvitePartner(String requestor, String invitedPartner, String documentId) {
        partnerService.invitePartner(requestor, invitedPartner, documentId);
    }

    private ITeamDAO thenTeamDaoExecuted() {
        return thenExecuted();
    }

    private void givenTeam(Team team) {
        when(teamDAO.getTeamByName(team.getName())).thenReturn(team);
    }

    private void givenDocument(Document document) {
        when(documentOperations.getDocument(anyString())).thenReturn(document);
    }

    private Team team(String teamName, User2Team... user2Teams) {
        Team team = team(teamName);
        team.setUsers2Team(Arrays.asList(user2Teams));
        return team;
    }

    private Notification thenNotification() {
        ArgumentCaptor<Notification> captor = ArgumentCaptor.forClass(Notification.class);
        verify(notificationDAO).save(captor.capture());
        return captor.getValue();
    }

    private IterableAssert<Notification.Parameter> thenNotificationParameters() {
        return assertThat(thenNotification().getParameters());
    }

    private AbstractComparableAssert<?, NotificationType> thenNotificationType() {
        return assertThat(thenNotification().getNotificationType());
    }

    private ITeamDAO thenExecuted() {
        return verify(teamDAO);
    }
}
