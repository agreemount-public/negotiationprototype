package com.agreemount.negotiation.member;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.INotificationDAO;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserLogDAO;
import com.agreemount.slaneg.db.DocumentOperations;
import org.assertj.core.api.AbstractComparableAssert;
import org.assertj.core.api.IterableAssert;
import org.assertj.core.api.ListAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.RECEIVER;
import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.TITLE;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SideServiceTest {
    public static final String TEAM = "myTeam";
    public static final String DOCUMENT_ID = "123789";
    public static final String SIDE_INVITING = "bob@builder.com";

    public static final String SIDE_OWNER = "john@builder.com";
    public static final String SIDE_SPOUSE = "ann@builder.com";

    public static final String SIDE_OWNER_PARTNER = "john-partner@builder.com";
    public static final String SIDE_SPOUSE_PARTNER = "ann-partner@builder.com";

    public static final String SIDE_NOT_CONFIRMED = "thomas@builder.com";
    public static final String SIDE_INCORRECT_EMAIL = "SIDE_INCORRECT_EMAIL";
    public static final boolean REGISTERED = true;
    public static final boolean NOT_REGISTERED = false;
    public static final boolean PARTNER = true;
    public static final boolean NOT_PARTNER = false;

    @Mock
    private ITeamDAO teamDAO;

    @Mock
    private IUserDAO userDao;

    @Mock
    private INotificationDAO notificationDAO;

    @Mock
    private DocumentOperations documentOperations;

    @Mock
    private UserLogDAO userLogDao;

    @InjectMocks
    private SideService sideService;

    private List<User2Team> users;

    @Before
    public void setUp() {
        when(userDao.getByEmail(SIDE_OWNER)).thenReturn(new Local());
        when(userDao.getByEmail(SIDE_SPOUSE)).thenReturn(new Local());
        when(userDao.getByEmail(SIDE_OWNER_PARTNER)).thenReturn(new Local());
        when(userDao.getByEmail(SIDE_SPOUSE_PARTNER)).thenReturn(new Local());
        when(userDao.getByEmail(SIDE_INVITING)).thenReturn(null);
        when(userDao.getByEmail(SIDE_NOT_CONFIRMED)).thenReturn(null);
    }

    @Test
    public void shouldInviteSide() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)
        ));

        whenInviteSide(SIDE_OWNER, SIDE_INVITING, DOCUMENT_ID);

        thenTeamDaoExecuted().addMemberToTeam(side(SIDE_INVITING, UserRole.SIDE2, NOT_REGISTERED), TEAM);
    }

    @Test
    public void shouldSendNotificationWhenSideHasBeenInvited() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM, side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)));

        whenInviteSide(SIDE_OWNER, SIDE_INVITING, DOCUMENT_ID);

        thenNotificationType().isEqualTo(NotificationType.ADD_SIDE);
        thenNotificationParameters().containsOnly(
                new Notification.Parameter(RECEIVER.getValue(), SIDE_INVITING),
                new Notification.Parameter(TITLE.getValue(), "Invitation to e-negotiation service"),
                new Notification.Parameter("documentId", DOCUMENT_ID)
        );
    }

    @Test
    public void shouldIgnorePartnersWhenInvitingSides() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_SPOUSE_PARTNER, UserRole.SIDE1, REGISTERED)
        ));

        whenInviteSide(SIDE_OWNER, SIDE_INVITING, DOCUMENT_ID);

        thenTeamDaoExecuted().addMemberToTeam(side(SIDE_INVITING, UserRole.SIDE2, NOT_REGISTERED), TEAM);
    }

    @Test
    public void shouldAllowToChangeSideWhenInvitedSideHasNotBeenRegisteredYet() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                side(SIDE_NOT_CONFIRMED, UserRole.SIDE2, NOT_REGISTERED)
        ));

        whenInviteSide(SIDE_OWNER, SIDE_INVITING, DOCUMENT_ID);

        thenTeamDaoExecuted().addMemberToTeam(side(SIDE_INVITING, UserRole.SIDE2, NOT_REGISTERED), TEAM);
    }

    @Test(expected = TooManySidesException.class)
    public void shouldThrowExceptionWhenTryingToInviteMoreThanTwoRegisteredSides() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                side(SIDE_SPOUSE, UserRole.SIDE2, NOT_REGISTERED)
        ));

        whenInviteSide(SIDE_OWNER, SIDE_INVITING, DOCUMENT_ID);
    }

    @Test(expected = DuplicateInvitationException.class)
    public void shouldThrowExceptionWhileTryingToInviteYourself() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)
        ));

        whenInviteSide(SIDE_OWNER, SIDE_OWNER, DOCUMENT_ID);
    }

    @Test(expected = EmailAddressException.class)
    public void shouldThrowExceptionWhenIncorrectSideEmailGiven() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM, side(SIDE_OWNER, UserRole.SIDE1, REGISTERED)));

        whenInviteSide(SIDE_OWNER, SIDE_INCORRECT_EMAIL, DOCUMENT_ID);
    }

    @Test
    public void shouldReturnListOfSides() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                side(SIDE_SPOUSE, UserRole.SIDE2, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE2, REGISTERED)
        ));

        whenFetchSides(DOCUMENT_ID);

        thenSides().containsExactly(
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                side(SIDE_SPOUSE, UserRole.SIDE2, REGISTERED)
        );
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenPartnerTriesToInviteSide() {
        givenDocument(document(DOCUMENT_ID, TEAM));
        givenTeam(team(TEAM,
                side(SIDE_OWNER, UserRole.SIDE1, REGISTERED),
                partner(SIDE_OWNER_PARTNER, UserRole.SIDE1, REGISTERED)
        ));

        whenInviteSide(SIDE_OWNER_PARTNER, SIDE_INVITING, DOCUMENT_ID);
    }

    @Test
    public void shouldAddFullAccessToInviteeIfInviteeExistsAndInviterHasFullAccess() {
        final String inviterEmail = "inver@email.com";
        final String inviteeEmail = "invtee@email.com";

        User inviter = mock(User.class);
        when(inviter.isHasFreeAccess()).thenReturn(true);
        when(inviter.getEmail()).thenReturn(inviterEmail);
        when(userDao.getByEmail(inviterEmail)).thenReturn(inviter);

        User invitee = mock(User.class);
        when(invitee.getEmail()).thenReturn(inviteeEmail);
        when(userDao.getByEmail(inviteeEmail)).thenReturn(invitee);

        final String documentId = "documentId";
        final String teamName = "team";

        Document document = mock(Document.class);
        when(document.getTeam()).thenReturn(teamName);
        when(documentOperations.getDocument(documentId)).thenReturn(document);

        Team team = mock(Team.class);
        User2Team user2Team = mock(User2Team.class);
        when(user2Team.getLogin()).thenReturn(inviterEmail);

        when(team.getUsers2Team()).thenReturn(Arrays.asList(user2Team));

        when(teamDAO.getTeamByName(teamName)).thenReturn(team);

        sideService.inviteSide(inviterEmail, inviteeEmail, documentId);

        verify(invitee).setHasFreeAccess(true);


        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userDao).save(userCaptor.capture());
        verify(userDao, times(1)).save(any(User.class));
        verify(userLogDao).insertNewLog(inviteeEmail, "give user full access as was invited by user with full access - " + inviterEmail);

        assertThat(userCaptor.getValue().getEmail()).isEqualTo(inviteeEmail);
    }

    @Test
    public void shouldNotAddFullAccessToInviteeIfInviteeExistsButInviterDoesNotHaveFullAccess() {
        final String inviterEmail = "inver@email.com";
        final String inviteeEmail = "invtee@email.com";

        User inviter = mock(User.class);
        when(inviter.isHasFreeAccess()).thenReturn(false);
        when(inviter.getEmail()).thenReturn(inviterEmail);
        when(userDao.getByEmail(inviterEmail)).thenReturn(inviter);

        User invitee = mock(User.class);
        when(invitee.getEmail()).thenReturn(inviteeEmail);
        when(userDao.getByEmail(inviteeEmail)).thenReturn(invitee);

        final String documentId = "documentId";
        final String teamName = "team";

        Document document = mock(Document.class);
        when(document.getTeam()).thenReturn(teamName);
        when(documentOperations.getDocument(documentId)).thenReturn(document);

        Team team = mock(Team.class);
        User2Team user2Team = mock(User2Team.class);
        when(user2Team.getLogin()).thenReturn(inviterEmail);

        when(team.getUsers2Team()).thenReturn(Arrays.asList(user2Team));

        when(teamDAO.getTeamByName(teamName)).thenReturn(team);

        sideService.inviteSide(inviterEmail, inviteeEmail, documentId);

        verify(userDao, times(0)).save(any(User.class));
        verify(userLogDao, times(0)).insertNewLog(anyString(), anyString());
    }

    @Test
    public void shouldNotAddFullAccessToInviteeIfInviteeDoesNotExist() {
        final String inviterEmail = "inver@email.com";
        final String inviteeEmail = "invtee@email.com";

        User inviter = mock(User.class);
        when(inviter.isHasFreeAccess()).thenReturn(false);
        when(inviter.getEmail()).thenReturn(inviterEmail);
        when(userDao.getByEmail(inviterEmail)).thenReturn(inviter);

        when(userDao.getByEmail(inviteeEmail)).thenReturn(null);

        final String documentId = "documentId";
        final String teamName = "team";

        Document document = mock(Document.class);
        when(document.getTeam()).thenReturn(teamName);
        when(documentOperations.getDocument(documentId)).thenReturn(document);

        Team team = mock(Team.class);
        User2Team user2Team = mock(User2Team.class);
        when(user2Team.getLogin()).thenReturn(inviterEmail);

        when(team.getUsers2Team()).thenReturn(Arrays.asList(user2Team));

        when(teamDAO.getTeamByName(teamName)).thenReturn(team);

        sideService.inviteSide(inviterEmail, inviteeEmail, documentId);

        verify(userDao, times(0)).save(any(User.class));
        verify(userLogDao, times(0)).insertNewLog(anyString(), anyString());
    }

    @Test
    public void shouldFindEmailOfOtherSide() throws Exception {
        Team team = mock(Team.class);
        User2Team user2Team1 = mock(User2Team.class);
        when(user2Team1.getLogin()).thenReturn(SIDE_OWNER);
        when(user2Team1.getRole()).thenReturn(UserRole.SIDE1);

        User2Team user2Team2 = mock(User2Team.class);
        when(user2Team2.getLogin()).thenReturn(SIDE_SPOUSE);
        when(user2Team2.getRole()).thenReturn(UserRole.SIDE2);

        when(team.getUsers2Team()).thenReturn(Arrays.asList(user2Team1, user2Team2));

        List<String> otherSide = sideService.getOtherSideEmail(team, SIDE_OWNER);
        assertThat(otherSide).containsExactly(SIDE_SPOUSE);
    }


    private ListAssert<User2Team> thenSides() {
        return assertThat(users);
    }

    private void whenFetchSides(String documentId) {
        users = sideService.fetchSides(documentId);
    }

    private Document document(String documentId, String teamName) {
        Document document = new Document();
        document.setTeam(teamName);
        document.setId(documentId);
        return document;
    }


    private User2Team member(String login, UserRole role, boolean isUserRegistered, boolean isPartner) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(role);
        user2Team.setUserRegistered(isUserRegistered);
        user2Team.setPartner(isPartner);
        return user2Team;
    }

    private User2Team side(String login, UserRole role, boolean isUserRegistered) {
        return member(login, role, isUserRegistered, NOT_PARTNER);
    }

    private User2Team partner(String login, UserRole role, boolean isUserRegistered) {
        return member(login, role, isUserRegistered, PARTNER);
    }

    private Team team(String name) {
        Team team = new Team();
        team.setName(name);
        return team;
    }

    private void whenInviteSide(String requestor, String invitedSide, String documentId) {
        sideService.inviteSide(requestor, invitedSide, documentId);
    }

    private ITeamDAO thenTeamDaoExecuted() {
        return verify(teamDAO, times(1));
    }

    private void givenTeam(Team team) {
        when(teamDAO.getTeamByName(team.getName())).thenReturn(team);
    }

    private void givenDocument(Document document) {
        when(documentOperations.getDocument(anyString())).thenReturn(document);
    }

    private Team team(String teamName, User2Team... user2Teams) {
        Team team = team(teamName);
        team.setUsers2Team(Arrays.asList(user2Teams));
        return team;
    }

    private Notification thenNotification() {
        ArgumentCaptor<Notification> captor = ArgumentCaptor.forClass(Notification.class);
        verify(notificationDAO, times(1)).save(captor.capture());
        return captor.getValue();
    }

    private IterableAssert<Notification.Parameter> thenNotificationParameters() {
        return assertThat(thenNotification().getParameters());
    }

    private AbstractComparableAssert<?, NotificationType> thenNotificationType() {
        return assertThat(thenNotification().getNotificationType());
    }
}
