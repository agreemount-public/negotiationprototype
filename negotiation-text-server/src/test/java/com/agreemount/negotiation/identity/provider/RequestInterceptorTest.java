package com.agreemount.negotiation.identity.provider;

import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.TeamMember;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.dao.impl.TeamDAO;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;



@RunWith(MockitoJUnitRunner.class)
public class RequestInterceptorTest {

    public static final String MEMBER_1 = "bob@builder.com";
    public static final String MEMBER_2 = "john@snow.com";
    public static final String TEAM_1 = "TEAM_1";
    public static final String TEAM_2 = "TEAM_2";
    public static final String TEAM_3 = "TEAM_3";

    @InjectMocks
    private RequestInterceptor requestInterceptor;

    @Mock
    private RequestIdentityProvider identityProvider;

    @Mock
    private TeamDAO teamDAO;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private Object handler;

    @Test
    public void shouldSetTeamMembersWithProperRoles() throws Exception {
        // given
        ArrayList<Team> daoTeams = Lists.newArrayList(
                getTeamWithMembers(TEAM_1, Lists.newArrayList(getMember(MEMBER_1, UserRole.SIDE1), getMember(MEMBER_2, UserRole.SIDE2))),
                getTeamWithMembers(TEAM_2, Lists.newArrayList(getMember(MEMBER_2, UserRole.SIDE1), getMember(MEMBER_1, UserRole.SIDE2))),
                getTeamWithMembers(TEAM_3, Lists.newArrayList(getMember(MEMBER_1, UserRole.SIDE1))));

        doNothing().when(identityProvider).setIdentity(any());
        when(request.getHeader("x-user-email")).thenReturn(MEMBER_1);
        when(teamDAO.getUserTeams(MEMBER_1)).thenReturn(daoTeams);

        // when
        requestInterceptor.preHandle(request, response, handler);
        ArgumentCaptor<Identity> identityCaptor = ArgumentCaptor.forClass(Identity.class);

        // then
        verify(identityProvider, times(1)).setIdentity(identityCaptor.capture());
        TeamMember  tm1 = new TeamMember();
        tm1.setRole(UserRole.SIDE1.getValue());
        tm1.setTeam(TEAM_1);
        TeamMember  tm2 = new TeamMember();
        tm2.setRole(UserRole.SIDE2.getValue());
        tm2.setTeam(TEAM_2);
        TeamMember  tm3 = new TeamMember();
        tm3.setRole(UserRole.SIDE1.getValue());
        tm3.setTeam(TEAM_3);

        assertThat(identityCaptor.getValue().getTeamMembers()).containsExactly(
                tm1, tm2, tm3);
    }


    private User2Team getMember(String login, UserRole role) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(role);
        return user2Team;
    }

    private Team getTeam(String name) {
        Team team = new Team();
        team.setName(name);
        return team;
    }

    private Team getTeamWithMembers(String name, List<User2Team> users) {
        Team team = getTeam(name);
        team.setUsers2Team(users);
        return team;
    }
}