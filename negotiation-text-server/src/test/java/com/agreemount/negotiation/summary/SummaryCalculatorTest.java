package com.agreemount.negotiation.summary;

import com.agreemount.bean.document.Document;
import org.apache.commons.collections.map.HashedMap;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

public class SummaryCalculatorTest {

    private SummaryCalculator summaryCalculator = new SummaryCalculator();

    @Test
    public void buildSummary() throws Exception {

        Summary summary = summaryCalculator.buildSummary(getItems());

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(summary.getTotalToDivide().getTotal()).isEqualTo(3200);
        softly.assertThat(summary.getTotalToDivide().getAssets()).isEqualTo(4000);
        softly.assertThat(summary.getTotalToDivide().getDebts()).isEqualTo(800);
        softly.assertThat(summary.getTotalToDivide().getPercentage()).isEqualTo(BigDecimal.valueOf(100));

        softly.assertThat(summary.getSide1().getTotal()).isEqualTo(800);
        softly.assertThat(summary.getSide1().getAssets()).isEqualTo(1200);
        softly.assertThat(summary.getSide1().getDebts()).isEqualTo(400);
        softly.assertThat(summary.getSide1().getPercentage()).isEqualTo(BigDecimal.valueOf(25));

        softly.assertThat(summary.getSide2().getTotal()).isEqualTo(2400);
        softly.assertThat(summary.getSide2().getAssets()).isEqualTo(2800);
        softly.assertThat(summary.getSide2().getDebts()).isEqualTo(400);
        softly.assertThat(summary.getSide2().getPercentage()).isEqualTo(BigDecimal.valueOf(75));

        softly.assertAll();
    }

    @Test
    public void shouldPercentageBeNullWhenOutsideOfRange0to100() throws Exception {
        List<Document> items = Arrays.asList(
                getItem(SummaryCalculator.ASSET, "SIDE1", null, 1000, null, null), //   +1000 / --
                getItem(SummaryCalculator.DEBT, "SIDE1", null, 200, null, null),  //    -200 / --
                getItem(SummaryCalculator.ASSET, "SIDE2", null, 100, null, null),//      -- / +100
                getItem(SummaryCalculator.DEBT, "SIDE2", null, 20000, null, null),//    -- / -20000
                getItem(SummaryCalculator.ASSET, "SPLIT", null, 1000, 20, 80),//        +200 / +800
                getItem(SummaryCalculator.DEBT, "SPLIT", null, 400, 50, 50)//           -200 / -200

        );

        Summary summary = summaryCalculator.buildSummary(items);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(summary.getTotalToDivide().getTotal()).isEqualTo(-18500);
        softly.assertThat(summary.getTotalToDivide().getAssets()).isEqualTo(2100);
        softly.assertThat(summary.getTotalToDivide().getDebts()).isEqualTo(20600);
        softly.assertThat(summary.getTotalToDivide().getPercentage()).isEqualTo(BigDecimal.valueOf(100));

        softly.assertThat(summary.getSide1().getTotal()).isEqualTo(800);
        softly.assertThat(summary.getSide1().getAssets()).isEqualTo(1200);
        softly.assertThat(summary.getSide1().getDebts()).isEqualTo(400);
        softly.assertThat(summary.getSide1().getPercentage()).isNull();//-4%

        softly.assertThat(summary.getSide2().getTotal()).isEqualTo(-19300);
        softly.assertThat(summary.getSide2().getAssets()).isEqualTo(900);
        softly.assertThat(summary.getSide2().getDebts()).isEqualTo(20200);
        softly.assertThat(summary.getSide2().getPercentage()).isNull();//104%
        softly.assertAll();
    }


    private List<Document> getItems() {
        return Arrays.asList(
                getItem(SummaryCalculator.ASSET, "SIDE1", null, 1000, null, null), //   +1000 / --
                getItem(SummaryCalculator.DEBT, "SIDE1", null, 200, null, null),  //    -200 / --
                getItem(SummaryCalculator.ASSET, "SIDE2", null, 2000, null, null),//    -- / +2000
                getItem(SummaryCalculator.DEBT, "SIDE2", null, 200, null, null),//      -- / -200
                getItem(SummaryCalculator.ASSET, "SPLIT", null, 1000, 20, 80),//      +200 / +800
                getItem(SummaryCalculator.DEBT, "SPLIT", null, 400, 50, 50),//        -200 / -200

                getItem(SummaryCalculator.ASSET, "SIDE1", true, 4000, null, null), //ignore
                getItem(SummaryCalculator.DEBT, "SPLIT", true, 6000, 75, 25) //ignore
        );
    }


    private Document getItem(String itemType, String itemOwner, Boolean itemIsSeparate, Integer itemValue, Integer splitPercentSIDE1, Integer splitPercentSIDE2) {
        Document item = new Document();

        Map<String,String> states = new HashMap<>();

        if (Objects.nonNull(itemType)) {
            states.put("itemType", itemType);
        }

        if (Objects.nonNull(itemOwner)) {
            states.put("itemOwner", itemOwner);
        }

        item.setStates(states);


        Map<String,Object> metrics = new HashedMap();

        if (Objects.nonNull(itemIsSeparate)) {
            metrics.put("itemIsSeparate", itemIsSeparate);
        }

        if (Objects.nonNull(itemValue)) {
            metrics.put("itemValue", itemValue);
        }

        if (Objects.nonNull(splitPercentSIDE1)) {
            metrics.put("splitPercentSIDE1", splitPercentSIDE1);
        }

        if (Objects.nonNull(splitPercentSIDE1)) {
            metrics.put("splitPercentSIDE2", splitPercentSIDE2);
        }

        item.setMetrics(metrics);

        return item;
    }

}