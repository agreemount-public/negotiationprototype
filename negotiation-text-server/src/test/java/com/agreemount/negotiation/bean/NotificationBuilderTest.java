package com.agreemount.negotiation.bean;

import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class NotificationBuilderTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void givenNotificationWithRequiredParamsShouldCreateNotification() {
        NotificationBuilder notificationBuilder = new NotificationBuilder();

        Notification notifi = notificationBuilder
                .addParameter(new Notification.Parameter("Another", "second arh"))
                .addParameter(new Notification.Parameter("Sedcond", "123"))
                .setNotificationStatus(NotificationStatus.FAILED)
                .setNotificationType(NotificationType.ADD_SIDE)
                .build();


        assertEquals(NotificationStatus.FAILED, notifi.getNotificationStatus());
        assertEquals(NotificationType.ADD_SIDE, notifi.getNotificationType());
        assertEquals(2, notifi.getParameters().size());
    }

    @Test
    public void shouldReturnValuesListForParameterWithMultipleEntries() {
        NotificationBuilder notificationBuilder = new NotificationBuilder();

        Notification notifi = notificationBuilder
                .addParameter(new Notification.Parameter("listOfParams", Lists.newArrayList("first", "second", "third")))
                .setNotificationStatus(NotificationStatus.PROCESSED)
                .setNotificationType(NotificationType.ADD_SIDE)
                .build();


        assertEquals(NotificationStatus.PROCESSED, notifi.getNotificationStatus());
        assertEquals(NotificationType.ADD_SIDE, notifi.getNotificationType());
        assertEquals(1, notifi.getParameters().size());

        assertThat(notifi.getParameters(),
                hasItems(new Notification.Parameter("listOfParams", Lists.newArrayList("first", "second", "third"))));
    }


    @Test
    public void givenNotificationWithoutTypeShouldThrowException() {
        NotificationBuilder notificationBuilder = new NotificationBuilder();

        thrown.expect(NullPointerException.class);
        thrown.expectMessage(containsString("notification type has to be set"));

        notificationBuilder
                .setNotificationStatus(NotificationStatus.FAILED)
                .build()
        ;
    }

    @Test
    public void givenNotificationWithoutStatusShouldSetNewStatus() {
        NotificationBuilder notificationBuilder = new NotificationBuilder();

        Notification notification = notificationBuilder
                .setNotificationType(NotificationType.ADD_SIDE)
                .build();

        assertEquals(NotificationStatus.NEW, notification.getNotificationStatus());
    }

}
