package com.agreemount.negotiation.util;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.logic.ITeamLogic;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class SideServiceUtilTest {
    private static final String LOGIN = "dd.kopec@gmail.com";
    private static final String TEAM_NAME = "0b7team-name-983";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private IdentityProvider identityProvider;

    @Mock
    private ITeamLogic teamLogic;

    @Mock
    private IUserDAO userDAO;

    @InjectMocks
    private SideServiceUtil sideServiceUtil;

    @Test
    public void shouldReturnSide() {
        //given
        UserRole side = UserRole.SIDE1;
        when(teamLogic.userRoleInTeam(argThat(equalTo(TEAM_NAME)), argThat(equalTo(LOGIN)))).thenReturn(Optional.of(side));
        when(identityProvider.getIdentity()).thenReturn(stubIdentity(LOGIN));

        //when
        UserRole actualSide = sideServiceUtil.getSideForLoggedInUser(stubDocument(TEAM_NAME));

        //then
        assertEquals(side, actualSide);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenNoUserInTeamFoundForGivenLoginAndTeamName() {
        //given
        Document document = stubDocument(TEAM_NAME);
        when(teamLogic.userRoleInTeam(argThat(equalTo(TEAM_NAME)), argThat(equalTo(LOGIN))))
                .thenReturn(Optional.empty());
        when(identityProvider.getIdentity()).thenReturn(stubIdentity(LOGIN));

        //then
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Can not get Side for given document " + document.toString() + " and user " + LOGIN);

        //when
        sideServiceUtil.getSideForLoggedInUser(document);
    }

    @Test
    public void shouldReturnUserByGivenRole() {
        //given
        String email = "user@domena.com";
        String name = "Firs and Last Name";
        Document document = stubDocument(TEAM_NAME);

        User user = stubUser(email, name);
        when(teamLogic.getUserWithRole(argThat(equalTo(TEAM_NAME)), argThat(equalTo(UserRole.SIDE1))))
                .thenReturn(Optional.ofNullable(stubUser2Team(user, UserRole.SIDE1)));
        when(userDAO.getByEmail(email)).thenReturn(user);


        //when
        Optional<User> mayBeActualUser = sideServiceUtil.getUserByRole(document, UserRole.SIDE1);

        //then
        assertThat(mayBeActualUser.get()).hasFieldOrPropertyWithValue("email", email);
        assertThat(mayBeActualUser.get()).hasFieldOrPropertyWithValue("name", name);
    }

    @Test
    public void shouldNotReturnUserWhenDocumentHasNoSuchUserRole() {
        //given
        UserRole side = UserRole.SIDE1;
        Document document = stubDocument(TEAM_NAME);

        when(teamLogic.getUserWithRole(argThat(equalTo(TEAM_NAME)), argThat(equalTo(UserRole.SIDE1))))
                .thenReturn(Optional.empty());

        //when
        Optional<User> mayBeUser = sideServiceUtil.getUserByRole(document, side);

        //then
        assertThat(mayBeUser).isNotPresent();
    }

    @Test
    public void shouldNotReturnUserWhenDocumentHasSuchUserRoleButCorrespondingUserCanNotBeFound() {
        //given
        UserRole side = UserRole.SIDE1;
        String email = "user@domena.com";
        String name = "Firs and Last Name";
        User user = stubUser(email, name);

        Document document = stubDocument(TEAM_NAME);
        when(teamLogic.getUserWithRole(argThat(equalTo(TEAM_NAME)), argThat(equalTo(UserRole.SIDE1))))
                .thenReturn(Optional.of(stubUser2Team(user, UserRole.SIDE1)));

        when(userDAO.getByEmail(email)).thenReturn(null);

        //when
        Optional<User> mayBeUser = sideServiceUtil.getUserByRole(document, side);

        assertThat(mayBeUser).isNotPresent();
    }

    @Test
    public void shouldThrowNPEwhenNoIdentityProvider() {
        //given
        when(identityProvider.getIdentity()).thenReturn(null);

        //then
        thrown.expect(NullPointerException.class);

        //when
        sideServiceUtil.getSideForLoggedInUser(stubDocument(TEAM_NAME));

    }

    /**
     * @Todo: te poniższe stuby wyciągnąć do jednej klasy z SampleBuilderami, na pewno podobnych duplikacji kodu jest sporo
     */
    private Document stubDocument(final String teamName) {
        Document document = new Document();
        document.setTeam(teamName);
        return document;
    }

    private Identity stubIdentity(final String login) {
        Identity identity = new Identity();
        identity.setLogin(login);
        return identity;
    }

    private User stubUser(String login, String name) {
        User user = new Local();
        user.setEmail(login);
        user.setName(name);
        return user;
    }

    private User2Team stubUser2Team(User user, UserRole userRole) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(user.getEmail());
        user2Team.setUserRegistered(true);
        user2Team.setRole(userRole);
        return user2Team;
    }
}