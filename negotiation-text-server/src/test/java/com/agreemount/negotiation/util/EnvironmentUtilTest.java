package com.agreemount.negotiation.util;

import org.assertj.core.api.AbstractCharSequenceAssert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class EnvironmentUtilTest {

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @InjectMocks
    private EnvironmentUtil environmentUtil;
    private String host;

    @Test
    public void shouldReturnLocalhostWhenEnvironmentNotSet() {
        givenDefaultHosts("http://localhost", "http://development", "http://production");

        whenGetHost();

        thenHost().isEqualTo("http://localhost");
    }

    @Test
    public void shouldReturnDevelopmentHost() {
        givenEnvironment("dev");
        givenDefaultHosts("http://localhost", "http://development", "http://production");

        whenGetHost();

        thenHost().isEqualTo("http://development");
    }

    @Test
    public void shouldReturnProductionHost() {
        givenEnvironment("prod");
        givenDefaultHosts("http://localhost", "http://development", "http://production");

        whenGetHost();

        thenHost().isEqualTo("http://production");
    }

    @Test
    public void shouldThrowExceptionForUnknownEnvironment() {
        givenEnvironment("unknown");
        givenDefaultHosts("http://localhost", "http://development", "http://production");

        thenExpectedException(IllegalStateException.class, "Unknown environment - terminate application");

        whenGetHost();
    }

    private void whenGetHost() {
        this.host = environmentUtil.getHost();
    }

    private AbstractCharSequenceAssert<?, String> thenHost() {
        return assertThat(host);
    }

    private void givenEnvironment(String environment) {
        environmentVariables.set("ENVIRONMENT", environment);
    }

    private void givenDefaultHosts(String local, String development, String production) {
        ReflectionTestUtils.setField(environmentUtil, "defaultHost", local);
        ReflectionTestUtils.setField(environmentUtil, "developmentHost", development);
        ReflectionTestUtils.setField(environmentUtil, "productionHost", production);
    }

    private void thenExpectedException(Class<? extends Exception> exeption, String message) {
        thrown.expectMessage(message);
        thrown.expect(exeption);
    }
}