package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;


public class DocDocumentTest {

    private static final String NEW_LINE = "\n";

    @Test
    public void shouldReturnTextFromDocFile() {

        String expectedContent = new StringBuilder()
                .append("This is Heading1 Text").append(NEW_LINE)
                .append("This is a regular paragraph with the default style of Normal.").append(NEW_LINE)
                .append("his is a regular paragraph with the default style of Normal. T").append(NEW_LINE)
                .append("his is a regular paragraph with the default style of Normal. Th").append(NEW_LINE)
                .append("is is a regular paragraph with the default style of Normal. Th").append(NEW_LINE)
                .append("is is a regular paragraph with the default style of Normal.")
                .toString();

        final String fileName = DocDocumentTest.class.getResource("/mock/sample.doc").getFile();
        File file = new File(fileName);
        IFileDocument docDocument = new DocFileDocument(file);

        String content = docDocument.convert();
        assertThat(content, containsString(expectedContent));
    }

    @Test(expected = DocumentConverterException.class)
    public void givenCorruptedFileWhenConvertThenThrowException() {
        File file = new File("corruptedfile");
        IFileDocument docDocument = new DocFileDocument(file);

        docDocument.convert();
    }

    @Test(expected = NullPointerException.class)
    public void givenNullInsteadOfFileWhenConvertThenThrowNullPointerException() {
        IFileDocument docDocument = new DocFileDocument(null);
        docDocument.convert();
    }

}
