package com.agreemount.negotiation.file;

import com.agreemount.negotiation.exception.UnsupportedFormatException;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;


public class FileFormatTest {

    @Test
    public void givenMappedEnumStringWhenCheckIfValueExistReturnTrue() {
        String docExt = "doc";
        Boolean result = FileFormat.contains(docExt);
        assertTrue(result);
    }

    @Test
    public void givenStringWithoutMappingWhenCheckIfValueExistReturnFalse() {
        String jpgExt = "jpg";
        Boolean result = FileFormat.contains(jpgExt);
        assertFalse(result);
    }

    @Test
    public void givenNullInsteadOfStringWhenCheckIfValueExistReturnFalse() {
        String nullExt = null;
        Boolean result = FileFormat.contains(nullExt);
        assertFalse(result);
    }

    @Test
    public void givenMappedEnumStringWhenGetKeyReturnProperEnumKey() {
        String docExt = "doc";
        FileFormat fileFormat = FileFormat.key(docExt);
        assertEquals(FileFormat.DOC, fileFormat);
    }

    @Test(expected = UnsupportedFormatException.class)
    public void givenStringWithoutMappingWhenGetKeyThenReturnNull() {
        String docExt = "jpg";
        FileFormat.key(docExt);
    }

}
