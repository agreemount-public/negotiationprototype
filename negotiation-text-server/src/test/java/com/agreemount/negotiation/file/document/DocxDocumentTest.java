package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;


public class DocxDocumentTest {

    private static final String NEW_LINE = "\n";

    @Test
    public void shouldReturnTextFromDocxFile() {

        String expectedContent = new StringBuilder()
                .append("Description").append(NEW_LINE)
                .append("Title of Invention : Sample Application")
                .toString();


        final String fileName = DocxDocumentTest.class.getResource("/mock/sample.docx").getFile();
        File file = new File(fileName);
        IFileDocument docDocument = new DocxFileDocument(file);

        String content = docDocument.convert();
        assertThat(content, containsString(expectedContent));
    }

    @Test(expected = DocumentConverterException.class)
    public void givenCorruptedDocxFileWhenConvertThenThrowException() {
        File file = new File("corruptedfile");
        IFileDocument docDocument = new DocxFileDocument(file);

        docDocument.convert();
    }

    @Test(expected = NullPointerException.class)
    public void givenNullInsteadOfDocxFileWhenConvertThenThrowNullPointerException() {
        IFileDocument docDocument = new DocxFileDocument(null);
        docDocument.convert();
    }

}
