package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


public class PdfDocumentTest {

    private static final String NEW_LINE = "\n";

    @Test
    public void shouldReturnTextFromPdfFile() throws IOException {
        String expectedContent = new StringBuilder()
                .append("Lorem Ipsum").append(NEW_LINE)
                .append("Text Text").append(NEW_LINE)
                .append("AAA bbb")
                .toString();

        final String fileName = PdfDocumentTest.class.getResource("/mock/test.pdf").getFile();
        File file = new File(fileName);

        IFileDocument document = new PdfFileDocument(file);

        final String content = document.convert();
        assertThat(content, containsString(expectedContent));
    }

    @Test(expected = DocumentConverterException.class)
    public void givenCorruptedPdfFileWhenConvertThenThrowException() {
        File file = new File("corruptedfile");
        IFileDocument document = new PdfFileDocument(file);
        document.convert();
    }

    @Test(expected = NullPointerException.class)
    public void givenNullInsteadOfPdfFileWhenConvertThenThrowNullPointerException() {
        IFileDocument docDocument = new PdfFileDocument(null);
        docDocument.convert();
    }


}
