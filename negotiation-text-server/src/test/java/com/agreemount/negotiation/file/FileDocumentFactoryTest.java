package com.agreemount.negotiation.file;

import com.agreemount.negotiation.exception.FileNotFoundException;
import com.agreemount.negotiation.exception.UnsupportedFormatException;
import com.agreemount.negotiation.file.document.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.when;



@RunWith(MockitoJUnitRunner.class)
public class FileDocumentFactoryTest {

    @Mock
    private MultipartFile file;

    @Test
    public void givenFileWhenFormatIsPdfThenCreatePdfDocument() throws IOException {
        when(file.getOriginalFilename()).thenReturn("file.pdf");
        when(file.getBytes()).thenReturn(new byte[0]);

        FileDocumentFactory factory = new FileDocumentFactory();
        IFileDocument pdfFile = factory.createFileDocument(file);

        assertThat(pdfFile, instanceOf(PdfFileDocument.class));
    }

    @Test
    public void givenFileWhenFormatIsTxtThenCreateTxtDocument() throws IOException {
        when(file.getOriginalFilename()).thenReturn("file.txt");

        FileDocumentFactory factory = new FileDocumentFactory();
        IFileDocument txtFile = factory.createFileDocument(file);

        assertThat(txtFile, instanceOf(TxtFileDocument.class));
    }

    @Test
    public void givenFileWhenFormatIsDocThenCreateDocDocument() throws IOException {
        when(file.getOriginalFilename()).thenReturn("file.doc");

        FileDocumentFactory factory = new FileDocumentFactory();
        IFileDocument docFile = factory.createFileDocument(file);

        assertThat(docFile, instanceOf(DocFileDocument.class));
    }

    @Test
    public void givenFileWhenFormatIsDocxThenCreateDocxDocument() throws IOException {
        when(file.getOriginalFilename()).thenReturn("file.docx");

        FileDocumentFactory factory = new FileDocumentFactory();
        IFileDocument docxFile = factory.createFileDocument(file);

        assertThat(docxFile, instanceOf(DocxFileDocument.class));
    }

    @Test
    public void givenFileWhenFormatIsRtfThenCreateRtfDocument() throws IOException {
        when(file.getOriginalFilename()).thenReturn("file.rtf");

        FileDocumentFactory factory = new FileDocumentFactory();
        IFileDocument rtfFile = factory.createFileDocument(file);

        assertThat(rtfFile, instanceOf(RtfFileDocument.class));
    }

    @Test(expected = UnsupportedFormatException.class)
    public void givenFileWithUnsupportedExtensionThenThrowUnsupportedFormatException() throws IOException {
        when(file.getOriginalFilename()).thenReturn("file.jpg");

        FileDocumentFactory factory = new FileDocumentFactory();
        factory.createFileDocument(file);
    }

    @Test(expected = FileNotFoundException.class)
    public void givenNullInsteadOfFileThenThrowException() throws IOException {
        when(file.getOriginalFilename()).thenReturn(null);

        FileDocumentFactory factory = new FileDocumentFactory();
        factory.createFileDocument(file);
    }

    @Before
    public void setUp() throws IOException {
        when(file.getBytes()).thenReturn(new byte[0]);
    }

}
