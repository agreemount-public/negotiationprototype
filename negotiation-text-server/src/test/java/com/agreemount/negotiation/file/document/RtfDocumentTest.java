package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;


public class RtfDocumentTest {

    private static final String NEW_LINE = "\n";

    @Test
    public void shouldReturnTextFromRtfFile() {
        String expectedContent = new StringBuilder()
                .append("This is a test RTF").append(NEW_LINE)
                .append("Hi! I’m a test file. This is some bold ").append(NEW_LINE)
                .append("text, and some italic text, as well as s")
                .toString();

        final String fileName = RtfDocumentTest.class.getResource("/mock/sample.rtf").getFile();
        File file = new File(fileName);
        IFileDocument docDocument = new RtfFileDocument(file);

        String content = docDocument.convert();
        assertThat(content, containsString(expectedContent));
    }

    @Test(expected = DocumentConverterException.class)
    public void givenCorruptedRtfFileWhenConvertThenThrowException() {
        File file = new File("corruptedfile.rtf");
        IFileDocument docDocument = new RtfFileDocument(file);

        docDocument.convert();
    }

    @Test(expected = NullPointerException.class)
    public void givenNullInsteadOfRtfFileWhenConvertThenThrowException() {
        IFileDocument docDocument = new RtfFileDocument(null);
        docDocument.convert();
    }

}
