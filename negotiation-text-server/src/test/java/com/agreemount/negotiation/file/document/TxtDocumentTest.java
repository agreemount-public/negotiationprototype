package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;


public class TxtDocumentTest {

    private static final String NEW_LINE = "\n";

    @Test
    public void shouldReturnTextFromTxtFile() throws IOException {
        String expectedContent = new StringBuilder()
                .append("Lorem ipsum dolor sit amet, consectetur adipiscing").append(NEW_LINE)
                .append("Ut quis enim pulvinar erat maximus sagittis").append(NEW_LINE)
                .append("Cras ligula nisi, rhoncus vel ante ac, porta cursus").append(NEW_LINE)
                .append("Aliquam convallis sem odio, at aliquet libero egestas").append(NEW_LINE)
                .append("Morbi turpis diam, viverra sit amet dictum non, auctor")
                .toString();

        final String fileName = TxtDocumentTest.class.getResource("/mock/mockfile.txt").getFile();
        IFileDocument document = new TxtFileDocument(new File(fileName));
        final String content = document.convert();
        assertEquals(expectedContent, content);
    }

    @Test(expected = DocumentConverterException.class)
    public void givenCorruptedTxtFileWhenConvertThenThrowException() {
        File file = new File("corruptedfile");
        IFileDocument document = new TxtFileDocument(file);

        final String content = document.convert();
        assertThat(content, containsString("Ut quis enim pulvinar erat maximus sagittis"));
    }

    @Test(expected = NullPointerException.class)
    public void givenNullInsteadOfTxtFileWhenConvertThenThrowException() {
        IFileDocument docDocument = new TxtFileDocument(null);
        docDocument.convert();
    }

    @Test(expected = Exception.class)
    public void givenJpgWithTxtExtensionWhenConvertThenThrowException() {
        final String fileName = TxtDocumentTest.class.getResource("/mock/thisisjpg.txt").getFile();
        IFileDocument document = new TxtFileDocument(new File(fileName));
        document.convert();
    }


}
