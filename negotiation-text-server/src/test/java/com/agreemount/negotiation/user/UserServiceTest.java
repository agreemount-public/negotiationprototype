package com.agreemount.negotiation.user;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.IUserDAO;
import org.assertj.core.api.AbstractListAssert;
import org.assertj.core.api.ObjectAssert;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    public static final String FIRST_USER = "user@first.com";
    public static final String SECOND_USER = "user@second.com";
    public static final String THIRD_USER = "user@third.com";
    public static final String EMAIL = "email@email.com";

    @Mock
    private IUserDAO iUserDAO;

    @InjectMocks
    private UserService userService;

    private Team team;

    private List<User> users;

    @Before
    public void setUp() {
        givenUsersForEmail(FIRST_USER, user(FIRST_USER));
        givenUsersForEmail(SECOND_USER, user(SECOND_USER));
        givenUsersForEmail(THIRD_USER, user(THIRD_USER));
    }

    @Test
    public void shouldReturnListOfUsers() {
        givenTeamWithUsers(user2Team(FIRST_USER), user2Team(SECOND_USER), user2Team(THIRD_USER));

        whenGetUsersOfTeam(team, SECOND_USER);

        thenUsers().containsExactly(user(FIRST_USER), user(THIRD_USER));

    }

    @Test
    public void shouldSetPasswordForUser() {
        Local local = new Local();
        local.setPassword("pass123".toCharArray());
        local.setEmail(EMAIL);

        Local user = userService.setPasswordForUser(local);

        assertThat(user.getEmail()).isEqualTo(EMAIL);
        assertThat(user.getHash()).isNotNull();
        assertThat(user.getSalt()).isNotNull();
        assertThat(user.getConfirmationHash()).isNotNull();
        assertThat(user.getPassword()).isNull();
    }

    private void givenUsersForEmail(String email, User result) {
        when(iUserDAO.getByEmail(email)).thenReturn(result);
    }

    private AbstractListAssert<?, List<? extends User>, User, ObjectAssert<User>> thenUsers() {
        return assertThat(users);
    }

    private void whenGetUsersOfTeam(Team team, String secondUser) {
        users = userService.getUsersOfTeam(team, secondUser);
    }

    private ArrayList<User> users(User... users) {
        return Lists.newArrayList(users);
    }

    private User user(String email) {
        User user = new Local();
        user.setEmail(email);
        return user;
    }

    private void givenTeamWithUsers(User2Team... users) {
        Team team = new Team();
        team.setUsers2Team(Lists.newArrayList(users));
        this.team = team;
    }

    private User2Team user2Team(String login) {
        User2Team user = new User2Team();
        user.setLogin(login);
        return user;
    }
}