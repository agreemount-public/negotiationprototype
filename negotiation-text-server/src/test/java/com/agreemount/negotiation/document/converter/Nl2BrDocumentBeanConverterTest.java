package com.agreemount.negotiation.document.converter;

import com.agreemount.bean.document.Document;
import com.google.common.collect.Maps;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.agreemount.negotiation.bean.enums.Metrics.ITEM_DESCRIPTION;
import static com.agreemount.negotiation.util.DocumentUtil.DOCUMENT_BODY_KEY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class Nl2BrDocumentBeanConverterTest {

    private static final String NEW_LINE = System.lineSeparator();
    private static final String BR_TAG = "<br>";

    @Mock
    private IConverter<String, String> converter;

    @InjectMocks
    private Nl2BrDocumentBeanConverter nl2BrDocumentBeanConverter = new Nl2BrDocumentBeanConverter();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void givenDocumentWithNullBodyWhenConvertThenNullPointerException() throws Exception {
        // given
        Document document = Mockito.mock(Document.class);
        when(document.getMetrics()).thenReturn(Maps.newHashMap());
        when(converter.convert(null)).thenThrow(NullPointerException.class);

        // then
        expectedException.expect(NullPointerException.class);

        // when
        Document actualDocument = nl2BrDocumentBeanConverter.convert(document);
    }


    @Test
    public void givenDocumentWithNewLinesBodyWhenConvertThenReturnConvertedDocument() throws Exception {
        // given
        Document document = Mockito.mock(Document.class);
        String givenContent = bodyWithNewLines(NEW_LINE);
        Document givenDocument = prepareDocumentWithContent(givenContent);

        String expectedContent = bodyWithNewLines(BR_TAG);
        when(converter.convert(anyString())).thenReturn(expectedContent);

        // when
        Document actualDocument = nl2BrDocumentBeanConverter.convert(givenDocument);

        // then
        verify(converter, times(1)).convert(anyString());
        assertDocumentWithContent(actualDocument, expectedContent);
    }

    private String bodyWithNewLines(String lineSeparator) {
        return new StringBuilder()
                .append("First line")
                .append(lineSeparator)
                .append("Second line")
                .append(lineSeparator)
                .append("Third long line")
                .toString();
    }


    private Document prepareDocumentWithContent(String content) {

        Map<String, Object> resoruces = Maps.newHashMap();
        resoruces.put(ITEM_DESCRIPTION.getValue(), content);
        Document document = new Document();

        document.setMetrics(resoruces);
        return document;
    }

    private void assertDocumentWithContent(Document document, String expectedContent) {

        assertNotNull(document);
        assertNotNull(expectedContent);
        String actualContent = (String) document.getMetrics().get(DOCUMENT_BODY_KEY);
        assertEquals(expectedContent, actualContent);
    }


}