package com.agreemount.negotiation.document.converter;

import com.agreemount.negotiation.document.model.Document;
import com.agreemount.negotiation.document.model.DocumentView;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class Nl2BrDocumentViewConverterTest {

    private static final String NEW_LINE = System.lineSeparator();

    @Mock
    private IConverter<Document, Document> documentConverter;

    @InjectMocks
    private Nl2BrDocumentViewConverter nl2BrDocumentViewConverter = new Nl2BrDocumentViewConverter();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void givenNullDocumentWhenConvertThenNullPointerException() throws Exception {
        // given
        DocumentView documentView = new DocumentView(null, Sets.newHashSet(), Sets.newHashSet());

        // when
        when(documentConverter.convert(null)).thenThrow(NullPointerException.class);

        // then
        expectedException.expect(NullPointerException.class);
        nl2BrDocumentViewConverter.convert(documentView);

    }


    @Test
    public void givenDocumentWithNewLinesBodyWhenConvertThenReturnConvertedDocument() throws Exception {
        // given
        String givenContent = new StringBuilder()
                .append("First line")
                .append(NEW_LINE)
                .append("Second <D1>line</D1>")
                .append(NEW_LINE)
                .append("Third long line")
                .toString();

        String expectedContent = new StringBuilder()
                .append("First line")
                .append("<br>")
                .append("Second <D1>line</D1>")
                .append("<br>")
                .append("Third long line")
                .toString();

        DocumentView documentView = mockDocumentView(givenContent, expectedContent);
        Document expectedDocument = new Document(expectedContent, "doc", Sets.newHashSet("D1"));
        when(documentConverter.convert(anyObject())).thenReturn(expectedDocument);

        // when
        DocumentView actualDocumentView = nl2BrDocumentViewConverter.convert(documentView);

        // then
        verify(documentConverter, times(1)).convert(anyObject());
        verify(documentView, times(1)).getConflictedTags();
        verify(documentView, times(1)).getUnresolvedTags();
        assertDocumentViewWithContent(actualDocumentView, expectedContent);
    }


    @Test
    public void givenDocumentWithNullBodyWhenConvertThenBodyOfReturnedDocumentViewNotAvailable() throws Exception {
        // given
        Document document = mock(Document.class);
        when(document.getBody()).thenReturn(null);
        when(document.getContentTags()).thenReturn(Sets.newHashSet("D1"));
        when(document.getDocumentTag()).thenReturn("doc");
        DocumentView documentView = mock(DocumentView.class);
        when(documentView.getDocument()).thenReturn(document);
        when(documentView.getBody()).thenThrow(NullPointerException.class);

        // when

        DocumentView actualDocumentView = nl2BrDocumentViewConverter.convert(documentView);

        // then
        verify(documentConverter, times(1)).convert(anyObject());

        expectedException.expect(NullPointerException.class);
        actualDocumentView.getBody();

    }

    private DocumentView mockDocumentView(String givenContent, String expectedContent) {

        Document document = mock(Document.class);
        when(document.getBody()).thenReturn(givenContent);
        when(document.getContentTags()).thenReturn(Sets.newHashSet("D1"));
        when(document.getDocumentTag()).thenReturn("doc");
        DocumentView documentView = mock(DocumentView.class);
        when(documentView.getDocument()).thenReturn(document);


        return documentView;
    }


    private void assertDocumentViewWithContent(DocumentView documentView, String expectedContent) {

        assertNotNull(documentView);
        assertNotNull(expectedContent);
        assertNotNull(documentView.getDocument());
        assertNotNull(documentView.getDocument().getBody());
        assertEquals(expectedContent, documentView.getDocument().getBody());
    }
}