package com.agreemount.negotiation.document.converter;

import com.agreemount.negotiation.document.model.Document;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class Nl2BrDocumentConverterTest {
    private static final String NEW_LINE = System.lineSeparator();

    @Mock
    private IConverter<String, String> converter;

    @InjectMocks
    private Nl2BrDocumentConverter nl2BrDocumentConverter = new Nl2BrDocumentConverter();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void givenDocumentWithNullBodyWhenConvertThenNullPointerException() throws Exception {
        // given
        Document document = new Document(null, null, Sets.newHashSet());

        // when
        when(converter.convert(null)).thenThrow(NullPointerException.class);

        // then
        expectedException.expect(NullPointerException.class);
        Document actualDocument = nl2BrDocumentConverter.convert(document);

    }


    @Test
    public void givenDocumentWithNewLinesBodyWhenConvertThenReturnConvertedDocument() throws Exception {
        // given
        String givenContent = new StringBuilder()
                .append("First line")
                .append(NEW_LINE)
                .append("Second <D1>line</D1>")
                .append(NEW_LINE)
                .append("Third long line")
                .toString();

        Document document = new Document(givenContent, "doc", Sets.newHashSet("D1"));

        String expectedContent = new StringBuilder()
                .append("First line")
                .append("<br>")
                .append("Second <D1>line</D1>")
                .append("<br>")
                .append("Third long line")
                .toString();

        when(converter.convert(anyString())).thenReturn(expectedContent);

        // when
        Document actualDocument = nl2BrDocumentConverter.convert(document);

        // then
        verify(converter, times(1)).convert(anyString());
        assertDocumentWithContent(actualDocument, expectedContent);
    }


    private void assertDocumentWithContent(Document document, String expectedContent) {

        assertNotNull(document);
        assertNotNull(expectedContent);
        assertEquals(expectedContent, document.getBody());
    }
}