package com.agreemount.negotiation.document.converter;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class Nl2BrConverterTest {

    private static final String NEW_LINE = System.lineSeparator();

    @Mock
    private Nl2BrConverter converter;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void givenNullToConvertWhenConvertThenThrowNullPointerException() throws Exception {
        expectedException.expect(NullPointerException.class);
        doThrow(NullPointerException.class).when(converter).convert(null);
        converter.convert(null);
    }

    @Test
    public void givenStringWithNewLinesWhenConvertThenReturnBrTagsAsNewLine() throws Exception {

        String given = new StringBuilder()
                .append("First line")
                .append(NEW_LINE)
                .append("Second line")
                .append(NEW_LINE)
                .append("Third long line")
                .toString();

        // when
        String expected = new StringBuilder()
                .append("First line")
                .append("<br>")
                .append("Second line")
                .append("<br>")
                .append("Third long line")
                .toString();

        when(converter.convert(anyString())).thenReturn(expected);

        String actual = converter.convert(given);
        // then

        assertNotNull(actual);
        assertEquals(expected, actual);

    }


}