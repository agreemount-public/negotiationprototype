package com.agreemount.negotiation.engine.extension.action.impl;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.engine.extension.action.definition.BumpUpSequence;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.db.DocumentOperations;
import org.assertj.core.util.Maps;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;


public class BumpUpSequenceImplTest {
    private static final String MAIN_DOCUMENT_ALIAS = "mainDocumentAlias";
    private static final String DOCUMENT_ALIAS = "documentAlias";
    private static final String MIAN_ROOT_UUID = UUID.randomUUID().toString();
    private static final String ROOT_UUID = UUID.randomUUID().toString();
    private static final String MAIN_SEQUENCE_KEY = "mainSequenceKey";
    private static final String SEQUENCE_KEY = "sequenceKey";
    private ActionContext<Document> actionContext;
    private Document mainDocument;
    private Document document;
    private BumpUpSequenceImpl bumpUpSequenceImpl;
    private BumpUpSequence actionDefinition;


    @Before
    public void setUp() {
        bumpUpSequenceImpl = new BumpUpSequenceImpl();
        DocumentOperations operations = mock(DocumentOperations.class);
        doNothing().when(operations).saveDocument(any());
        bumpUpSequenceImpl.setDocumentOperations(operations);
        mainDocument = new Document();
        mainDocument.setRootUuid(MIAN_ROOT_UUID);

        document = new Document();
        document.setRootUuid(ROOT_UUID);

        actionDefinition = new BumpUpSequence();
        actionContext = new ActionContext<>();
        actionContext.addDocument(MAIN_DOCUMENT_ALIAS, mainDocument);
        actionContext.addDocument(DOCUMENT_ALIAS, document);
//        actionContext.addSingeVariable(MAIN_SEQUENCE_KEY, "maxSequence");
//        actionContext.addSingeVariable(SEQUENCE_KEY, "sequence");
    }

    @Test
    public void shouldThrowExceptionWhenDocumentAliasNotDefined() {
        givenActionDefinition(MAIN_DOCUMENT_ALIAS, null, MAIN_SEQUENCE_KEY, SEQUENCE_KEY);

        assertThat(catchThrowable(() -> bumpUpSequenceImpl.run(actionContext)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("documentAlias is not set!");

    }

    @Test
    public void shouldThrowExceptionWhenMainDocumentAliasNotDefined() {
        givenActionDefinition(null, DOCUMENT_ALIAS, MAIN_SEQUENCE_KEY, SEQUENCE_KEY);

        assertThat(catchThrowable(() -> bumpUpSequenceImpl.run(actionContext)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("mainDocumentAlias is not set!");
    }

    @Test
    public void shouldThrowExceptionWhenSequenceKeyNotDefined() {
        givenActionDefinition(MAIN_DOCUMENT_ALIAS, DOCUMENT_ALIAS, MAIN_SEQUENCE_KEY, null);

        assertThat(catchThrowable(() -> bumpUpSequenceImpl.run(actionContext)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("sequenceKey is not set!");

    }

    @Test
    public void shouldThrowExceptionWhenMainSequenceKeyNotDefined() {
        givenActionDefinition(MAIN_DOCUMENT_ALIAS, DOCUMENT_ALIAS, null, SEQUENCE_KEY);

        assertThat(catchThrowable(() -> bumpUpSequenceImpl.run(actionContext)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("mainSequenceKey is not set!");
    }

    @Test
    public void shouldThrowExceptionWhenMainDocumentNotFound() {
        String mainDocumentAlias = MAIN_DOCUMENT_ALIAS + "notMatchedToContext";
        givenActionDefinition(mainDocumentAlias, DOCUMENT_ALIAS, MAIN_SEQUENCE_KEY, SEQUENCE_KEY);

        assertThat(catchThrowable(() -> bumpUpSequenceImpl.run(actionContext)))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("document with alias [" + mainDocumentAlias + "] was not found");
    }

    @Test
    public void shouldThrowExceptionWhenDocumentNotFound() {
        String documentAlias = DOCUMENT_ALIAS + "notMatchedToContext";
        givenActionDefinition(MAIN_DOCUMENT_ALIAS, documentAlias, MAIN_SEQUENCE_KEY, SEQUENCE_KEY);


        assertThat(catchThrowable(() -> bumpUpSequenceImpl.run(actionContext)))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("document with alias [" + documentAlias + "] was not found");
    }

    @Test
    public void shouldInitSequenceInMainDocument() {
        givenActionDefinition(MAIN_DOCUMENT_ALIAS, DOCUMENT_ALIAS, MAIN_SEQUENCE_KEY, SEQUENCE_KEY);
        givenActionContextWithMaxSequence(new HashMap<>());
        bumpUpSequenceImpl.run(actionContext);

        assertMainDocumentMaxSequence(1);
        assertModifiedDocumentSequence(0);
    }

    @Test
    public void shouldUseMaxSequenceInModifiedDocumentAndBumpItUpInMainDocument() {
        givenActionDefinition(MAIN_DOCUMENT_ALIAS, DOCUMENT_ALIAS, MAIN_SEQUENCE_KEY, SEQUENCE_KEY);
        givenActionContextWithMaxSequence(Maps.newHashMap(MAIN_SEQUENCE_KEY, 10));
        bumpUpSequenceImpl.run(actionContext);

        assertMainDocumentMaxSequence(11);
        assertModifiedDocumentSequence(10);
    }

    @Test
    public void shouldMainAndModifiedDocumentsHaveTheSameMaxSequenceAndSequenceValues() {
        givenActionDefinition(MAIN_DOCUMENT_ALIAS, DOCUMENT_ALIAS, MAIN_SEQUENCE_KEY, SEQUENCE_KEY);
        givenActionContextWithEqualDocumentsAndSeparateSequenceKeys(new HashMap<>());
        bumpUpSequenceImpl.run(actionContext);

        assertMainDocumentSequences(0, 1);
        assertModifiedDocumentSequences(0, 1);

    }

    @Test
    public void shouldBeOnlyOneDocumentUpdatedWithOneSequenceWhenDocumentAliasesAndSequencesKeyAreTheSame() {
        givenActionDefinition(MAIN_DOCUMENT_ALIAS, DOCUMENT_ALIAS, SEQUENCE_KEY, SEQUENCE_KEY);
        givenActionContextWithEqualDocumentsAndSequenceKeys(new HashMap<>());
        bumpUpSequenceImpl.run(actionContext);

        assertThat(actionContext.getDocument(MAIN_DOCUMENT_ALIAS)).isNotNull();
        assertThat(actionContext.getDocument(MAIN_DOCUMENT_ALIAS)).isEqualTo(actionContext.getDocument(DOCUMENT_ALIAS));
        assertThat(actionContext.getDocument(MAIN_DOCUMENT_ALIAS).getMetrics()).isEqualTo(new HashMap<String, Object>() {
            {
                put(SEQUENCE_KEY, 1);
            }
        });
    }

    private void givenActionDefinition(String mainDocumentAlias, String documentAlias, String mainSequenceKey, String sequenceKey) {
        actionDefinition.setMainDocumentAlias(mainDocumentAlias);
        actionDefinition.setDocumentAlias(documentAlias);
        actionDefinition.setMainSequenceKey(mainSequenceKey);
        actionDefinition.setSequenceKey(sequenceKey);
        bumpUpSequenceImpl.setDefinition(actionDefinition);
    }

    private void assertModifiedDocumentSequence(Integer sequenceValue) {
        assertThat(actionContext.getDocument(DOCUMENT_ALIAS)).isNotNull();
        assertThat(actionContext.getDocument(DOCUMENT_ALIAS).getMetrics())
                .isEqualTo(new HashMap<String, Object>() {
                    {
                        put(SEQUENCE_KEY, sequenceValue);
                    }
                });
    }

    private void assertMainDocumentMaxSequence(Integer maxSequenceValue) {
        assertThat(actionContext.getDocument(MAIN_DOCUMENT_ALIAS)).isNotNull();
        assertThat(actionContext.getDocument(MAIN_DOCUMENT_ALIAS).getMetrics())
                .isEqualTo(new HashMap<String, Object>() {
                    {
                        put(MAIN_SEQUENCE_KEY, maxSequenceValue);
                    }
                });
    }

    private void assertModifiedDocumentSequences(Integer sequenceValue, Integer maxSequenceValue) {
        assertThat(actionContext.getDocument(DOCUMENT_ALIAS)).isNotNull();
        assertThat(actionContext.getDocument(DOCUMENT_ALIAS).getMetrics()).isEqualTo(new HashMap<String, Object>() {
            {
                put(MAIN_SEQUENCE_KEY, maxSequenceValue);
                put(SEQUENCE_KEY, sequenceValue);
            }
        });
    }

    private void assertMainDocumentSequences(Integer sequenceValue, Integer maxSequnceValue) {
        assertThat(actionContext.getDocument(MAIN_DOCUMENT_ALIAS)).isNotNull();
        assertThat(actionContext.getDocument(MAIN_DOCUMENT_ALIAS).getMetrics()).isEqualTo(new HashMap<String, Object>() {
            {
                put(MAIN_SEQUENCE_KEY, maxSequnceValue);
                put(SEQUENCE_KEY, sequenceValue);
            }
        });
    }

    private void givenActionContextWithMaxSequence(Map<String, Object> metricsWithSequence) {
        mainDocument.setMetrics(metricsWithSequence);
        actionContext.addDocument(MAIN_DOCUMENT_ALIAS, mainDocument);
        actionContext.addDocument(DOCUMENT_ALIAS, document);

        actionContext.addSingeVariable(MAIN_SEQUENCE_KEY, "maxSequence");
        actionContext.addSingeVariable(SEQUENCE_KEY, "sequence");
    }

    private void givenActionContextWithEqualDocumentsAndSeparateSequenceKeys(Map<String, Object> metricsWithSequence) {
        mainDocument.setMetrics(metricsWithSequence);
        actionContext.addDocument(MAIN_DOCUMENT_ALIAS, mainDocument);
        actionContext.addDocument(DOCUMENT_ALIAS, mainDocument);

        actionContext.addSingeVariable(MAIN_SEQUENCE_KEY, "maxSequence");
        actionContext.addSingeVariable(SEQUENCE_KEY, "sequence");
    }

    private void givenActionContextWithEqualDocumentsAndSequenceKeys(Map<String, Object> metricsWithSequence) {
        mainDocument.setMetrics(metricsWithSequence);
        actionContext.addDocument(MAIN_DOCUMENT_ALIAS, mainDocument);
        actionContext.addDocument(DOCUMENT_ALIAS, mainDocument);

        actionContext.addSingeVariable(MAIN_SEQUENCE_KEY, "sequence");
        actionContext.addSingeVariable(SEQUENCE_KEY, "sequence");
    }


}