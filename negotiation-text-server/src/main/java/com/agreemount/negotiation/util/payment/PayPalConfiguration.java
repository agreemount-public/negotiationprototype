package com.agreemount.negotiation.util.payment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class PayPalConfiguration {
    @Value("${paypal.mode}")
    private String mode;

    @Value("${paypal.http.ConnectionTimeOut}")
    private String httpConnectionTimeOut;

    @Value("${paypal.http.Retry}")
    private String httpRetry;

    @Value("${paypal.http.ReadTimeOut}")
    private String httpReadTimeOut;

    @Value("${paypal.http.MaxConnection}")
    private String httpMaxConnection;

    public final Map<String, String> getConfig() {
        Map<String, String> configMap = new HashMap<>();
        configMap.put("mode", mode);
        configMap.put("http.ConnectionTimeOut", httpConnectionTimeOut);
        configMap.put("http.Retry", httpRetry);
        configMap.put("http.ReadTimeOut", httpReadTimeOut);
        configMap.put("http.MaxConnection", httpMaxConnection);
        return configMap;
    }

    @Override
    public String toString() {
        return "PayPalConfiguration{" +
                "mode='" + mode + '\'' +
                ", httpConnectionTimeOut='" + httpConnectionTimeOut + '\'' +
                ", httpRetry='" + httpRetry + '\'' +
                ", httpReadTimeOut='" + httpReadTimeOut + '\'' +
                ", httpMaxConnection='" + httpMaxConnection + '\'' +
                '}';
    }
}
