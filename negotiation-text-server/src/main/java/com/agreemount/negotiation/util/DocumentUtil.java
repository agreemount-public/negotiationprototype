package com.agreemount.negotiation.util;

import com.agreemount.bean.document.Document;
import com.google.common.base.Preconditions;

public final class DocumentUtil {
    public static final String DOCUMENT_BODY_KEY = "content";

    private DocumentUtil() {
    }

    public static final String getBodyOf(Document document) {
        check(document);
        return (String) document.getMetrics().get(DOCUMENT_BODY_KEY);
    }

    public static final void setBody(Document document, String body) {
        check(document);
        document.getMetrics().put(DOCUMENT_BODY_KEY, body);
    }

    public static final String getTagName(Document document) {
        Preconditions.checkNotNull("Document can not be null", document);
        return document.getRootUuid();
    }

    private static void check(Document document) {

        Preconditions.checkNotNull("Document can not be null", document);
        Preconditions.checkNotNull("Metrics for document can not be null", document.getMetrics());
    }

}
