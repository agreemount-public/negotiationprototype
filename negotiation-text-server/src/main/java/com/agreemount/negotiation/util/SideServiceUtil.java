package com.agreemount.negotiation.util;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.logic.ITeamLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class SideServiceUtil {

    @Autowired
    private ITeamLogic teamLogic;

    @Autowired
    private IdentityProvider identityProvider;

    @Autowired
    private IUserDAO userDAO;

    public UserRole getSideForLoggedInUser(Document document) {
        String login = identityProvider.getIdentity().getLogin();
        String team = document.getTeam();
        String message = String.format("Can not get Side for given document %s and user %s", document, login);
        return teamLogic.userRoleInTeam(team, login)
                .orElseThrow(() -> new IllegalArgumentException(message));
    }

    public Optional<User> getUserByRole(Document document, UserRole side) {
        return Optional.ofNullable(
                getUser2Team(document, side)
                        .filter(Objects::nonNull)
                        .map(user2Team -> userDAO.getByEmail(user2Team.getLogin()))
                        .orElse(null)
        );
    }

    private Optional<User2Team> getUser2Team(Document document, UserRole side) {
        String teamName = document.getTeam();
        return teamLogic.getUserWithRole(teamName, side);
    }
}
