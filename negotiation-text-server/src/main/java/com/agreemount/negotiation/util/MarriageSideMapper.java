package com.agreemount.negotiation.util;

import com.agreemount.negotiation.bean.UserRole;


public class MarriageSideMapper {

    public static MarriageSide mapLoggedInUserToHusband(UserRole loggedInUserSide, UserRole valueSide) {
        return valueSide.equals(loggedInUserSide) ? MarriageSide.HUSBAND : MarriageSide.WIFE;
    }

    public enum MarriageSide {
        HUSBAND("husband"), WIFE("wife");
        private final String value;

        MarriageSide(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
