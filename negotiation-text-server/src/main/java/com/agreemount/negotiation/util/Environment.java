package com.agreemount.negotiation.util;

public enum Environment {
    DEVELOPMENT("dev"),
    PRODUCTION("prod");

    private String name;

    Environment(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
