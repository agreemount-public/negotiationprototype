package com.agreemount.negotiation.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EnvironmentUtil {

    private static final String UNKNOWN_ENVIRONMENT = "Unknown environment - terminate application";

    @Value("${app.host.default}")
    private String defaultHost;

    @Value("${app.host.development}")
    private String developmentHost;

    @Value("${app.host.production}")
    private String productionHost;

    public String getHost() {
        String environment = System.getenv("ENVIRONMENT");

        if (environment == null) {
            return defaultHost;
        }

        if (Environment.DEVELOPMENT.getName().equals(environment)) {
            return developmentHost;
        }

        if (Environment.PRODUCTION.getName().equals(environment)) {
            return productionHost;
        }

        throw new IllegalStateException(UNKNOWN_ENVIRONMENT);
    }

}
