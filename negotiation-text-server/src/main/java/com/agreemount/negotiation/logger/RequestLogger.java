package com.agreemount.negotiation.logger;

import lombok.extern.log4j.Log4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;



@Aspect
@Component
@Log4j
public class RequestLogger {

    private static final List<String> PROHIBITED_KEYS = Arrays.asList("password", "hash", "pass", "salt", "user");

    private static final String SENSITIVE_DATA_REPLACEMENT = "************";

    @Before("execution(* com.agreemount.controller.*.*(..)) || execution(* com.agreemount.negotiation.controller.*.*(..))")
    public void logBefore(JoinPoint joinPoint) {
        final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        final Class<?>[] parameterTypes = signature.getParameterTypes();
        final String[] argsNames = signature.getParameterNames();
        List<Object> args = Arrays.asList(joinPoint.getArgs());

        StringBuilder msgBuilder = new StringBuilder(">> RequestLogger << ")
                .append(joinPoint.getSignature().getDeclaringType())
                .append(".")
                .append(joinPoint.getSignature().getName())
                .append("(");

        IntStream.range(0, args.size())
                .forEach(idx -> {
                    if (idx != 0) {
                        msgBuilder.append(", ");
                    }
                    String key = argsNames[idx];
                    Object valueObject = args.get(idx);
                    String value = valueObject == null ? "null" : "'" + valueObject.toString() + "'";
                    if (PROHIBITED_KEYS.contains(key)) {
                        value = "'" + SENSITIVE_DATA_REPLACEMENT + "'";
                    }
                    msgBuilder.append(parameterTypes[idx])
                            .append(" ")
                            .append(key)
                            .append("=")
                            .append(value);
                });

        msgBuilder.append(")");
        log.info(msgBuilder);
    }

}