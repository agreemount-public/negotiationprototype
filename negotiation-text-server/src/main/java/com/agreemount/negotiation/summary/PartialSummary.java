package com.agreemount.negotiation.summary;

import java.math.BigDecimal;

public class PartialSummary {
    private Integer total = 0;
    private Integer assets = 0;
    private Integer debts = 0;
    private BigDecimal percentage = null;

    public void addAsset(Integer value) {
        this.assets += value;
        this.total += value;
    }

    public void addDebts(Integer value) {
        this.debts += value;
        this.total -= value;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage.setScale(0, BigDecimal.ROUND_HALF_UP);
    }

    public Integer getTotal() {
        return total;
    }

    public Integer getAssets() {
        return assets;
    }

    public Integer getDebts() {
        return debts;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }
}
