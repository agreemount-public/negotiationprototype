package com.agreemount.negotiation.summary;

import com.agreemount.bean.document.Document;
import com.agreemount.engine.facade.QueryFacade;
import com.agreemount.slaneg.action.ActionContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SummaryDataService {

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Autowired
    private QueryFacade queryFacade;

    private static final String AGREED_ITEMS_IN_SPLIT = "AgreedItemsInSplit";

    public List<Document> getAgreedItems(Document negotiation) {
        return queryFacade.getDocumentsForQuery(AGREED_ITEMS_IN_SPLIT,
                actionContextFactory.createInstance(negotiation));
    }

}
