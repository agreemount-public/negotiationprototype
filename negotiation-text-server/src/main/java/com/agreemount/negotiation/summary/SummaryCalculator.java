package com.agreemount.negotiation.summary;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.enums.Metrics;
import com.agreemount.negotiation.bean.enums.States;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Component
public class SummaryCalculator {

    protected static final String ASSET = "asset";
    protected static final String DEBT = "debt";

    public Summary buildSummary(List<Document> items) {
        final Summary summary = new Summary();

        items.forEach(item -> addToSummary(summary, item));
        fillPercentages(summary);

        return summary;
    }

    private void fillPercentages(Summary summary) {
        Integer totalToDivide = summary.getTotalToDivide().getTotal();

        if (totalToDivide == 0) {
            return;
        }

        summary.getTotalToDivide().setPercentage(BigDecimal.valueOf(100));
        calculatePartialPercentage(summary.getSide1(), totalToDivide);
        calculatePartialPercentage(summary.getSide2(), totalToDivide);
    }

    private void calculatePartialPercentage(PartialSummary partialSummary, Integer totalToDivide) {
        BigDecimal percentage = BigDecimal.valueOf(partialSummary.getTotal())
                .divide(BigDecimal.valueOf(totalToDivide), 3, RoundingMode.HALF_UP)
                .multiply(BigDecimal.valueOf(100));

        if (isInRange(percentage, BigDecimal.valueOf(100), BigDecimal.ZERO)) {
            partialSummary.setPercentage(percentage);
        }

    }

    private boolean isInRange(BigDecimal percentage, final BigDecimal max, final BigDecimal min) {
        return percentage.compareTo(max) <= 0 && percentage.compareTo(min) >= 0;
    }

    private void addToSummary(Summary summary, Document document) {
        if (!isPartOfSummary(document)) {
            return;
        }

        String itemType = document.getState(States.ITEM_TYPE.getValue());
        String itemOwner = document.getState(States.ITEM_OWNER.getValue());

        Integer value = (Integer) document.getMetrics().get(Metrics.ITEM_VALUE.getValue());
        assignAssetOrDebt(value, itemType, summary.getTotalToDivide());

        switch (itemOwner) {
            case "SIDE1":
                assignAssetOrDebt(value, itemType, summary.getSide1());
                break;
            case "SIDE2":
                assignAssetOrDebt(value, itemType, summary.getSide2());
                break;
            case "SPLIT":
                Integer side1Value = value * (Integer) document.getMetrics().get(Metrics.SPLIT_PERCENT_SIDE1.getValue()) / 100;
                Integer side2Value = value * (Integer) document.getMetrics().get(Metrics.SPLIT_PERCENT_SIDE2.getValue()) / 100;

                assignAssetOrDebt(side1Value, itemType, summary.getSide1());
                assignAssetOrDebt(side2Value, itemType, summary.getSide2());
                break;
        }
    }
    private boolean isPartOfSummary(Document document) {
        boolean itemIsSeparate = document.getMetrics().get(Metrics.ITEM_IS_SEPARATE.getValue()) != null
                                && (Boolean) document.getMetrics().get(Metrics.ITEM_IS_SEPARATE.getValue());

        boolean hasValue = document.getMetrics().get(Metrics.ITEM_VALUE.getValue()) != null;

        return !itemIsSeparate && hasValue;
    }

    private void assignAssetOrDebt(Integer value, String itemType, PartialSummary partialSummary) {
        if (ASSET.equals(itemType)) {
            partialSummary.addAsset(value);
        }
        else {
            partialSummary.addDebts(value);
        }
    }
}
