package com.agreemount.negotiation.summary;

public class Summary {
    private PartialSummary totalToDivide = new PartialSummary();
    private PartialSummary side1 = new PartialSummary();
    private PartialSummary side2 = new PartialSummary();

    public PartialSummary getTotalToDivide() {
        return totalToDivide;
    }

    public PartialSummary getSide1() {
        return side1;
    }

    public PartialSummary getSide2() {
        return side2;
    }
}
