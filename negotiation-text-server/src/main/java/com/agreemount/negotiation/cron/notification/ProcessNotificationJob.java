package com.agreemount.negotiation.cron.notification;

import com.agreemount.negotiation.logic.processing.IProcessNotificationLogic;
import lombok.extern.log4j.Log4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;


@Log4j
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class ProcessNotificationJob extends QuartzJobBean {

    private IProcessNotificationLogic processNotificationLogic;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.debug("CRON: ProcessNotificationJob start...");
        processNotificationLogic.retrieve();
        log.debug("CRON: ProcessNotificationJob end...");
    }

    public void setProcessNotificationLogic(IProcessNotificationLogic processNotificationLogic) {
        this.processNotificationLogic = processNotificationLogic;
    }
}
