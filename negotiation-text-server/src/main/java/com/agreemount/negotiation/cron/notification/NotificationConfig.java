package com.agreemount.negotiation.cron.notification;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.configuration.CronConfiguration;
import com.agreemount.negotiation.logic.processing.IProcessNotificationLogic;
import com.agreemount.slaneg.fixtures.FileRulesProvidersConfiguration;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import java.util.HashMap;
import java.util.Map;



@ComponentScan(
        basePackages = {"com.agreemount"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Application.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = FileRulesProvidersConfiguration.class)
        }
)
@Configuration
@Log4j
public class NotificationConfig {

    @Value("${cron.notificationProcess.repeatInterval}")
    private int repeatInterval;

    @Value("${cron.notificationProcess.startDelay}")
    private int startDelay;

    @Autowired
    private IProcessNotificationLogic processNotificationLogic;

    @Bean
    public SimpleTriggerFactoryBean processNotifications() {
        SimpleTriggerFactoryBean stFactory = new SimpleTriggerFactoryBean();
        stFactory.setJobDetail(processNotificationsJob().getObject());
        stFactory.setStartDelay(startDelay * CronConfiguration.ONE_MINUTE_IN_MS);
        stFactory.setRepeatInterval(repeatInterval * CronConfiguration.ONE_MINUTE_IN_MS);
        return stFactory;
    }

    @Bean
    public JobDetailFactoryBean processNotificationsJob() {
        JobDetailFactoryBean factory = new JobDetailFactoryBean();
        factory.setJobClass(ProcessNotificationJob.class);

        Map<String, Object> map = new HashMap<>();
        map.put("processNotificationLogic", processNotificationLogic);
        factory.setJobDataAsMap(map);

        factory.setName("processNotificationsJob");
        return factory;
    }

}
