package com.agreemount.negotiation.cron.sender;

import com.agreemount.negotiation.logic.ISendLogic;
import lombok.extern.log4j.Log4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;



@Log4j
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class SenderJob extends QuartzJobBean {

    private ISendLogic sendLogic;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.debug("CRON: SenderJob start...");
        sendLogic.send();
        log.debug("CRON: SenderJob end...");
    }

    public void setSendLogic(ISendLogic sendLogic) {
        this.sendLogic = sendLogic;
    }
}
