package com.agreemount.negotiation.exception;


public class UnsupportedFormatException extends RuntimeException{

    public UnsupportedFormatException(String message){
        super(message);
    }

    public UnsupportedFormatException(Throwable cause){
        super(cause);
    }

    public UnsupportedFormatException(String message, Throwable cause){
        super(message, cause);
    }


}
