package com.agreemount.negotiation.exception;

public class DocumentConverterException extends RuntimeException {

    public DocumentConverterException(String message){
        super(message);
    }

    public DocumentConverterException(Throwable cause){
        super(cause);
    }

    public DocumentConverterException(String message, Throwable cause){
        super(message, cause);
    }

}
