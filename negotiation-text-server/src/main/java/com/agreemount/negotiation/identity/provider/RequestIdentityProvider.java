package com.agreemount.negotiation.identity.provider;

import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;
import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;


@Component
@Scope(value = SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class RequestIdentityProvider implements IdentityProvider {

    private Identity identity;

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public Identity getIdentity() {
        return identity;
    }
}
