package com.agreemount.negotiation.identity.provider;

import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.TeamMember;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.dao.impl.TeamDAO;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;


@Component
@Log4j
public class RequestInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private RequestIdentityProvider identityProvider;

    @Autowired
    private TeamDAO teamDAO;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String loggedUserEmail = request.getHeader("x-user-email");
        log.info("logged as user: " + loggedUserEmail);

        List<Team> teams = teamDAO.getUserTeams(loggedUserEmail);
        List<TeamMember> teamMembers = new ArrayList<>();
        setTeamMembers(loggedUserEmail, teams, teamMembers);

        identityProvider.setIdentity(getIdentity(loggedUserEmail, teamMembers));
        log.info("logged as user: " + identityProvider.getIdentity());
        return true;
    }

    private void setTeamMembers(String loggedUserEmail, List<Team> teams, List<TeamMember> teamMembers) {
        for (Team team : teams) {
            for (User2Team user2Team : team.getUsers2Team()) {
                setTeamMember(loggedUserEmail, teamMembers, team, user2Team);
            }
        }
    }

    private void setTeamMember(String loggedUserEmail, List<TeamMember> teamMembers, Team team, User2Team user2Team) {
        if (user2Team.getLogin().equals(loggedUserEmail)) {
            TeamMember teamMember = new TeamMember();
            teamMember.setRole(user2Team.getRole().getValue());
            teamMember.setTeam(team.getName());
            teamMembers.add(teamMember);
        }
    }

    private Identity getIdentity(String loggedUserEmail, List<TeamMember> teamMembers) {
        Identity identity = new Identity();
        identity.setLogin(loggedUserEmail);
        List<String> roles = new ArrayList<>();
        roles.add("customer");
        identity.setRoles(roles);
        identity.setTeamMembers(teamMembers);
        return identity;
    }

}
