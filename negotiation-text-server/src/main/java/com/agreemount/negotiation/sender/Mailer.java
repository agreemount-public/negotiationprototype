package com.agreemount.negotiation.sender;

import com.agreemount.negotiation.bean.PreparedEmail;
import lombok.extern.log4j.Log4j;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;



@Component
@Log4j
public class Mailer implements ISender {

    @Autowired
    private Environment environment;

    private HtmlEmail email;

    @Override
    public void send(PreparedEmail preparedEmail) {
        email = new HtmlEmail();
        email.setHostName(environment.getProperty("mailer.host"));
        email.setSmtpPort(Integer.parseInt(environment.getProperty("mailer.port")));
        email.setAuthenticator(new DefaultAuthenticator(
                environment.getProperty("mailer.username"),
                environment.getProperty("mailer.password")
        ));
        email.setStartTLSEnabled(true);
        email.setSubject(preparedEmail.getTitle());

        try {
            email.setFrom(environment.getProperty("mailer.senderEmail"), environment.getProperty("mailer.senderName"));
            email.setHtmlMsg(preparedEmail.getContent());
            email.addTo(preparedEmail.getReceiver());
            addBcc(preparedEmail);
            addCc(preparedEmail);
            email.send();
        } catch (EmailException e) {
            log.error("Cannot send message [" + preparedEmail + "]", e);
        }
    }

    private void addBcc(PreparedEmail preparedEmail) throws EmailException {
        List<String> bccEmails = preparedEmail.getBcc();
        if (bccEmails==null){
            return;
        }

        for (String bccEmail :bccEmails){
            email.addBcc(bccEmail);
        }
    }

    private void addCc(PreparedEmail preparedEmail) throws EmailException {
        List<String> ccEmails = preparedEmail.getCc();
        if (ccEmails==null){
            return;
        }

        for (String ccEmail :ccEmails){
            email.addCc(ccEmail);
        }
    }

}
