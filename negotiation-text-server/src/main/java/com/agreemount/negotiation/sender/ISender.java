package com.agreemount.negotiation.sender;

import com.agreemount.negotiation.bean.PreparedEmail;


@FunctionalInterface
public interface ISender {

    void send(PreparedEmail notification);
}
