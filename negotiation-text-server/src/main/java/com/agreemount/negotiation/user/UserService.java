package com.agreemount.negotiation.user;

import com.agreemount.negotiation.auth.Password;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.IUserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class UserService {

    @Autowired
    private IUserDAO iUserDAO;

    public List<User> getUsersOfTeam(Team team, String ommitUserEmail) {
        return team.getUsers2Team().stream()
                .filter(user2Team -> !user2Team.getLogin().equals(ommitUserEmail))
                .map(user2Team -> iUserDAO.getByEmail(user2Team.getLogin()))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public Local setPasswordForUser(Local user) {
        byte[] salt = Password.getSalt();
        byte[] hash = Password.hashPassword(user.getPassword(), salt);

        user.setHash(hash);
        user.setSalt(salt);
        user.setPassword(null);
        user.setConfirmationHash(UUID.randomUUID().toString());

        return user;
    }

}
