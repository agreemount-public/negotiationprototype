package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.member.PartnerService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Scope("request")
@Controller
@Log4j
public class PartnerController {

    @Autowired
    private PartnerService partnerService;

    @RequestMapping(value = "/document/{documentId}/partner/inviter/{inviterEmail:.+}/invitee/{inviteeEmail:.+}",
            method = RequestMethod.POST)
    @ResponseBody
    public Response invitePartner(@PathVariable("inviterEmail") String inviterEmail,
                                  @PathVariable("inviteeEmail") String inviteeEmail,
                                  @PathVariable("documentId") String documentId) {
        Response response = new Response();

        try {
            partnerService.invitePartner(inviterEmail, inviteeEmail, documentId);
            response.setStatus(ResponseType.SUCCESS);
            response.setMessage("Invitation has been sent to partner" + inviteeEmail + " for document " + documentId);
        } catch (Exception e) {
            log.error("Cannot send invitation to partner", e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage("Cannot send invitation to partner => " + e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/document/{documentId}/partners/requestorEmail/{requestorEmail:.+}", method = RequestMethod.GET)
    @ResponseBody
    public Response fetchPartners(@PathVariable("requestorEmail") String requestorEmail,
                                  @PathVariable("documentId") String documentId) {
        Response response = new Response();

        try {
            List<User2Team> members = partnerService.fetchPartners(requestorEmail, documentId);
            response.setStatus(ResponseType.SUCCESS);
            response.setData(members);
        } catch (Exception e) {
            log.error("Cannot execute fetchPartners ", e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage("Cannot execute fetchPartners " + e.getMessage());
        }

        return response;
    }

    @RequestMapping(value = "/document/{documentId}/partner/requestorEmail/{requestorEmail:.+}/partnerEmail/{partnerEmail:.+}",
                    method = RequestMethod.DELETE)
    @ResponseBody
    public Response deletePartner(@PathVariable("requestorEmail") String requestorEmail,
                                  @PathVariable("partnerEmail") String partnerEmail,
                                  @PathVariable("documentId") String documentId) {
        Response response = new Response();

        try {
            partnerService.deletePartner(requestorEmail, partnerEmail, documentId);
            response.setStatus(ResponseType.SUCCESS);
        } catch (Exception e) {
            log.error("Cannot execute fetchPartners ", e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage("Cannot delete partner " + e.getMessage());
        }

        return response;
    }

}