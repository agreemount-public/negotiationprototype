package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.file.FileDocumentLogic;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Log4j
@RestController
public class FileController {

    @Autowired
    private FileDocumentLogic fileDocumentLogic;

    @RequestMapping(value="/negotiation-engine/files", method=RequestMethod.POST)
    @CrossOrigin
    public Response handleFileUpload(@RequestParam("documentName") String documentName,
                                     @RequestParam("file") MultipartFile file){

        Response response = new Response();

        try {
            final String docId = fileDocumentLogic.retrieveAndSave(documentName, file);
            response.setMessage("You successfully uploaded " + file.getOriginalFilename() + "!");
            response.setStatus(ResponseType.SUCCESS);
            response.setData(docId);
        } catch (Exception e) {
            log.error("Cannot retrieve or save file", e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage("Cannot upload file: [" + file.getOriginalFilename()+"]");
        }

        return response;
    }
}