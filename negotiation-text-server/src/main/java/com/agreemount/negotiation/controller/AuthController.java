package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.auth.AuthContext;
import com.agreemount.negotiation.auth.ResetPasswordService;
import com.agreemount.negotiation.bean.ResetPasswordDto;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@Log4j
public class AuthController {

    @Autowired
    private AuthContext authContext;

    @Autowired
    private ResetPasswordService resetPasswordService;

    @RequestMapping(value = "/auth/signUp", method = RequestMethod.POST)
    public Response signUp(@RequestBody User user) {
        Response response = new Response();
        try {
            response.setData(authContext.signUp(user));
            response.setStatus(ResponseType.SUCCESS);
        } catch (Exception ex) {
            log.error("Cannot signUp ", ex);
            response.setStatus(ResponseType.FAILED);
            response.setMessage(ex.getMessage());
        }

        return response;
    }

    @RequestMapping(value = "/auth/confirmResetPassword", method = RequestMethod.POST)
    public Response confirmResetPassword(@RequestBody String email) {
        Response response = new Response();

        try {
            resetPasswordService.resetPasswordConfirmation(email);
            response.setStatus(ResponseType.SUCCESS);
            response.setMessage("Confirmation will be sent shortly");
        } catch (Exception ex) {
            log.error("Cannot reset password ", ex);
            response.setStatus(ResponseType.FAILED);
            response.setMessage(ex.getMessage());
        }

        return response;
    }

    @RequestMapping(value = "/auth/resetPassword", method = RequestMethod.POST)
    public Response resetPassword(@RequestBody ResetPasswordDto resetPasswordDto) {
        Response response = new Response();

        try {
            resetPasswordService.resetPassword(resetPasswordDto);
            response.setStatus(ResponseType.SUCCESS);
            response.setMessage("Password has been successfully reset");
        } catch (Exception ex) {
            log.error("Cannot reset password ", ex);
            response.setStatus(ResponseType.FAILED);
            response.setMessage(ex.getMessage());
        }

        return response;
    }

    @RequestMapping(value = "/auth/login", method = RequestMethod.GET)
    public Response login(
            @RequestHeader("email") String email,
            @RequestHeader("password") char[] password) {
        Response response = new Response();
        try {
            User user = authContext.login(email, password);
            response.setData(user);
            response.setStatus(ResponseType.SUCCESS);
        } catch (Exception ex) {
            log.error("Cannot login ", ex);
            response.setStatus(ResponseType.FAILED);
            response.setMessage(ex.getMessage());
        }

        return response;
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public Response activateAccount(@RequestParam("hash") String hash) {
        Response response = new Response();
        try {
            boolean isActivated = authContext.activateAccount(hash);
            response.setData(isActivated);
            response.setStatus(ResponseType.SUCCESS);
        } catch (Exception ex) {
            log.error("Cannot activate account ", ex);
            response.setStatus(ResponseType.FAILED);
            response.setMessage(ex.getMessage());
        }

        return response;
    }
}