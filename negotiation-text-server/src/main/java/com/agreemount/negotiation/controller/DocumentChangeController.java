package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.document.change.IDocumentChange;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Log4j
@Controller
class DocumentChangeController {

    private static final String MSG_UPDATE_SUCCESS = "Document successfully updated";
    private static final String MSG_UPDATE_FAILD = "Document update failed";

    @Autowired
    private IDocumentChange documentChange;

    @RequestMapping(value = "document", method = RequestMethod.PUT)
    @ResponseBody
    Response update(@RequestParam("documentId") String documentId,
                    @RequestParam("content") String content,
                    @RequestParam("positionStart") int positionStart,
                    @RequestParam("positionEnd") int positionEnd) {

        Response res = new Response();
        res.setStatus(ResponseType.SUCCESS);
        res.setData(documentId);
        res.setMessage(MSG_UPDATE_SUCCESS);

        try {
            documentChange.save(documentId, content, positionStart, positionEnd);
        } catch (Exception e) {
            log.error("Can not update document " + documentId, e);
            res.setStatus(ResponseType.FAILED);
            res.setData(e.getMessage());
            res.setMessage(MSG_UPDATE_FAILD);
        }

        return res;
    }

}