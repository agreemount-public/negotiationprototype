package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.member.SideService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Scope("request")
@Controller
@Log4j
public class SideController {

    @Autowired
    private SideService sideService;

    @RequestMapping(value = "/document/{documentId}/side/inviter/{inviterEmail:.+}/invitee/{inviteeEmail:.+}", method = RequestMethod.POST)
    @ResponseBody
    public Response inviteSide(@PathVariable("inviterEmail") String inviterEmail,
                               @PathVariable("inviteeEmail") String inviteeEmail,
                               @PathVariable("documentId") String documentId) {
        Response response = new Response();

        try {
            sideService.inviteSide(inviterEmail, inviteeEmail, documentId);
            response.setStatus(ResponseType.SUCCESS);
            response.setMessage("Invitation has been sent to side" + inviteeEmail + " for document " + documentId);
        } catch (Exception e) {
            log.error("Cannot send invitation ", e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage("Cannot send invitation to side => " + e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/document/{documentId}/sides", method = RequestMethod.GET)
    @ResponseBody
    public Response fetchSides(@PathVariable("documentId") String documentId) {
        Response response = new Response();

        try {
            List<User2Team> members = sideService.fetchSides(documentId);
            response.setStatus(ResponseType.SUCCESS);
            response.setData(members);
        } catch (Exception e) {
            log.error("Cannot execute fetchSides ", e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage("Cannot execute fetchSides " + e.getMessage());
        }
        return response;
    }

}
