package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.logic.IPaymentLogic;
import com.agreemount.negotiation.util.payment.PayPalConfiguration;
import com.paypal.ipn.IPNMessage;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/paypal")
@Log4j
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PayPalController {

    private IPNMessage ipnListener;

    @Autowired
    private IPaymentLogic paymentLogic;

    @Autowired
    private Environment environment;

    @Autowired
    private PayPalConfiguration payPalConfiguration;

    @RequestMapping(value = "/listener", method = RequestMethod.POST)
    public void listener(HttpServletRequest request) {
        log.info("[PayPal] Listener start");

        initializeIpnListener(request);
        if (ipnListener.validate()) {
            log.info("[PayPal] Listener ipnListener.validate() == TRUE");
            paymentLogic.changeAccountBalance(ipnListener.getIpnMap());
        }

        log.info("[PayPal] Listener end");
    }

    @RequestMapping(value = "/env", method = RequestMethod.GET)
    public String environment() {
        return environment.getProperty("spring.profiles.active");
    }

    private void initializeIpnListener(HttpServletRequest request) {
        if (ipnListener == null) {
            log.info("[PayPal] payPalConfiguration.getConfig(): ["+payPalConfiguration.getConfig()+"]");
            ipnListener = new IPNMessage(request, payPalConfiguration.getConfig());
        }
    }

}
