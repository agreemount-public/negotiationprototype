package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.logic.impl.AgreemountDocumentLogic;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Log4j
@RestController
public class EmptyDocumentController {

    @Autowired
    private AgreemountDocumentLogic agreemountDocumentLogic;

    @RequestMapping(value = "/document/empty/{negotiationType}", method = RequestMethod.POST)
    @CrossOrigin
    public Response create(@PathVariable("negotiationType") String negotiationType) {

        Response response = new Response();

        try {
            String docId = agreemountDocumentLogic.saveEmptyDocument(negotiationType);
            response.setMessage("Document has been created successfully");
            response.setStatus(ResponseType.SUCCESS);
            response.setData(docId);
        } catch (Exception e) {
            log.error("Cannot create document", e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage("Cannot create document");
        }

        return response;
    }

}
