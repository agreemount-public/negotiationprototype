package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.auth.AuthContext;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/users")
@Log4j
public class UserController {

    @Autowired
    private AuthContext authContext;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Response fetchUserById(@PathVariable String id) {
        Response response = new Response();
        try {
            User user = authContext.getUser(id);
            if (user != null) {
                response.setData(user);
                response.setStatus(ResponseType.SUCCESS);
            } else {
                response.setStatus(ResponseType.FAILED);
            }
        } catch (Exception ex) {
            log.error("Cannot fetch user by id: " + id, ex);
            response.setStatus(ResponseType.FAILED);
            response.setMessage(ex.getMessage());
        }

        return response;
    }

}
