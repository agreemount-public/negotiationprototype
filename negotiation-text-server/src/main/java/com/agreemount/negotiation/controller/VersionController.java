package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/negotiation-engine/version")
public class VersionController {

    @Value("${app.version}")
    private String currentVersion;

    @Autowired
    private Environment environment;

    @RequestMapping(method = RequestMethod.GET)
    public Response fetchCurrentVersion() {
        Response response = new Response();
        response.setStatus(ResponseType.SUCCESS);
        response.setData(currentVersion);
        return response;
    }

    @RequestMapping(value = "/env", method = RequestMethod.GET)
    public String environment() {
        return environment.getProperty("spring.profiles.active");
    }
}
