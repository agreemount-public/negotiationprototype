package com.agreemount.negotiation.controller;

import com.agreemount.EngineFacade;
import com.agreemount.Response;
import com.agreemount.bean.document.Document;
import com.agreemount.bean.document.DocumentAndActionsWrapper;
import com.agreemount.bean.response.ActionResponse;
import com.agreemount.logic.ActionLogic;
import com.agreemount.negotiation.bean.NegotiationOnDashboard;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.document.DocumentMergerFacade;
import com.agreemount.negotiation.document.action.IDocumentAction;
import com.agreemount.negotiation.document.converter.IConverter;
import com.agreemount.negotiation.document.renderer.DocumentChangeRenderer;
import com.agreemount.negotiation.interaction.notification.dao.InteractionNotificationDAO;
import com.agreemount.negotiation.logic.impl.AgreemountDocumentLogic;
import com.agreemount.negotiation.merge.DocumentWithBunchOfStuff;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.action.definition.Action;
import com.agreemount.slaneg.db.DocumentOperations;
import com.agreemount.slaneg.exception.ForbiddenException;
import com.agreemount.slaneg.message.Message;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j
@Controller
class NegotiationController {
    @Autowired
    @Qualifier(value = "nl2brDocumentBeanConverter")
    private IConverter<Document, Document> nl2brDocumentBeanConverter;

    @Autowired
    private DocumentChangeRenderer documentChangeRenderer;

    @Autowired
    private DocumentMergerFacade documentMergerFacade;

    @Autowired
    private DocumentOperations documentOperations;

    @Autowired
    private ActionLogic actionLogic;

    @Autowired
    private EngineFacade engineFacade;

    @Autowired
    @Qualifier(value = "acceptChange")
    private IDocumentAction acceptChangeAction;

    @Autowired
    @Qualifier(value = "rejectChange")
    private IDocumentAction rejectChangeAction;

    @Autowired
    @Qualifier(value = "cancelChange")
    private IDocumentAction cancelChangeAction;

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Autowired
    private AgreemountDocumentLogic agreemountDocumentLogic;

    @RequestMapping(value = "document/merged", method = RequestMethod.GET)
    @ResponseBody
    public Response<DocumentWithBunchOfStuff> merge(@RequestParam String documentId) {

        Document negotiationDocument = findNegotiationDocument(documentId);
        List<Action> availableActions = engineFacade.getActionsAvailableForDocument(negotiationDocument);

        DocumentWithBunchOfStuff wrapper = new DocumentWithBunchOfStuff();
        wrapper.setDocument(nl2brDocumentBeanConverter.convert(negotiationDocument));
        wrapper.setActions(availableActions);
        wrapper.setSubDocuments(documentChangeRenderer.render(negotiationDocument));
        wrapper.setMergedDocumentView(documentMergerFacade.merge(negotiationDocument));

        Response<DocumentWithBunchOfStuff> response = new Response<>();
        response.setData(wrapper);

        return response;

    }

    @RequestMapping(value = "document/action", method = RequestMethod.PUT)
    @ResponseBody
    Response<ActionResponse> invokeAction(@RequestParam String actionId, @RequestParam String documentId) {

        log.info("invokeAction for documentId " + documentId);

        Response<ActionResponse> response = new Response<>();

        //perform action
        Document change = documentOperations.getDocument(documentId);
        Preconditions.checkNotNull(change, "Document was not found, document id [%s]", documentId);
        Action action = actionLogic.getActionById(actionId);
        Preconditions.checkNotNull(action, "Action was not found, action id [%s]", actionId);
        actionLogic.runAction(change, action, null);

        if ("acceptChange".equalsIgnoreCase(action.getId())) {
            response = acceptChangeAction.apply(change);
        } else if ("rejectChange".equalsIgnoreCase(action.getId())) {
            response = rejectChangeAction.apply(change);
        } else if ("cancelChange".equalsIgnoreCase(action.getId())) {
            response = cancelChangeAction.apply(change);
        }

        return response;
    }

    @RequestMapping(value = "document/get-with-messages-and-actions/{documentId}", method = RequestMethod.GET)
    @ResponseBody
    Response<DocumentAndActionsWrapper> getDocumentWithMessagesAndAcstions(@PathVariable String documentId) {
        Document document = findNegotiationDocument(documentId);

        checkDocumentVisibility(document);

        List<Message> messages = engineFacade.getAvailableMessages(actionContextFactory.createInstance(document));
        List<Action> actions = engineFacade.getActionsAvailableForDocument(document);

        DocumentAndActionsWrapper documentAndActionsWrapper = new DocumentAndActionsWrapper();
        documentAndActionsWrapper.setDocument(document);
        documentAndActionsWrapper.setMessages(messages);
        documentAndActionsWrapper.setActions(actions);

        return new Response<>(documentAndActionsWrapper);
    }

    @RequestMapping(value = "/negotiations", method = RequestMethod.GET)
    @ResponseBody
    public com.agreemount.negotiation.bean.communication.Response getDashboardData() {
        com.agreemount.negotiation.bean.communication.Response response = new com.agreemount.negotiation.bean.communication.Response();

        try {
            List<NegotiationOnDashboard> negotiationOnDashboards = agreemountDocumentLogic.fetchDashboardData();
            response.setData(negotiationOnDashboards);
            response.setStatus(ResponseType.SUCCESS);
        } catch (Exception e) {
            log.error("Cannot fetch documents ", e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage("Cannot fetch documents => " + e.getMessage());
        }
        return response;
    }

    private void checkDocumentVisibility(Document document) {
        if (!engineFacade.isConstraintAvailable("documentVisibilityConstraint", actionContextFactory.createInstance(document))) {
            throw new ForbiddenException("User is not allowed to see document " + document.getId());
        }
    }

    private Document findNegotiationDocument(String documentId) {
        Document document = documentOperations.getDocument(documentId);
        Preconditions.checkNotNull(document, "Can not find negotiation case by documentId[" + documentId + "]");
        return document;
    }
}
