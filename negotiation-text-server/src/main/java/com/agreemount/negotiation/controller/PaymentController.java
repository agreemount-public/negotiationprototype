package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.auth.AuthContext;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.communication.Response;
import com.agreemount.negotiation.bean.communication.ResponseType;
import com.agreemount.negotiation.logic.impl.PaymentLogic;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/payment")
@Log4j
public class PaymentController {

    private static final String MSG_CANNOT_FETCH_USER_ACCOUNT_BALANCE = "Cannot fetch user account balance";
    private static final String MSG_CANNOT_CHANGE_USER_ACCOUNT_BALANCE = "Cannot change user account balance";
    private static final String MSG_SUCCESSFULLY_CHANGED_USER_ACCOUNT_BALANCE = "Successfully changed user account balance";

    @Autowired
    private AuthContext authContext;

    @Autowired
    private PaymentLogic paymentLogic;

    @RequestMapping(value = "/account/{userId}/balance", method = RequestMethod.GET)
    public Response getAccountBalance(@PathVariable String userId) {
        Response response = new Response();

        try {
            User user = authContext.getUser(userId);
            response.setStatus(ResponseType.SUCCESS);
            response.setData(user.getPaymentUnits());
        } catch (Exception e) {
            log.error(MSG_CANNOT_FETCH_USER_ACCOUNT_BALANCE, e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage(MSG_CANNOT_FETCH_USER_ACCOUNT_BALANCE);
        }

        return response;
    }

    /**
     * @todo: protect this action with password.... NS-145
     */
//    @RequestMapping(value = "/account/{userId}/balance/{paymentUnits}", method = RequestMethod.PUT)
    public Response changeAccountBalance(@PathVariable String userId, @PathVariable Long paymentUnits) {
        Response response = new Response();

        try {
            paymentLogic.changeAccountBalanceManually(userId, paymentUnits);
            response.setMessage(MSG_SUCCESSFULLY_CHANGED_USER_ACCOUNT_BALANCE);
            response.setStatus(ResponseType.SUCCESS);
        } catch (Exception e) {
            log.error(MSG_CANNOT_CHANGE_USER_ACCOUNT_BALANCE, e);
            response.setStatus(ResponseType.FAILED);
            response.setMessage(MSG_CANNOT_CHANGE_USER_ACCOUNT_BALANCE);
        }

        return response;
    }
}
