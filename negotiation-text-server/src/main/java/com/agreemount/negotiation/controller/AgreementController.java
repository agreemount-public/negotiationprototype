package com.agreemount.negotiation.controller;

import com.agreemount.negotiation.agreement.template.AgreementFileGenerator;
import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.agreement.template.converter.DocxToPdfConverter;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;

@Log4j
@RestController
public class AgreementController {

    private static final String WORD_MEDIA_TYPE = "application/pdf";
    private static final String AGREEMENT_FILENAME = "Agreement.pdf";

    @Autowired
    private AgreementFileGenerator agreementFileGenerator;

    @Autowired
    private DocxToPdfConverter docxToPdfConverter;

    @RequestMapping(value = "agreement/{documentId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> download(@PathVariable String documentId) {
        try {
            Path path = docxToPdfConverter.convert(agreementFileGenerator.generate(documentId, Region.MY_DIVORCE));
            byte[] contents = Files.readAllBytes(path);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType(WORD_MEDIA_TYPE));
            headers.setContentDispositionFormData(AGREEMENT_FILENAME, AGREEMENT_FILENAME);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
            return response;
        } catch (Exception e) {
            log.error(format("Cannot download %s document due to an exception:", AGREEMENT_FILENAME), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}