package com.agreemount.negotiation.divorce;


public class NewItem {
    private String type;
    private boolean isEmotional;
    private boolean isBeforeMarriage;
    private String name;
    private String category;
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEmotional() {
        return isEmotional;
    }

    public void setEmotional(boolean emotional) {
        isEmotional = emotional;
    }

    public boolean isBeforeMarriage() {
        return isBeforeMarriage;
    }

    public void setBeforeMarriage(boolean beforeMarriage) {
        isBeforeMarriage = beforeMarriage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
