package com.agreemount.negotiation.divorce.controller;

import com.agreemount.EngineFacade;
import com.agreemount.Response;
import com.agreemount.bean.document.Document;
import com.agreemount.logic.ConstraintLogic;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.db.DocumentOperations;
import com.agreemount.slaneg.exception.ForbiddenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
class DivorceController {

    @Autowired
    private DocumentOperations documentOperations;

    @Autowired
    private ConstraintLogic constraintLogic;

    @Autowired
    private ActionContextFactory<?> actionContextFactory;

    @Autowired
    private EngineFacade engineFacade;


    /**
     * @param documentId
     * @return
     * @deprecated
     */
    @Deprecated
    @RequestMapping(value = "document/{documentId}", method = RequestMethod.GET)
    @ResponseBody
    Response<Document> getDocument(@PathVariable String documentId) {

        Document document = documentOperations.getDocument(documentId);
        checkDocumentVisibility(document);

        return new Response<>(document);
    }


    @RequestMapping(value = "divorce/new-item/{documentId}", method = RequestMethod.POST)
    @ResponseBody
    Response<Document> addItem(@PathVariable String documentId) {

        Document document = documentOperations.getDocument(documentId);
        ActionContext actionContext = actionContextFactory.createInstance();
        actionContext.addDocument("BASE", document);


        actionContext.setMetricValues(new HashMap<String, Object>());

        ActionContext result = engineFacade.runAction(actionContext, "createItem");

        return new Response<>(result.getDocument("doc1"));
    }


    private void checkDocumentVisibility(Document document) {
        if (!constraintLogic.isAvailable("documentVisibilityConstraint", actionContextFactory.createInstance(document))) {
            throw new ForbiddenException("User is not allowed to see document " + document.getId());
        }
    }
}