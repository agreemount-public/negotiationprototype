package com.agreemount.negotiation.configuration;

import com.agreemount.negotiation.Application;
import com.agreemount.slaneg.fixtures.FileRulesProvidersConfiguration;
import lombok.extern.log4j.Log4j;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import java.util.Map;

@ComponentScan(
        basePackages = {"com.agreemount"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Application.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = FileRulesProvidersConfiguration.class)
        }
)
@Configuration
@Log4j
public class CronConfiguration {

    public static final long ONE_MINUTE_IN_MS = 60000;

    @Autowired
    private ApplicationContext appContext;

    @Bean(name = "schedulerFactory")
    public SchedulerFactoryBean schedulerFactoryBean() {
        Map<String, SimpleTriggerFactoryBean> cronTriggers = appContext.getBeansOfType(SimpleTriggerFactoryBean.class);

        cronTriggers.forEach((triggerName, trigger) -> log.debug("Cron trigger [" + triggerName + "] has been found"));

        Trigger[] triggers = cronTriggers.values()
                .stream()
                .map(simpleTriggerFactoryBean -> simpleTriggerFactoryBean.getObject())
                .toArray(value -> new Trigger[cronTriggers.size()]);

        SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
        scheduler.setTriggers(triggers);
        return scheduler;
    }
}
