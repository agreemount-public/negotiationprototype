package com.agreemount.negotiation.merge;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.slaneg.action.definition.Action;

import java.util.List;

@lombok.Getter
@lombok.Setter
@lombok.ToString
@lombok.EqualsAndHashCode
public class DocumentWithBunchOfStuff {

    private Document document;
    private List<Action> actions;
    private List<Document> subDocuments;

    private DocumentView mergedDocumentView;

    //TODO add merger result here
}
