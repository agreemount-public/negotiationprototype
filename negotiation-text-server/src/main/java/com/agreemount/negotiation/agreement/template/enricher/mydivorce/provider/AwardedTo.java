package com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider;

/**
 * Created by damian on 23.10.18.
 */
public enum AwardedTo {
    SPOUSE_1, SPOUSE_2, COMMUNITY
}
