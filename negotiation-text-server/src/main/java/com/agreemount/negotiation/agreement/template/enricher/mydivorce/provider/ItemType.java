package com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider;

/**
 * Created by damian on 23.10.18.
 */
public enum ItemType {
    ASSET, DEBT
}
