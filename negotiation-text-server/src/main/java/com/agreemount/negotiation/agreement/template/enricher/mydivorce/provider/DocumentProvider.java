package com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider;

import com.agreemount.bean.document.Document;
import com.agreemount.engine.facade.QueryFacade;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.*;
import static com.agreemount.negotiation.bean.enums.States.DOCUMENT_TYPE;
import static com.agreemount.negotiation.bean.enums.States.ITEM_TYPE;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.StringUtils.EMPTY;

@Component
public class DocumentProvider {
    private static final String AGREED_ITEMS_IN_SPLIT = "AgreedItemsInSplit";
    private static final String AGREED_ITEMS_AWARDED_TO_SIDE_1 = "AgreedItemsAwardedToSIDE1";
    private static final String AGREED_ITEMS_AWARDED_TO_SIDE_2 = "AgreedItemsAwardedToSIDE2";

    private static final Map<AwardedTo, String> queryNameMap = ImmutableMap.of(
            COMMUNITY, AGREED_ITEMS_IN_SPLIT,
            SPOUSE_1, AGREED_ITEMS_AWARDED_TO_SIDE_1,
            SPOUSE_2, AGREED_ITEMS_AWARDED_TO_SIDE_2);

    @Autowired
    private QueryFacade queryFacade;

    @Autowired
    private ActionContextFactory actionContextFactory;

    public List<Document> getAgreedDocuments(Document negotiation, ItemType assetOrDebt, AwardedTo awardedTo) {
        return assetsOrDebtsStream(negotiation, queryNameMap.get(awardedTo))
                .filter(matchItemType(assetOrDebt))
                .collect(toList());
    }

    private Stream<Document> assetsOrDebtsStream(Document negotiation, String queryName) {
        return queryFacade.getDocumentsForQuery(queryName, actionContextFactory.createInstance(negotiation))
                .stream()
                .filter(isNegotiationItem());
    }

    private Predicate<Document> isNegotiationItem() {
        return document -> document.getStates().getOrDefault(DOCUMENT_TYPE.getValue(), EMPTY).equals("item");
    }

    private Predicate<Document> matchItemType(ItemType assetOrDebt) {
        return document -> document.getStates().getOrDefault(ITEM_TYPE.getValue(), EMPTY).equalsIgnoreCase(assetOrDebt.name());
    }

}
