package com.agreemount.negotiation.agreement.template.enricher.mydivorce;

import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.agreement.template.StaticDataEnricher;
import com.agreemount.negotiation.util.LocalDateTimeProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Component
public class MyDivorceStaticDataEnricher implements StaticDataEnricher {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
    
    @Autowired
    private LocalDateTimeProvider localDateTimeProvider;

    @Override
    public Region getRegion() {
        return Region.MY_DIVORCE;
    }

    public void dateOfAgreement(Map<String, Object> placeholderValueMap) {
        LocalDateTime localDateTime = localDateTimeProvider.now();
        placeholderValueMap.put("${dateTime.of.agreement}", localDateTime.format(DATE_TIME_FORMATTER));
    }
}