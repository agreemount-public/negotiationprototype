package com.agreemount.negotiation.agreement.template;

import java.util.Map;

/**
 * Created by damian on 13.10.18.
 */
public interface StaticDataEnricher extends RegionAware {
    void dateOfAgreement(Map<String, Object> placeholderValueMap);
}
