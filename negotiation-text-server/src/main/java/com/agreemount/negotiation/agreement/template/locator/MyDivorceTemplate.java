package com.agreemount.negotiation.agreement.template.locator;

import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.agreement.template.TemplateLocator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by damian on 13.10.18.
 */
@Service
public class MyDivorceTemplate implements TemplateLocator {
    @Value("${mydivorce.agreement.template.uri}")
    String agreementTemplateURI;

    @Override
    public String getAgreementTemplateURI() {
        return agreementTemplateURI;
    }

    @Override
    public Region getRegion() {
        return Region.MY_DIVORCE;
    }
}
