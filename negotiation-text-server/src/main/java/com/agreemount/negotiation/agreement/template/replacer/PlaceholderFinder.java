package com.agreemount.negotiation.agreement.template.replacer;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class PlaceholderFinder {

    private static String PLACEHOLDER_REGEXP = "\\$\\{(([A-Za-z.@ /]+[_0-9]*)+[\\[\\d+\\]]*)\\}";

    public Set<String> findIn(String text) {
        Set<String> placeholders = new HashSet<>();
        Pattern pattern = Pattern.compile(PLACEHOLDER_REGEXP);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            String group = matcher.group();
            placeholders.add(group);
        }

        return placeholders;
    }
}
