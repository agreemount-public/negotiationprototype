package com.agreemount.negotiation.agreement.template.docx.sandbox;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Service;

@Service
public class DocxService {

    public XWPFDocument createEmpty() {
        return new XWPFDocument();
    }

}
