package com.agreemount.negotiation.agreement.template.replacer;

import lombok.extern.log4j.Log4j;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTNumbering;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Component()
@Scope("prototype")
@Log4j
public class PlaceholderReplacer {

    private static final String DEFAULT_FILL_IN = "_";
    private static final int INT = 2;
    private static final int DEFAULT_FONT_SIZE = 10;
    private static final String DEFAULT_FONT_FAMILY = "Times New Roman";
    private static final String DEFAULT_COLOR = "262626";
    private static String cTAbstractNumBulletXML =
            "<w:abstractNum xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" w:abstractNumId=\"0\">"
                    + "<w:multiLevelType w:val=\"hybridMultilevel\"/>"
                    + "<w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Wingdings\" w:hAnsi=\"Wingdings\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "<w:lvl w:ilvl=\"1\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"-\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Courier New\" w:hAnsi=\"Courier New\" w:cs=\"Courier New\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "<w:lvl w:ilvl=\"2\" w:tentative=\"1\"><w:start w:val=\"1\"/><w:numFmt w:val=\"bullet\"/><w:lvlText w:val=\"\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"360\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Symbol\" w:hAnsi=\"Symbol\" w:hint=\"default\"/></w:rPr></w:lvl>"
                    + "</w:abstractNum>";

    @Autowired
    private PlaceholderFinder placeholderFinder;

    private CTNumbering cTNumbering = CTNumbering.Factory.parse(cTAbstractNumBulletXML);
    private CTAbstractNum cTAbstractNum = cTNumbering.getAbstractNumArray(0);
    private XWPFAbstractNum abstractNum1 = new XWPFAbstractNum(cTAbstractNum);
    private XWPFNumbering numbering;
    private Map<XWPFRun, Collection<String>> todo = new HashMap<>();


    public PlaceholderReplacer() throws XmlException {
    }

    public XWPFDocument replace(Map<String, Object> placeholderValueModel, XWPFDocument document) {
        numbering = document.createNumbering();
        Set<String> documentPlaceholders = findPlaceholders(document);
        logMissingPlaceholders(placeholderValueModel, documentPlaceholders);

        Map<String, Object> allDocumentPlaceholdersWithValues = fillInMissingPlaceholders(
                placeholderValueModel, documentPlaceholders);


        for (XWPFParagraph paragraph: document.getParagraphs()) {
            findAndReplace(paragraph, allDocumentPlaceholdersWithValues);
        }

        for (Map.Entry<XWPFRun, Collection<String>> entry : todo.entrySet()) {
            BigInteger abstractNumID1 = numbering.addAbstractNum(abstractNum1);
            BigInteger numID1 = numbering.addNum(abstractNumID1);

            XWPFParagraph paragraph = entry.getKey().getParagraph();

            for (String value : entry.getValue()) {
                XmlCursor cursor = paragraph.getCTP().newCursor();
                XWPFParagraph newParagraph = document.insertNewParagraph(cursor);
                newParagraph.setNumID(numID1);
                XWPFRun run = newParagraph.createRun();
                run.setText(value);
            }

            removePlaceholderRuns(paragraph, 0, paragraph.getRuns().size() - 1);
        }

        return document;
    }

    private void findAndReplace(XWPFParagraph paragraph, Map<String, Object> placeholderValueModel) {
        for (Map.Entry<String, Object> plaholderEntry : placeholderValueModel.entrySet()) {
            findAndReplace(paragraph, plaholderEntry.getKey(), plaholderEntry.getValue());
        }
    }

    private void findAndReplace(XWPFParagraph xwpfParagraph, String placeholder, Object values) {
        TextSegement placeholderTextSegment = xwpfParagraph.searchText(placeholder, new PositionInParagraph());
        if (placeholderTextSegment != null) {

            int beginPosRun = placeholderTextSegment.getBeginRun();
            int endPosRun = placeholderTextSegment.getEndRun();

            if (values instanceof Collection) {
                Collection<String> value = (Collection<String>) values;
                XWPFRun beginRunOfPlaceholder = xwpfParagraph.getRuns().get(beginPosRun);

                todo.put(beginRunOfPlaceholder, value);
            }
            else {
                String value = String.valueOf(values);
                String placeholderRunsText = collectTextFromRuns(xwpfParagraph, beginPosRun, endPosRun);
                String replacedPlaceholderByValue = placeholderRunsText.replace(placeholder, value);
                XWPFRun beginRunOfPlaceholder = xwpfParagraph.getRuns().get(beginPosRun);

                XWPFRun newRun = xwpfParagraph.insertNewRun(beginPosRun);
                beginPosRun++;
                endPosRun++;
                newRun.setText(replacedPlaceholderByValue);

                copyStyles(beginRunOfPlaceholder, newRun);
                removePlaceholderRuns(xwpfParagraph, beginPosRun, endPosRun);
            }

        }
    }

    private Set<String> findPlaceholders(XWPFDocument document) {
        String documentText = new XWPFWordExtractor(document).getText();
        return placeholderFinder.findIn(documentText);
    }

    private void removePlaceholderRuns(XWPFParagraph xwpfParagraph, int beginRun, int endRun) {
        for (int i = endRun; i >= beginRun; i--) {
            xwpfParagraph.removeRun(i);
        }
    }

    private String collectTextFromRuns(XWPFParagraph xwpfParagraph, int beginRun, int endRun) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = beginRun; i <= endRun; i++) {
            XWPFRun xwpfRun = xwpfParagraph.getRuns().get(i);
            String runText = xwpfRun.getText(xwpfRun.getTextPosition());
            stringBuilder.append(runText);
        }
        return stringBuilder.toString();
    }

    private void copyStyles(XWPFRun fromRun, XWPFRun toRun) {
        toRun.setBold(fromRun.isBold());
        toRun.setCapitalized(fromRun.isCapitalized());
        toRun.setColor(Objects.isNull(fromRun.getColor()) ? DEFAULT_COLOR : fromRun.getColor());
        toRun.setDoubleStrikethrough(fromRun.isDoubleStrikeThrough());
        toRun.setEmbossed(fromRun.isEmbossed());
        toRun.setFontFamily(Objects.isNull(fromRun.getFontFamily()) ? DEFAULT_FONT_FAMILY : fromRun.getFontFamily());
        toRun.setFontSize(fromRun.getFontSize() == -1 ? DEFAULT_FONT_SIZE : fromRun.getFontSize());
        toRun.setImprinted(fromRun.isImprinted());
        toRun.setItalic(fromRun.isItalic());
        toRun.setKerning(fromRun.getKerning());
        toRun.setShadow(fromRun.isShadowed());
        toRun.setSmallCaps(fromRun.isSmallCaps());
        toRun.setStrikeThrough(fromRun.isStrikeThrough());
        toRun.setSubscript(fromRun.getSubscript());
        toRun.setUnderline(fromRun.getUnderline());
    }

    private Map<String, Object> fillInMissingPlaceholders(
            Map<String, Object> placeholderValueMap, Set<String> documentPlaceholders) {
        return documentPlaceholders
                .stream()
                .collect(Collectors.toMap(
                        Function.identity(),
                        placeholder -> placeholderValueMap.getOrDefault(placeholder, defaultValue(placeholder))
                ));
    }

    private String defaultValue(String placeholderKey) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < placeholderKey.length(); i++) {
            stringBuilder.append(DEFAULT_FILL_IN);
        }

        return stringBuilder.toString();
    }

    private void logMissingPlaceholders(Map<String, Object> placeholderValueMap, Set<String> documentPlaceholders) {
        Set<String> missingDataForDocumentPlaceholder = documentPlaceholders
                .stream()
                .filter(documentPlaceholder -> !placeholderValueMap.containsKey(documentPlaceholder))
                .collect(Collectors.toSet());

        if (!missingDataForDocumentPlaceholder.isEmpty()) {
            log.warn(String.format("Missing data for document placeholder:%s Each placeholder is replaced by ___," +
                    " provide data enrichment if applicable", missingDataForDocumentPlaceholder.toString()));
        }

        Set<String> missingDocumentPlaceholder = placeholderValueMap.keySet()
                .stream()
                .filter(modelPlaceholder -> !documentPlaceholders.contains(modelPlaceholder))
                .collect(Collectors.toSet());

        if (!missingDocumentPlaceholder.isEmpty()) {
            log.warn(String.format("There is no placeholders:%s in document content",
                    missingDocumentPlaceholder.toString()));
        }

    }
}
