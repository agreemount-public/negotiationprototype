package com.agreemount.negotiation.agreement.template.enricher;

import org.springframework.stereotype.Component;

import java.text.NumberFormat;
import java.util.Currency;

import static java.util.Locale.US;
import static urn.ebay.apis.eBLBaseComponents.CurrencyCodeType.USD;

@Component
public class USDValueFormatter {

    private NumberFormat format;

    public USDValueFormatter() {
        Currency usd = Currency.getInstance(String.valueOf(USD));
        format = java.text.NumberFormat.getCurrencyInstance(US);
        format.setCurrency(usd);
    }

    public String format(Integer value) {
        return format.format(value);
    }
}
