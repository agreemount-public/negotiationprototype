package com.agreemount.negotiation.agreement.template;

import com.agreemount.bean.document.Document;

import javax.validation.constraints.NotNull;
import java.util.Map;

public interface AgreementModelEnricher extends RegionAware {
    void enrich(Map<String, Object> placeholderValueMap, @NotNull Document negotiation);
}
