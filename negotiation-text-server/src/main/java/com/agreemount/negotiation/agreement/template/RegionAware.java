package com.agreemount.negotiation.agreement.template;

/**
 * Created by damian on 13.10.18.
 */
public interface RegionAware {
    Region getRegion();
}
