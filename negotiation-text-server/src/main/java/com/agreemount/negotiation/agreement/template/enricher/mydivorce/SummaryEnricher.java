package com.agreemount.negotiation.agreement.template.enricher.mydivorce;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.AgreementModelEnricher;
import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.agreement.template.enricher.USDValueFormatter;
import com.agreemount.negotiation.summary.Summary;
import com.agreemount.negotiation.summary.SummaryCalculator;
import com.agreemount.negotiation.summary.SummaryDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

@Component
public class SummaryEnricher implements AgreementModelEnricher {

    @Autowired
    private SummaryCalculator summaryCalculator;

    @Autowired
    private SummaryDataService summaryDataService;

    @Autowired
    private USDValueFormatter usdValueFormatter;

    @Override
    public Region getRegion() {
        return Region.MY_DIVORCE;
    }

    @Override
    public void enrich(Map<String, Object> placeholderValueMap, @NotNull Document negotiation) {
        Summary summary = summaryCalculator.buildSummary(summaryDataService.getAgreedItems(negotiation));

        placeholderValueMap.put("${summary.totalToDivide.total}", toUsd(summary.getTotalToDivide().getTotal()));
        placeholderValueMap.put("${summary.totalToDivide.assets}",toUsd(summary.getTotalToDivide().getAssets()));
        placeholderValueMap.put("${summary.totalToDivide.debts}", toUsd(summary.getTotalToDivide().getDebts()));

        placeholderValueMap.put("${summary.spouse1.total}", toUsd(summary.getSide1().getTotal()));
        placeholderValueMap.put("${summary.spouse1.percentage}", formaPercentage(summary.getSide1().getPercentage()));
        placeholderValueMap.put("${summary.spouse1.assets}", toUsd(summary.getSide1().getAssets()));
        placeholderValueMap.put("${summary.spouse1.debts}", toUsd(summary.getSide1().getDebts()));

        placeholderValueMap.put("${summary.spouse2.total}", toUsd(summary.getSide2().getTotal()));
        placeholderValueMap.put("${summary.spouse2.percentage}",formaPercentage(summary.getSide2().getPercentage()));
        placeholderValueMap.put("${summary.spouse2.assets}", toUsd(summary.getSide2().getAssets()));
        placeholderValueMap.put("${summary.spouse2.debts}", toUsd(summary.getSide2().getDebts()));
    }

    private String formaPercentage(BigDecimal percentage) {
        return Optional.ofNullable(percentage).map(percent -> percent + "%").orElse("--%");
    }

    private String toUsd(Integer value) {
        return usdValueFormatter.format(value);
    }
}
