package com.agreemount.negotiation.agreement.template;

/**
 * Created by damian on 17.10.18.
 */
public interface TemplateLocator extends RegionAware {
    String getAgreementTemplateURI();
}
