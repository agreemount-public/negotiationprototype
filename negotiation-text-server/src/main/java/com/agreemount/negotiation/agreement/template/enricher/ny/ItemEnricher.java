package com.agreemount.negotiation.agreement.template.enricher.ny;

import com.agreemount.bean.document.Document;
import com.agreemount.engine.facade.QueryFacade;
import com.agreemount.negotiation.agreement.template.AgreementModelEnricher;
import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.util.MarriageSideMapper;
import com.agreemount.negotiation.util.SideServiceUtil;
import com.agreemount.slaneg.action.ActionContextFactory;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.agreemount.negotiation.bean.enums.Metrics.*;
import static com.agreemount.negotiation.bean.enums.States.DOCUMENT_TYPE;
import static com.agreemount.negotiation.bean.enums.States.ITEM_TYPE;


@Component
public class ItemEnricher implements AgreementModelEnricher {

    private static final String LACK_OF_FIELD_EXCEPTION = "%s should not be null for %s";
    private static final String AGREED_ITEMS_IN_SPLIT = "AgreedItemsInSplit";

    @Autowired
    private SideServiceUtil sideServiceUtil;

    @Autowired
    private QueryFacade queryFacade;

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Override
    public Region getRegion() {
        return Region.NY;
    }

    @Override
    public void enrich(Map<String, Object> placeholderValueMap, Document negotiation) {
        UserRole mySide = sideServiceUtil.getSideForLoggedInUser(negotiation);
        assetsOrDebtsStream(negotiation)
                .map(document -> new Item(type(document), category(document), document.getMetrics()))
                .forEach((item) -> {
                    enrichBySide(placeholderValueMap, item, mySide, UserRole.SIDE1);
                    enrichBySide(placeholderValueMap, item, mySide, UserRole.SIDE2);
                });
    }

    private Stream<Document> assetsOrDebtsStream(Document negotiation) {
        return queryFacade.getDocumentsForQuery(AGREED_ITEMS_IN_SPLIT,
                actionContextFactory.createInstance(negotiation))
                .stream()
                .filter(isNegotiationItem());
    }

    private Predicate<Document> isNegotiationItem() {
        return document -> document.getStates().getOrDefault(DOCUMENT_TYPE.getValue(), "").equals("item");
    }

    private String category(Document document) {
        return Optional
                .ofNullable(document.getMetrics().get(ITEM_CATEGORY.getValue()))
                .orElseThrow(() -> new IllegalStateException(
                        String.format(LACK_OF_FIELD_EXCEPTION, "document.metrics.itemCategory", document.toString())))
                .toString();
    }

    private String type(Document document) {
        return Optional
                .ofNullable(document.getStates().get(ITEM_TYPE.getValue()))
                .orElseThrow(() -> new IllegalStateException(
                        String.format(LACK_OF_FIELD_EXCEPTION, "document.states.itemType", document.toString())));
    }

    private void enrichBySide(Map<String, Object> placeholderValueMap, Item item, @NotNull UserRole mySide, UserRole currentSide) {
        Map<String, Object> metrics = item.getMetrics();

        String splitPercent = getSplitPercent(metrics, currentSide);

        if (!"0".equals(splitPercent)) {
            String itemDetailsPerSide = new StringBuilder()
                    .append(metrics.get(ITEM_DESCRIPTION.getValue()))
                    .append(" ")
                    .append(splitPercent)
                    .append("% of ")
                    .append(metrics.get(ITEM_VALUE.getValue()))
                    .toString();


            String key = getKey(item, mySide, currentSide);
            placeholderValueMap.merge(
                    key,
                    itemDetailsPerSide,
                    (current, newOne) -> String.format("%s, %s", current, newOne)
            );
        }
    }

    private String getSplitPercent(Map<String, Object> metrics, UserRole currentSide) {
        String splitPercentKey = SPLIT_PERCENT.getValue() + currentSide.getValue();
        return Optional.ofNullable(metrics.get(splitPercentKey))
                .orElseThrow(() -> new IllegalStateException("metrics " + splitPercentKey + " should not be null"))
                .toString();
    }

    private String getKey(Item item, UserRole mySide, UserRole currentSide) {
        MarriageSideMapper.MarriageSide marriageSide = MarriageSideMapper.mapLoggedInUserToHusband(mySide, currentSide);
        return "${" + item.getType() + "." + item.getCategory() + "." + marriageSide.getValue() + "}";
    }

    @Getter
    private static class Item {
        private String type;
        private String category;
        private Map<String, Object> metrics;

        public Item(String type, String category, Map<String, Object> metrics) {
            this.type = type;
            this.category = category;
            this.metrics = metrics;
        }
    }
}


