package com.agreemount.negotiation.agreement.template.locator;

import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.agreement.template.TemplateLocator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by damian on 13.10.18.
 */
@Service
public class NYTemplate implements TemplateLocator {
    @Value("${agreement.template.uri}")
    private String agreementTemplateURI;

    @Override
    public String getAgreementTemplateURI() {
        return agreementTemplateURI;
    }

    @Override
    public Region getRegion() {
        return Region.NY;
    }
}
