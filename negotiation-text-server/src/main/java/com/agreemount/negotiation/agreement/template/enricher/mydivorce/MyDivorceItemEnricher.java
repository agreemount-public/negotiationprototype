package com.agreemount.negotiation.agreement.template.enricher.mydivorce;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.AgreementModelEnricher;
import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.formatter.ItemFormatter;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.DocumentProvider;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import groovy.lang.Tuple2;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.SPOUSE_1;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.SPOUSE_2;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.COMMUNITY;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.ASSET;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.DEBT;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Log4j
@Component
public class MyDivorceItemEnricher implements AgreementModelEnricher {
    private static final List<Tuple2<ItemType, AwardedTo>> DOCUMENT_SECTIONS_COMBINATION = ImmutableList.of(
            new Tuple2<>(ASSET, COMMUNITY), new Tuple2<>(DEBT, COMMUNITY),
            new Tuple2<>(ASSET, SPOUSE_1), new Tuple2<>(DEBT, SPOUSE_1),
            new Tuple2<>(ASSET, SPOUSE_2), new Tuple2<>(DEBT, SPOUSE_2)
    );

    @Autowired
    private DocumentProvider documentProvider;

    @Autowired
    private ItemFormatter itemFormatter;

    @Override
    public Region getRegion() {
        return Region.MY_DIVORCE;
    }

    @Override
    public void enrich(Map<String, Object> placeholderValueMap, Document negotiation) {
        log.info(format("Formatting agreed negotiation items under negotiation Document.id[%s] for MyDivorcePaper agreement.", negotiation.getId()));
        Map<String, List<String>> keyValues = DOCUMENT_SECTIONS_COMBINATION
                .stream()
                .map(section -> Maps.immutableEntry(getKey(section), agreedFormattedItems(negotiation, section)))
                .peek(entry -> log.info(format("Agreement section[%s] and formatted values[%s]", entry.getKey(), entry.getValue())))
                .collect(toMap(entry -> entry.getKey(), entry -> entry.getValue()));

        placeholderValueMap.putAll(keyValues);
    }

    private List<String> agreedFormattedItems(Document negotiation, Tuple2<ItemType, AwardedTo> section) {
        List<String> formattedItems =  documentProvider.getAgreedDocuments(negotiation, section.getFirst(), section.getSecond())
                .stream()
                .map(formatItem(section))
                .collect(toList());

        if(formattedItems.isEmpty()) {
            return Collections.singletonList("(no such items)");
        }

        return formattedItems;
    }

    private Function<Document, String> formatItem(Tuple2<ItemType, AwardedTo> section) {
        return document -> itemFormatter.itemAsString(document, section.getSecond());
    }

    private String getKey(Tuple2<ItemType, AwardedTo> itemType2AwardedTo) {
        return format("${%s.%s.[]}", itemType2AwardedTo.getSecond().name(), itemType2AwardedTo.getFirst().name());
    }

}
