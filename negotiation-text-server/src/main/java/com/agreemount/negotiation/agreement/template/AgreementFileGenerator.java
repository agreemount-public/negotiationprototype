package com.agreemount.negotiation.agreement.template;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.docx.DocumentHelper;
import com.agreemount.negotiation.agreement.template.replacer.PlaceholderReplacer;
import com.agreemount.slaneg.db.DocumentOperations;
import com.google.common.base.Preconditions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static java.lang.String.format;

@Component
public class AgreementFileGenerator {
    @Value("${enriched.agreement.folder.uri}")
    String enrichedAgreementFolder;
    @Autowired
    private List<TemplateLocator> templateLocators;
    @Autowired
    private List<StaticDataEnricher> staticDataEnrichers;
    @Autowired
    private List<AgreementModelEnricher> agreementModelEnrichers;
    @Autowired
    private DocumentHelper docxHelper;
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private DocumentOperations documentOperations;

    private PlaceholderReplacer getReplacer() {
        return applicationContext.getBean(PlaceholderReplacer.class);
    }

    public Path generate(String documentId, Region region) {
        Document negotiation = findNegotiationDocument(documentId);

        Path uri = Paths.get(getAgreementTemplateURI(region));
        XWPFDocument template = docxHelper.readTemplate(uri);

        Map<String, Object> placeholderValueMap = preparePlaceholderValueMapFor(negotiation, region);
        XWPFDocument enrichedTemplate = getReplacer().replace(placeholderValueMap, template);

        Path toPath = toPath(negotiation);
        docxHelper.write(enrichedTemplate, toPath);
        return toPath;
    }

    private String getAgreementTemplateURI(Region region) {
        return templateLocators.stream()
                .filter(isRegion(region))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format(
                        "Could not find TemplateLocator for region[%s]", region)))
                .getAgreementTemplateURI();
    }

    private Path toPath(Document negotiation) {
        StringBuilder toFile = new StringBuilder()
                .append(enrichedAgreementFolder)
                .append(File.separator)
                .append(negotiation.getSlaUuid())
                .append(".docx");
        return Paths.get(toFile.toString());
    }

    private Map<String, Object> preparePlaceholderValueMapFor(Document negotiation, Region region) {
        Map<String, Object> placeholderValueMap = new HashMap<>();
        staticDataEnrichers.stream()
                .filter(isRegion(region))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(format("Can not find static data enricher for region[%s]", region)))
                .dateOfAgreement(placeholderValueMap);

        agreementModelEnrichers.stream()
                .filter(isRegion(region))
                .forEach(enricher -> enricher.enrich(placeholderValueMap, negotiation));

        return placeholderValueMap;
    }

    private Predicate<RegionAware> isRegion(Region region) {
        return enricher -> region.equals(enricher.getRegion()) || Region.ALL.equals(enricher.getRegion());
    }

    private Document findNegotiationDocument(String documentId) {
        Document document = documentOperations.getDocument(documentId);
        Preconditions.checkNotNull(document, "Can not find negotiation case by documentId[" + documentId + "]");
        return document;
    }

}
