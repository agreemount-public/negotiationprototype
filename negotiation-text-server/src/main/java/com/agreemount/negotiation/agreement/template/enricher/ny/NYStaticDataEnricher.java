package com.agreemount.negotiation.agreement.template.enricher.ny;

import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.agreement.template.StaticDataEnricher;
import com.agreemount.negotiation.util.LocalDateTimeProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

@Component
public class NYStaticDataEnricher implements StaticDataEnricher {

    @Autowired
    private LocalDateTimeProvider localDateTimeProvider;

    @Override
    public Region getRegion() {
        return Region.NY;
    }

    public void dateOfAgreement(Map<String, Object> placeholderValueMap) {
        LocalDateTime localDateTime = localDateTimeProvider.now();
        String monthAndDay = String.format("%d, %d", localDateTime.getMonthValue(), localDateTime.getDayOfMonth());
        placeholderValueMap.put("${month.and.day.of.agreement}", monthAndDay);
        placeholderValueMap.put("${year.of.agreement}", String.valueOf(localDateTime.getYear()).substring(2, 4));
    }
}