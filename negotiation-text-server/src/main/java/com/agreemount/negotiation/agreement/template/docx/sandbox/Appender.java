package com.agreemount.negotiation.agreement.template.docx.sandbox;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.stereotype.Component;

@Component
public class Appender {
    public XWPFDocument appendText(String text, XWPFDocument document) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setStyle("Heading1");
        XWPFRun run = paragraph.createRun();
        run.setFontSize(12);
        run.setBold(true);
        run.setText(text);

        return document;
    }
}
