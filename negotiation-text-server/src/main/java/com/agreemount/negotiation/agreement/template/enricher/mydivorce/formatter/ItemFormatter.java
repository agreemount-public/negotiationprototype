package com.agreemount.negotiation.agreement.template.enricher.mydivorce.formatter;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.enricher.USDValueFormatter;
import com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo;
import com.agreemount.negotiation.bean.UserRole;
import com.google.common.collect.ImmutableMap;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.AwardedTo.*;
import static com.agreemount.negotiation.agreement.template.enricher.mydivorce.provider.ItemType.DEBT;
import static com.agreemount.negotiation.bean.UserRole.SIDE1;
import static com.agreemount.negotiation.bean.UserRole.SIDE2;
import static com.agreemount.negotiation.bean.enums.Metrics.*;
import static com.agreemount.negotiation.bean.enums.States.ITEM_TYPE;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static org.apache.commons.lang.StringUtils.EMPTY;

@Log4j
@Component
public class ItemFormatter {

    @Autowired
    private USDValueFormatter usdValueFormatter;

    private static final Map<AwardedTo, UserRole> awardedToUserRoleMap = ImmutableMap.of(
            SPOUSE_1, SIDE1,
            SPOUSE_2, SIDE2
    );

    public String itemAsString(Document item, AwardedTo awardedTo) {
        checkNotNull(item, "Agreed item to format should not be null");

        if (COMMUNITY.equals(awardedTo)) {
            return communityAwarded(item);
        }

        return splitOrSpouseAwarded(item, awardedTo);
    }

    private String communityAwarded(Document item) {
        log.info(String.format("Community agreed item %s is formatted", item));
        return new StringBuilder()
                .append(basicInformation(item))
                .append(monthlyPayment(item))
                .toString();
    }

    private String basicInformation(Document item) {
        Map<String, Object> metrics = item.getMetrics();
        String category = (String) metrics.get(ITEM_CATEGORY.getValue());
        String description = (String) metrics.get(ITEM_DESCRIPTION.getValue());
        Integer value = (Integer) metrics.get(ITEM_VALUE.getValue());
        return format("%s: %s of value %s", category, description, usdValueFormatter.format(value));
    }

    private String splitOrSpouseAwarded(Document item, AwardedTo awardedTo) {
        log.info(String.format("Split or spouse awarded item %s is formatted", item.toString()));
        Integer splitPercent = splitPercent(item.getMetrics(), awardedTo);
        if (splitPercent == 0) {
            log.info(String.format("Document.id[%s] Is not awarded to", item.getId(), awardedTo.name().toString()));
            return "";
        }

        return new StringBuilder()
                .append(basicInformation(item))
                .append(monthlyPayment(item))
                .append(awardedIn(item, splitPercent))
                .append(splitComment(item, splitPercent))
                .toString();
    }

    private Integer splitPercent(Map<String, Object> metrics, AwardedTo awardedTo) {
        return Optional.ofNullable(awardedToUserRoleMap.get(awardedTo))
                .map(this::splitPercentKey)
                .map(splitPercentValue(metrics))
                .orElseThrow(()-> new IllegalStateException("splitPercent can not be found in agreed item"));
    }

    private Function<String, Integer> splitPercentValue(Map<String, Object> metrics) {
        return splitPercentKey -> (Integer) metrics.getOrDefault(splitPercentKey, 0);
    }

    private String splitPercentKey(UserRole userRole) {
        return SPLIT_PERCENT.getValue() + userRole.getValue();
    }

    private String monthlyPayment(Document item) {
        if(notDebt(item)){
            log.info(String.format("Document.id[%s] itemType is not DEBT", item.getId()));
            return EMPTY;
        }

        Map<String, Object> metrics = item.getMetrics();
        return Optional.ofNullable((Integer) metrics.get(ITEM_MONTHLY_PAYMENT.getValue()))
                .map(usdValueFormatter::format)
                .map(monthly -> String.format(" with monthly payment %s", monthly))
                .orElse(EMPTY);
    }

    private boolean notDebt(Document item) {
        return !DEBT.name().equalsIgnoreCase(item.getState(ITEM_TYPE.getValue()));
    }

    private String awardedIn(Document item, Integer splitPercent) {
        if (splitPercent == 100) {
            return " is awarded in total";
        }

        Integer value = (Integer) item.getMetrics().get(ITEM_VALUE.getValue());
        return inPart(splitPercent, value);
    }

    private String inPart(Integer splitPercent, Integer itemValue) {
        BigDecimal valueBD = BigDecimal.valueOf(itemValue);
        BigDecimal splitPercentBD = BigDecimal.valueOf(splitPercent);
        int partValue = valueBD
                .multiply(splitPercentBD)
                .divide(BigDecimal.valueOf(100))
                .setScale(0, RoundingMode.HALF_UP)
                .toBigInteger()
                .intValue();

        return String.format(" is awarded in part %d%% which is %s", splitPercent, usdValueFormatter.format(partValue));
    }

    private String splitComment(Document item, Integer splitPercent) {
        if (splitPercent > 0 && splitPercent < 100) {
            return Optional.ofNullable((String) item.getMetrics().getOrDefault(SPLIT_DESCRIPTION.getValue(), EMPTY))
                    .filter(description -> StringUtils.isNotBlank(description))
                    .map(splitDescription -> String.format("; spouses agreed also that %s", splitDescription))
                    .orElse(EMPTY);
        }

        return EMPTY;
    }

}
