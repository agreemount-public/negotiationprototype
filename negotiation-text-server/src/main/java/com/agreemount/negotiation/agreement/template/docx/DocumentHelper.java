package com.agreemount.negotiation.agreement.template.docx;

import lombok.extern.log4j.Log4j;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

@Component
@Log4j
public class DocumentHelper {
    public XWPFDocument readTemplate(Path templatePath) {
        try (InputStream inputStream = new ClassPathResource(templatePath.toString()).getInputStream()) {
            XWPFDocument xwpfDocument = new XWPFDocument(inputStream);
            return xwpfDocument;
        } catch (IOException exception) {
            String message = String.format("Can not read template from given path %s and create POI Document",
                    templatePath.toString());
            throw new RuntimeException(message, exception);
        }
    }

    public void write(XWPFDocument document, Path toPath) {
        try (OutputStream outputStream = Files.newOutputStream(toPath)) {
            document.write(outputStream);
        } catch (IOException exception) {
            String message = String.format("Could not write POI document into given path %s",
                    toPath.toString());
            throw new RuntimeException(message, exception);
        }
    }
}
