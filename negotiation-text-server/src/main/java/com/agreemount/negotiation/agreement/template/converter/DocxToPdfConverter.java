package com.agreemount.negotiation.agreement.template.converter;

import lombok.extern.log4j.Log4j;
import org.jodconverter.JodConverter;
import org.jodconverter.office.LocalOfficeManager;
import org.jodconverter.office.OfficeException;
import org.jodconverter.office.OfficeUtils;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.String.format;

@Log4j
@Service
public class DocxToPdfConverter {

    private static final String PDF = ".pdf";
    private static final String DOCX = ".docx";

    public Path convert(Path docxPath) {
        log.info(format("Start converting file docxPath[%s]", docxPath));
        LocalOfficeManager officeManager = LocalOfficeManager.builder().install().portNumbers(8100).build();
        log.info("Office manager is prepared to be started");
        try {
            if(officeManager.isRunning()) {
                log.info("Office manager has already been started");
            } else {
                officeManager.start();
                log.info("Office manager is started");
            }
            Path pdfPath = replaceExtension(docxPath, DOCX, PDF);
            log.info(format("to pdfPath[%s]", pdfPath));
            JodConverter
                .convert(docxPath.toFile())
                .to(pdfPath.toFile())
                .execute();
            Thread.sleep(500);
            log.info("Finished converting file");
            return pdfPath;
        } catch (OfficeException | InterruptedException e) {
            throw new IllegalStateException(format("Could not convert docx file docxPath[%s], to pdf", docxPath), e);
        } finally {
            OfficeUtils.stopQuietly(officeManager);
            log.info("officeManager quietly stopped");
        }

    }

    private Path replaceExtension(Path docxPath, CharSequence oldExtenstion, CharSequence newExtension) {
        String docxPathString = docxPath.toUri().toString();
        String replacedDocx = docxPathString.replace(oldExtenstion, newExtension);
        return Paths.get(URI.create(replacedDocx));
    }

}
