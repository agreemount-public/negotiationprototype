package com.agreemount.negotiation.agreement.template.enricher;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.agreement.template.AgreementModelEnricher;
import com.agreemount.negotiation.agreement.template.Region;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.util.MarriageSideMapper;
import com.agreemount.negotiation.util.SideServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.agreemount.negotiation.bean.enums.User.EMAIL;
import static com.agreemount.negotiation.bean.enums.User.NAME;


@Component
public class PersonalDataEnricher implements AgreementModelEnricher {

    @Autowired
    private SideServiceUtil sideServiceUtil;

    @Override
    public Region getRegion() {
        return Region.ALL;
    }

    public void enrich(Map<String, Object> placeholderValueMap, Document negotiation) {
        UserRole mySide = sideServiceUtil.getSideForLoggedInUser(negotiation);
        userDataPerSide(placeholderValueMap, negotiation, mySide, UserRole.SIDE1);
        userDataPerSide(placeholderValueMap, negotiation, mySide, UserRole.SIDE2);
    }

    private void userDataPerSide(Map<String, Object> placeholderValueMap, Document negotiation, UserRole mySide, final UserRole currentSide) {
        sideServiceUtil.getUserByRole(negotiation, currentSide).ifPresent(user -> {
            MarriageSideMapper.MarriageSide marriageSide = MarriageSideMapper.mapLoggedInUserToHusband(mySide, currentSide);

            placeholderValueMap.put(getKey(EMAIL.getValue(), marriageSide), user.getEmail());
            placeholderValueMap.put(getKey(NAME.getValue(), marriageSide), user.getName());
        });
    }

    private String getKey(String fieldName, MarriageSideMapper.MarriageSide marriageSide) {
        return "${personalData." + fieldName + "." + marriageSide.getValue() + "}";
    }

}
