package com.agreemount.negotiation.notification.processors;

import com.agreemount.negotiation.notification.ProcessorTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DefaultProcessor extends ProcessorTemplate {

    @Override
    protected Map<String, Object> prepareParameters(Map<String, Object> params) {
        return params;
    }

}
