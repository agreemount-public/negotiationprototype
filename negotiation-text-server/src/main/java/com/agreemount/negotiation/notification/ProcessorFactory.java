package com.agreemount.negotiation.notification;

import com.agreemount.negotiation.bean.notification.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ProcessorFactory {

    private static final String NOTIFICATION_PROCESSOR_POSTFIX = "Processor";
    private static final String DEFAULT_NOTIFICATION_PROCESSOR = "default";
    @Autowired
    private ApplicationContext applicationContext;

    public ProcessorTemplate createProcessor(Notification notification) {
        Map<String, ProcessorTemplate> processors = applicationContext.getBeansOfType(ProcessorTemplate.class);

        String processorBeanName = notification.getNotificationType().getValue() + NOTIFICATION_PROCESSOR_POSTFIX;
        if (processors.containsKey(processorBeanName)) {
            return processors.get(processorBeanName);
        }

        return processors.get(DEFAULT_NOTIFICATION_PROCESSOR + NOTIFICATION_PROCESSOR_POSTFIX);
    }

}
