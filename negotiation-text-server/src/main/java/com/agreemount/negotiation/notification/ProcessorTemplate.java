package com.agreemount.negotiation.notification;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.PreparedEmail;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.notification.GeneratedNotification;
import com.agreemount.negotiation.bean.notification.GeneratedNotificationStatus;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.dao.IGeneratedNotificationDAO;
import com.agreemount.negotiation.dao.INotificationDAO;
import com.agreemount.negotiation.logic.ITeamLogic;
import com.agreemount.negotiation.logic.impl.AgreemountDocumentLogic;
import com.agreemount.negotiation.util.EnvironmentUtil;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.velocity.VelocityEngineUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j
public abstract class ProcessorTemplate {

    private static final String VELOCITY_EXTENSION = "vm";
    private static final int TITLE_MAX_CHARS = 25;

    @Autowired
    private IGeneratedNotificationDAO generatedNotificationDAO;

    @Autowired
    private INotificationDAO notificationDAO;

    @Autowired
    private VelocityEngine velocityEngine;

    @Autowired
    private ITeamLogic teamLogic;

    @Autowired
    private AgreemountDocumentLogic agreemountDocumentLogic;

    @Autowired
    private EnvironmentUtil environmentUtil;

    private static void setTitle(PreparedEmail preparedEmail, Map<String, Object> params, String content) {
        if (params.containsKey(Notification.Parameter.Keys.TITLE.getValue())) {
            String title = (String) params.get(Notification.Parameter.Keys.TITLE.getValue());
            preparedEmail.setTitle(title);
        } else {
            preparedEmail.setTitle(extractTitleFromContent(content));
        }
    }

    private static String extractTitleFromContent(String content) {
        String contentCopy = new String(content);
        contentCopy = contentCopy.replaceAll("\\<[^>]*>", "").replaceAll("\\r|\\n", " ");
        return contentCopy.substring(0, TITLE_MAX_CHARS);
    }

    protected abstract Map<String, Object> prepareParameters(Map<String, Object> params);

    public final void processNotification(Notification notification) {
        String templatePath = "mails/" + notification.getNotificationType().toString().toLowerCase() + "." + VELOCITY_EXTENSION;

        try {
            Map<String, Object> params = prepareParameters(prepareStandardParameters(notification.getParameters()));
            String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templatePath, "UTF-8", params);
            generatedNotificationDAO.save(getGeneratedNotification(content, params));

            notification.setNotificationStatus(NotificationStatus.PROCESSED);
            notification.setProcessedAt(LocalDateTime.now());
            notificationDAO.save(notification);
        } catch (Exception ex) {
            log.error("Cannot generate content from template in path [" + templatePath + "] ", ex);
            notification.setNotificationStatus(NotificationStatus.FAILED);
            notification.setMessage(ex.getMessage());
            notificationDAO.save(notification);
        }
    }

    private GeneratedNotification getGeneratedNotification(String content, Map<String, Object> params) {
        validateParams(params);

        GeneratedNotification generatedNotification = new GeneratedNotification();

        PreparedEmail preparedEmail = new PreparedEmail();
        preparedEmail.setContent(content);
        preparedEmail.setReceiver(getReceiver(params));

        setTitle(preparedEmail, params, content);
        setBcc(preparedEmail, params);
        setCc(preparedEmail, params);

        LocalDateTime now = LocalDateTime.now();
        generatedNotification.setPreparedEmail(preparedEmail);
        generatedNotification.setStatus(GeneratedNotificationStatus.WAITING);
        generatedNotification.setNextProcessing(now);
        generatedNotification.setCreatedAt(now);
        return generatedNotification;
    }

    private String getReceiver(Map<String, Object> params) {
        String receiver = (String) params.get(Notification.Parameter.Keys.RECEIVER.getValue());
        if (receiver != null) {
            return receiver;
        }

        Document document = agreemountDocumentLogic.getCurrentNegotiation((String) params.get(Notification.Parameter.Keys.DOCUMENT_ID.getValue()));
        String receiverSide = (String) params.get(Notification.Parameter.Keys.RECEIVER_SIDE.getValue());
        UserRole userSide = UserRole.valueOf(receiverSide.toUpperCase().trim());
        User2Team userWithRole = teamLogic.getUserWithRole(document.getTeam(), userSide)
                .orElseThrow(() -> new NullPointerException("User for side [" + userSide + "] not found"));

        return userWithRole.getLogin();
    }

    private Map<String, Object> prepareStandardParameters(Set<Notification.Parameter> params) {
        Map<String, Object> standardParams = params.stream().collect(
                Collectors.toMap(p -> p.getKey(), p -> {
                    List<String> list = p.getValue();
                    if (list.size() <= 1) {
                        return list.get(0);
                    }
                    return list;
                })
        );

        standardParams.put(Notification.Parameter.Keys.HOST.getValue(), environmentUtil.getHost());
        return standardParams;
    }

    private void validateParams(Map<String, Object> params) {
        Object receiver = params.get(Notification.Parameter.Keys.RECEIVER.getValue());
        Object receiverSide = params.get(Notification.Parameter.Keys.RECEIVER_SIDE.getValue());

        if (receiver == null && receiverSide == null) {
            throw new NullPointerException("Either receiver or receiverSide is required");
        }

        if (receiver != null && receiverSide != null) {
            throw new NullPointerException("Cannot set both: receiver and receiverSide");
        }
    }


    private void setBcc(PreparedEmail preparedEmail, Map<String, Object> params) {
        Object bccList = params.get(Notification.Parameter.Keys.BCC.getValue());
        if (bccList == null) {
            return;
        }

        if (!(bccList instanceof List)) {
            preparedEmail.setBcc(Lists.newArrayList((String) bccList));
        } else {
            preparedEmail.setBcc((List) bccList);
        }
    }

    private void setCc(PreparedEmail preparedEmail, Map<String, Object> params) {
        Object ccList = params.get(Notification.Parameter.Keys.CC.getValue());
        if (ccList == null) {
            return;
        }

        if (!(ccList instanceof List)) {
            preparedEmail.setCc(Lists.newArrayList((String) ccList));
        } else {
            preparedEmail.setCc((List) ccList);
        }
    }

    protected AgreemountDocumentLogic getAgreemountDocumentLogic() {
        return agreemountDocumentLogic;
    }
}
