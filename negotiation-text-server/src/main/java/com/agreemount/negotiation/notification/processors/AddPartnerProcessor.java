package com.agreemount.negotiation.notification.processors;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.notification.ProcessorTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AddPartnerProcessor extends ProcessorTemplate {

    @Override
    protected Map<String, Object> prepareParameters(Map<String, Object> standardParameters) {
        String documentId = (String) standardParameters.get(Notification.Parameter.Keys.DOCUMENT_ID.getValue());
        String requestor = (String) standardParameters.get(Notification.Parameter.Keys.REQUESTOR.getValue());
        Document document = getAgreemountDocumentLogic().getCurrentNegotiation(documentId);
        Document parentDocument = getAgreemountDocumentLogic().getMainNegotiation(documentId);

        standardParameters.put("negotiationId", parentDocument.getId());
        standardParameters.put("docName", document.getName());
        standardParameters.put("requestor", requestor);
        return standardParameters;
    }

}
