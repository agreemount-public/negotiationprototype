package com.agreemount.negotiation.notification.processors;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.notification.ProcessorTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.DOCUMENT_ID;

@Component
public class AddSideProcessor extends ProcessorTemplate {

    @Override
    protected Map<String, Object> prepareParameters(Map<String, Object> standardParameters) {
        String documentId = (String) standardParameters.get(DOCUMENT_ID.getValue());
        Document document = getAgreemountDocumentLogic().getCurrentNegotiation(documentId);
        Document parentDocument = getAgreemountDocumentLogic().getMainNegotiation(documentId);

        standardParameters.put("negotiationId", parentDocument.getId());
        standardParameters.put("docName", document.getName());
        standardParameters.put("senderEmail", parentDocument.getAuthor());
        return standardParameters;
    }

}
