package com.agreemount.negotiation.negotiation;

import com.agreemount.bean.document.Document;
import com.agreemount.logic.QueryLogic;
import com.agreemount.negotiation.util.DocumentUtil;
import com.agreemount.slaneg.action.ActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ChangesRepository implements IChangesRepository {
    @Autowired
    private QueryLogic queryLogic;

    @Override
    public List<Document> allChanges(Document negotiationDocument) {
        ActionContext queryContext = new ActionContext();
        queryContext.addDocument("BASE", negotiationDocument);

        return queryLogic.loadDocumentsForQuery("All changes related to the negotiations", queryContext);
    }

    @Override
    public Map<String, String> allChangesUuidConent(Document negotiationDocument) {
        return convertToId2BodyMap(allChanges(negotiationDocument));
    }

    @Override
    public Set<String> acceptedChangesUuid(Document negotiationDocument) {
        return convertToIdsSet(acceptedChanges(negotiationDocument));
    }

    @Override
    public Set<String> remainsChangesUuid(Document negotiationDocument) {
        Document mainChange = mainChange(negotiationDocument);
        String documentTag = mainChange == null ? null : mainChange.getRootUuid();

        Set<String> acceptedIds = acceptedChangesUuid(negotiationDocument);
        Set<String> remainIds = convertToIdsSet(remainsChanges(negotiationDocument));

        return remainIds
                .stream()
                .filter(p -> !acceptedIds.contains(p) && mainChange != null && !documentTag.equals(p))
                .collect(Collectors.toSet());
    }

    @Override
    public Document mainChange(Document negotiationDocument) {
        ActionContext queryContext = new ActionContext();
        queryContext.addDocument("BASE", negotiationDocument);
        List<Document> mainChange = queryLogic.loadDocumentsForQuery("Main change related to the negotiations", queryContext);

        return (mainChange == null || mainChange.isEmpty()) ? null : mainChange.get(0);
    }

    @Override
    public Document mainForChange(Document change) {
        ActionContext queryContext = new ActionContext();
        queryContext.addDocument("BASE", change);
        List<Document> mains = queryLogic.loadDocumentsForQuery("mainForChange", queryContext);
        return mains.get(0);
    }

    private Set<String> convertToIdsSet(List<Document> documents) {
        return new HashSet(documents.stream().map(Document::getRootUuid).collect(Collectors.toList()));
    }


    private Map<String, String> convertToId2BodyMap(List<Document> documents) {

        return documents.stream()
                .collect(Collectors.toMap(
                        Document::getRootUuid,
                        d -> DocumentUtil.getBodyOf(d) != null ?
                                (String) DocumentUtil.getBodyOf(d) : ""
                ));
    }

    private List<Document> acceptedChanges(Document negotiationDocument) {
        ActionContext queryContext = new ActionContext();
        queryContext.addDocument("BASE", negotiationDocument);
        return queryLogic.loadDocumentsForQuery("All agreed changes related to the negotiations", queryContext);

    }

    private List<Document> remainsChanges(Document negotiationDocument) {
        ActionContext queryContext = new ActionContext();
        queryContext.addDocument("BASE", negotiationDocument);
        return queryLogic.loadDocumentsForQuery("All visible changes related to the negotiations", queryContext);
    }
}
