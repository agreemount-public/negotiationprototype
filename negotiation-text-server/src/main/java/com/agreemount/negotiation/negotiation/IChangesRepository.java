package com.agreemount.negotiation.negotiation;

import com.agreemount.bean.document.Document;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface IChangesRepository {

    Document mainChange(Document negotiationDocument);

    List<Document> allChanges(Document negotiationDocument);

    Map<String, String> allChangesUuidConent(Document negotiationDocument);

    Set<String> acceptedChangesUuid(Document negotiationDocument);

    Set<String> remainsChangesUuid(Document negotiationDocument);

    Document mainForChange(Document change);
}

