package com.agreemount.negotiation.prometheus;

import io.prometheus.client.Histogram;
import lombok.extern.log4j.Log4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
@Log4j
public class PrometheusMetrics {

    private static Map<String, Histogram> metrics = new HashMap<>();

    @Around("execution(* com.agreemount.controller.*.*(..)) || execution(* com.agreemount.negotiation.controller.*.*(..))")
    public Object doBasicProfiling(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        String metricName = genMetricName(proceedingJoinPoint);

        log.debug("Requested metric: " + metricName);

        if (!metrics.containsKey(metricName)) {
            log.debug("Metric '" + metricName + "' does not exits, creating new");
            metrics.put(metricName,
                    Histogram.build()
                            .name(metricName)
                            .help(proceedingJoinPoint.getSignature().toString())
                            .register()
            );
        }

        Histogram histogram = metrics.get(metricName);

        log.debug("Starting timer for: " + metricName);
        Histogram.Timer timer = histogram.startTimer();
        Object retVal = proceedingJoinPoint.proceed();
        timer.observeDuration();
        log.debug("Timer for '" + metricName + "' stopped");

        return retVal;
    }

    private String genMetricName(ProceedingJoinPoint proceedingJoinPoint) {
        return proceedingJoinPoint.getSignature()
                .toString()
                .replaceAll("\\[", "_")
                .replaceAll("]", "_")
                .replaceAll("\\.| |,", "_")
                .replaceAll("\\(|\\)", "")
                .toLowerCase();
    }
}
