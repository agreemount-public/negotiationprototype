package com.agreemount.negotiation;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.identity.provider.RequestInterceptor;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.action.DocumentFactory;
import com.agreemount.slaneg.fixtures.FileRulesProvidersConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.Filter;

@SpringBootApplication
@PropertySources({
        @PropertySource("classpath:application.properties"),
        @PropertySource("classpath:engine.properties"),
        @PropertySource("classpath:mongo/mongo-${spring.profiles.active}.properties"),
        @PropertySource("classpath:paypal/paypal-${spring.profiles.active}.properties"),
        @PropertySource("classpath:mailer/mailer-${spring.profiles.active}.properties"),
        @PropertySource("classpath:cron/cron-${spring.profiles.active}.properties"),
        @PropertySource("classpath:tag-finder.properties"),
        @PropertySource("classpath:log4j.properties")
})

@ComponentScan(
        basePackages = {"com.agreemount"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                        value = FileRulesProvidersConfiguration.class)
        }
)
@EnableAspectJAutoProxy
public class Application extends WebMvcConfigurerAdapter {

    @Autowired
    RequestInterceptor requestInterceptor;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Filter characterEncodingFilter() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestInterceptor);
    }


    @Bean
    public ActionContextFactory<ActionContext> getActionContextFactory() {
        return new ActionContextFactory<>(ActionContext.class);
    }

    @Bean
    public DocumentFactory<Document> getDocumentFactory() {
        return new DocumentFactory<>(Document.class);
    }
}
