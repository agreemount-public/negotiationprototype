package com.agreemount.negotiation.mydivorcepapers;

import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Component
public class PasswordCoder {

    private static final String ALGORITHM = "AES";

    public char[] encrypt(char[] valueToEnc, byte[] keyValue) throws Exception {
        Cipher cipher = prepareCipher(Cipher.ENCRYPT_MODE, keyValue);

        byte[] encValue = cipher.doFinal(charArrayToByteArray(valueToEnc));
        byte[] encodedValue = Base64.getEncoder().encode(encValue);

        return byteArrayToCharArray(encodedValue);
    }

    public char[] decrypt(char[] encryptedValue, byte[] keyValue) throws Exception {
        Cipher cipher = prepareCipher(Cipher.DECRYPT_MODE, keyValue);

        ByteBuffer byteBuffer = Charset.forName(StandardCharsets.UTF_8.name()).encode(CharBuffer.wrap(encryptedValue));
        byte[] bytes = cipher.doFinal(Base64.getDecoder().decode(byteBuffer).array());

        return byteArrayToCharArray(bytes);
    }

    private Cipher prepareCipher(int mode, byte[] keyValue) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Key key = new SecretKeySpec(keyValue, ALGORITHM);

        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(mode, key);

        return cipher;
    }

    private byte[] charArrayToByteArray(char[] charArray) {
        byte[] byteArray = new byte[charArray.length];
        for (int i = 0; i < charArray.length; i++) {
            byteArray[i] = (byte) (0xFF & (int) charArray[i]);
        }
        return byteArray;
    }


    private char[] byteArrayToCharArray(byte[] byteArray) {
        char[] charArray = new char[byteArray.length];
        for (int i = 0; i < byteArray.length; i++) {
            charArray[i] = (char) (byteArray[i] & 0xff);
        }
        return charArray;
    }
}
