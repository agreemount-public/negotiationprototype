package com.agreemount.negotiation.mydivorcepapers;

import lombok.Data;

@Data
public class BlockUserRequest {
    private String email;
    private String reason;
}
