package com.agreemount.negotiation.mydivorcepapers;

import com.agreemount.EngineFacade;
import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.auth.Password;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserLogDAO;
import com.agreemount.negotiation.logic.impl.TeamLogic;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class MyDivorcePapersUserLogic {

    protected static final byte[] keyValue = new byte[]{'T', 'j', 'o', 'g', 's', 's', 'A', 'a', 'e', 'K', 'R', 'P', 'D', 'V', 'e', 'y'};
    protected static final String USER_CREATED = "User created";
    protected static final String EXISTING_USER_RECEIVED_PRIVILEGES = "Existing user received privileges.";
    static final String NEW_NEGOTIATION_ACTION_NAME = "createNewNegotiation-split";

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private PasswordCoder passwordCoder;

    @Autowired
    private UserLogDAO userLogDAO;

    @Autowired
    private TeamLogic teamLogic;

    @Autowired
    private EngineFacade engineFacade;

    @Autowired
    private ActionContextFactory actionContextFactory;


    public String createAccount(NewUserRequest newUserRequest) throws Exception {

        Preconditions.checkNotNull(newUserRequest.getPasswordEncoded(), "Password is not set");
        Preconditions.checkNotNull(newUserRequest.getEmail(), "Email is not set");
        Preconditions.checkNotNull(newUserRequest.getName(), "User name is not set");

        User user = userDAO.getByEmail(newUserRequest.getEmail());

        boolean createNewNegotiation = true;
        String result;

        if (user != null) {
            createNewNegotiation = !user.isHasFreeAccess();
            updateUser(user);
            result = EXISTING_USER_RECEIVED_PRIVILEGES;
        } else {
            user = createUser(newUserRequest);
            result = USER_CREATED;
        }

        if (createNewNegotiation) {
            createNegotiation(user);
        }
        return result;
    }

    private void createNegotiation(User user) {
        Team team = teamLogic.saveTeamWithOwner(user.getEmail());

        Document document = new Document();
        document.setName(UUID.randomUUID().toString());
        document.setTeam(team.getName());

        ActionContext actionContext = actionContextFactory.createInstance(document);

        engineFacade.runAction(actionContext, NEW_NEGOTIATION_ACTION_NAME);
    }

    private void updateUser(User existingUser) {
        existingUser.setHasFreeAccess(true);
        userDAO.save(existingUser);

        userLogDAO.insertNewLog(existingUser.getEmail(), "give user full access by my divorce papers api");
    }

    private User createUser(NewUserRequest newUserRequest) throws Exception {
        MyDivorcePapersUser user = new MyDivorcePapersUser();

        char[] password = passwordCoder.decrypt(newUserRequest.getPasswordEncoded(), keyValue);
        byte[] salt = Password.getSalt();
        byte[] hash = Password.hashPassword(password, salt);

        user.setHash(hash);
        user.setSalt(salt);
        user.setPassword(null);
        user.setEmail(newUserRequest.getEmail());
        user.setName(newUserRequest.getName());
        user.setConfirmed(true);
        user.setHasFreeAccess(true);

        userDAO.insert(user);

        userLogDAO.insertNewLog(newUserRequest.getEmail(), "create new user with full access by my divorce papers api");

        return user;
    }

    public void blockUser(BlockUserRequest blockUserRequest) {
        Preconditions.checkNotNull(blockUserRequest.getEmail());
        Preconditions.checkNotNull(blockUserRequest.getReason());

        User user = userDAO.getByEmail(blockUserRequest.getEmail());
        Preconditions.checkNotNull(user);

        user.setHasFreeAccess(false);
        user.setBlocked(true);
        userDAO.save(user);

        userLogDAO.insertNewLog(blockUserRequest.getEmail(), "block user by my divorce papers api. Reason: "
                + blockUserRequest.getReason());

        teamLogic.getNegotiationSidesOf(blockUserRequest.getEmail())
                .stream()
                .forEach(sideUser -> blockSide(sideUser, blockUserRequest.getEmail()));


    }

    private void blockSide(User sideUser, String blockedUserEmail) {
        if (sideUser.isHasFreeAccess()) {
            sideUser.setHasFreeAccess(false);
            sideUser.setBlocked(true);
            userDAO.save(sideUser);

            userLogDAO.insertNewLog(sideUser.getEmail(), "block user by my divorce papers api. Reason: api blocked user "
                    + blockedUserEmail + " who is side in negotiation.");

        }
    }
}
