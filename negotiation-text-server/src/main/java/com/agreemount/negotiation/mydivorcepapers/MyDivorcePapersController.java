package com.agreemount.negotiation.mydivorcepapers;

import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Log4j
@Controller
@RequestMapping("/my-divorce-papers/users")
public class MyDivorcePapersController {

    @Autowired
    private MyDivorcePapersUserLogic myDivorcePapersUserLogic;

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public ResponseEntity createAccount(@RequestBody NewUserRequest newUserRequest) {
        String result;
        try {
            result = myDivorcePapersUserLogic.createAccount(newUserRequest);
        } catch (Exception e) {
            log.error(e);

            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(result, HttpStatus.ACCEPTED);
    }


    @RequestMapping(value = "/block", method = RequestMethod.POST)
    public ResponseEntity blockAccount(@RequestBody BlockUserRequest blockUserRequest) {
        try {
            myDivorcePapersUserLogic.blockUser(blockUserRequest);
        } catch (Exception e) {
            log.error(e);

            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

}
