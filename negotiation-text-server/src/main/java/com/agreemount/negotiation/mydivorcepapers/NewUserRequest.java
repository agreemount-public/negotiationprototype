package com.agreemount.negotiation.mydivorcepapers;

import lombok.Data;

@Data
public class NewUserRequest {
    private String email;
    private char[] passwordEncoded;
    private String name;
}
