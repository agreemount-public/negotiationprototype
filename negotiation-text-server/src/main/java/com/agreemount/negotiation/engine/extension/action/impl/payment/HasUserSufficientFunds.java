package com.agreemount.negotiation.engine.extension.action.impl.payment;

import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.agreemount.negotiation.engine.extension.action.definition.payment.HasUserSufficientFundsDef;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.constraint.action.impl.QualifierImpl;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Log4j
@Component
@Scope("prototype")
public class HasUserSufficientFunds<T extends ActionContext> extends QualifierImpl<HasUserSufficientFundsDef, T> {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private IdentityProvider identityProvider;

    @Override
    public boolean isAvailable() {
        Preconditions.checkNotNull(getPaymentUnitsToOperate(), "required payment units is not set");
        Preconditions.checkNotNull(getIdentity(), "identityProvider is not initialized");
        Preconditions.checkNotNull(getIdentity().getLogin(), "required login is not set");

        User user = userDAO.getByEmail(getIdentity().getLogin());

        if (user == null) {
            throw new AuthException("IsPaymentApplicable: user not found");
        }

        return user.getPaymentUnits().compareTo(getPaymentUnitsToOperate()) >= 0;
    }

    private Identity getIdentity() {
        return identityProvider.getIdentity();
    }


    private Long getPaymentUnitsToOperate() {
        return getConstraintDefinition().getPaymentUnits();
    }

}
