package com.agreemount.negotiation.engine.yamlparser;

import com.agreemount.bean.Query;
import com.agreemount.bean.document.state.State;
import com.agreemount.bean.metric.Metric;
import com.agreemount.bean.metric.MetricCategory;
import com.agreemount.bean.query.QueryCategory;
import com.agreemount.slaneg.action.definition.Action;
import com.agreemount.slaneg.constraint.action.definition.ActionConstraint;
import com.agreemount.slaneg.fixtures.CachingRuleProvider;
import com.agreemount.slaneg.fixtures.DirectoryYamlProvider;
import com.agreemount.slaneg.fixtures.RulesProvider;
import com.agreemount.slaneg.message.Message;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class YamlProvidersConfiguration {

    @Bean
    public RulesProvider<Metric> metrics() {
        return new DirectoryYamlProvider<>("metrics");
    }

    @Bean(name = "metricsYamlProvider")
    public RulesProvider<Metric> metricCachingYamlProvider(RulesProvider<Metric> metrics) {
        return new CachingRuleProvider<>(metrics);
    }

    @Bean
    public RulesProvider<MetricCategory> metricCategories() {
        return new DirectoryYamlProvider<>("metricCategories");
    }

    @Bean(name = "metricCategoriesYamlProvider")
    public RulesProvider<MetricCategory> metricCategoryCachingYamlProvider(
            RulesProvider<MetricCategory> metricCategories) {
        return new CachingRuleProvider<>(metricCategories);
    }

    @Bean
    public RulesProvider<Action> actions() {
        return new DirectoryYamlProvider<>("actions");
    }

    @Bean(name = "actionsYamlProvider")
    public RulesProvider<Action> actionCachingYamlProvider(
            RulesProvider<Action> actions) {
        return new CachingRuleProvider<>(actions);
    }

    @Bean
    public RulesProvider<QueryCategory> queryCategories() {
        return new DirectoryYamlProvider<>("queryCategories");
    }

    @Bean(name = "queryCategoriesYamlProvider")
    public RulesProvider<QueryCategory> queryCategoriesCachingYamlProvider(
            RulesProvider<QueryCategory> queryCategories) {
        return new CachingRuleProvider<>(queryCategories);
    }

    @Bean
    public RulesProvider<Query> queries() {
        return new DirectoryYamlProvider<>("queries");
    }

    @Bean(name = "queriesYamlProvider")
    public RulesProvider<Query> queryCachingYamlProvider(RulesProvider<Query> queries) {
        return new CachingRuleProvider<>(queries);
    }

    @Bean
    public RulesProvider<State> states() {
        return new DirectoryYamlProvider<>("states");
    }

    @Bean(name = "statesYamlProvider")
    public RulesProvider<State> stateCachingYamlProvider(
            RulesProvider<State> states) {
        return new CachingRuleProvider<>(states);
    }


    @Bean
    public RulesProvider<ActionConstraint> constraints() {
        return new DirectoryYamlProvider<>("constraints");
    }

    @Bean(name = "constraintsYamlProvider")
    public RulesProvider<ActionConstraint> constraintCachingYamlProvider(
            RulesProvider<ActionConstraint> constraints) {
        return new CachingRuleProvider<>(constraints);
    }

    @Bean
    public RulesProvider<Message> messages() {
        return new DirectoryYamlProvider<>("messages");
    }

    @Bean(name = "messagesYamlProvider")
    public RulesProvider<Message> messageCachingYamlProvider(RulesProvider<Message> messages) {
        return new CachingRuleProvider<>(messages);
    }
}
