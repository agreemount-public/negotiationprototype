package com.agreemount.negotiation.engine.extension.action.definition.payment;

import com.agreemount.negotiation.engine.extension.action.impl.payment.HasUserSufficientFunds;
import com.agreemount.slaneg.annotation.HandleWithImplementation;
import com.agreemount.slaneg.constraint.action.definition.ActionConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@HandleWithImplementation(runner = HasUserSufficientFunds.class)
public class HasUserSufficientFundsDef extends ActionConstraint {

    private Long paymentUnits;

}