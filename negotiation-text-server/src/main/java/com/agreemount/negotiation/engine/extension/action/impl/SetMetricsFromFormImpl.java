package com.agreemount.negotiation.engine.extension.action.impl;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.engine.extension.action.definition.SetMetricsFromForm;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.impl.AbstractActionImpl;
import com.google.common.base.Preconditions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


@Component
@Scope("prototype")
public class SetMetricsFromFormImpl extends AbstractActionImpl<SetMetricsFromForm, Document, ActionContext<Document>> {

    @Override
    public void run(ActionContext actionContext) {

        String alias = definition.getAlias();
        Preconditions.checkArgument(!StringUtils.isEmpty(alias), "Alias is not set!");

        Document document = actionContext.getDocument(alias);
        Preconditions.checkNotNull(document, "document with alias [%s] was not found", alias);

        document.setMetrics(actionContext.getMetricValues());

        documentOperations.saveDocument(document);

        actionContext.addDocument(alias, document);
    }
}
