package com.agreemount.negotiation.engine.extension.action.constraint.definition;

import com.agreemount.negotiation.engine.extension.action.constraint.impl.NegotiationHasMinXUsers;
import com.agreemount.slaneg.annotation.HandleWithImplementation;
import com.agreemount.slaneg.constraint.action.definition.ActionConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@EqualsAndHashCode
@HandleWithImplementation(runner = NegotiationHasMinXUsers.class)
public class NegotiationHasMinXUsersDef extends ActionConstraint {
    private String alias = "BASE";
    private int minUsersCnt;
}
