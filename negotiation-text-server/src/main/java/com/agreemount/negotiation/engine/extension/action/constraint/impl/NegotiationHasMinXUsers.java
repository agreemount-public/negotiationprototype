package com.agreemount.negotiation.engine.extension.action.constraint.impl;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.engine.extension.action.constraint.definition.NegotiationHasMinXUsersDef;
import com.agreemount.negotiation.member.SideService;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.constraint.action.impl.QualifierImpl;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Log4j
@Component
@Scope("prototype")
public class NegotiationHasMinXUsers<T extends ActionContext> extends QualifierImpl<NegotiationHasMinXUsersDef, T> {

    @Autowired
    private SideService sideService;

    @Override
    public boolean isAvailable() {
        String negotiationAlias = getConstraintDefinition().getAlias();
        Document negotiation = getActionContext().getDocument(negotiationAlias);
        Preconditions.checkNotNull(negotiation, "negotiation was not found, alias: " + negotiationAlias);

        int membersCnt = sideService.fetchSides(negotiation.getId()).size();
        return membersCnt >= getConstraintDefinition().getMinUsersCnt();
    }
}
