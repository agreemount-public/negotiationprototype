package com.agreemount.negotiation.engine.extension.action.impl.payment;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.UserPaymentUnitsEntry;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.agreemount.negotiation.engine.extension.action.definition.payment.ChangeAccountBalanceDef;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.impl.AbstractActionImpl;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Scope("prototype")
@Log4j
public class ChangeAccountBalance extends AbstractActionImpl<ChangeAccountBalanceDef, Document, ActionContext<Document>> {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private IdentityProvider identityProvider;

    @Override
    protected void run(ActionContext<Document> documentActionContext) {
        Preconditions.checkNotNull(getPaymentUnitsToAdd(), "required payment units is not set");
        Preconditions.checkNotNull(getIdentity(), "identityProvider is not initialized");
        Preconditions.checkNotNull(getIdentity().getLogin(), "required login is not set");

        User user = userDAO.getByEmail(getIdentity().getLogin());
        if (user == null) {
            throw new AuthException("IsPaymentApplicable: user " + getIdentity().getLogin() + " not found");
        }

        Long currentPaymentUnits = user.getPaymentUnits();
        Long calculatedPaymentUnits = calculateAndReturnUserAccountBalance(user);
        Document baseDocument = documentActionContext.getDocument("BASE");

        user.setPaymentUnits(calculatedPaymentUnits);
        user.addUserPaymentUnitsEntry(getPaymentUnitsEntry(currentPaymentUnits, calculatedPaymentUnits, baseDocument));
        userDAO.save(user);
    }

    private Long calculateAndReturnUserAccountBalance(User user) {
        Long initialPaymentUnits = user.getPaymentUnits() == null ? 0L : user.getPaymentUnits();
        return initialPaymentUnits + getPaymentUnitsToAdd();
    }

    private UserPaymentUnitsEntry getPaymentUnitsEntry(Long paymentUnitsBefore, Long paymentUnitsAfter, Document baseDocument) {
        UserPaymentUnitsEntry entry = new UserPaymentUnitsEntry();
        entry.setCreatedAt(LocalDateTime.now());
        entry.setRequestedAmount(getPaymentUnitsToAdd());
        entry.setPaymentUnitsBefore(paymentUnitsBefore);
        entry.setPaymentUnitsAfter(paymentUnitsAfter);
        entry.setDescription(getDescription());
        if (baseDocument != null) {
            entry.setDocumentId(baseDocument.getId());
        }
        return entry;
    }

    private Identity getIdentity() {
        return identityProvider.getIdentity();
    }

    private Long getPaymentUnitsToAdd() {
        return definition.getPaymentUnits();
    }

    private String getDescription() {
        return definition.getDescription();
    }

}
