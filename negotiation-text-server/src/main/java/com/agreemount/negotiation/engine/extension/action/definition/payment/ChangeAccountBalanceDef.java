package com.agreemount.negotiation.engine.extension.action.definition.payment;

import com.agreemount.negotiation.engine.extension.action.impl.payment.ChangeAccountBalance;
import com.agreemount.slaneg.action.definition.Action;
import com.agreemount.slaneg.annotation.HandleWithImplementation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@HandleWithImplementation(runner = ChangeAccountBalance.class)
public class ChangeAccountBalanceDef extends Action {

    private Long paymentUnits;

    private String description;

}
