package com.agreemount.negotiation.engine.extension.action.definition;

import com.agreemount.negotiation.engine.extension.action.impl.BumpUpSequenceImpl;
import com.agreemount.slaneg.action.definition.Action;
import com.agreemount.slaneg.annotation.HandleWithImplementation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@HandleWithImplementation(runner = BumpUpSequenceImpl.class)
public class BumpUpSequence extends Action {

    private String documentAlias;
    private String sequenceKey;

    private String mainDocumentAlias;
    private String mainSequenceKey;

}
