package com.agreemount.negotiation.engine.extension.action.impl;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.engine.extension.action.definition.BumpUpSequence;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.impl.AbstractActionImpl;
import com.agreemount.slaneg.db.DocumentOperations;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Objects;

@Component
@Scope("prototype")
@Log4j
public class BumpUpSequenceImpl extends AbstractActionImpl<BumpUpSequence, Document, ActionContext<Document>> {

    public static final int INIT_SEQUENCE_VALUE = 0;

    @Override
    protected void run(ActionContext<Document> documentActionContext) {
        Document document = getDocument(documentActionContext);
        String sequenceKey = getSequenceKey();

        Document mainDocument = getMainDocument(documentActionContext);
        String mainSequenceKey = getMainSequenceKey();

        Integer maxSequenceValue = getMaxSequenceValue(mainDocument, mainSequenceKey);

        updateSequence(document, sequenceKey, maxSequenceValue);
        updateSequence(mainDocument, mainSequenceKey, maxSequenceValue + 1);

    }

    private String getSequenceKey() {
        String sequenceKey = definition.getSequenceKey();
        Preconditions.checkArgument(!StringUtils.isEmpty(sequenceKey), "sequenceKey is not set!");
        return sequenceKey;
    }


    private String getMainSequenceKey() {
        String sequenceKey = definition.getMainSequenceKey();
        Preconditions.checkArgument(!StringUtils.isEmpty(sequenceKey), "mainSequenceKey is not set!");
        return sequenceKey;
    }

    private void updateSequence(Document sequenceHolder, String sequenceKey, Integer sequenceValue) {
        sequenceHolder.getMetrics().put(sequenceKey, sequenceValue);
        documentOperations.saveDocument(sequenceHolder);
    }

    private Document getMainDocument(ActionContext<Document> documentActionContext) {
        String documentAlias = definition.getMainDocumentAlias();
        Preconditions.checkArgument(!StringUtils.isEmpty(documentAlias), "mainDocumentAlias is not set!");
        return getDocumentByAlias(documentActionContext, documentAlias);
    }

    private Document getDocument(ActionContext<Document> documentActionContext) {
        String documentAlias = definition.getDocumentAlias();
        Preconditions.checkArgument(!StringUtils.isEmpty(documentAlias), "documentAlias is not set!");
        return getDocumentByAlias(documentActionContext, documentAlias);
    }

    private Document getDocumentByAlias(ActionContext<Document> documentActionContext, String documentAlias) {
        Document document = documentActionContext.getDocument(documentAlias);
        Preconditions.checkNotNull(document, "document with alias [%s] was not found", documentAlias);
        return document;
    }

    private Integer getMaxSequenceValue(Document sequenceHolder, String mainSequenceKey) {
        return sequenceHolder.getMetrics().entrySet()
                .stream()
                .filter(entry -> mainSequenceKey.equals(entry.getKey().toString()))
                .filter(Objects::nonNull)
                .map(entry -> Integer.valueOf(entry.getValue().toString()))
                .findAny()
                .orElse(INIT_SEQUENCE_VALUE);
    }

    void setDocumentOperations(DocumentOperations operations) {
        documentOperations = operations;
    }
}
