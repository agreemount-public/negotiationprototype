package com.agreemount.negotiation.engine.extension.action.definition;

import com.agreemount.negotiation.engine.extension.action.impl.SendEmailImpl;
import com.agreemount.slaneg.action.definition.Action;
import com.agreemount.slaneg.annotation.HandleWithImplementation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;



@Getter
@Setter
@ToString
@EqualsAndHashCode
@HandleWithImplementation(runner = SendEmailImpl.class)
public class SendEmail extends Action{

    private String documentAlias;

    private String notificationType;

    private String receiver;

    private String title;

    private List<String> customParameters;

    private List<String> cc;

    private List<String> bcc;

    private String receiverSide;

}
