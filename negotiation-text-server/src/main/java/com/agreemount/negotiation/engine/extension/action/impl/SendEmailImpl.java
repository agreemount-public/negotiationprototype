package com.agreemount.negotiation.engine.extension.action.impl;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.INotificationDAO;
import com.agreemount.negotiation.engine.extension.action.definition.SendEmail;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.impl.AbstractActionImpl;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


@Component
@Scope("prototype")
@Log4j
public class SendEmailImpl extends AbstractActionImpl<SendEmail, Document, ActionContext<Document>> {

    @Autowired
    private INotificationDAO notificationDAO;

    @Override
    protected void run(ActionContext actionContext) {
        try {
            String alias = definition.getDocumentAlias();
            Preconditions.checkArgument(!StringUtils.isEmpty(alias), "Alias is not set!");

            Document document = actionContext.getDocument(alias);
            Preconditions.checkNotNull(document, "document with alias [%s] was not found", alias);

            Notification notification = new NotificationBuilder()
                    .setNotificationType(NotificationType.valueOf(definition.getNotificationType()))
                    .addParameter(new Notification.Parameter(Notification.Parameter.Keys.DOCUMENT_ID.getValue(), document.getId()))
                    .addParameter(new Notification.Parameter(Notification.Parameter.Keys.BCC.getValue(), definition.getBcc()))
                    .addParameter(new Notification.Parameter(Notification.Parameter.Keys.CC.getValue(), definition.getCc()))
                    .addParameter(new Notification.Parameter(Notification.Parameter.Keys.CUSTOM.getValue(), definition.getCustomParameters()))
                    .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), definition.getReceiver()))
                    .addParameter(new Notification.Parameter(Notification.Parameter.Keys.TITLE.getValue(), definition.getTitle()))
                    .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER_SIDE.getValue(), definition.getReceiverSide()))
                    .build();

            notificationDAO.save(notification);
        } catch (Exception ex) {
            log.error("Cannot send email", ex);
        }
    }

}
