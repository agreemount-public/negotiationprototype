package com.agreemount.negotiation.engine.extension.action.definition;

import com.agreemount.negotiation.engine.extension.action.impl.SetMetricsFromFormImpl;
import com.agreemount.slaneg.action.definition.Action;
import com.agreemount.slaneg.annotation.HandleWithImplementation;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@EqualsAndHashCode
@HandleWithImplementation(runner = SetMetricsFromFormImpl.class)
public class SetMetricsFromForm extends Action {
    private String alias;
}
