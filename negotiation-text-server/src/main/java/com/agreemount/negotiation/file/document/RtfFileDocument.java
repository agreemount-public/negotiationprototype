package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.apache.commons.io.FileUtils;

import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;
import java.io.ByteArrayInputStream;
import java.io.File;

import static com.google.common.base.Preconditions.checkNotNull;


public class RtfFileDocument implements IFileDocument {

    private File file;

    public RtfFileDocument(File file) {
        checkNotNull(file, "given null instead of file");
        this.file = file;
    }

    @Override
    public String convert() {
        try{
            ByteArrayInputStream in =
                    new ByteArrayInputStream(FileUtils.readFileToByteArray(file));

            RTFEditorKit rtfParser = new RTFEditorKit();
            Document document = rtfParser.createDefaultDocument();
            rtfParser.read(in, document, 0);
            return document.getText(0, document.getLength());
        }
        catch (Exception e){
            throw new DocumentConverterException("Cannot process rtf document ", e);
        }
    }
}
