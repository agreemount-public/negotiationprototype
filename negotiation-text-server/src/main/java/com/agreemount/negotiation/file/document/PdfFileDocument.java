package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.File;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;


public class PdfFileDocument implements IFileDocument {

    private File file;

    public PdfFileDocument(File file) {
        checkNotNull(file, "given null instead of file");
        this.file = file;
    }

    @Override
    public String convert() {
        try {
            PDDocument pd = PDDocument.load(file.getAbsolutePath());
            PDFTextStripper reader = new PDFTextStripper();
            return reader.getText(pd);
        } catch (IOException e) {
            throw new DocumentConverterException(e);
        }
    }
}

