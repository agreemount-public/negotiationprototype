package com.agreemount.negotiation.file.document;



@FunctionalInterface
public interface IFileDocument {

    String convert();

}
