package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

import java.io.File;
import java.io.FileInputStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkNotNull;

public class DocFileDocument implements IFileDocument {

    private File file;

    public DocFileDocument(File file) {
        checkNotNull(file, "given null instead of file");
        this.file = file;
    }

    @Override
    public String convert() {
        try{
            WordExtractor extractor;
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            HWPFDocument document = new HWPFDocument(fis);
            extractor = new WordExtractor(document);

            return Stream.of(extractor.getParagraphText())
                    .collect(Collectors.joining())
                    .replaceAll("\r\n", "\n")
                    .trim();
        }
        catch (Exception e){
            throw new DocumentConverterException("Cannot process doc document ", e);
        }
    }
}
