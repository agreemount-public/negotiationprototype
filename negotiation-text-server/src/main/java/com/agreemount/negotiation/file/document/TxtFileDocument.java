package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.google.common.base.Preconditions.checkNotNull;


public class TxtFileDocument implements IFileDocument {

    private File file;

    public TxtFileDocument(File file) {
        checkNotNull(file, "given null instead of file");
        this.file = file;
    }

    @Override
    public String convert() {
        try {
            return Files.lines(Paths.get(file.getAbsolutePath()))
                    .map(e -> e.toString())
                    .reduce("", (a, b) -> a+"\n"+b)
                    .trim();

        } catch (IOException e) {
            throw new DocumentConverterException("Cannot process txt document ", e);
        }
    }

}
