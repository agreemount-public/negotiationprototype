package com.agreemount.negotiation.file;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.logic.ActionLogic;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.file.document.IFileDocument;
import com.agreemount.negotiation.logic.impl.TeamLogic;
import com.agreemount.slaneg.action.ActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import static com.agreemount.negotiation.util.DocumentUtil.DOCUMENT_BODY_KEY;

@Component
public class FileDocumentLogic {

    @Autowired
    private IdentityProvider identityProvider;

    @Autowired
    private ActionLogic actionLogic;

    @Autowired
    private FileDocumentFactory fileDocumentFactory;

    @Autowired
    private TeamLogic teamLogic;

    private String saveDocument(String documentName, String documentBody) {
        Team team = teamLogic.saveTeamWithOwner(identityProvider.getIdentity().getLogin());

        Document document = new Document();
        document.setName(documentName);
        document.setTeam(team.getName());
        document.getMetrics().put(DOCUMENT_BODY_KEY, documentBody);

        ActionContext actionContext = actionLogic.runAction(document, "BASE", "createNewNegotiation-textual");
        document = actionContext.getDocument("newRoot");

        return document.getId();
    }

    public String retrieveAndSave(String documentName, MultipartFile multipartFile) {
        IFileDocument fileDocument = fileDocumentFactory.createFileDocument(multipartFile);
        String documentBody = fileDocument.convert();
        return this.saveDocument(documentName, documentBody);
    }
}
