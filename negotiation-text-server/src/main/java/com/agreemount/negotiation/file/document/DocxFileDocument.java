package com.agreemount.negotiation.file.document;

import com.agreemount.negotiation.exception.DocumentConverterException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class DocxFileDocument implements IFileDocument {

    private File file;

    public DocxFileDocument(File file) {
        checkNotNull(file, "given null instead of file");
        this.file = file;
    }

    @Override
    public String convert() {
        try (FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            XWPFDocument document = new XWPFDocument(fis)){
            List<XWPFParagraph> paragraphs = document.getParagraphs();

            return paragraphs
                .stream()
                .map(e -> e.getText())
                .reduce("", (a, b) -> a+"\n"+b)
            ;
        } catch (Exception e) {
            throw new DocumentConverterException("Cannot process docx document ", e);
        }
    }
}
