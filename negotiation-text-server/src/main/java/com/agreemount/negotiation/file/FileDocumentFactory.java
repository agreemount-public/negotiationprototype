package com.agreemount.negotiation.file;

import com.agreemount.negotiation.exception.DocumentConverterException;
import com.agreemount.negotiation.exception.FileNotFoundException;
import com.agreemount.negotiation.exception.UnsupportedFormatException;
import com.agreemount.negotiation.file.document.*;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Log4j
@Component
public class FileDocumentFactory {

    public IFileDocument createFileDocument(MultipartFile multipartFile)  {
        if (multipartFile==null || multipartFile.getOriginalFilename()==null){
            log.error("given null instead of multipart file");
            throw new FileNotFoundException();
        }

        File file = convertMultipartToFile(multipartFile);
        String extension = FilenameUtils.getExtension(file.getName());

        switch (FileFormat.key(extension.toLowerCase())){
            case PDF:
                return new PdfFileDocument(file);
            case TXT:
                return new TxtFileDocument(file);
            case DOC:
                return new DocFileDocument(file);
            case RTF:
                return new RtfFileDocument(file);
            case DOCX:
                return new DocxFileDocument(file);
            default:
                throw new UnsupportedFormatException("unsupported format exception ["+extension+"]");
        }
    }

    private File convertMultipartToFile(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());

        try (FileOutputStream fos = new FileOutputStream(convFile)){
            convFile.createNewFile();
            fos.write(file.getBytes());
        } catch (IOException e) {
            throw new DocumentConverterException("Cannot convert multipart to convFile ", e);
        }
        return convFile;
    }

}
