package com.agreemount.negotiation.file;

import com.agreemount.negotiation.exception.UnsupportedFormatException;

public enum FileFormat {
    PDF("pdf"),
    DOC("doc"),
    DOCX("docx"),
    RTF("rtf"),
    TXT("txt"),
    PAGES("pages");

    String value;

    FileFormat(String value){
        this.value = value;
    }

    public final String value() {
        return value;
    }

    public static Boolean contains(String ext) {
        for (FileFormat format : FileFormat.values()){
            if (format.value().equals(ext)){
                return true;
            }
        }
        return false;
    }

    public static FileFormat key(String extension) {
        for (FileFormat format : FileFormat.values()){
            if (format.value().equals(extension)){
                return format;
            }
        }

        throw new UnsupportedFormatException("unsupported format exception ["+extension+"]");
    }
}
