package com.agreemount.negotiation.interaction.cron;

import com.agreemount.negotiation.Application;
import com.agreemount.negotiation.configuration.CronConfiguration;
import com.agreemount.negotiation.interaction.processing.InteractionLogsProcessor;
import com.agreemount.slaneg.fixtures.FileRulesProvidersConfiguration;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import java.util.HashMap;
import java.util.Map;


@ComponentScan(
        basePackages = {"com.agreemount"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Application.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = FileRulesProvidersConfiguration.class)
        }
)
@Configuration
@Log4j
public class ProcessInteractionLogJobConfig {

    @Value("${cron.processInteractionLog.repeatInterval}")
    private int repeatInterval;

    @Value("${cron.processInteractionLog.startDelay}")
    private int startDelay;

    @Autowired
    private InteractionLogsProcessor interactionLogsProcessor;

    @Bean
    public SimpleTriggerFactoryBean processInteractionLog() {
        SimpleTriggerFactoryBean stFactory = new SimpleTriggerFactoryBean();
        stFactory.setJobDetail(processInteractionLogJob().getObject());
        stFactory.setStartDelay(startDelay * CronConfiguration.ONE_MINUTE_IN_MS);
        stFactory.setRepeatInterval(repeatInterval * CronConfiguration.ONE_MINUTE_IN_MS);
        return stFactory;
    }

    @Bean
    public JobDetailFactoryBean processInteractionLogJob() {
        JobDetailFactoryBean factory = new JobDetailFactoryBean();
        factory.setJobClass(ProcessInteractionLogJob.class);

        Map<String, Object> map = new HashMap<>();
        map.put("interactionLogsProcessor", interactionLogsProcessor);
        factory.setJobDataAsMap(map);

        factory.setName("processInteractionLogJob");
        return factory;
    }

}
