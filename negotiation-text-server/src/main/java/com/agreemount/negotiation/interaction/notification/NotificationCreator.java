package com.agreemount.negotiation.interaction.notification;

import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.interaction.InteractionLog;
import com.agreemount.negotiation.interaction.notification.dao.InteractionNotificationDAO;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j
public class NotificationCreator {

    protected final static String TITLE = "Your spouse have send you new offer or made some change";

    @Autowired
    private InteractionNotificationDAO interactionNotificationDAO;

    @Autowired
    private CreateNotificationFromInteractionPredicate createNotificationFromInteractionPredicate;

    public void create(String negotiationDocumentId, InteractionLog interactionLog, String userEmail) {
        try {
            if (createNotificationFromInteractionPredicate.test(new InterActionLogWithReceiver(negotiationDocumentId, interactionLog, userEmail))) {

                NotificationBuilder notificationBuilder = new NotificationBuilder();
                notificationBuilder
                        .setNotificationStatus(NotificationStatus.NEW)
                        .setNotificationType(NotificationType.OTHER_SIDE_INTERACTION)
                        .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), userEmail))
                        .addParameter(new Notification.Parameter(Notification.Parameter.Keys.DOCUMENT_ID.getValue(), negotiationDocumentId))
                        .addParameter(new Notification.Parameter(Notification.Parameter.Keys.TITLE.getValue(), TITLE))
                ;

                Notification notification = notificationBuilder.build();
                interactionNotificationDAO.save(notification);
            }
        }
        catch (Exception e) {
            log.error(e);
        }
    }

}
