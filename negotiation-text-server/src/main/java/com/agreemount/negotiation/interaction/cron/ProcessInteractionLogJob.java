package com.agreemount.negotiation.interaction.cron;

import com.agreemount.negotiation.interaction.processing.InteractionLogsProcessor;
import lombok.extern.log4j.Log4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;

@Log4j
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class ProcessInteractionLogJob extends QuartzJobBean {

    private InteractionLogsProcessor interactionLogsProcessor;

    @Override
        protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.debug("CRON: ProcessInteractionLogJob start...");
        interactionLogsProcessor.processAll();
        log.debug("CRON: ProcessInteractionLogJob end...");
    }

    public void setInteractionLogsProcessor(InteractionLogsProcessor interactionLogsProcessor) {
        this.interactionLogsProcessor = interactionLogsProcessor;
    }
}
