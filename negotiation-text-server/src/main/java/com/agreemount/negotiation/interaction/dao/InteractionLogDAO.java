package com.agreemount.negotiation.interaction.dao;

import com.agreemount.negotiation.dao.impl.BaseDAO;
import com.agreemount.negotiation.interaction.InteractionLog;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public class InteractionLogDAO extends BaseDAO {

    public InteractionLog getNext() {
        Query query = new Query().addCriteria(Criteria.where("processedAt").is(null));

        return getMongoTemplate()
                .findOne(query, InteractionLog.class);
    }

    public void markSimilarAsProcessed(String slaUuid, String authorEmail) {
        Query query = new Query().addCriteria(new Criteria().andOperator(
                Criteria.where("processedAt").is(null),
                Criteria.where("slaUuid").is(slaUuid),
                Criteria.where("authorEmail").is(authorEmail)
        ));

        getMongoTemplate().updateMulti(query, Update.update("processedAt", LocalDateTime.now()), InteractionLog.class);
    }
}
