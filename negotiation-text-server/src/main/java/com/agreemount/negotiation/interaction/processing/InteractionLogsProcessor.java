package com.agreemount.negotiation.interaction.processing;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.dao.impl.TeamDAO;
import com.agreemount.negotiation.interaction.InteractionLog;
import com.agreemount.negotiation.interaction.dao.InteractionLogDAO;
import com.agreemount.negotiation.interaction.notification.NotificationCreator;
import com.agreemount.negotiation.logic.impl.AgreemountDocumentLogic;
import com.agreemount.negotiation.member.SideService;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j
public class InteractionLogsProcessor {

    @Autowired
    private SideService sideService;

    @Autowired
    private TeamDAO teamDAO;

    @Autowired
    private InteractionLogDAO interactionLogDAO;

    @Autowired
    private NotificationCreator notificationCreator;

    @Autowired
    private AgreemountDocumentLogic agreemountDocumentLogic;


    public void processAll() {
        InteractionLog interactionLog;

        do {
            interactionLog = interactionLogDAO.getNext();
            if (interactionLog != null) {
                process(interactionLog);
            }
        } while (interactionLog != null);
    }

    void process(InteractionLog interactionLog) {
        String authorsEmail = interactionLog.getAuthorEmail();
        String documentId = interactionLog.getDocumentId();

        Document mainNegotiation = null;

        try {
            mainNegotiation = agreemountDocumentLogic.getMainNegotiation(documentId);

            String mainNegotiationId = mainNegotiation.getId();

            Team team = teamDAO.getTeamByName(mainNegotiation.getTeam());
            Preconditions.checkNotNull(team, "Team with name " + mainNegotiation.getTeam() + " was not found");

            List<String> otherSides = sideService.getOtherSideEmail(team, authorsEmail);

            otherSides.forEach(email -> notificationCreator.create(mainNegotiationId, interactionLog, email));
        }
        catch (Exception e) {
            //future improvement - if notification log has valid data - don`t make it as processed in case of exceptions
            log.error(e);
        }

        if (mainNegotiation != null) {
            interactionLogDAO.markSimilarAsProcessed(mainNegotiation.getSlaUuid(), interactionLog.getAuthorEmail());
        }
    }


}
