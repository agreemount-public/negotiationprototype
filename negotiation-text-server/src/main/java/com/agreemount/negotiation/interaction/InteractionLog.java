package com.agreemount.negotiation.interaction;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "interactionLog")
@Data
public class InteractionLog {

    @Id
    private String id;
    private String documentId;
    private String slaUuid;
    private String authorEmail;
    private LocalDateTime createdAt;
    private LocalDateTime processedAt;
}
