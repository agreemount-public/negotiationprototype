package com.agreemount.negotiation.interaction.action;

import com.agreemount.slaneg.action.definition.Action;
import com.agreemount.slaneg.annotation.HandleWithImplementation;
import lombok.Data;

@Data
@HandleWithImplementation(runner = CreateInteractionLogActionImpl.class)
public class CreateInteractionLogDef extends Action {
    private String documentAlias;
}
