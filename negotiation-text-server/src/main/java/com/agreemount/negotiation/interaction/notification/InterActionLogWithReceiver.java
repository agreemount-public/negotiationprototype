package com.agreemount.negotiation.interaction.notification;

import com.agreemount.negotiation.interaction.InteractionLog;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class InterActionLogWithReceiver {
    private String negotiationDocumentId;
    private InteractionLog interactionLog;
    private String receiver;
}
