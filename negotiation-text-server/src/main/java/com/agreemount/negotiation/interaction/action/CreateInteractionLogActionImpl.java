package com.agreemount.negotiation.interaction.action;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.Identity;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.interaction.InteractionLog;
import com.agreemount.negotiation.interaction.dao.InteractionLogDAO;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.impl.AbstractActionImpl;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@Component
@Scope("prototype")
@Log4j
public class CreateInteractionLogActionImpl extends AbstractActionImpl<CreateInteractionLogDef, Document, ActionContext<Document>> {

    @Autowired
    private IdentityProvider identityProvider;

    @Autowired
    private InteractionLogDAO interactionLogDAO;

    @Override
    protected void run(ActionContext actionContext) {
        String alias = definition.getDocumentAlias();
        Preconditions.checkArgument(!StringUtils.isEmpty(alias), "Alias is not set!");

        Document document = actionContext.getDocument(alias);
        Preconditions.checkNotNull(document, "document with alias [%s] was not found", alias);

        Identity identity = identityProvider.getIdentity();
        Preconditions.checkNotNull(identity, "no identity");

        InteractionLog interactionLog = new InteractionLog();
        interactionLog.setDocumentId(document.getId());
        interactionLog.setSlaUuid(document.getSlaUuid());
        interactionLog.setCreatedAt(LocalDateTime.now());
        interactionLog.setAuthorEmail(identity.getLogin());

        interactionLogDAO.save(interactionLog);
    }
}
