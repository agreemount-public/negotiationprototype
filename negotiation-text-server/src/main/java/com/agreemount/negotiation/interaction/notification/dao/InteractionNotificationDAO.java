package com.agreemount.negotiation.interaction.notification.dao;

import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.impl.BaseDAO;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.DOCUMENT_ID;
import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.RECEIVER;

@Repository
public class InteractionNotificationDAO extends BaseDAO{

    public boolean existNewerThanDay(String documentId, String email) {
        Query query = new Query()
                .addCriteria(new Criteria().andOperator(
                        Criteria.where("notificationType").is(NotificationType.OTHER_SIDE_INTERACTION),
                        Criteria.where("parameters").in(new Notification.Parameter(RECEIVER.getValue(), email)),
                        Criteria.where("parameters").in(new Notification.Parameter(DOCUMENT_ID.getValue(), documentId)),
                        Criteria.where("notificationStatus").ne(NotificationStatus.FAILED),
                        Criteria.where("createdAt").gte(LocalDateTime.now().minusDays(1l))
                ));

        return getMongoTemplate().exists(query, Notification.class);
    }

}
