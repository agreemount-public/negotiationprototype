package com.agreemount.negotiation.interaction.notification;

import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.agreemount.negotiation.interaction.InteractionLog;
import com.agreemount.negotiation.interaction.notification.dao.InteractionNotificationDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

@Component
public class CreateNotificationFromInteractionPredicate implements Predicate<InterActionLogWithReceiver> {

    public static final long LAST_LOGIN_OFFSET = 15l;
    @Autowired
    private InteractionNotificationDAO interactionNotificationDAO;

    @Autowired
    private UserDAO userDAO;

    public boolean test(InterActionLogWithReceiver interActionLogWithReceiver) {
        String userEmail = interActionLogWithReceiver.getReceiver();

        User user = userDAO.getByEmail(userEmail);
        if (user == null) {
            return false;
        }

        boolean hasNotificationNewerThanDay = interactionNotificationDAO.existNewerThanDay(interActionLogWithReceiver.getNegotiationDocumentId(), userEmail);
        boolean notLoggedRecently = notLoggedRecently(user, interActionLogWithReceiver.getInteractionLog());

        return notLoggedRecently && !hasNotificationNewerThanDay;
    }

    private boolean notLoggedRecently(User user, InteractionLog interactionLog) {
        return user.getLastLoginAt() == null
                || user.getLastLoginAt().plusMinutes(LAST_LOGIN_OFFSET).isBefore(interactionLog.getCreatedAt());

    }
}
