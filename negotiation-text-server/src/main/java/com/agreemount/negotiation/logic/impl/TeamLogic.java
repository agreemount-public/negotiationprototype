package com.agreemount.negotiation.logic.impl;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.logic.ITeamLogic;
import com.agreemount.negotiation.user.UserService;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Component
public class TeamLogic implements ITeamLogic {

    @Autowired
    private ITeamDAO teamDAO;

    @Autowired
    private UserService userService;

    private static User2Team getUser2Team(String login) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(login);
        user2Team.setRole(UserRole.SIDE1);
        return user2Team;
    }

    @Override
    public Team saveTeamWithOwner(String login) {
        Preconditions.checkArgument(!StringUtils.isEmpty(login), "Cannot save team without a member");
        Team team = new Team();
        team.setUsers2Team(Lists.newArrayList(getUser2Team(login)));
        teamDAO.save(team);
        return team;
    }

    @Override
    public Optional<User2Team> getUserWithRole(String teamName, UserRole userRole) {
        Team team = teamDAO.getTeamByName(teamName);
        return Optional.ofNullable(team)
                .map(Team::getUsers2Team)
                .orElse(Lists.newArrayList())
                .stream()
                .filter(hasRole(userRole))
                .findAny();
    }

    @Override
    public Optional<UserRole> userRoleInTeam(String teamName, @NotNull String login) {
        return Optional.ofNullable(teamDAO.getTeamByName(teamName))
                .map(Team::getUsers2Team)
                .orElse(new ArrayList<>())
                .stream()
                .filter(hasUser(login))
                .map(User2Team::getRole)
                .findAny();
    }

    @Override
    public List<User> getNegotiationSidesOf(String email) {
        List<Team> teams = teamDAO.getUserTeams(email);
        return teams.stream()
                .map(team -> userService.getUsersOfTeam(team, email))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Predicate<User2Team> hasRole(UserRole userRole) {
        return user2Team -> user2Team.getRole().equals(userRole);
    }

    private Predicate<User2Team> hasUser(String userLogin) {
        return user2Team -> userLogin.equals(user2Team.getLogin());
    }
}
