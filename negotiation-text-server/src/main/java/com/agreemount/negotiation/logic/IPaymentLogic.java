package com.agreemount.negotiation.logic;

import java.util.Map;


public interface IPaymentLogic {

    void changeAccountBalanceManually(String userId, Long paymentUnits);

    void changeAccountBalance(Map<String, String> payload);

}
