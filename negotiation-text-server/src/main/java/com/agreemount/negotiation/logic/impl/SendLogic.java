package com.agreemount.negotiation.logic.impl;

import com.agreemount.negotiation.bean.notification.GeneratedNotification;
import com.agreemount.negotiation.bean.notification.GeneratedNotificationStatus;
import com.agreemount.negotiation.dao.IGeneratedNotificationDAO;
import com.agreemount.negotiation.logic.ISendLogic;
import com.agreemount.negotiation.sender.ISender;
import com.mongodb.MongoException;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;



@Log4j
@Component
public class SendLogic implements ISendLogic {

    @Value("${mailer.nextProcessing.delayInHours}")
    private int nextProcessingDelayInHours;

    @Value("${mailer.maxSendingAttempts}")
    private int maxSendingAttempts;

    @Autowired
    private IGeneratedNotificationDAO generatedNotificationDAO;

    @Autowired
    private ISender sender;

    @Override
    public void send() {
        GeneratedNotification notifi;

        do {
            notifi = generatedNotificationDAO.getNext();
            if (notifi != null) {
                sendEmail(notifi);
                updateNotification(notifi);
            }

        } while (notifi != null);
    }

    private void sendEmail(GeneratedNotification generatedNotification) {
        try {
            sender.send(generatedNotification.getPreparedEmail());
            generatedNotification.setSentAt(LocalDateTime.now());
            generatedNotification.setStatus(GeneratedNotificationStatus.PROCESSED);
        } catch (Exception ex) {
            log.error("Cannot send email [" + generatedNotification.toString() + "]", ex);
        }
    }

    private void updateNotification(GeneratedNotification generatedNotification) {
        LocalDateTime today = LocalDateTime.now();
        if (generatedNotification.getSentAt() == null) {
            if (generatedNotification.getAttempts() >= maxSendingAttempts) {
                generatedNotification.setStatus(GeneratedNotificationStatus.FAILED);
            }
            generatedNotification.setNextProcessing(today.plusHours(nextProcessingDelayInHours));
        }
        generatedNotification.setAttempts(generatedNotification.getAttempts() + 1);

        try {
            generatedNotificationDAO.save(generatedNotification);
        } catch (MongoException ex) {
            log.error("Cannot save generated notification [" + generatedNotification.toString() + "]", ex);
        }
    }

}
