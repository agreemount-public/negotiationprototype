package com.agreemount.negotiation.logic.processing;

import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.dao.INotificationDAO;
import com.agreemount.negotiation.notification.ProcessorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
public class ProcessNotificationLogic implements IProcessNotificationLogic {

    @Autowired
    private INotificationDAO notificationDAO;

    @Autowired
    private ProcessorFactory processorFactory;

    @Override
    public void retrieve() {
        Notification notification;

        do {
            notification = notificationDAO.getNext();
            if (notification != null) {
                processorFactory
                    .createProcessor(notification)
                    .processNotification(notification)
                ;
            }
        } while (notification != null);
    }

}
