package com.agreemount.negotiation.logic;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;

import java.util.List;
import java.util.Optional;


public interface ITeamLogic {
    Team saveTeamWithOwner(String login);

    List<User> getNegotiationSidesOf(String email);

    Optional<User2Team> getUserWithRole(String teamName, UserRole userRole);

    Optional<UserRole> userRoleInTeam(String teamName, String login);
}
