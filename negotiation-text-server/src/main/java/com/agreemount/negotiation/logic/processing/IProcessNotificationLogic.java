package com.agreemount.negotiation.logic.processing;


@FunctionalInterface
public interface IProcessNotificationLogic {

    void retrieve();

}
