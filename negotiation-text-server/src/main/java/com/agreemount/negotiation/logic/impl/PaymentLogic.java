package com.agreemount.negotiation.logic.impl;

import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.UserPaymentUnitsEntry;
import com.agreemount.negotiation.dao.IPayPalResponseDAO;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.logic.IPaymentLogic;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;


@Component
@Log4j
public class PaymentLogic implements IPaymentLogic {

    private static final String HISTORY_DESCRIPTION = "REST payment";
    private static final String CANNOT_SAVE_USER = "Cannot save user:";
    private static final String CANNOT_CONVERT_PAYPAL_UNITS = "Cannot convert paypal units:";
    private static final String USER_ID_FIELD = "custom";
    private static final String SELECTED_OPTION = "option_selection1";

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private IPayPalResponseDAO payPalResponseDAO;

    @Override
    public void changeAccountBalanceManually(String userId, Long paymentUnitsToAdd) {
        updateUserBalance(userId, paymentUnitsToAdd);
    }

    @Override
    public void changeAccountBalance(Map<String, String> payload) {
        String payPalPaymentUnits = payload.get(SELECTED_OPTION);
        String userId = payload.get(USER_ID_FIELD);

        log.info("[PayPal] PaymentLogic payPalPaymentUnits: ["+payPalPaymentUnits+"] userId: ["+userId+"]");

        try {
            Optional<Long> units = Arrays
                    .stream(payPalPaymentUnits.trim().split("\\s"))
                    .map(s -> Long.parseLong(s))
                    .findFirst();

            updateUserBalance(userId, units.get());
            insertAuditRecord(payload);
        } catch (Exception ex) {
            throw new AuthException(CANNOT_CONVERT_PAYPAL_UNITS + " [" + payPalPaymentUnits + "]", ex);
        }
    }

    private void insertAuditRecord(Map<String, String> payload) {
        payPalResponseDAO.insert(payload);
    }

    private void updateUserBalance(String userId, Long paymentUnitsToAdd) {
        try {
            User user = userDAO.getById(userId);
            Long userPaymentUnits = user.getPaymentUnits() != null ? user.getPaymentUnits() : 0L;
            user.setPaymentUnits(userPaymentUnits + paymentUnitsToAdd);
            UserPaymentUnitsEntry entry = createUserPaymentUnitsEntry(paymentUnitsToAdd, user, userPaymentUnits);
            user.addUserPaymentUnitsEntry(entry);
            userDAO.save(user);
        } catch (Exception ex) {
            throw new AuthException(CANNOT_SAVE_USER + " [" + userId + "]", ex);
        }
    }

    private static UserPaymentUnitsEntry createUserPaymentUnitsEntry(Long paymentUnitsToAdd, User user, Long userPaymentUnits) {
        UserPaymentUnitsEntry entry = new UserPaymentUnitsEntry();
        entry.setCreatedAt(LocalDateTime.now());
        entry.setRequestedAmount(paymentUnitsToAdd);
        entry.setPaymentUnitsBefore(userPaymentUnits);
        entry.setPaymentUnitsAfter(user.getPaymentUnits());
        entry.setDescription(HISTORY_DESCRIPTION);
        return entry;
    }
}
