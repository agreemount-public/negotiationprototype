package com.agreemount.negotiation.logic;


@FunctionalInterface
public interface ISendLogic {

    void send();
}
