package com.agreemount.negotiation.logic.impl;

import com.agreemount.EngineFacade;
import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.engine.facade.QueryFacade;
import com.agreemount.logic.ActionLogic;
import com.agreemount.negotiation.bean.NegotiationOnDashboard;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.agreemount.slaneg.db.DocumentOperations;
import com.agreemount.slaneg.exception.ForbiddenException;
import com.agreemount.slaneg.message.Message;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class AgreemountDocumentLogic {

    private static final String FETCH_MAIN_NEGOTIATION_QUERY = "fetchMainNegotiation";

    @Autowired
    private IdentityProvider identityProvider;

    @Autowired
    private ActionLogic actionLogic;

    @Autowired
    private TeamLogic teamLogic;

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Autowired
    private QueryFacade queryFacade;

    @Autowired
    private EngineFacade engineFacade;

    @Autowired
    private DocumentOperations documentOperations;


    public String saveEmptyDocument(String negotiationType) {
        if (identityProvider.getIdentity() == null) {
            throw new ForbiddenException("You have to be logged in to create new negotiation");
        }

        Team team = teamLogic.saveTeamWithOwner(identityProvider.getIdentity().getLogin());

        Document document = new Document();
        document.setName(UUID.randomUUID().toString());
        document.setTeam(team.getName());
        ActionContext actionContext = actionLogic.runAction(document, "BASE", "createNewNegotiation-" + negotiationType);
        document = actionContext.getDocument("newRoot");

        return document.getId();
    }

    public List<NegotiationOnDashboard> fetchDashboardData() {
        List<Document> negotiations = queryFacade.getDocumentsForQuery("mySplits", actionContextFactory.createInstance());
        List<NegotiationOnDashboard> results = new ArrayList<>();

        for (Document negotiation : negotiations) {
            List<Message> messages = engineFacade.getAvailableMessages(actionContextFactory.createInstance(negotiation));
            NegotiationOnDashboard nod = new NegotiationOnDashboard(negotiation, messages);
            setParticipants(negotiation.getTeam(), nod);

            results.add(nod);
        }
        return results;
    }

    private void setParticipants(String team, NegotiationOnDashboard nod) {
        Optional<User2Team> owner = teamLogic.getUserWithRole(team, UserRole.SIDE1);
        Optional<User2Team> invited = teamLogic.getUserWithRole(team, UserRole.SIDE2);

        if (invited.isPresent()) {
            nod.setInvited(invited.get().getLogin());
        }

        if (owner.isPresent()) {
            nod.setOwner(owner.get().getLogin());
        }
    }

    public Document getCurrentNegotiation(String documentId) {
        Preconditions.checkNotNull(documentId, "required parameter is missing [documentId]");
        Document document = documentOperations.getDocument(documentId);

        Preconditions.checkNotNull(document, "document not found: [" + documentId + "]");
        return document;
    }

    public Document getMainNegotiation(String childDocumentId) {
        Document document = getCurrentNegotiation(childDocumentId);
        return queryFacade.getDocumentsForQuery(FETCH_MAIN_NEGOTIATION_QUERY, actionContextFactory.createInstance(document))
                .stream()
                .findFirst()
                .orElseThrow(() -> new NullPointerException("Main negotiation document not found "));
    }

}
