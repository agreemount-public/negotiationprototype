package com.agreemount.negotiation.document.action;

import com.agreemount.Response;
import com.agreemount.bean.document.Document;
import com.agreemount.bean.response.ActionResponse;


@FunctionalInterface
public interface IDocumentAction {
    Response<ActionResponse> apply(Document change);
}
