package com.agreemount.negotiation.document.merger;

import com.agreemount.bean.document.Document;


@FunctionalInterface
public interface IChangeMerger {
    Document merge(Document mainDocument, Document documentChange);
}
