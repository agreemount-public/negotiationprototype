package com.agreemount.negotiation.document.action;

import com.agreemount.EngineFacade;
import com.agreemount.Response;
import com.agreemount.bean.document.Document;
import com.agreemount.bean.response.ActionResponse;
import com.agreemount.bean.response.RedirectActionResponse;
import com.agreemount.negotiation.document.cleaner.ITagCleaner;
import com.agreemount.negotiation.negotiation.IChangesRepository;
import com.agreemount.negotiation.util.DocumentUtil;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component(value = "rejectChange")
public class RejectChange implements IDocumentAction{

    @Autowired
    private IChangesRepository changesRepository;

    @Autowired
    private ActionContextFactory actionContextFactory;

    @Autowired
    private EngineFacade engineFacade;

    @Autowired
    @Qualifier(value="tagCleaner")
    private ITagCleaner<String, String> tagCleaner;

    @Override
    public Response<ActionResponse> apply(Document change){
        Response<ActionResponse> response = new Response<>();
        Document main = changesRepository.mainForChange(change);
        Map<String, Object> cleanContent = cleanContent(change, main);
        ActionContext actionContext = updateContenOfMainDocument(main, cleanContent);

        addRedirectToResponse(actionContext, response);
        return response;
    }

    private ActionContext updateContenOfMainDocument(Document main, Map<String, Object> mergedContent) {

        ActionContext actionContext = actionContextFactory.createInstance(main);
        Document metricsDocument = new Document();
        metricsDocument.setMetrics(mergedContent);
        actionContext.addDocument("METRICS",metricsDocument);
        engineFacade.runAction(actionContext, "createNewVersionOfChange");
        return actionContext;
    }

    private Map<String, Object> cleanContent(Document change, Document main) {
        String mainContent = DocumentUtil.getBodyOf(main);
        String changeTagName = DocumentUtil.getTagName(change);

        String cleanedMainContent = tagCleaner.clean(mainContent, Sets.newHashSet(changeTagName));

        Map<String,Object> contentOfNewVersion = new HashMap<>();
        contentOfNewVersion.put("content", cleanedMainContent);
        return contentOfNewVersion;
    }

    private void addRedirectToResponse(ActionContext actionContext, Response response) {
        if (actionContext.getRedirectToAlias() != null && actionContext.getRedirectToAlias() != "") {
            Document documentToRedirect = actionContext.getDocument(actionContext.getRedirectToAlias());
            if (documentToRedirect != null) {
                RedirectActionResponse redirectActionResponse = new RedirectActionResponse();
                redirectActionResponse.setRedirectToDocument(documentToRedirect.getId());

                response.setData(redirectActionResponse);
            }
        }
    }
}
