package com.agreemount.negotiation.document.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Set;

@Builder
@EqualsAndHashCode
public class DocumentView {

    //Document with merged body and Set of unresolved/conflicted ContentTags
    private final Document document;
    private final Set<String> conflictedTags;//nie dało się zmergowac
    private final Set<String> unresolvedTags;//nie dało się wyświetlić

    public DocumentView(Document document, Set<String> conflictedTags,
                        Set<String> unresolvedTags) {
        super();
        this.document = document;
        this.conflictedTags = conflictedTags;
        this.unresolvedTags = unresolvedTags;
    }

    public String getBody() {
        return document.getBody();
    }

    public Document getDocument() {
        return document;
    }

    public Set<String> getConflictedTags() {
        if (conflictedTags.isEmpty()) {
            return new HashSet<>();
        }

        return conflictedTags;
    }

    public Set<String> getUnresolvedTags() {
        if (unresolvedTags.isEmpty()) {
            return new HashSet<>();
        }

        return unresolvedTags;
    }

}
