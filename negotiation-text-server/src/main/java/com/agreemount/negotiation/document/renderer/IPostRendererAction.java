package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.document.model.DocumentView;


@FunctionalInterface
public interface IPostRendererAction {
    DocumentView run(DocumentView documentView);
}
