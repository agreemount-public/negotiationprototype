package com.agreemount.negotiation.document.converter;

import com.agreemount.negotiation.document.model.Document;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component(value = "nl2BrDocumentConverter")
public class Nl2BrDocumentConverter implements IConverter<Document, Document> {
    @Autowired
    @Qualifier(value = "nl2BrConverter")
    private IConverter<String, String> converter;

    @Override
    public Document convert(Document document) {
        Preconditions.checkNotNull(document, "Can not convert null Object");
        return new Document(
                converter.convert(document.getBody()),
                document.getDocumentTag(),
                document.getContentTags()
        );
    }
}
