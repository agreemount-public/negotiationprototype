package com.agreemount.negotiation.document.converter;

import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@Component(value = "nl2BrConverter")
@Log4j
public class Nl2BrConverter implements IConverter<String, String> {

    @Value("${converter.new.line.html.tag}")
    private String newLineHtmlTag;

    @Override
    public String convert(String input) {
        Preconditions.checkNotNull(input, "Can not convert null object");

        Scanner scanner = new Scanner(input);
        List lines = new ArrayList<>();

        while (scanner.hasNextLine()) {
            lines.add(scanner.nextLine());
        }

        return String.join(newLineHtmlTag, lines);
    }
}
