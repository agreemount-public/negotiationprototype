package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.document.model.DocumentResource;
import com.agreemount.negotiation.document.model.IDocumentResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DocumentResourceCleaner {

    @Autowired
    private ITagCleaner<Map<String, String>, String> documentPartsCleaner;

    @Autowired
    private ITagCleaner<String, String> documentBodyCleaner;

    public IDocumentResource clean(IDocumentResource resource) {

        return new DocumentResource(
                resource.getAcceptedTags(),
                resource.getRemainTags(),
                documentBodyCleaner.clean(resource.getBody(), unnecessaryTags(resource)),
                resource.getTag(),
                documentPartsCleaner.clean(resource.getDocumentParts(), unnecessaryTags(resource))
        );
    }

    private Set<String> unnecessaryTags(IDocumentResource resource) {
        Set<String> removedTags = new HashSet<>();
        for (String tagName : unnecessaryTagNames(resource)) {
            removedTags.add(tagName);
        }

        return removedTags;
    }

    private static Set<String> allDocumentTags(Map<String, String> documentPatrs) {
        return documentPatrs.keySet();
    }

    private Set<String> unnecessaryTagNames(IDocumentResource resource) {

        return allDocumentTags(resource.getDocumentParts()).stream()
                .filter(p -> !resource.getAcceptedTags().contains(p))
                .filter(p -> !resource.getRemainTags().contains(p))
                .distinct()
                .collect(Collectors.toSet());
    }
}
