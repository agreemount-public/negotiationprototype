package com.agreemount.negotiation.document.change;

import com.agreemount.negotiation.document.model.ITagDecorator;
import com.agreemount.negotiation.document.model.TagDecorator;

public class DocumentBody {

    private ITagDecorator tagDecorator;
    private String body;

    public DocumentBody(String originalDocumentBody) {
        body = originalDocumentBody;
        tagDecorator = new TagDecorator();
    }


    public String getWithChange(String documentChangeId, int positionStart, int positionEnd) {
        String openTag = tagDecorator.openTag(documentChangeId);
        String closeTag = tagDecorator.closeTag(documentChangeId);

        StringBuilder sb = new StringBuilder(body);
        sb.insert(positionStart, openTag.toCharArray());
        sb.insert(positionEnd + openTag.length(), closeTag.toCharArray());
        return sb.toString();
    }

}
