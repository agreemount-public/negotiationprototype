package com.agreemount.negotiation.document.converter;


@FunctionalInterface
public interface IConverter<T, R> {
    R convert(T t);
}
