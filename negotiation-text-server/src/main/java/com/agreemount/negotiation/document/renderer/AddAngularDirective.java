package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.document.model.Document;
import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.negotiation.document.renderer.actions.IAngularDirectiveAction;

public class AddAngularDirective implements IPostRendererAction {

    private final String directive;
    private final IAngularDirectiveAction angularDirective;

    public AddAngularDirective(IAngularDirectiveAction angularDirective, String directive) {
        this.angularDirective = angularDirective;
        this.directive = directive;
    }

    @Override
    public DocumentView run(DocumentView documentView){

        String bodyWithDirective = angularDirective.doIt(
            documentView.getBody(),
            documentView.getDocument().getContentTags(),
            this.directive
        );


        Document newDocument = new Document(
                bodyWithDirective,
                documentView.getDocument().getDocumentTag(),
                documentView.getDocument().getContentTags()
        );

        return new DocumentView(
                newDocument,
                documentView.getConflictedTags(),
                documentView.getUnresolvedTags()
        );
    }


}
