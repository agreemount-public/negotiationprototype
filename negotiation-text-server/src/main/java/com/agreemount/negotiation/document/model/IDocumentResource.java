package com.agreemount.negotiation.document.model;

import java.util.Map;
import java.util.Set;

public interface IDocumentResource {

    Set<String> getAcceptedTags();

    Set<String> getRemainTags();

    String getBody();

    String getTag();

    Map<String, String> getDocumentParts();

}
