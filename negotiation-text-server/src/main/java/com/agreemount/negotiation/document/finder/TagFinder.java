package com.agreemount.negotiation.document.finder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component(value = "tagFinder")
public class TagFinder implements ITagFinder<String, String> {

    @Autowired
    private TagRegexp tagRegexp;

    @Override
    public Set<String> find(String tagText) {
        Set<String> tags = new HashSet<>();
        Pattern pattern = Pattern.compile(tagRegexp.getTagWithContent());
        Matcher matcher = pattern.matcher(tagText);
        while (matcher.find()) {
            tags.add(matcher.group());
        }

        return tags;
    }

}
