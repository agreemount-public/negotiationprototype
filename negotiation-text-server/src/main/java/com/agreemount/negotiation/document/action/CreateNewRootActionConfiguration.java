package com.agreemount.negotiation.document.action;

import com.agreemount.bean.document.Document;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.definition.CreateNewDocument;
import com.agreemount.slaneg.action.definition.CreateNewRoot;
import com.agreemount.slaneg.action.impl.document.subaction.ApplyActionExtension;
import com.agreemount.slaneg.action.impl.document.subaction.root.AbstractCreateNewRootExtensionsProvider;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CreateNewRootActionConfiguration extends AbstractCreateNewRootExtensionsProvider {

    @Override
    public List<ApplyActionExtension<CreateNewRoot>> getActionExtensions() {
        return Lists.newArrayList(new SetDefaultOrder());
    }


    class SetDefaultOrder implements  ApplyActionExtension {

        @Override
        public void apply(Document document, ActionContext actionContext, CreateNewDocument def) {

            long time = System.currentTimeMillis();


            document.getAttributes().put(OrderSide.ORDER_SIDE_1.getValue(), time);
            document.getAttributes().put(OrderSide.ORDER_SIDE_2.getValue(), time);
        }
    }

}