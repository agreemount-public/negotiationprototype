package com.agreemount.negotiation.document.finder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;


@Getter
@Builder
@EqualsAndHashCode
public class TagRegexp {
    private final String openTag;
    private final String closeTag;
    private final String tagWithContent;
    private final String tagName;
    private final int tagNameRegexpGroup;
}
