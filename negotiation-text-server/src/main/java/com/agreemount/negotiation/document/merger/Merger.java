package com.agreemount.negotiation.document.merger;

import com.agreemount.negotiation.document.model.DocumentResource;
import com.agreemount.negotiation.document.model.IDocumentResource;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class Merger implements IMerger {

    @Override
    public IDocumentResource merge(IDocumentResource resource) {
        boolean replaced;
        do {
            replaced = false;

            Set<String> tags = new HashSet<>();
            tags.addAll(resource.getAcceptedTags());
            for (String tagName : tags) {
                Matcher matcher = contentTagMatcher(resource.getBody(), tagName);
                if (matcher.find()) {
                    replaced = true;
                    resource = replaceContentTags(resource, tagName, matcher);
                }
            }
        } while (replaced);

        return resource;
    }

    private static IDocumentResource replaceContentTags(IDocumentResource resource, String tagName, Matcher matcher) {

        String documentPart = resource.getDocumentParts().get(tagName);
        String replacedDocumentBody = matcher.replaceAll(documentPart);

        Map<String, String> filteredDocumentParts = resource.getDocumentParts()
                .entrySet().stream()
                .filter(p -> !p.getKey().equals(tagName))
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
        resource.getAcceptedTags().remove(tagName);

        return new DocumentResource(
                resource.getAcceptedTags(),
                resource.getRemainTags(),
                replacedDocumentBody,
                resource.getTag(),
                filteredDocumentParts
        );

    }

    private static Matcher contentTagMatcher(String documentBody, String tagName) {
        String regxp = new StringBuilder()
                .append(openTagRegexp(tagName))
                .append("((.*))")
                .append(closeTagRegexp(tagName))
                .toString();

        Pattern pattern = Pattern.compile(regxp);
        return pattern.matcher(documentBody);
    }

    private static String openTagRegexp(String tagName) {
        return new StringBuilder()
                .append("<(")
                .append(tagName)
                .append(")( +.+)*>")
                .toString();
    }

    private static String closeTagRegexp(String tagName) {
        return new StringBuilder()
                .append("</")
                .append(tagName)
                .append(">")
                .toString();
    }

}
