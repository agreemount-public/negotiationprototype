package com.agreemount.negotiation.document.merger;

import com.agreemount.negotiation.document.model.IDocumentResource;

@FunctionalInterface
public interface IMerger {
    IDocumentResource merge(IDocumentResource resource);
}
