package com.agreemount.negotiation.document.model;

import org.springframework.stereotype.Component;

@Component
public class TagDecorator implements ITagDecorator {

    @Override
    public String openTag(String tagName) {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append(tagName);
        sb.append(">");

        return sb.toString();
    }

    @Override
    public String closeTag(String tagName) {
        StringBuilder sb = new StringBuilder();
        sb.append("</");
        sb.append(tagName);
        sb.append(">");

        return sb.toString();
    }
}
