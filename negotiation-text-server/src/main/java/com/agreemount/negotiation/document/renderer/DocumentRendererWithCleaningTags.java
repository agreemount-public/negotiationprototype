package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.document.cleaner.DocumentResourceCleaner;
import com.agreemount.negotiation.document.cleaner.IDocumentConflictCleaner;
import com.agreemount.negotiation.document.converter.IConverter;
import com.agreemount.negotiation.document.merger.IMerger;
import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.negotiation.document.model.IDocumentResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component(value = "documentRendererWithCleaningTags")
public class DocumentRendererWithCleaningTags implements IRenderer {

    @Autowired
    private DocumentResourceCleaner documentResourceCleaner;

    @Autowired
    private IMerger merger;

    @Autowired
    private IDocumentConflictCleaner conflictCleaner;

    @Autowired
    @Qualifier(value = "nl2BrDocumentViewConverter")
    private IConverter<DocumentView, DocumentView> nl2brDocumentViewConverter;


    @Override
    public DocumentView render(IDocumentResource resource, IPostRendererAction postRendererAction) {
        IDocumentResource cleanedResource = documentResourceCleaner.clean(resource);
        IDocumentResource mergedResource = merger.merge(cleanedResource);

        DocumentView documentView = conflictCleaner.clean(
                mergedResource.getBody(),
                resource.getTag(),
                resource.getDocumentParts(),
                resource.getAcceptedTags(),
                resource.getRemainTags()
        );

        if (postRendererAction == null) {
            return nl2brDocumentViewConverter.convert(documentView);
        }

        return nl2brDocumentViewConverter.convert(postRendererAction.run(documentView));
    }


}

