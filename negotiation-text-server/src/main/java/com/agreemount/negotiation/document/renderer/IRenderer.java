package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.negotiation.document.model.IDocumentResource;

@FunctionalInterface
public interface IRenderer {
    DocumentView render(IDocumentResource resource, IPostRendererAction postRendererAction);
}