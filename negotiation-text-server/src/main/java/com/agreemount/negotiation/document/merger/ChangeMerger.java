package com.agreemount.negotiation.document.merger;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.util.DocumentUtil;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ChangeMerger implements IChangeMerger {

    @Override
    public Document merge(Document mainDocument, Document documentChange) {
        Document mergedDocument = mainDocument;
        String tagName = documentChange.getRootUuid();
        Matcher matcher = contentTagMatcher(DocumentUtil.getBodyOf(mainDocument), tagName);
        if (matcher.find()) {
            String mergedBody = matcher.replaceAll(DocumentUtil.getBodyOf(documentChange));
            DocumentUtil.setBody(mergedDocument, mergedBody);
        } else {
            throw new ChangeMergerException("Can not find tag in Main Document body");
        }

        return mergedDocument;
    }


    private static Matcher contentTagMatcher(String documentBody, String tagName) {
        String regxp = new StringBuilder()
                .append(openTagRegexp(tagName))
                .append("((.*))")
                .append(closeTagRegexp(tagName))
                .toString();

        Pattern pattern = Pattern.compile(regxp);
        return pattern.matcher(documentBody);
    }

    private static String openTagRegexp(String tagName) {
        return new StringBuilder()
                .append("<(")
                .append(tagName)
                .append(")( +.+)*>")
                .toString();
    }

    private static String closeTagRegexp(String tagName) {
        return new StringBuilder()
                .append("</")
                .append(tagName)
                .append(">")
                .toString();
    }

}
