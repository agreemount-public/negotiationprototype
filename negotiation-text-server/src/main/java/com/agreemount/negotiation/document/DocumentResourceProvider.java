package com.agreemount.negotiation.document;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.document.model.DocumentResource;
import com.agreemount.negotiation.document.model.IDocumentResource;
import com.agreemount.negotiation.negotiation.IChangesRepository;
import com.agreemount.negotiation.util.DocumentUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

@Component
public class DocumentResourceProvider {

    @Autowired
    private IChangesRepository changesRepository;

    public IDocumentResource provideFor(Document negotiationDocument) {

        Set<String> acceptedIds = changesRepository.acceptedChangesUuid(negotiationDocument);
        Set<String> remainIds = changesRepository.remainsChangesUuid(negotiationDocument);
        Map<String, String> allChanges = changesRepository.allChangesUuidConent(negotiationDocument);

        Document mainChange = changesRepository.mainChange(negotiationDocument);
        String documentBody = mainChange == null ? null : DocumentUtil.getBodyOf(mainChange);
        String documentTag = mainChange == null ? null : mainChange.getRootUuid();

        return new DocumentResource(acceptedIds, remainIds,
                documentBody, documentTag, allChanges);
    }
}
