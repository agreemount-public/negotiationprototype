package com.agreemount.negotiation.document.renderer;

import com.agreemount.bean.document.Document;
import com.agreemount.bean.identity.provider.IdentityProvider;
import com.agreemount.negotiation.document.converter.IConverter;
import com.agreemount.negotiation.document.finder.ITagFinder;
import com.agreemount.negotiation.document.helper.ITagHelper;
import com.agreemount.negotiation.document.renderer.actions.IAngularDirectiveAction;
import com.agreemount.negotiation.negotiation.IChangesRepository;
import com.agreemount.negotiation.util.DocumentUtil;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

import static com.agreemount.negotiation.bean.enums.States.DOCUMENT_TYPE;

@Component
@Log4j
public class DocumentChangeRenderer {

    private static final String CHANGE_STATE = "changeState";
    @Value("${angular.directive.content-tag}")
    private String angularDirective;

    @Autowired
    private IChangesRepository changesRepository;

    @Autowired
    @Qualifier(value = "tagFinder")
    private ITagFinder<String, String> tagFinder;

    @Autowired
    private IAngularDirectiveAction addDirective;

    @Autowired
    private ITagHelper tagHelper;

    @Autowired
    private IdentityProvider identityProvider;

    @Autowired
    @Qualifier(value = "nl2brDocumentBeanConverter")
    private IConverter<Document, Document> nl2brDocumentBeanConverter;

    public List<Document> render(Document negotiationDocument) {
        Document mainChange = changesRepository.mainChange(negotiationDocument);
        List<Document> allChanges = changesRepository.allChanges(negotiationDocument);
        List<Document> documents = Lists.newArrayList();


        for (Document document : allChanges) {
            /**
             * @Todo: przerobić na użycie kwerendy z engine'a ktore zwroci tylko zmiany widoczne dokumentyz uwzględnieniem:
             * moje drafty, moje wyslane, czyjes wysłane, w tym change i changeFromChange
             */
            if ("change".equalsIgnoreCase(document.getStates().get(DOCUMENT_TYPE.getValue()))
                    && document.getStates().get("isMain") == null
                    && ("draft".equalsIgnoreCase(document.getStates().get(CHANGE_STATE)))
                    && !document.getAuthor().equalsIgnoreCase(identityProvider.getIdentity().getLogin())
                    || "inactive".equalsIgnoreCase(document.getStates().get(CHANGE_STATE))
                    || "rejected".equalsIgnoreCase(document.getStates().get(CHANGE_STATE))) {

                log.info("ommit " + document.toString());
                continue;
            }

            if (mainChange != null && !document.getId().equals(mainChange.getId())) {
                appendDirective(document);
            }

            documents.add(nl2brDocumentBeanConverter.convert(document));
        }

        return documents;
    }

    private void appendDirective(Document document) {
        String body = DocumentUtil.getBodyOf(document);
        Set<String> tags = tagHelper.tagNamesFromTags(tagFinder.find(body));
        String newBody = addDirective.doIt(body, tags, angularDirective);
        DocumentUtil.setBody(document, newBody);
    }

}
