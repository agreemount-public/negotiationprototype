package com.agreemount.negotiation.document.model;

import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode
public class Document {

    private final String body;
    private final String documentTag;
    private Set<String> contentTags;


    public Document(String body, String documentTag, Set<String> contentTags) {
        super();
        this.body = body;
        this.documentTag = documentTag;
        this.contentTags = contentTags;
    }

    public String getBody() {
        return body;
    }


    public String getDocumentTag() {
        return documentTag;
    }

    public Set<String> getContentTags() {
        if (contentTags.isEmpty()) {
            return new HashSet<>();
        }

        return contentTags;
    }
}
