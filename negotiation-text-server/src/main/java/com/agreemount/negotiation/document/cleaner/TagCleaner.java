package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.document.model.ITagDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component(value = "tagCleaner")
public class TagCleaner implements ITagCleaner<String, String> {

    @Autowired
    private ITagDecorator tagDecorator;

    @Override
    public String clean(String originalText, Set<String> removedTags) {

        String resultText = originalText;

        for (String tagName : removedTags) {
            resultText = resultText.replaceAll(tagDecorator.openTag(tagName), "");
            resultText = resultText.replaceAll(tagDecorator.closeTag(tagName), "");
        }

        return resultText;
    }

}
