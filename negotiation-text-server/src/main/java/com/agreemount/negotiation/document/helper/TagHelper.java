package com.agreemount.negotiation.document.helper;

import com.agreemount.negotiation.document.finder.ITagElementFinder;
import com.agreemount.negotiation.document.finder.ITagFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;


@Component
public class TagHelper implements ITagHelper{

    @Autowired
    @Qualifier(value="openTagFinder")
    private ITagFinder<String,String> openTagFinder;

    @Autowired
    @Qualifier(value="tagNameFinder")
    private ITagElementFinder<String> tagNameFinder;

    @Override
    public Set<String> tagNamesFromTags(Set<String> tags){

        Set<String> tagNames = new HashSet<>();
        for(String tag : tags) {
            Set<String> openTags = openTagFinder.find(tag);
            for(String openTag : openTags) {
                String tagName = tagNameFinder.find(openTag);
                if(tagName != null && !tagName.isEmpty()){
                    tagNames.add(tagName);
                }
            }
        }

        return tagNames;
    }
}
