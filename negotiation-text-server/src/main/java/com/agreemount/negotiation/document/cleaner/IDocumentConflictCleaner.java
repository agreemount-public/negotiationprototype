package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.document.model.DocumentView;

import java.util.Map;
import java.util.Set;

/**
 * @deprecated: zmiana podejścia wyswietlania dokumentu. Strona negocjacji 'widzi' wszystkie tagi dokumentu,
 * ale nie dostaje fizycznie treści dokumentów do których nie powinna mieć wglądu
 * do tej pory było tak, ze dokument był czyszczony z tagów którch dana strona nie powinna zobaczyć
 * zostawiam te implementację gdyby się plany zmieniły
 */
@FunctionalInterface
@Deprecated
public interface IDocumentConflictCleaner {
    DocumentView clean(String documentBody, String documentTag, Map<String, String> allDocumentParts, Set<String> acceptedTags, Set<String> remainsTags);
}
