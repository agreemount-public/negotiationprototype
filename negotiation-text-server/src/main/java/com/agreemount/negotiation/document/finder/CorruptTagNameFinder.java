package com.agreemount.negotiation.document.finder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CorruptTagNameFinder implements ICorruptTagNameFinder {

    @Autowired
    private ITagElementFinder<String> tagNameFinder;

    @Autowired
    @Qualifier(value = "openTagFinder")
    private ITagFinder<String, String> openTagFinder;

    @Autowired
    @Qualifier(value = "closeTagFinder")
    private ITagFinder<String, String> closeTagFinder;


    @Override
    public Set<String> onlyOpenTags(final String text) {

        Set<String> result = new HashSet();
        Set<String> openTags = openTagFinder.find(text);
        Set<String> closeTags = closeTagFinder.find(text);

        Set<String> openTagNames = openTags.stream()
                .map(tagNameFinder::find)
                .collect(Collectors.toSet());

        Set<String> closeTagNames = closeTags.stream()
                .map(tagNameFinder::find)
                .collect(Collectors.toSet());


        result.addAll(
                openTagNames.stream()
                        .filter(p -> !closeTagNames.contains(p))
                        .collect(Collectors.toSet())
        );
        return result;
    }

    @Override
    public Set<String> onlyCloseTags(final String text) {
        Set<String> result = new HashSet();
        Set<String> openTags = openTagFinder.find(text);
        Set<String> closeTags = closeTagFinder.find(text);

        Set<String> openTagNames = openTags.stream()
                .map(tagNameFinder::find)
                .collect(Collectors.toSet());

        Set<String> closeTagNames = closeTags.stream()
                .map(tagNameFinder::find)
                .collect(Collectors.toSet());


        result.addAll(
                closeTagNames.stream()
                        .filter(p -> !openTagNames.contains(p))
                        .collect(Collectors.toSet())
        );
        return result;
    }

}
