package com.agreemount.negotiation.document.change;


@FunctionalInterface
public interface IDocumentChange {
    boolean save(String documentId, String content, int positionStart, int positionEnd);
}
