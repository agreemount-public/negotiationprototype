package com.agreemount.negotiation.document.renderer.actions;

import java.util.Set;


@FunctionalInterface
public interface IAngularDirectiveAction {
    String doIt(String body, Set<String> tags, String directive);
}
