package com.agreemount.negotiation.document.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class DocumentResource implements IDocumentResource {

    private Set<String> acceptedTags;
    private Set<String> remainTags;
    //body of document which we want to modify by algorithm
    private final String body;
    private final String tag;
    //documentId => documentBody
    private Map<String, String> documentParts;

    public DocumentResource(Set<String> acceptedTags, Set<String> remainTags,
                            String body, String tag, Map<String, String> documentParts) {
        super();
        this.acceptedTags = acceptedTags;
        this.remainTags = remainTags;
        this.body = body;
        this.tag = tag;
        this.documentParts = documentParts;
    }

    @Override
    public Set<String> getAcceptedTags() {
        if (acceptedTags.isEmpty()) {
            return new HashSet<>();
        }

        return acceptedTags;
    }

    @Override
    public Set<String> getRemainTags() {
        if (remainTags.isEmpty()) {
            return new HashSet<>();
        }

        return remainTags;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public Map<String, String> getDocumentParts() {
        if (documentParts.isEmpty()) {
            return new HashMap<>();
        }

        return new HashMap<>(documentParts);
    }

    @Override
    public String getTag() {
        return tag;
    }

}
