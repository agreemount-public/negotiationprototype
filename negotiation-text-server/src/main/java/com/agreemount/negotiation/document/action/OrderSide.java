package com.agreemount.negotiation.document.action;


public enum OrderSide {

    ORDER_SIDE_1("orderSIDE1"),
    ORDER_SIDE_2("orderSIDE2");

    private String value;

    OrderSide(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "OrderSide{" +
                "value='" + value + '\'' +
                '}';
    }
}
