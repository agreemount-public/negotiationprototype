package com.agreemount.negotiation.document.action;

import com.agreemount.bean.document.Document;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.definition.CreateNewLeaf;
import com.agreemount.slaneg.action.impl.document.subaction.ApplyActionExtension;
import com.agreemount.slaneg.action.impl.document.subaction.leaf.AbstractCreateNewLeafExtensionsProvider;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CreateNewLeafActionConfiguration extends AbstractCreateNewLeafExtensionsProvider {

    @Override
    public List<ApplyActionExtension<CreateNewLeaf>> getActionExtensions() {
        return Lists.newArrayList(new CopyOrderFromParent());
    }


    class CopyOrderFromParent implements  ApplyActionExtension<CreateNewLeaf> {

        @Override
        public void apply(Document document, ActionContext actionContext, CreateNewLeaf def) {
            //TODO alias of parent should be read from CreateNewLeafDef - engine needs some fixes...
            Document parent = actionContext.getDocument(def.getParentAlias());

            document.getAttributes().put(OrderSide.ORDER_SIDE_1.getValue(), parent.getAttributes().get(OrderSide.ORDER_SIDE_1.getValue()));
            document.getAttributes().put(OrderSide.ORDER_SIDE_2.getValue(), parent.getAttributes().get(OrderSide.ORDER_SIDE_2.getValue()));
        }
    }

}