package com.agreemount.negotiation.document.converter;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.util.DocumentUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component(value = "nl2brDocumentBeanConverter")
public class Nl2BrDocumentBeanConverter implements IConverter<Document, Document> {
    @Autowired
    @Qualifier(value = "nl2BrConverter")
    private IConverter<String, String> nl2brConverter;

    @Override
    public Document convert(Document document) {
        String body = DocumentUtil.getBodyOf(document);
        String newBody = nl2brConverter.convert(body);
        DocumentUtil.setBody(document, newBody);
        return document;
    }
}
