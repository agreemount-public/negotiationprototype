package com.agreemount.negotiation.document.finder;

import java.util.Set;

public interface ICorruptTagNameFinder {

    Set<String> onlyOpenTags(String text);

    Set<String> onlyCloseTags(String text);

}
