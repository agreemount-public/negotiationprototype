package com.agreemount.negotiation.document.renderer;

import com.agreemount.negotiation.document.converter.IConverter;
import com.agreemount.negotiation.document.finder.IMergedDocumentConflictFinder;
import com.agreemount.negotiation.document.merger.IMerger;
import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.negotiation.document.model.IDocumentResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component(value = "documentRenderer")
public class DocumentRenderer implements IRenderer {

    @Autowired
    private IMerger merger;

    @Autowired
    private IMergedDocumentConflictFinder mergedDocumentConflictFinder;

    @Autowired
    @Qualifier(value = "nl2BrDocumentViewConverter")
    private IConverter<DocumentView, DocumentView> nl2brDocumentViewConverter;


    @Override
    public DocumentView render(IDocumentResource resource, IPostRendererAction postRendererAction) {
        IDocumentResource mergedResource = merger.merge(resource);
        DocumentView documentView = mergedDocumentConflictFinder.find(mergedResource, resource);

        if (postRendererAction == null) {
            return nl2brDocumentViewConverter.convert(documentView);
        }

        return nl2brDocumentViewConverter.convert(postRendererAction.run(documentView));
    }


}
