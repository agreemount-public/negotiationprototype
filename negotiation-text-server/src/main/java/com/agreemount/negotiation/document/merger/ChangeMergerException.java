package com.agreemount.negotiation.document.merger;

public class ChangeMergerException extends RuntimeException{

    public ChangeMergerException(String message){
        super(message);
    }

    public ChangeMergerException(Throwable cause){
        super(cause);
    }

    public ChangeMergerException(String message, Throwable cause){
        super(message, cause);
    }
}
