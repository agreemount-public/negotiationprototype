package com.agreemount.negotiation.document.finder;

import com.agreemount.negotiation.document.cleaner.ITagCleaner;
import com.agreemount.negotiation.document.helper.ITagHelper;
import com.agreemount.negotiation.document.model.Document;
import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.negotiation.document.model.IDocumentResource;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Log4j
public class MergedDocumentConflictFinder implements IMergedDocumentConflictFinder {

    @Autowired
    private ICorruptTagNameFinder corruptedTagsFinder;

    @Autowired
    @Qualifier(value = "tagCleaner")
    private ITagCleaner<String, String> tagCleaner;

    @Autowired
    @Qualifier(value = "tagFinder")
    private ITagFinder<String, String> tagFinder;

    @Autowired
    private ITagHelper tagHelper;


    @Override
    public DocumentView find(IDocumentResource mergedResource, IDocumentResource originalResource) {

        String documentBody = mergedResource.getBody();
        String documentTag = originalResource.getTag();
        Set<String> notMergedTags = mergedResource.getAcceptedTags();
        Map<String, String> allDocumentParts = originalResource.getDocumentParts();
        Set<String> acceptedTags = originalResource.getAcceptedTags();
        Set<String> remainsTags = originalResource.getRemainTags();

        Set<String> documentPartsAsTags = allDocumentParts.keySet();//wszystkie dokumenty

        Set<String> correctTags = allContentTags(documentBody, allDocumentParts);//wszystkie poprawne tagi dokumentów

        Set<String> correctTagsName = tagHelper.tagNamesFromTags(correctTags);//wszystkie nazwy poprawnych tagów
        String bodyWihoutCorrectTags = cleanFromCorrectTags(documentBody, correctTagsName);
        Set<String> corruptedTagsName = corruptedTagsName(bodyWihoutCorrectTags);

        Set<String> conflictedTags = conflictedTags(corruptedTagsName, acceptedTags);
        Set<String> unresolvedTags = unresolvedTags(corruptedTagsName, remainsTags);

        Set<String> artefacts = corruptedTagsName.stream()
                .filter(p -> !conflictedTags.contains(p))
                .filter(p -> !unresolvedTags.contains(p))
                .collect(Collectors.toSet());

        log.info("Not merged tags " + notMergedTags.size() + " vs conflictedTags" + conflictedTags.size());
        log.info("artefacts " + artefacts);

        return new DocumentView(
                new Document(documentBody, documentTag, documentPartsAsTags),
                notMergedTags,
                unresolvedTags
        );
    }

    private Set<String> allContentTags(String documentBody, Map<String, String> allDocumentParts) {
        Set<String> allContentTags = tagFinder.find(documentBody);

        for (Map.Entry<String, String> documentPart : allDocumentParts.entrySet()) {
            allContentTags.addAll(tagFinder.find(documentPart.getValue()));
        }

        return allContentTags;
    }


    private String cleanFromCorrectTags(String body, Set<String> properTags) {
        return tagCleaner.clean(body, properTags);
    }

    private Set<String> conflictedTags(Set<String> corruptedTagsName, Set<String> acceptedTags) {
        return filter(corruptedTagsName, acceptedTags);
    }

    private Set<String> unresolvedTags(Set<String> corruptedTagsName, Set<String> remainsTags) {
        return filter(corruptedTagsName, remainsTags);
    }

    private Set<String> filter(Set<String> allTags, Set<String> toFilterOut) {
        return allTags.stream()
                .filter(toFilterOut::contains)
                .collect(Collectors.toSet());
    }

    private Set<String> corruptedTagsName(String body) {
        Set<String> corruptedTagsName = new HashSet<>();
        corruptedTagsName.addAll(corruptedTagsFinder.onlyOpenTags(body));
        corruptedTagsName.addAll(corruptedTagsFinder.onlyCloseTags(body));
        return corruptedTagsName;

    }

}
