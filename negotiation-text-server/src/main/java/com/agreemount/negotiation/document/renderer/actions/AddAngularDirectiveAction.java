package com.agreemount.negotiation.document.renderer.actions;

import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class AddAngularDirectiveAction implements IAngularDirectiveAction {

    @Override
    public String doIt(String body, Set<String> tags, String directive) {

        String tempBody = body;
        for (String tagName : tags) {
            Matcher matcher = tagMatcher(tempBody, tagName);
            if (matcher.find()) {
                tempBody = matcher.replaceAll(replacedString(tagName, directive));
            }
        }

        return tempBody;
    }


    private static String replacedString(String tagName, String directive) {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append(tagName);
        sb.append(" ");
        sb.append(directive);
        sb.append(">");


        return sb.toString();
    }

    private static Matcher tagMatcher(String body, String tagName) {

        String regexp = openTagRegexp(tagName);
        Pattern pattern = Pattern.compile(regexp);
        return pattern.matcher(body);
    }

    private static String openTagRegexp(String tagName) {
        return new StringBuilder()
                .append("<(")
                .append(tagName)
                .append(")( +.+)*>")
                .toString();
    }

}
