package com.agreemount.negotiation.document.model;

public interface ITagDecorator {

    String openTag(String tagName);

    String closeTag(String tagName);
}
