package com.agreemount.negotiation.document.finder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FinderConfiguration {

    @Value("${finder.openTag.regexp}")
    private String openTag;
    @Value("${finder.closeTag.regexp}")
    private String closeTag;
    @Value("${finder.tagWithContent.regexp}")
    private String tagWithContent;
    @Value("${finder.tagName.regexp}")
    private String tagName;
    @Value("${finder.tagName.regexp.group}")
    private int tagNameRegexpGroup;

    @Bean
    public TagRegexp tagRegexpBean(){
        return TagRegexp.builder()
                .openTag(openTag)
                .closeTag(closeTag)
                .tagWithContent(tagWithContent)
                .tagName(tagName)
                .tagNameRegexpGroup(tagNameRegexpGroup)
                .build();

    }


}
