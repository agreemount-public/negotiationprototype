package com.agreemount.negotiation.document;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.negotiation.document.model.IDocumentResource;
import com.agreemount.negotiation.document.renderer.AddAngularDirective;
import com.agreemount.negotiation.document.renderer.IRenderer;
import com.agreemount.negotiation.document.renderer.actions.IAngularDirectiveAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DocumentMergerFacade {
    @Autowired
    private DocumentResourceProvider documentResourceProvider;

    @Autowired
    @Qualifier(value = "documentRenderer")
    private IRenderer renderer;

    @Autowired
    private IAngularDirectiveAction angularDirective;

    public DocumentView merge(Document negotiationDocument) {
        AddAngularDirective addDirective = new AddAngularDirective(angularDirective,"content-tag");
        IDocumentResource documentResource = documentResourceProvider.provideFor(negotiationDocument);
        return renderer.render(documentResource, addDirective);
    }

}
