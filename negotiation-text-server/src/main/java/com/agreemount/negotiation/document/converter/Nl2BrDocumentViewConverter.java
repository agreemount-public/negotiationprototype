package com.agreemount.negotiation.document.converter;

import com.agreemount.negotiation.document.model.Document;
import com.agreemount.negotiation.document.model.DocumentView;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component(value = "nl2BrDocumentViewConverter")
public class Nl2BrDocumentViewConverter implements IConverter<DocumentView, DocumentView> {

    @Autowired
    @Qualifier(value = "nl2BrDocumentConverter")
    private IConverter<Document, Document> nl2BrDocumentConverter;

    @Override
    public DocumentView convert(DocumentView documentView) {
        Preconditions.checkNotNull(documentView, "Can not convert null object");
        return new DocumentView(
                nl2BrDocumentConverter.convert(documentView.getDocument()),
                documentView.getConflictedTags(),
                documentView.getUnresolvedTags()
        );
    }
}
