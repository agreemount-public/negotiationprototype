package com.agreemount.negotiation.document.finder;

@FunctionalInterface
public interface ITagElementFinder<T> {
    String find(T object);
}
