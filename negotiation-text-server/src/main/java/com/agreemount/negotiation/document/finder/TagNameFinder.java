package com.agreemount.negotiation.document.finder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component(value = "tagNameFinder")
public class TagNameFinder implements ITagElementFinder<String> {

    @Autowired
    private TagRegexp tagRegexp;

    @Override
    public String find(String tagText) {
        Pattern pattern = Pattern.compile(tagRegexp.getTagName());
        Matcher matcher = pattern.matcher(tagText);
        matcher.find();
        return matcher.group(tagRegexp.getTagNameRegexpGroup());
    }

}
