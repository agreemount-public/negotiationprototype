package com.agreemount.negotiation.document.finder;

import java.util.Set;

@FunctionalInterface
public interface ITagFinder<T, R> {

    Set<T> find(R object);
}
