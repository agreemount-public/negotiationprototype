package com.agreemount.negotiation.document.cleaner;

import com.agreemount.negotiation.document.finder.ICorruptTagNameFinder;
import com.agreemount.negotiation.document.finder.ITagFinder;
import com.agreemount.negotiation.document.helper.ITagHelper;
import com.agreemount.negotiation.document.model.Document;
import com.agreemount.negotiation.document.model.DocumentView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DocumentConflictCleaner implements IDocumentConflictCleaner {

    @Autowired
    private ICorruptTagNameFinder corruptedTagsFinder;

    @Autowired
    @Qualifier(value = "tagCleaner")
    private ITagCleaner<String, String> tagCleaner;

    @Autowired
    @Qualifier(value = "tagFinder")
    private ITagFinder<String, String> tagFinder;

    @Autowired
    private ITagHelper tagHelper;

    @Override
    public DocumentView clean(String documentBody, String documentTag, Map<String, String> allDocumentParts, Set<String> acceptedTags, Set<String> remainsTags) {

        Set<String> allTags = allDocumentParts.keySet();

        Set<String> allContentTags = allContentTags(documentBody, allDocumentParts);

        Set<String> allTagsName = tagHelper.tagNamesFromTags(allContentTags);
        String bodyWihoutCorrectTags = cleanFromCorrectTags(documentBody, allTagsName);
        Set<String> corruptedTagsName = corruptedTagsName(bodyWihoutCorrectTags);

        Set<String> conflictedCorruptedTags = findConflictedCorruptedTags(corruptedTagsName, acceptedTags);
        Set<String> unresolvedCorruptedTags = findUnresolvedCorruptedTags(corruptedTagsName, remainsTags);
        Set<String> tagsToClean = tagsToClean(conflictedCorruptedTags, unresolvedCorruptedTags, acceptedTags);
        String cleanBody = cleanFromTags(documentBody, tagsToClean);

        Set<String> conflictedTags = allConflictedTags(conflictedCorruptedTags, allTags, acceptedTags);
        Set<String> unresolvedTags = allUnresolvedTags(unresolvedCorruptedTags, remainsTags, allTagsName);

        return new DocumentView(
                new Document(cleanBody, documentTag, allTags),
                conflictedTags,
                unresolvedTags
        );
    }

    private Set<String> allContentTags(String documentBody, Map<String, String> allDocumentParts) {
        Set<String> allContentTags = tagFinder.find(documentBody);

        for (Map.Entry<String, String> documentPart : allDocumentParts.entrySet()) {
            allContentTags.addAll(tagFinder.find(documentPart.getValue()));
        }

        return allContentTags;
    }

    private static Set<String> tagsToClean(Set<String> conflictedTags, Set<String> unresolvedTags, Set<String> acceptedTags) {
        Set<String> tags = new HashSet<>();
        tags.addAll(conflictedTags);
        tags.addAll(unresolvedTags);
        for (String acceptedTagName : acceptedTags) {
            tags.add(acceptedTagName);
        }

        return tags;
    }

    private String cleanFromCorrectTags(String body, Set<String> properTags) {
        return tagCleaner.clean(body, properTags);
    }

    private Set<String> findConflictedCorruptedTags(Set<String> corruptedTagsName, Set<String> acceptedTags) {
        return filter(corruptedTagsName, acceptedTags);
    }

    private Set<String> findUnresolvedCorruptedTags(Set<String> corruptedTagsName, Set<String> remainsTags) {
        return filter(corruptedTagsName, remainsTags);
    }

    private Set<String> filter(Set<String> allTags, Set<String> toFilterOut) {
        return allTags.stream()
                .filter(toFilterOut::contains)
                .collect(Collectors.toSet());
    }

    private Set<String> corruptedTagsName(String body) {
        Set<String> corruptedTagsName = new HashSet<>();
        corruptedTagsName.addAll(corruptedTagsFinder.onlyOpenTags(body));
        corruptedTagsName.addAll(corruptedTagsFinder.onlyCloseTags(body));
        return corruptedTagsName;

    }

    private String cleanFromTags(String body, Set<String> corruptedTags) {
        return tagCleaner.clean(body, corruptedTags);
    }

    private Set<String> allConflictedTags(Set<String> conflictedCorruptedTags, Set<String> allTags, Set<String> acceptedTags) {
        Set<String> tags = new HashSet<>();
        tags.addAll(conflictedCorruptedTags);
        tags.addAll(
                allTags.stream()
                        .filter(acceptedTags::contains)
                        .collect(Collectors.toSet())
        );

        return tags;
    }

    private Set<String> allUnresolvedTags(Set<String> unresolvedCorruptedTags, Set<String> remainsTags, Set<String> cleanedDocumentBodyTags) {
        Set<String> tags = new HashSet<>();
        tags.addAll(unresolvedCorruptedTags);
        tags.addAll(
                remainsTags.stream()
                        .filter(p -> !cleanedDocumentBodyTags.contains(p))
                        .collect(Collectors.toSet())
        );

        return tags;
    }
}
