package com.agreemount.negotiation.document.cleaner;

import java.util.Set;

@FunctionalInterface
public interface ITagCleaner<T, R> {
    T clean(T resource, Set<R> removedTags);
}
