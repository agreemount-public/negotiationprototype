package com.agreemount.negotiation.document.change;

import com.agreemount.EngineFacade;
import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.util.DocumentUtil;
import com.agreemount.slaneg.action.ActionContext;
import com.agreemount.slaneg.action.ActionContextFactory;
import com.google.common.base.Preconditions;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Log4j
@Component
public class DocumentChange implements IDocumentChange {

    @Autowired
    private EngineFacade engineFacade;

    @Autowired
    private ActionContextFactory actionContextFactory;

    private ActionContext prepareActionContext(Document document, String content) {
        Map<String,Object> metrics = new HashMap<>();
        metrics.put("content",content);

        Document metricsDocument = new Document();
        metricsDocument.setMetrics(metrics);
        ActionContext actionContext = actionContextFactory.createInstance(document);
        actionContext.addDocument("METRICS",metricsDocument);

        return actionContext;
    }

    @Override
    public boolean save(String documentId, String content, int positionStart, int positionEnd) {

        Preconditions.checkNotNull(documentId,"Can not update document without specifying documentId");

        Document document = engineFacade.getDocument(documentId);
        ActionContext actionContext = prepareActionContext(document, content);
        engineFacade.runAction(actionContext, "createChangeFromChange");

        Document change = actionContext.getDocument("newChange");
        Preconditions.checkNotNull(change,"Not received alias newChange from action createChangeFromChange. Probably the action failed to execute.");

        String changedContent = documentBodyWithChange(document,change.getRootUuid(), positionStart, positionEnd);
        ActionContext actionContext2 = prepareActionContext(document,changedContent);
        engineFacade.runAction(actionContext2, "createNewVersionOfChange");

        return true;
    }

    private String documentBodyWithChange(Document document, String changeDocumentId, int positionStart, int positionEnd) {
        DocumentBody documentBody = new DocumentBody(DocumentUtil.getBodyOf(document));
        return documentBody.getWithChange(changeDocumentId, positionStart, positionEnd);
    }

}
