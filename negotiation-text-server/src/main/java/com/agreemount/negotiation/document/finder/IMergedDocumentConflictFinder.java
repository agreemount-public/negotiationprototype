package com.agreemount.negotiation.document.finder;

import com.agreemount.negotiation.document.model.DocumentView;
import com.agreemount.negotiation.document.model.IDocumentResource;



@FunctionalInterface
public interface IMergedDocumentConflictFinder {

    DocumentView find(IDocumentResource mergedResource, IDocumentResource originalResource);
}
