package com.agreemount.negotiation.document.helper;

import java.util.Set;


@FunctionalInterface
public interface ITagHelper {
    Set<String> tagNamesFromTags(Set<String> tags);
}
