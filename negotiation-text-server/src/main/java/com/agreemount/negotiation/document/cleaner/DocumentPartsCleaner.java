package com.agreemount.negotiation.document.cleaner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class DocumentPartsCleaner implements ITagCleaner<Map<String, String>, String> {

    @Autowired
    @Qualifier(value = "tagCleaner")
    private ITagCleaner<String, String> tagCleaner;

    @Override
    public Map<String, String> clean(Map<String, String> resource,
                                     Set<String> removedTags) {

        Map<String, String> cleanDocumentParts = new HashMap<>();


        for (Map.Entry<String, String> entry : resource.entrySet()) {
            String documentTagName = entry.getKey();
            String documentResource = entry.getValue();

            if (!containsDocumentTagName(removedTags, documentTagName)) {
                cleanDocumentParts.put(documentTagName, tagCleaner.clean(documentResource, removedTags));
            }
        }

        return cleanDocumentParts;
    }


    private static boolean containsDocumentTagName(Set<String> removedTags, String documentTagName) {
        for (String tagName : removedTags) {
            if (tagName.equals(documentTagName)) {
                return true;
            }
        }

        return false;
    }

}
