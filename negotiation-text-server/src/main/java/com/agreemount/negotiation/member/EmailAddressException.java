package com.agreemount.negotiation.member;

public class EmailAddressException extends RuntimeException {

    public EmailAddressException(Throwable cause) {
        super(cause);
    }

}
