package com.agreemount.negotiation.member;

public class TooManySidesException extends RuntimeException {

    public TooManySidesException(String message) {
        super(message);
    }
}
