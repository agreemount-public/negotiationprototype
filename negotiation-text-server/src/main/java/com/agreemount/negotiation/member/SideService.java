package com.agreemount.negotiation.member;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.INotificationDAO;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserLogDAO;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Component
@Log4j
public class SideService extends MemberService {

    private static final int MAX_NUMBER_OF_SIDES = 2;
    private static final boolean NOT_PARTNER = false;

    @Autowired
    private ITeamDAO teamDAO;

    @Autowired
    private INotificationDAO notificationDAO;

    @Autowired
    private IUserDAO userDao;

    @Autowired
    private UserLogDAO userLogDao;

    public void inviteSide(String inviterEmail, String inviteeEmail, String documentId) {
        Team team = this.fetchEnrichedTeam(documentId);
        User2Team inviter = this.getInviter(inviterEmail, team);

        validateInvitingSide(inviter, inviteeEmail, team);

        addSideToTeam(inviteeEmail, team);
        persistSideNotification(NotificationType.ADD_SIDE, inviteeEmail, documentId);
        giveFullAccessToInvitee(inviterEmail, inviteeEmail);
    }

    public List<User2Team> fetchSides(String documentId) {
        Team team = this.fetchEnrichedTeam(documentId);

        return team.getUsers2Team().stream()
                .filter(user2Team -> !user2Team.isPartner()).collect(toList());
    }


    private void validateInvitingSide(User2Team inviter, String inviteeEmail, Team team) {
        this.validateEmailAddress(inviteeEmail);
        this.validateDuplication(inviteeEmail, team);
        validateNumberOfRegisteredSides(team);
        this.validateInviter(inviter);
    }

    private void validateNumberOfRegisteredSides(Team team) {
        Integer numberOfRegisteredUser = Math.toIntExact(Optional.of(team.getUsers2Team())
                .orElse(Collections.emptyList())
                .stream()
                .filter(user2Team -> !user2Team.isPartner())
                .filter(User2Team::isUserRegistered)
                .count());

        if (numberOfRegisteredUser < MAX_NUMBER_OF_SIDES) {
            return;
        }
        throw new TooManySidesException("You cannot invite more than one side");
    }

    private void addSideToTeam(String inviteeEmail, Team team) {
        if (team.getUsers2Team().size() == MAX_NUMBER_OF_SIDES) {
            teamDAO.deleteMembersFromTeamByUserRole(UserRole.SIDE2);
        }

        teamDAO.addMemberToTeam(this.createMember(inviteeEmail, UserRole.SIDE2, NOT_PARTNER), team.getName());
    }


    private void persistSideNotification(NotificationType type, String inviteeEmail, String documentId) {
        NotificationBuilder notificationBuilder = this.buildWithDefaults(type, inviteeEmail, documentId);

        notificationDAO.save(notificationBuilder.build());
    }

    private void giveFullAccessToInvitee(String inviterEmail, String inviteeEmail) {
        User invitee = userDao.getByEmail(inviteeEmail);
        if (invitee != null && userDao.getByEmail(inviterEmail).isHasFreeAccess()) {
            invitee.setHasFreeAccess(true);
            userDao.save(invitee);
            userLogDao.insertNewLog(inviteeEmail, "give user full access as was invited by user with full access - " + inviterEmail);
        }
    }

    public List<String> getOtherSideEmail(Team team, String email) {
        UserRole role = team.getUsers2Team().stream()
                .filter(user2Team -> user2Team.getLogin().equals(email))
                .findAny()
                .get()
                .getRole();

        return team.getUsers2Team().stream()
                .filter(user2Team -> !user2Team.getRole().equals(role))
                .map(User2Team::getLogin)
                .collect(toList());
    }

}
