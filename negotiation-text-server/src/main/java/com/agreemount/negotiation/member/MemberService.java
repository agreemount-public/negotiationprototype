package com.agreemount.negotiation.member;

import com.agreemount.bean.document.Document;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.slaneg.db.DocumentOperations;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.internet.InternetAddress;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.*;

abstract class MemberService {

    public static final String INVITATION_TO_E_NEGOTIATION_SERVICE = "Invitation to e-negotiation service";

    @Autowired
    private ITeamDAO teamDAO;

    @Autowired
    private IUserDAO userDao;

    @Autowired
    private DocumentOperations documentOperations;

    User2Team createMember(String email, UserRole side, boolean isPartner) {
        User2Team user2Team = new User2Team();
        user2Team.setLogin(email);
        user2Team.setRole(side);
        user2Team.setPartner(isPartner);
        return user2Team;
    }

    Team fetchEnrichedTeam(String documentId) {
        Document document = documentOperations.getDocument(documentId);
        Team team = teamDAO.getTeamByName(document.getTeam());

        Optional.of(team.getUsers2Team())
                .orElse(Collections.emptyList())
                .stream()
                .filter(user2Team -> Objects.nonNull(userDao.getByEmail(user2Team.getLogin())))
                .forEach(user2Team -> user2Team.setUserRegistered(true));

        return team;
    }

    User2Team getInviter(String inviterEmail, Team team) {
        return team.getUsers2Team().stream()
                .filter(user2Team -> user2Team.getLogin().equals(inviterEmail))
                .findAny()
                .orElseThrow(() -> new NoSuchElementException("Inviter not found"));
    }

    void validateInviter(User2Team inviter) {
        if (inviter.isPartner()) {
            throw new IllegalStateException("Partner cannot invite side/partner");
        }
    }


    void validateEmailAddress(String email) {
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (javax.mail.internet.AddressException ex) {
            throw new EmailAddressException(ex);
        }
    }

    void validateDuplication(String email, Team team) {
        team.getUsers2Team().stream()
                .map(User2Team::getLogin)
                .filter(email::equals)
                .findAny()
                .ifPresent(user2Team -> {
                    throw new DuplicateInvitationException("User: " + email + " is already assigned to team ");
                });
    }

    NotificationBuilder buildWithDefaults(NotificationType type, String email, String documentId) {
        return new NotificationBuilder()
                .setNotificationType(type)
                .addParameter(new Notification.Parameter(RECEIVER.getValue(), email))
                .addParameter(new Notification.Parameter(TITLE.getValue(), INVITATION_TO_E_NEGOTIATION_SERVICE))
                .addParameter(new Notification.Parameter(DOCUMENT_ID.getValue(), documentId));
    }


    void addMemberToTeam(User2Team invitee, String teamName) {
        teamDAO.addMemberToTeam(invitee, teamName);
    }

    void deleteMemberFromTeam(String login) {
        teamDAO.deleteByLogin(login);
    }


}
