package com.agreemount.negotiation.member;

import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.auth.strategies.AccessVerifier;
import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.impl.NotificationDAO;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.REQUESTOR;
import static java.util.stream.Collectors.toList;

@Component
@Log4j
public class PartnerService extends MemberService {

    private static final boolean PARTNER = true;

    @Autowired
    private NotificationDAO notificationDAO;

    @Autowired
    private AccessVerifier verifier;

    public void invitePartner(String inviterEmail, String inviteeEmail, String documentId) {
        Team team = this.fetchEnrichedTeam(documentId);
        User2Team inviter = this.getInviter(inviterEmail, team);
        User2Team invitee = this.createMember(inviteeEmail, inviter.getRole(), PARTNER);

        validateInvitingPartner(inviter, invitee, team);

        this.addMemberToTeam(invitee, team.getName());

        persistPartnerNotification(NotificationType.ADD_PARTNER, inviterEmail, inviteeEmail, documentId);
    }

    public List<User2Team> fetchPartners(String requestorEmail, String documentId) {
        Team team = this.fetchEnrichedTeam(documentId);

        User2Team requestor = team.getUsers2Team()
                .stream()
                .filter(user2Team -> user2Team.getLogin().equals(requestorEmail))
                .findFirst().get();

        validateInviter(requestor);
        validateFreeAccess(requestorEmail);

        return team.getUsers2Team().stream()
                .filter(User2Team::isPartner)
                .filter(user2Team -> user2Team.getRole().equals(requestor.getRole()))
                .collect(toList());
    }

    public void deletePartner(String requestorEmail, String partnerEmail, String documentId) {
        validateFreeAccess(requestorEmail);

        List<User2Team> user2Teams = fetchPartners(requestorEmail, documentId);
        User2Team user2Team1 = user2Teams.stream()
                .filter(user2Team -> user2Team.getLogin().equals(partnerEmail))
                .findAny()
                .orElseThrow(() -> new IllegalStateException("Partner to delete not found"));

        this.deleteMemberFromTeam(user2Team1.getLogin());

        persistPartnerNotification(NotificationType.DELETE_PARTNER, requestorEmail, partnerEmail, documentId);
    }

    private void validateInvitingPartner(User2Team inviter, User2Team invitee, Team team) {
        validateEmailAddress(invitee.getLogin());
        validateDuplication(invitee.getLogin(), team);
        validateInviter(inviter);
        validateFreeAccess(inviter.getLogin());
    }

    private void validateFreeAccess(String email) {
        boolean hasFreeAccess = verifier.hasUserFreeAccess(email);
        if (hasFreeAccess) {
            throw new AuthException("Cannot invite partner - no access");
        }
    }

    private void persistPartnerNotification(NotificationType type, String inviterEmail, String inviteeEmail, String documentId) {
        NotificationBuilder notificationBuilder = this.buildWithDefaults(type, inviteeEmail, documentId)
                .addParameter(new Notification.Parameter(REQUESTOR.getValue(), inviterEmail));

        notificationDAO.save(notificationBuilder.build());
    }

}
