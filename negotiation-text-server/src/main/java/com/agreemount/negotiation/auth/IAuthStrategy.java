package com.agreemount.negotiation.auth;

import com.agreemount.negotiation.bean.User;


public interface IAuthStrategy {

    User signUp(User user);

    User login(String email, char[] password);

    default Boolean confirmAccountAddress(String confirmationHash) {
        return true;
    }

    default void validateIfAccountIsBlocked(User user) {
        if (user.isBlocked()) {
            throw new AuthException("Account is blocked");
        }
    }


}
