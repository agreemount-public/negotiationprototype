package com.agreemount.negotiation.auth.strategies;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.agreemount.negotiation.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class AccessVerifier {

    @Autowired
    private ITeamDAO teamDAO;

    @Autowired
    private UserService userService;

    public boolean hasUserFreeAccess(String email) {
        List<Team> teams = teamDAO.getUserTeams(email);
        return teams.stream()
                .map(team -> userService.getUsersOfTeam(team, email))
                .flatMap(Collection::stream)
                .filter(User::isHasFreeAccess)
                .findAny()
                .isPresent();
    }

}
