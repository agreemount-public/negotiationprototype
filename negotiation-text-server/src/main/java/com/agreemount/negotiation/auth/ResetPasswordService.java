package com.agreemount.negotiation.auth;

import com.agreemount.negotiation.bean.ResetPassword;
import com.agreemount.negotiation.bean.ResetPasswordDto;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.impl.NotificationDAO;
import com.agreemount.negotiation.dao.impl.ResetPasswordDAO;
import com.agreemount.negotiation.dao.impl.UserDAO;
import com.agreemount.negotiation.user.UserService;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;

import static com.agreemount.negotiation.bean.notification.Notification.Parameter.Keys.*;

@Component
public class ResetPasswordService {

    private static final String RESET_YOUR_PASSWORD = "[Agreemount] Reset your password";
    private static final String YOUR_PASSWORD_HAS_BEEN_CHANGED = "[Agreemount] Your password has been changed";

    @Autowired
    private ResetPasswordDAO resetPasswordDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NotificationDAO notificationDAO;

    @Autowired
    private UserService userService;

    public void resetPasswordConfirmation(String email) {
        Preconditions.checkNotNull(userDAO.getByEmail(email), "User not found: " + email);

        ResetPassword resetPassword = buildResetPassword(email);

        saveResetPasswordRecord(resetPassword);
        sendPasswordResetConfirmationNotification(resetPassword);
    }

    public void resetPassword(ResetPasswordDto resetPasswordDto) {
        ResetPassword resetPassword = resetPasswordDAO.getByHash(resetPasswordDto.getHash());
        Preconditions.checkNotNull(resetPassword, "Hash not found: " + resetPasswordDto.getHash());
        Preconditions.checkState(!resetPassword.isUsed(), "Hash has been already used: " + resetPasswordDto.getHash());

        Local persistedUser = (Local) userDAO.getByEmail(resetPassword.getEmail());
        Preconditions.checkNotNull(resetPasswordDto.getPassword(), "Password not set");
        Preconditions.checkNotNull(persistedUser, "User not found: " + resetPassword.getEmail());
        Preconditions.checkState(passwordNotEqual(resetPasswordDto), "Given passwords are not equal");

        persistedUser.setPassword(resetPasswordDto.getPassword());
        Local user = userService.setPasswordForUser(persistedUser);

        userDAO.save(user);

        markResetPaswordAsUsed(resetPassword);
        sendPasswordResetNotification(persistedUser);
    }

    private void markResetPaswordAsUsed(ResetPassword resetPassword) {
        resetPassword.setUsed(true);
        resetPasswordDAO.save(resetPassword);
    }

    private boolean passwordNotEqual(ResetPasswordDto resetPasswordDto) {
        return Arrays.equals(resetPasswordDto.getPassword(), resetPasswordDto.getPasswordConfirmation());
    }

    private ResetPassword buildResetPassword(String email) {
        ResetPassword resetPassword = new ResetPassword();
        resetPassword.setEmail(email);
        resetPassword.setCreatedAt(LocalDateTime.now());
        resetPassword.setHash(UUID.randomUUID().toString());
        return resetPassword;
    }

    private void saveResetPasswordRecord(ResetPassword resetPassword) {
        resetPasswordDAO.save(resetPassword);
    }

    private void sendPasswordResetConfirmationNotification(ResetPassword resetPassword) {
        Notification notification = new NotificationBuilder()
                .setNotificationType(NotificationType.RESET_PASSWORD_CONFIRMATION)
                .addParameter(new Notification.Parameter(TITLE.getValue(), RESET_YOUR_PASSWORD))
                .addParameter(new Notification.Parameter(CONFIRMATION_HASH.getValue(), resetPassword.getHash()))
                .addParameter(new Notification.Parameter(RECEIVER.getValue(), resetPassword.getEmail()))
                .build();

        notificationDAO.save(notification);
    }

    private void sendPasswordResetNotification(Local user) {
        Notification notification = new NotificationBuilder()
                .setNotificationType(NotificationType.RESET_PASSWORD)
                .addParameter(new Notification.Parameter(TITLE.getValue(), YOUR_PASSWORD_HAS_BEEN_CHANGED))
                .addParameter(new Notification.Parameter(RECEIVER.getValue(), user.getEmail()))
                .build();

        notificationDAO.save(notification);
    }

}
