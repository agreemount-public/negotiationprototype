package com.agreemount.negotiation.auth;

import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.providers.Local;
import com.agreemount.negotiation.dao.IUserDAO;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Log4j
public class AuthContext {

    private IAuthStrategy authStrategy;

    @Autowired
    private ApplicationContext appContext;

    @Autowired
    private IUserDAO userDAO;

    public User signUp(User user) {
        setAuthStrategy(user);
        return authStrategy.signUp(user);
    }

    public Boolean activateAccount(String confirmationHash) {
        setAuthStrategy(new Local());
        return authStrategy.confirmAccountAddress(confirmationHash);
    }

    public User getUser(String id) {
        return userDAO.getById(id);
    }

    public User login(String email, char[] password) {
        setAuthStrategy(new Local());
        return authStrategy.login(email, password);
    }

    private void setAuthStrategy(User user) {
        Map<String, IAuthStrategy> authStrategies = appContext.getBeansOfType(IAuthStrategy.class);
        authStrategy = authStrategies.get(user.getClass().getSimpleName().toLowerCase());
    }
}
