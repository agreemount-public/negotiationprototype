package com.agreemount.negotiation.auth.strategies;

import com.agreemount.negotiation.auth.IAuthStrategy;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserLogDAO;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SocialStrategy implements IAuthStrategy {
    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private AccessVerifier accessVerifier;

    @Autowired
    private UserLogDAO userLogDAO;

    @Override
    public User signUp(User user) {
        User persistedUser = userDAO.getById(user.getId());
        if (persistedUser != null) {
            validateIfAccountIsBlocked(persistedUser);
            userDAO.updateLastLoginDate(persistedUser);
            return persistedUser;
        }

        createUser(user);
        return user;
    }

    private void createUser(User user) {
        user.setHasFreeAccess(accessVerifier.hasUserFreeAccess(user.getEmail()));
        userDAO.insert(user);
        userLogDAO.insertNewLog(user.getEmail(), "give user full access during sign up");
    }

    @Override
    public User login(String email, char[] password) {
        throw new NotImplementedException();
    }

}
