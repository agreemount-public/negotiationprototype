package com.agreemount.negotiation.auth;

public class HashPasswordException extends RuntimeException {

    public HashPasswordException(String message){
        super(message);
    }

    public HashPasswordException(Throwable cause){
        super(cause);
    }

    public HashPasswordException(String message, Throwable cause){
        super(message, cause);
    }

}
