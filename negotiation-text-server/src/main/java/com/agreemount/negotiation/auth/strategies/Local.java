package com.agreemount.negotiation.auth.strategies;

import com.agreemount.negotiation.auth.AuthException;
import com.agreemount.negotiation.auth.IAuthStrategy;
import com.agreemount.negotiation.auth.Password;
import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationBuilder;
import com.agreemount.negotiation.bean.notification.NotificationType;
import com.agreemount.negotiation.dao.INotificationDAO;
import com.agreemount.negotiation.dao.IUserDAO;
import com.agreemount.negotiation.dao.impl.UserLogDAO;
import com.agreemount.negotiation.user.UserService;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class Local implements IAuthStrategy {

    private static final String ACTIVATE_YOUR_ACCOUNT = "[Agreemount] Activate your account";
    private static final String AUTHENTICATION_FAILED = "Authentication failed: incorrect username or password";

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private INotificationDAO notificationDAO;

    @Autowired
    private AccessVerifier accessVerifier;

    @Autowired
    private UserLogDAO userLogDAO;

    @Autowired
    private UserService userService;

    @Override
    public User signUp(User user) {
        Preconditions.checkNotNull(((com.agreemount.negotiation.bean.providers.Local) user).getPassword(),
                "Password is not set");

        User persistedUser = userDAO.getByEmail(user.getEmail());
        if (persistedUser == null) {
            user = userService.setPasswordForUser((com.agreemount.negotiation.bean.providers.Local) user);
            user.setHasFreeAccess(accessVerifier.hasUserFreeAccess(user.getEmail()));

            userDAO.insert(user);
            sendNotification((com.agreemount.negotiation.bean.providers.Local) user);

            if (user.isHasFreeAccess()) {
                userLogDAO.insertNewLog(user.getEmail(), "give user full access during sign up");
            }

        } else {
            throw new AuthException("That email is already taken:" + user.getEmail());
        }
        return user;
    }

    private void sendNotification(com.agreemount.negotiation.bean.providers.Local user) {
        Notification notification = new NotificationBuilder()
                .setNotificationType(NotificationType.ACCOUNT_ACTIVATION)
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.TITLE.getValue(), ACTIVATE_YOUR_ACCOUNT))
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.CONFIRMATION_HASH.getValue(), user.getConfirmationHash()))
                .addParameter(new Notification.Parameter(Notification.Parameter.Keys.RECEIVER.getValue(), user.getEmail()))
                .build();

        notificationDAO.save(notification);
    }

    @Override
    public User login(String email, char[] password) {
        User user = userDAO.getByEmail(email);

        try {
            validate(password, user);
        } catch (NullPointerException ex) {
            throw new AuthException(AUTHENTICATION_FAILED, ex);
        }

        userDAO.updateLastLoginDate(user);

        ((com.agreemount.negotiation.bean.providers.Local) user).setPassword(null);
        ((com.agreemount.negotiation.bean.providers.Local) user).setSalt(null);
        ((com.agreemount.negotiation.bean.providers.Local) user).setHash(null);

        return user;
    }

    @Override
    public Boolean confirmAccountAddress(String confirmationHash) {
        User user = userDAO.getByConfirmationHash(confirmationHash);
        Preconditions.checkNotNull(user, "User not found - hash code not found: [" + confirmationHash + "]");

        if (((com.agreemount.negotiation.bean.providers.Local) user).isConfirmed()) {
            throw new AuthException("Account is already activated");
        }

        ((com.agreemount.negotiation.bean.providers.Local) user).setConfirmed(Boolean.TRUE);
        userDAO.save(user);
        return ((com.agreemount.negotiation.bean.providers.Local) user).isConfirmed();
    }

    private void validate(char[] password, User user) {
        Preconditions.checkNotNull(user, "User not found");

        socialMediaChecker(user);

        Preconditions.checkNotNull(((com.agreemount.negotiation.bean.providers.Local) user).getHash(), "User has no hash");
        Preconditions.checkNotNull(((com.agreemount.negotiation.bean.providers.Local) user).getSalt(), "User has no salt");

        isAccountConfirmed((com.agreemount.negotiation.bean.providers.Local) user);
        isAuthorized(password, (com.agreemount.negotiation.bean.providers.Local) user);
        validateIfAccountIsBlocked(user);
    }

    private void socialMediaChecker(User user) {
        if (!(user instanceof com.agreemount.negotiation.bean.providers.Local)) {
            throw new AuthException("Given address is assigned to social media account");
        }
    }

    private void isAccountConfirmed(com.agreemount.negotiation.bean.providers.Local user) {
        if (!user.isConfirmed()) {
            throw new AuthException("Account email address is not confirmed");
        }
    }

    private void isAuthorized(char[] password, com.agreemount.negotiation.bean.providers.Local user) {
        boolean isAuthorized = Password.isExpectedPassword(password, user.getSalt(), user.getHash());

        if (!isAuthorized) {
            throw new AuthException(AUTHENTICATION_FAILED);
        }
    }
}
