package com.agreemount.negotiation.bean.enums;

import lombok.Getter;


@Getter
public enum User {
    EMAIL("email"), NAME("name");

    private String value;

    User(String value) {
        this.value = value;
    }
}
