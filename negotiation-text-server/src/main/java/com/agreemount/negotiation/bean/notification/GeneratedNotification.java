package com.agreemount.negotiation.bean.notification;

import com.agreemount.negotiation.bean.PreparedEmail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "generatedNotification")
@Getter
@Setter
@ToString
public class GeneratedNotification {

    @Id
    private String id;

    private PreparedEmail preparedEmail;

    private int attempts;

    private LocalDateTime createdAt;

    private LocalDateTime sentAt;

    private LocalDateTime nextProcessing;

    private GeneratedNotificationStatus status;

}