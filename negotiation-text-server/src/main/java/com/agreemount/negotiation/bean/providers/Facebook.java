package com.agreemount.negotiation.bean.providers;

import com.agreemount.negotiation.bean.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode
public class Facebook extends User {

    private String token;

}
