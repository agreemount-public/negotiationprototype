package com.agreemount.negotiation.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class User2Team {

    private String login;

    private UserRole role;

    @JsonProperty("isUserRegistered")
    private boolean isUserRegistered;

    @JsonProperty("isPartner")
    private boolean isPartner;

}
