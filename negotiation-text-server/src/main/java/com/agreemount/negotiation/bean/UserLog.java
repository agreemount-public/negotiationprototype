package com.agreemount.negotiation.bean;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Getter
@Setter
public class UserLog {
    @Id
    private String id;
    private String userEmail;
    private String createdAt;
    private String comment;

    public UserLog() {
        this.createdAt = LocalDateTime.now().toString();
    }
}
