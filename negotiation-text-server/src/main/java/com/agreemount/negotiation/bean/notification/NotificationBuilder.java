package com.agreemount.negotiation.bean.notification;

import com.google.common.base.Preconditions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NotificationBuilder {

    private String id;

    private NotificationStatus notificationStatus;

    private NotificationType notificationType;

    private Set<Notification.Parameter> parameters = new HashSet<>();

    public NotificationBuilder setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
        return this;
    }

    public NotificationBuilder setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
        return this;
    }

    public NotificationBuilder addParameter(Notification.Parameter param) {
        List<String> value = param.getValue();
        if (value != null && !value.isEmpty() && value.get(0) != null) {
            this.parameters.add(param);
        }
        return this;
    }

    public NotificationBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public Notification build() {
        Preconditions.checkNotNull(notificationType, "notification type has to be set");
        if (notificationStatus == null) {
            notificationStatus = NotificationStatus.NEW;
        }

        Notification notification = new Notification();

        notification.setId(id);
        notification.setNotificationStatus(notificationStatus);
        notification.setNotificationType(notificationType);
        notification.setParameters(parameters);

        return notification;
    }
}
