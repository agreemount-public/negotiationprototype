package com.agreemount.negotiation.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Document(collection = "email")
@Getter
@Setter
@ToString
public class PreparedEmail {

    private String title;
    private String content;
    private String receiver;
    private List<String> bcc;
    private List<String> cc;

}
