package com.agreemount.negotiation.bean.payment;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;
import java.util.Map;


@Document(collection = "payPalResponse")
@Setter
@Getter
@ToString
public class PayPalResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private Map<String, String> payload;
    private LocalDateTime createdAt;

    private String payerEmail;
    private String receiverEmail;
    private String payerStatus;
    private String receiverId;
    private String txnId;
    private String optionSelection1;
    private String paymentGross;
    private String verifySign;
    private String firstName;
    private String paymentDate;
    private String business;
    private String paymentStatus;
    private String agreemountUserId;
    private String lastName;
    private String payerId;
    private String ipnTrackId;
}
