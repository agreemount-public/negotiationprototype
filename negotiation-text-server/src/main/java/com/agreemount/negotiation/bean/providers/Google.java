package com.agreemount.negotiation.bean.providers;

import com.agreemount.negotiation.bean.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class Google extends User {

    private String token;

}
