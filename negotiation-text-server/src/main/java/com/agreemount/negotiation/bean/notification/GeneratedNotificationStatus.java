package com.agreemount.negotiation.bean.notification;

public enum GeneratedNotificationStatus {
    WAITING,
    PROCESSED,
    FAILED
}
