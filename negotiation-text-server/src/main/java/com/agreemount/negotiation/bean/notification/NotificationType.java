package com.agreemount.negotiation.bean.notification;

public enum NotificationType {
    TEST("test"),
    ADD_SIDE("addSide"),
    ADD_PARTNER("addPartner"),
    DELETE_PARTNER("deletePartner"),
    ACCOUNT_ACTIVATION("accountActivation"),
    NEW_DOCUMENT("newDocument"),
    OTHER_SIDE_INTERACTION("otherSideInteraction"),
    RESET_PASSWORD_CONFIRMATION("resetPasswordConfirmation"),
    RESET_PASSWORD("resetPassword");

    private final String value;

    NotificationType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
