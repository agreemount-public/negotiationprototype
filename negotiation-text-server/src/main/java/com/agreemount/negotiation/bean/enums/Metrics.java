package com.agreemount.negotiation.bean.enums;

import com.agreemount.negotiation.bean.UserRole;
import lombok.Getter;


@Getter
public enum Metrics {
    ITEM_CATEGORY("itemCategory"),
    ITEM_DESCRIPTION("itemDescription"),
    ITEM_VALUE("itemValue"),
    ITEM_MONTHLY_PAYMENT("itemMonthlyPayment"),
    SPLIT_PERCENT("splitPercent"),
    ITEM_IS_SEPARATE("itemIsSeparate"),
    SPLIT_DESCRIPTION("splitDescription"),
    SPLIT_PERCENT_SIDE1(SPLIT_PERCENT.value + UserRole.SIDE1),
    SPLIT_PERCENT_SIDE2(SPLIT_PERCENT.value + UserRole.SIDE2);


    private String value;

    Metrics(String value) {
        this.value = value;
    }



}
