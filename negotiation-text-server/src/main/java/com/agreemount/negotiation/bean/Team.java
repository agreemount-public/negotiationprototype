package com.agreemount.negotiation.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;



@Document(collection = "teams")
@Setter
@Getter
@ToString
public class Team {

    @Id
    private String id;

    private String name;

    private List<User2Team> users2Team = new ArrayList<>();

    public void addUser2Team(User2Team user2Team) {
        users2Team.add(user2Team);
    }

}
