package com.agreemount.negotiation.bean.enums;

import lombok.Getter;


@Getter
public enum States {
    AGREED_CONTENT_SIDE1("agreedContentSIDE1"),
    AGREED_CONTENT_SIDE2("agreedContentSIDE2"),
    AGREED_OWNER_SIDE1("agreedOwnerSIDE1"),
    AGREED_OWNER_SIDE2("agreedOwnerSIDE2"),
    DOCUMENT_TYPE("documentType"),
    ITEM_DESC_HAS_CHANGES("itemDescriptionHasChanges"),
    ITEM_OWNER("itemOwner"),
    ITEM_STATE("itemState"),
    ITEM_TYPE("itemType"),
    ITEM_VALUE_HAS_CHANGES("itemValueHasChanges"),
    MAIN_STATE("mainState");

    private String value;

    States(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
