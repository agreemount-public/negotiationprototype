package com.agreemount.negotiation.bean;

import com.agreemount.bean.document.Document;
import com.agreemount.slaneg.message.Message;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;


@Getter
@ToString
@Setter
@EqualsAndHashCode
public class NegotiationOnDashboard {

    private Document negotiation;
    private List<Message> messages;
    private String owner;
    private String invited;

    public NegotiationOnDashboard(Document negotiation, List<Message> messages) {
        this.negotiation = negotiation;
        this.messages = messages;
    }

}
