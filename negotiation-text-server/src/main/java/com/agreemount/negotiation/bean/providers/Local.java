package com.agreemount.negotiation.bean.providers;

import com.agreemount.negotiation.bean.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Setter
@Getter
@ToString
@EqualsAndHashCode
public class Local extends User {

    private char[] password;

    private byte[] hash;

    private byte[] salt;

    private boolean isConfirmed;

    private String confirmationHash;

}
