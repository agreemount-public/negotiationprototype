package com.agreemount.negotiation.bean;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;


@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserPaymentUnitsEntry {

    private Long paymentUnitsBefore;

    private Long requestedAmount;

    private Long paymentUnitsAfter;

    private String description;

    private String documentId;

    private LocalDateTime createdAt;

}
