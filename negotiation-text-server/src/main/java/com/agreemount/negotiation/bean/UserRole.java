package com.agreemount.negotiation.bean;

public enum UserRole {
    SIDE1("SIDE1", "owner"),
    SIDE2("SIDE2", "invitedUser");

    private String value;

    private String description;

    UserRole(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getValue() {
        return value;
    }


}
