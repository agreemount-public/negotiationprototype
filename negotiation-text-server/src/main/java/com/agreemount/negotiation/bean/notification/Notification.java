package com.agreemount.negotiation.bean.notification;

import com.google.common.collect.Lists;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document(collection = "notification")
@Getter
@Setter
@ToString

/**
 * It's highly recommended to use this builder to create notification instead of using Notification bean directly
 */
public class Notification {

    @Id
    private String id;

    private LocalDateTime createdAt = LocalDateTime.now();

    private NotificationStatus notificationStatus;

    private String message;

    private NotificationType notificationType;

    private Set<Parameter> parameters = new HashSet<>();

    private LocalDateTime processedAt;

    @ToString
    @Getter
    @EqualsAndHashCode
    public static class Parameter {
        private String key;
        private List<String> value = Lists.newArrayList();

        public Parameter(String key, String value) {
            this.key = key;
            this.value.add(value);
        }

        @PersistenceConstructor
        public Parameter(String key, List<String> value) {
            this.key = key;
            this.value = value;
        }

        public enum Keys {
            CONFIRMATION_HASH("confirmationHash"),
            REQUESTOR("requestor"),
            RECEIVER("receiver"),
            RECEIVER_SIDE("receiverSide"),
            TITLE("title"),
            CUSTOM("customParameters"),
            CC("cc"),
            BCC("bcc"),
            DOCUMENT_ID("documentId"),
            HOST("host");

            private String value;

            Keys(String value) {
                this.value = value;
            }

            public String getValue() {
                return value;
            }
        }

    }

}
