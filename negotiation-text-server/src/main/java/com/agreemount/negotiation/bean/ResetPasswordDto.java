package com.agreemount.negotiation.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResetPasswordDto {

    private char[] password;
    private char[] passwordConfirmation;
    private String hash;

}
