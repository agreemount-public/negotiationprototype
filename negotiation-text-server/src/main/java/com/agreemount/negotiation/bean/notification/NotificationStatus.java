package com.agreemount.negotiation.bean.notification;


public enum NotificationStatus {
    NEW,
    PROCESSED,
    FAILED
}
