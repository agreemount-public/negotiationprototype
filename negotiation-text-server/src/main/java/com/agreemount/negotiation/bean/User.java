package com.agreemount.negotiation.bean;

import com.agreemount.negotiation.bean.providers.Facebook;
import com.agreemount.negotiation.bean.providers.Google;
import com.agreemount.negotiation.bean.providers.Local;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.collect.Lists;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;
import java.util.List;


@Getter
@Setter
@ToString
@Document(collection = "users")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Facebook.class, name = "facebook"),
        @JsonSubTypes.Type(value = Google.class, name = "google"),
        @JsonSubTypes.Type(value = Local.class, name = "local")
})
@EqualsAndHashCode
public abstract class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    private String email;

    private String name;

    private Long paymentUnits;

    private boolean hasFreeAccess;

    private boolean blocked;

    private String _class;

    private List<UserPaymentUnitsEntry> paymentUnitEntries = Lists.newArrayList();

    private LocalDateTime lastLoginAt;

    public void addUserPaymentUnitsEntry(UserPaymentUnitsEntry entry) {
        paymentUnitEntries.add(entry);
    }

}
