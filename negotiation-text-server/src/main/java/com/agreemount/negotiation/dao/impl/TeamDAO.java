package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.agreemount.negotiation.dao.ITeamDAO;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;


@Repository
public class TeamDAO extends BaseDAO implements ITeamDAO {

    @Override
    public void save(Object entity) {
        Team team = (Team) entity;
        if (isNull(team.getName())) {
            team.setName(UUID.randomUUID().toString());
        }

        super.save(entity);
    }

    @Override
    public void addMemberToTeam(User2Team user2Team, String teamId) {
        Team team = getTeamByName(teamId);
        team.addUser2Team(user2Team);
        super.save(team);
    }

    @Override
    public Team getTeamByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return getMongoTemplate().findOne(query, Team.class);
    }

    @Override
    public List<Team> getUserTeams(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("users2Team.login").in(email));
        return getMongoTemplate().find(query, Team.class);
    }

    @Override
    public WriteResult deleteMembersFromTeamByUserRole(UserRole userRole) {
        Update update = new Update().pull("users2Team", new BasicDBObject("role", userRole));
        return getMongoTemplate().updateMulti(new Query(), update, Team.class);
    }

    @Override
    public WriteResult deleteByLogin(String login) {
        Update update = new Update().pull("users2Team", new BasicDBObject("login", login));
        return getMongoTemplate().updateMulti(new Query(), update, Team.class);
    }

}

