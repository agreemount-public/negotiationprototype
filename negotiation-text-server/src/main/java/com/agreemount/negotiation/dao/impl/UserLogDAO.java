package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.bean.UserLog;
import org.springframework.stereotype.Repository;

@Repository
public class UserLogDAO extends BaseDAO {

    public void insertNewLog(String userEmail, String comment) {
        UserLog userLog = new UserLog();
        userLog.setUserEmail(userEmail);
        userLog.setComment(comment);

        this.insert(userLog);
    }
}
