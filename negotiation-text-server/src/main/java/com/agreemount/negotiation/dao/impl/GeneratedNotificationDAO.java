package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.bean.notification.GeneratedNotification;
import com.agreemount.negotiation.bean.notification.GeneratedNotificationStatus;
import com.agreemount.negotiation.dao.IGeneratedNotificationDAO;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class GeneratedNotificationDAO extends BaseDAO implements IGeneratedNotificationDAO {

    @Override
    public GeneratedNotification getNext() {
        Criteria criteria = Criteria
            .where("nextProcessing")
            .lt(LocalDateTime.now())
            .and("status")
            .is(GeneratedNotificationStatus.WAITING)
        ;

        Query query = new Query()
            .addCriteria(criteria)
            .with(new Sort(Sort.Direction.DESC, "nextProcessing"))
        ;

        return getMongoTemplate()
                .findOne(query, GeneratedNotification.class);
    }

}
