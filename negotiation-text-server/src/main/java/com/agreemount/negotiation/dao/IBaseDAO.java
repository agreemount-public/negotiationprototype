package com.agreemount.negotiation.dao;


public interface IBaseDAO {

    void insert(Object entity);

    void save(Object entity);

    void delete(Object entity);

    void deleteAll(Class entityClass);

}
