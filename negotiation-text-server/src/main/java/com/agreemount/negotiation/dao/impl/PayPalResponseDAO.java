package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.bean.payment.PayPalResponse;
import com.agreemount.negotiation.dao.IPayPalResponseDAO;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;



@Repository
public class PayPalResponseDAO extends BaseDAO implements IPayPalResponseDAO {

    @Override
    public void insert(Object entity) {
        Map<String, String> payload = (Map<String, String>) entity;

        PayPalResponse payPalResponse = new PayPalResponse();
        payPalResponse.setPayerEmail(payload.get("payer_email"));
        payPalResponse.setReceiverEmail(payload.get("receiver_email"));
        payPalResponse.setPayerStatus(payload.get("payer_status"));
        payPalResponse.setReceiverId(payload.get("receiver_id"));
        payPalResponse.setTxnId(payload.get("txn_id"));
        payPalResponse.setOptionSelection1(payload.get("option_selection1"));
        payPalResponse.setPaymentGross(payload.get("payment_gross"));
        payPalResponse.setVerifySign(payload.get("verify_sign"));
        payPalResponse.setFirstName(payload.get("first_name"));
        payPalResponse.setPaymentDate(payload.get("payment_date"));
        payPalResponse.setBusiness(payload.get("business"));
        payPalResponse.setPaymentStatus(payload.get("payment_status"));
        payPalResponse.setAgreemountUserId(payload.get("custom"));
        payPalResponse.setLastName(payload.get("last_name"));
        payPalResponse.setPayerId(payload.get("payer_id"));
        payPalResponse.setIpnTrackId(payload.get("ipn_track_id"));
        payPalResponse.setPayload(payload);
        payPalResponse.setCreatedAt(LocalDateTime.now());

        getMongoTemplate().insert(payPalResponse);
    }

}
