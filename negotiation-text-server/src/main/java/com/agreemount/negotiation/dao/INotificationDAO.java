package com.agreemount.negotiation.dao;

import com.agreemount.negotiation.bean.notification.Notification;


public interface INotificationDAO extends IBaseDAO {

    Notification getNext();

    Notification getById(String id);

}
