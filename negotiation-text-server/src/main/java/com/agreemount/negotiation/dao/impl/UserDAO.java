package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.bean.User;
import com.agreemount.negotiation.dao.IUserDAO;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;


@Repository
public class UserDAO extends BaseDAO implements IUserDAO {

    public static final Long INITIAL_PAYMENT_UNITS = 10L;

    @Override
    public void updateLastLoginDate(User user) {
        user.setLastLoginAt(LocalDateTime.now());
        save(user);
    }

    @Override
    public void insert(Object entity) {
        User user = (User) entity;
        user.setPaymentUnits(INITIAL_PAYMENT_UNITS);
        super.insert(entity);
    }

    @Override
    public User getById(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        return getMongoTemplate().findOne(query, User.class);
    }

    @Override
    public User getByEmail(String email) {
        Query query = new Query(Criteria.where("email").is(email));
        return getMongoTemplate().findOne(query, User.class);
    }

    @Override
    public User getByConfirmationHash(String confirmationHash) {
        Query query = new Query(Criteria.where("confirmationHash").is(confirmationHash));
        return getMongoTemplate().findOne(query, User.class);
    }

}
