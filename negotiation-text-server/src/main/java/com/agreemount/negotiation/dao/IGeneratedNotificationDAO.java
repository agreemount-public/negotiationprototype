package com.agreemount.negotiation.dao;

import com.agreemount.negotiation.bean.notification.GeneratedNotification;


public interface IGeneratedNotificationDAO extends IBaseDAO {

    GeneratedNotification getNext();

}


