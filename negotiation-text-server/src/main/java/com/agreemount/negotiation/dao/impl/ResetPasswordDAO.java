package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.bean.ResetPassword;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ResetPasswordDAO extends BaseDAO {

    public ResetPassword getByHash(String hash) {
        Query query = new Query()
                .addCriteria(Criteria.where("hash").is(hash));

        return getMongoTemplate()
                .findOne(query, ResetPassword.class);
    }
}
