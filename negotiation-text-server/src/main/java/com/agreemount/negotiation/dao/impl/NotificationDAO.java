package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.bean.notification.Notification;
import com.agreemount.negotiation.bean.notification.NotificationStatus;
import com.agreemount.negotiation.dao.INotificationDAO;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;



@Repository
public class NotificationDAO extends BaseDAO implements INotificationDAO {

    @Override
    public Notification getNext() {
        Criteria criteria = Criteria
                .where("notificationStatus")
                .is(NotificationStatus.NEW);

        Query query = new Query()
                .addCriteria(criteria);

        return getMongoTemplate()
                .findOne(query, Notification.class);
    }

    @Override
    public Notification getById(String id) {
        Criteria criteria = Criteria.where("_id").is(id);
        Query query = new Query()
                .addCriteria(criteria);
        return getMongoTemplate()
                .findOne(query, Notification.class);
    }
}
