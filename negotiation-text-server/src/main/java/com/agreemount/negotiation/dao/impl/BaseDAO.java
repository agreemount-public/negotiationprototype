package com.agreemount.negotiation.dao.impl;

import com.agreemount.negotiation.dao.IBaseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

public abstract class BaseDAO implements IBaseDAO {

    @Autowired
    private MongoTemplate mongoTemplate;

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    @Override
    public void save(Object entity) {
        mongoTemplate.save(entity);
    }

    @Override
    public void insert(Object entity) {
        mongoTemplate.insert(entity);
    }

    @Override
    public void delete(Object entity) {
        mongoTemplate.remove(entity);
    }

    @Override
    public void deleteAll(Class entityClass) {
        mongoTemplate.remove(new Query(), entityClass);
    }

}
