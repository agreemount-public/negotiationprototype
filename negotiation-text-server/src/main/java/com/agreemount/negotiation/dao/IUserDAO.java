package com.agreemount.negotiation.dao;

import com.agreemount.negotiation.bean.User;


public interface IUserDAO extends IBaseDAO {

    void updateLastLoginDate(User user);

    User getById(String id);

    User getByEmail(String email);

    User getByConfirmationHash(String confirmationHash);

}
