package com.agreemount.negotiation.dao;

import com.agreemount.negotiation.bean.Team;
import com.agreemount.negotiation.bean.User2Team;
import com.agreemount.negotiation.bean.UserRole;
import com.mongodb.WriteResult;

import java.util.List;


public interface ITeamDAO extends IBaseDAO {

    void addMemberToTeam(User2Team user2Team, String teamId);

    Team getTeamByName(String name);

    List<Team> getUserTeams(String email);

    WriteResult deleteMembersFromTeamByUserRole(UserRole userRole);

    WriteResult deleteByLogin(String login);

}
