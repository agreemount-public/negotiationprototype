#!/bin/sh -e

APP="negotiation-text-server"

LAST_TAG=$(git describe --tags $(git rev-list --tags="$APP-*\.*\.*" --max-count=1))

if [ -z ${1+x} ]; then
    echo "Getting last version"
    VERSION=$(echo $LAST_TAG | grep -o "[0-9.]\+")
    LATEST="latest"
else
    echo "Getting version $1"
    VERSION=$1
    LATEST=$1
fi

echo "$(date) - Downloading $APP-$VERSION.jar from nexus"

set +o noclobber
url=http://nexus.docker/service/local/repositories/releases/content/com/agreemount/$APP/$VERSION/$APP-$VERSION.jar
echo "$(date) - URL $url"
status_code=$(curl -o /dev/null -sSI --write-out %{http_code} $url)
if [[ $status_code -ne 200 ]] ; then
    echo "$(date) - $APP-$VERSION.jar not found, exitting"
    exit 1
fi
curl -sS "$url" > $APP-$VERSION.jar
exitcode=$?
echo "$(date) - curl ($exitcode)"

echo "$(date) - Build docker image agreemount/$APP:$VERSION"

docker build \
    --build-arg VERSION="$VERSION" \
    --build-arg APP="$APP" \
    -t agreemount/$APP:$LATEST .

if [ "$VERSION" != "$LATEST" ]; then
    docker tag agreemount/$APP:$LATEST agreemount/$APP:$VERSION
fi

echo "$(date) - Cleaning"
rm $APP-$VERSION.jar

echo "$(date) - Finished"
